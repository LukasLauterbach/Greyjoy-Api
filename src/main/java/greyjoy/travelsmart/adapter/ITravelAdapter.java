package greyjoy.travelsmart.adapter;

import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ITravelAdapter
{
	public Flux<Event> getEvents(Filter filter) throws Exception;

	public Mono<EventDetails> getEventDetails(String eventID);

	public String getVendorID();
	
	public String getVendorName();
}
