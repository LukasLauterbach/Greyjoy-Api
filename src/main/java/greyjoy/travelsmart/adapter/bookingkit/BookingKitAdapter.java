package greyjoy.travelsmart.adapter.bookingkit;

import java.awt.print.Book;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;

import greyjoy.travelsmart.adapter.ITravelAdapter;
import greyjoy.travelsmart.adapter.bookingkit.model.BKEvent;
import greyjoy.travelsmart.adapter.bookingkit.model.Date;
import greyjoy.travelsmart.adapter.bookingkit.model.DateEvents;
import greyjoy.travelsmart.adapter.bookingkit.model.Token;
import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.handler.CategoryHandler;
import greyjoy.travelsmart.api.model.Category;
import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.api.model.Rating;
import greyjoy.travelsmart.api.model.Ticket;
import greyjoy.travelsmart.api.model.price.Price;
import greyjoy.travelsmart.api.model.price.Prices;
import greyjoy.travelsmart.api.model.price.Prices.TYPE;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.retry.Retry;

@Service
public class BookingKitAdapter implements ITravelAdapter
{
	private @Autowired Log logger;

	WebClientException a;

	private final Retry<Event> retry = Retry.<Event> onlyIf(ctx -> {
		Throwable t = ctx.exception();
		return t instanceof WebClientResponseException && ((WebClientResponseException) t).getStatusCode() == HttpStatus.TOO_MANY_REQUESTS;
	}).fixedBackoff(Duration.ofMillis(2000));

	private final WebClient webClient;

	String eventBlackList;

	Mono<String> tokenCache;

	public BookingKitAdapter(WebClient.Builder webClientBuilder)
	{
		this.webClient = webClientBuilder.baseUrl("https://api.bookingkit.de/").build();

		StringBuilder builder = new StringBuilder();
		builder.append("9fc8b48c13fa9e0d1fa5cc2922babb25,");
		builder.append("96f98efca65a9484d78165c6330c16ec,");
		builder.append("9fc04bb0871bc6ab27d76102f524ac7b,");
		builder.append("aeb5d6a9f2bd2655dcbdaefdb65adf66,");
		builder.append("75284c2fd54e7aa2c484b8a34e3f4672,");
		builder.append("85922f3e7a7deb012374077101338776,");
		builder.append("4fb88045a1684eea900db9ad9e7b6e6d,");
		builder.append("b8a61598bd49c6cb745370850d529e9f,");
		builder.append("d32daa13e5cc4c39dab834d71bc59a03,");
		builder.append("4c7d9aa6bfc2c30041cab38f7f132c92,");
		builder.append("1f0d594655b664b680e9feda6d775eaf,");
		builder.append("33b22e455c1783a68d7df019b77f6b58,");
		builder.append("83e2513ce71a6661f92fba4d5085bb5c,");
		builder.append("8d20ce5d47ab2c9b7a3040157a4e6e9c,");
		builder.append("9ab0475c9eb9d31f8bc8157364f6f54e,");
		builder.append("92b5bbae4efa9f2ce94db7f3f4e85257,");
		builder.append("c347ae87f18eaf27195ece9665a985f5,");
		builder.append("8775838159ea012b3429907756c8a2de,");
		builder.append("3cad5ff92bc7cb94b81f362200bf6d0e,");
		builder.append("74071614657e3aaaa597ed167f488d0e,");
		builder.append("1ae5091961311ec1a128c918106c19ec,");
		builder.append("07136f733313484a40b0b530fe736bda,");
		builder.append("d4d5d62e333bce8ac79c88e001345334,");
		builder.append("0b58eb7fede3491d083182079c25c3f7,");
		builder.append("faf1b041d47e12d6eb3f01753308e358,");
		builder.append("ae2be4f4e8db9bbe5249d1b133f33196,");
		builder.append("621975220eca09384cbcc53475a9255f,");
		builder.append("9722327b6be45f0e511233f53352491f,");
		builder.append("0f4764cc45c10f7c2136832582eda350,");
		builder.append("9eeb06b7b39704d1412a273f0aa80d86,");
		builder.append("3e3cc8b7096bf7a2de7e18c7f8f33c6c,");
		builder.append("15a797eefa949371adf11d1a770756fb,");
		builder.append("b62d440cfe0664efb7f6a9ff8ed655ae,");
		builder.append("0f4764cc45c10f7c2136832582eda350");

		eventBlackList = builder.toString();
	}

	@PostConstruct
	public void init()
	{
		// Generate Token Cache
		this.tokenCache = getToken().cache(Duration.ofMinutes(50));
		this.tokenCache.subscribe(t -> logger.info("Initial BK Token: " + t));
	}

	public Mono<String> getToken()
	{
		return webClient.post().uri("oauth/token").contentType(MediaType.APPLICATION_FORM_URLENCODED).body(BodyInserters.fromFormData("client_id", "demo").with("client_secret", "demo").with("grant_type", "client_credentials")).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(Token.class).map(token -> token.getToken());
	}

	@Override
	public Flux<Event> getEvents(Filter filter)
	{
		Map<String, BKEvent> eventCache = Collections.synchronizedMap(new HashMap<String, BKEvent>());

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath("v3/eventDates").queryParam("limit", 100000).queryParam("fields", "title,description,images,location,location_lng,location_lat,date,available_slots,total_slots,prices,categories").queryParam("exclude_events", eventBlackList).queryParam("available", true);

		if (filter.filterLocation())
		{
			uriBuilder.queryParam("geolocation", BookingKitHelper.convertCoordinates(filter.location_radius(), filter.location_latitude(), filter.location_longitude()));
		}

		if (filter.filterDate())
		{
			uriBuilder.queryParam("start_date", filter.date_min());
			uriBuilder.queryParam("end_date", filter.date_max());
		}

		String uri = uriBuilder.build().toUriString();
		return tokenCache.flatMapMany(token -> {
			return webClient.get().uri(uri).header("Authorization", "Bearer " + token).retrieve().bodyToMono(DateEvents.class).map(de -> {

				Iterator<Date> iterator = de.getDates().iterator();
				while (iterator.hasNext())
				{
					Date d = iterator.next();
					String event_id = d.getEvent_id();

					if (eventCache.containsKey(event_id))
					{
						BKEvent e = eventCache.get(event_id);
						d.setEvent(e);
						e.getDates().add(d);
					}
					else
					{
						boolean found = false;
						for (BKEvent e : de.getEvents())
						{
							if (e.getId().equals(event_id))
							{
								eventCache.put(event_id, e);
								d.setEvent(e);

								e.getDates().add(d);

								found = true;

								break;
							}
						}

						if (!found || d.getEvent().getTitle().isEmpty())
						{
							iterator.remove();
						}
					}
				}

				return de;
			}).map(de -> {

				List<Event> events = new ArrayList<Event>();

				for (BKEvent bke : de.getEvents())
				{
					Event e = new Event();

					e.setTitle(bke.getTitle());
					e.setImages(bke.getImages());
					e.setLocation(bke.getLocation());
					e.setLocation_lat(bke.getLocation_lat());
					e.setLocation_long(bke.getLocation_lng());
					e.setLanguages(new String[] { "de" }); // TODO: FAKE
					e.setAdapterID(bke.getId());
					e.setRating(new Rating(0, 0));
					e.setDuration(bke.getDuration());

					greyjoy.travelsmart.adapter.bookingkit.model.Category[] bkCategories = bke.getCategories();

					for (greyjoy.travelsmart.adapter.bookingkit.model.Category bkCategory : bkCategories)
					{
						if (BookingKitCategoryMap.categoryMap.containsKey(bkCategory.getId()))
						{
							e.setCategory(CategoryHandler.get().getCategoryFromID(BookingKitCategoryMap.categoryMap.get(bkCategory.getId())));

							break;
						}
					}

					Prices prices = new Prices();
					prices.setType(TYPE.SIMPLE);

					List<Price> priceList = new ArrayList<Price>();

					for (greyjoy.travelsmart.adapter.bookingkit.model.Price bp : bke.getPrices())
					{
						Price p = new Price();
						p.setName(bp.getTitle());
						p.setValue(bp.getValue());

						priceList.add(p);
					}

					prices.setList(priceList);
					e.setPrices(prices);
					e.setDisplayPrice(priceList.get(0).getValue());

					events.add(e);
				}

				return events;
			}).flatMapMany(o -> Flux.fromIterable(o)).filter(e -> !filter.filterLanguage() || Arrays.stream(e.getLanguages()).anyMatch(s -> s.equals(filter.language())));
		}).retryWhen(retry);
	}

	@Override
	public String getVendorName()
	{
		return "bookingkit";
	}

	@Override
	public String getVendorID()
	{
		return "bookingkit";
	}

	@Override
	public Mono<EventDetails> getEventDetails(String eventID)
	{
		String uri = UriComponentsBuilder.fromPath("v3/eventDates").queryParam("accept_events", eventID).queryParam("fields", "title,description,images,location,location_lng,location_lat,date,available_slots,total_slots,prices,highlights,categories").queryParam("available", true).build().toUriString();

		return tokenCache.flatMap(token -> {
			return webClient.get().uri(uri).header("Authorization", "Bearer " + token).retrieve().bodyToMono(DateEvents.class).map(de -> {

				BKEvent bke = de.getEvents().get(0);
				List<Date> dates = de.getDates();
				List<Ticket> tickets = new ArrayList<Ticket>();

				dates.stream().map(d -> {
					Ticket t = new Ticket();
					t.setAvailable_slots(d.getAvailable_slots());
					t.setTotal_slots(d.getTotal_slots());
					t.setDate(d.getDate());

					return t;
				}).forEach(tickets::add);
				
				while (tickets.size()>20)
				{
					tickets.remove(tickets.size()-1);
				}

				EventDetails details = new EventDetails();

				details.setTitle(bke.getTitle());
				details.setDescription(bke.getDescription().replaceAll("\\<.*?>", ""));
				details.setImages(bke.getImages());
				details.setLocation(bke.getLocation());
				details.setLocation_lat(bke.getLocation_lat());
				details.setLocation_long(bke.getLocation_lng());
				details.setLanguages(new String[] { "de" }); // TODO: FAKE
				details.setTickets(tickets);
				details.setHighlights(bke.getHighlights());
				details.setDuration(bke.getDuration());

				greyjoy.travelsmart.adapter.bookingkit.model.Category[] bkCategories = bke.getCategories();

				for (greyjoy.travelsmart.adapter.bookingkit.model.Category bkCategory : bkCategories)
				{
					if (BookingKitCategoryMap.categoryMap.containsKey(bkCategory.getId()))
					{
						details.setCategory(CategoryHandler.get().getCategoryFromID(BookingKitCategoryMap.categoryMap.get(bkCategory.getId())));

						break;
					}
				}

				Prices prices = new Prices();
				prices.setType(TYPE.SIMPLE);

				List<Price> priceList = new ArrayList<Price>();

				for (greyjoy.travelsmart.adapter.bookingkit.model.Price bp : bke.getPrices())
				{
					Price p = new Price();
					p.setName(bp.getTitle());
					p.setValue(bp.getValue());

					priceList.add(p);
				}

				prices.setList(priceList);
				details.setPrices(prices);
				details.setDisplayPrice(priceList.get(0).getValue());

				return details;
			});
		}).retryWhen(retry);
	}
}
