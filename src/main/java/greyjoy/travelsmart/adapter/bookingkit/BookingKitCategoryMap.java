package greyjoy.travelsmart.adapter.bookingkit;

import java.util.HashMap;

public class BookingKitCategoryMap
{
	public static HashMap<Integer, Integer> categoryMap;

	static
	{
		categoryMap = new HashMap<Integer, Integer>();

		// Sightseeing
		categoryMap.put(1, 3);
		categoryMap.put(2, 3);
		categoryMap.put(3, 3);
		categoryMap.put(6, 3);

		for (int i = 8; i < 61; i++)
		{
			categoryMap.put(i, 3);
		}

		categoryMap.put(74, 3);

		for (int i = 126; i < 131; i++)
		{
			categoryMap.put(i, 3);
		}

		categoryMap.put(338, 3);
		categoryMap.put(339, 3);

		for (int i = 348; i < 358; i++)
		{
			categoryMap.put(i, 3);
		}

		categoryMap.put(369, 3);
		categoryMap.put(372, 3);
		categoryMap.put(373, 3);
		categoryMap.put(376, 3);

		// Sport und Aktivurlaub
		categoryMap.put(7, 2);
		categoryMap.put(86, 2);

		for (int i = 119; i < 126; i++)
		{
			categoryMap.put(i, 2);
		}

		categoryMap.put(132, 2);
		categoryMap.put(192, 2);

		for (int i = 194; i < 235; i++)
		{
			categoryMap.put(i, 2);
		}

		for (int i = 248; i < 268; i++)
		{
			categoryMap.put(i, 2);
		}

		for (int i = 275; i < 280; i++)
		{
			categoryMap.put(i, 2);
		}

		categoryMap.put(315, 2);
		categoryMap.put(316, 2);

		for (int i = 318; i < 327; i++)
		{
			categoryMap.put(i, 2);
		}

		categoryMap.put(340, 2);
		categoryMap.put(361, 2);
		categoryMap.put(362, 2);
		categoryMap.put(365, 2);
		categoryMap.put(366, 2);
		categoryMap.put(367, 2);
		categoryMap.put(368, 2);
		categoryMap.put(371, 2);
		categoryMap.put(377, 2);
		categoryMap.put(378, 2);
		categoryMap.put(380, 2);
		categoryMap.put(381, 2);
		categoryMap.put(950, 2);
		categoryMap.put(1086, 2);

		// Essen und Trinken
		categoryMap.put(191, 5);

		for (int i = 280; i < 309; i++)
		{
			categoryMap.put(i, 5);
		}

		for (int i = 388; i < 404; i++)
		{
			categoryMap.put(i, 5);
		}

		// Wellness und Beauty
		for (int i = 141; i < 185; i++)
		{
			categoryMap.put(i, 8);
		}

		categoryMap.put(190, 8);

		// Abenteuerurlaub

		for (int i = 61; i < 119; i++)
		{
			if (i != 86)
				categoryMap.put(i, 4);
		}

		categoryMap.put(131, 4);

		for (int i = 133; i < 141; i++)
		{
			categoryMap.put(i, 4);
		}
		
		for (int i = 235; i < 248; i++)
		{
			categoryMap.put(i, 4);
		}
		
		for (int i = 268; i < 275; i++)
		{
			categoryMap.put(i, 4);
		}
		
		categoryMap.put(359, 4);
		categoryMap.put(360, 4);
		categoryMap.put(363, 4);
		categoryMap.put(364, 4);
		categoryMap.put(382, 4);
		categoryMap.put(383, 4);
		categoryMap.put(959, 4);
		
		// Nachtleben
		categoryMap.put(309, 6);
		categoryMap.put(310, 6);
		categoryMap.put(311, 6);
		categoryMap.put(312, 6);
		categoryMap.put(313, 6);
		categoryMap.put(358, 6);
		categoryMap.put(374, 6);
	}

}
