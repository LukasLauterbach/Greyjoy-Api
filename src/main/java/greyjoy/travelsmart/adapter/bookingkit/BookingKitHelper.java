package greyjoy.travelsmart.adapter.bookingkit;

import java.util.Locale;

import greyjoy.travelsmart.util.MathUtil;
import reactor.util.function.Tuple4;

public class BookingKitHelper
{
	public static String convertCoordinates(int radius, double latitude, double longitude)
	{
		Tuple4<Double, Double, Double, Double> rectTuple = MathUtil.pointRadiusToRectangle(radius, latitude, longitude);

		return String.format(Locale.ROOT, "%f,%f,%f,%f", rectTuple.getT1(), rectTuple.getT2(), rectTuple.getT3(), rectTuple.getT4());
	}
}
