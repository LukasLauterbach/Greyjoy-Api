package greyjoy.travelsmart.adapter.bookingkit.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BKEvent
{
	String id;
	String root_event_id;
	String title;
	String description;
	int duration;
	int pre_time;
	int max_participants;

	String location;
	double location_lng;
	double location_lat;

	String meeting_location;
	double meeting_location_lng;
	double meeting_location_lat;

	String vendor_id;
	String vendor_name;

	String bring;
	String advice;
	String hint;
	String participant_hint;

	String youtubeVideoId;

	String voucher_checkout_url;

	Category[] categories;

	String first_date;
	int min_participants;
	int latest_booking_time;

	Price[] prices = new Price[0];

	String[] images;

	String[] highlights;

	List<Date> dates = new ArrayList<Date>();

	public String getId()
	{
		return id;
	}

	public String getRoot_event_id()
	{
		return root_event_id;
	}

	public String getTitle()
	{
		return title;
	}

	public String getDescription()
	{
		return description;
	}

	public int getDuration()
	{
		return duration;
	}

	public int getPre_time()
	{
		return pre_time;
	}

	public int getMax_participants()
	{
		return max_participants;
	}

	public String getLocation()
	{
		return location;
	}

	public double getLocation_lng()
	{
		return location_lng;
	}

	public double getLocation_lat()
	{
		return location_lat;
	}

	public String getMeeting_location()
	{
		return meeting_location;
	}

	public double getMeeting_location_lng()
	{
		return meeting_location_lng;
	}

	public double getMeeting_location_lat()
	{
		return meeting_location_lat;
	}

	public String getVendor_id()
	{
		return vendor_id;
	}

	public String getVendor_name()
	{
		return vendor_name;
	}

	public String getBring()
	{
		return bring;
	}

	public String getAdvice()
	{
		return advice;
	}

	public String getHint()
	{
		return hint;
	}

	public String getParticipant_hint()
	{
		return participant_hint;
	}

	public String getYoutubeVideoId()
	{
		return youtubeVideoId;
	}

	public String getVoucher_checkout_url()
	{
		return voucher_checkout_url;
	}

	public Category[] getCategories()
	{
		return categories;
	}

	public String getFirst_date()
	{
		return first_date;
	}

	public int getMin_participants()
	{
		return min_participants;
	}

	public int getLatest_booking_time()
	{
		return latest_booking_time;
	}

	public String[] getImages()
	{
		return images;
	}

	public String[] getHighlights()
	{
		return highlights;
	}

	public List<Date> getDates()
	{
		return dates;
	}

	public void setDates(List<Date> dates)
	{
		this.dates = dates;
	}

	public Price[] getPrices()
	{
		return prices;
	}

}
