package greyjoy.travelsmart.adapter.bookingkit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Date
{
	String id;

	String event_id;
	BKEvent event;

	String date;
	int available_slots;
	int total_slots;
	String checkout_url;

	public void setEvent(BKEvent e)
	{
		this.event = e;
	}

	public BKEvent getEvent()
	{
		return event;
	}

	public String getId()
	{
		return id;
	}

	public String getEvent_id()
	{
		return event_id;
	}

	public String getDate()
	{
		return date;
	}

	public int getAvailable_slots()
	{
		return available_slots;
	}

	public int getTotal_slots()
	{
		return total_slots;
	}

	public String getCheckout_url()
	{
		return checkout_url;
	}
}
