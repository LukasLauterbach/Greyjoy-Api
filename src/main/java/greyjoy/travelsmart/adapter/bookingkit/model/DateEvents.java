package greyjoy.travelsmart.adapter.bookingkit.model;

import java.util.List;

public class DateEvents
{
	List<Date> dates;

	List<BKEvent> events;

	public List<Date> getDates()
	{
		return dates;
	}

	public List<BKEvent> getEvents()
	{
		return events;
	}

}
