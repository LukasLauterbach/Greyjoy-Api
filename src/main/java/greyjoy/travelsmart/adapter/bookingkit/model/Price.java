package greyjoy.travelsmart.adapter.bookingkit.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Price
{
	int id;

	String title;

	String currency;

	float value;

	@JsonProperty("public")
	String isPublic;

	public float getValue()
	{
		return value;
	}

	public String getTitle()
	{
		return title;
	}

	
}
