package greyjoy.travelsmart.adapter.bookingkit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token
{
	public String access_token;

	public String getToken()
	{
		return access_token;
	}
}
