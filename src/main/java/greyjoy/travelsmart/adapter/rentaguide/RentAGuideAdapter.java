package greyjoy.travelsmart.adapter.rentaguide;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.net.ssl.SSLException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import greyjoy.travelsmart.adapter.ITravelAdapter;
import greyjoy.travelsmart.adapter.rentaguide.model.availabilities.AvailabilityList;
import greyjoy.travelsmart.adapter.rentaguide.model.availabilities.AvailabilityList.Availability;
import greyjoy.travelsmart.adapter.rentaguide.model.details.TourDetails;
import greyjoy.travelsmart.adapter.rentaguide.model.list.TourList;
import greyjoy.travelsmart.adapter.rentaguide.model.list.TourList.Tour;
import greyjoy.travelsmart.adapter.rentaguide.model.tags.TagList;
import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.handler.CategoryHandler;
import greyjoy.travelsmart.api.handler.DatabaseHandler;
import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.api.model.Rating;
import greyjoy.travelsmart.api.model.Ticket;
import greyjoy.travelsmart.api.model.price.Price;
import greyjoy.travelsmart.api.model.price.Prices;
import greyjoy.travelsmart.api.model.price.Prices.TYPE;
import greyjoy.travelsmart.config.Config;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.retry.Retry;

@Service
public class RentAGuideAdapter implements ITravelAdapter
{
	private @Autowired Log logger;

	private final WebClient webClient_api;

	private final Retry<Event> eventRetry = Retry.<Event> any().fixedBackoff(Duration.ofMillis(2000));
	private final Retry<TourDetails> detailsRetry = Retry.<TourDetails> any().fixedBackoff(Duration.ofMillis(2000));
	private final Retry<AvailabilityList> availabilityRetry = Retry.<AvailabilityList> any().fixedBackoff(Duration.ofMillis(2000));

	JAXBContext detailsContext;

	JAXBContext listContext;

	JAXBContext availabilitiesContext;

	JAXBContext tagListContext;

	PreparedStatement detailsSearchStatement;
	PreparedStatement detailsSaveStatement;

	PreparedStatement tagsSearchStatement;
	PreparedStatement tagsSaveStatement;

	public RentAGuideAdapter(WebClient.Builder webClientBuilder, DatabaseHandler databaseHandler)
	{
		SslContext sslContext = null;
		try
		{
			sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
		}
		catch (SSLException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SslContext cc = sslContext;

		this.webClient_api = webClientBuilder.baseUrl("https://api.rent-a-guide.de/").clientConnector(new ReactorClientHttpConnector(options -> {
			options.compression(true);
			options.sslSupport();
			options.sslContext(cc);
		})).build();

		try
		{
			detailsContext = JAXBContext.newInstance(TourDetails.class);

			listContext = JAXBContext.newInstance(TourList.class);

			availabilitiesContext = JAXBContext.newInstance(AvailabilityList.class);
			tagListContext = JAXBContext.newInstance(TagList.class);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		databaseHandler.executeUpdate("CREATE TABLE IF NOT EXISTS RENTAGUIDE_DETAILS (\r\n" + "    id     TEXT PRIMARY KEY,\r\n" + "    xml TEXT\r\n" + ")");
		databaseHandler.executeUpdate("CREATE TABLE IF NOT EXISTS RENTAGUIDE_TAGS (\r\n" + "    id     TEXT PRIMARY KEY,\r\n" + "    xml TEXT\r\n" + ")");

		this.detailsSearchStatement = databaseHandler.prepare("SELECT xml FROM RENTAGUIDE_DETAILS WHERE id = ?");
		this.detailsSaveStatement = databaseHandler.prepare("INSERT OR IGNORE INTO RENTAGUIDE_DETAILS VALUES (?,?)");

		this.tagsSearchStatement = databaseHandler.prepare("SELECT xml FROM RENTAGUIDE_TAGS WHERE id = ?");
		this.tagsSaveStatement = databaseHandler.prepare("INSERT OR IGNORE INTO RENTAGUIDE_TAGS VALUES (?,?)");
	}

	public List<Tour> getTourList()
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(TourList.class);

			Unmarshaller un = context.createUnmarshaller();

			String xml = this.webClient_api.get().retrieve().bodyToMono(String.class).block();

			TourList list = (TourList) un.unmarshal(new StringReader(xml));

			return list.getTour();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Flux<Event> getEvents(Filter filter)
	{
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath("tours");

		if (filter.filterLocation())
		{
			uriBuilder.queryParam("lon", filter.location_longitude());
			uriBuilder.queryParam("lat", filter.location_latitude());
			uriBuilder.queryParam("radius", filter.location_radius());
		}

		if (filter.filterDate())
		{
			uriBuilder.queryParam("start_date", filter.date_min());
			uriBuilder.queryParam("end_date", filter.date_max());
		}

		if (filter.filterLanguage())
		{
			uriBuilder.queryParam("lang", filter.language());
		}

		String uri = uriBuilder.build().toUriString();
		
		return this.webClient_api.get().uri(uri).header("Authorization", "Basic cHJha3Rpa3VtQHN3Yy5yd3RoLWFhY2hlbi5kZTpTUS5fRUBwQXdqTjI=").retrieve().bodyToMono(String.class).retryWhen(eventRetry).map(xml -> {			
			try
			{
				Unmarshaller listUm = listContext.createUnmarshaller();
				return (TourList) listUm.unmarshal(new StringReader(xml));
			}
			catch (JAXBException e)
			{
				e.printStackTrace();
			}
			return new TourList();
		}).map(tl -> tl.getTour()).flatMapMany(l -> Flux.<Tour> fromIterable(l)).distinct(t -> t.getId()).flatMap(tour -> {
			return Mono.zip(getDetails(tour.getId()), getTags(tour.getId()));
		}, 4).map(tuple -> {
			Event outputEvent = new Event();
			TourDetails details = tuple.getT1();
			TagList tagList = tuple.getT2();

			outputEvent.setTitle(details.getTitle());
			outputEvent.setImages(details.getImages().get(0).getImage().stream().map(i -> i.getHref().replace("_small", "")).toArray(String[]::new));
			outputEvent.setLocation(details.getMeetingPoint().replaceAll("\\<.*?>", ""));

			String ort = null;
			String bezirk = null;

			for (int i = 0; i < tagList.getTag().size(); i++)
			{
				TagList.Tag tag = tagList.getTag().get(i);
				if (tag.getType().equalsIgnoreCase("Ort"))
				{
					ort = tag.getValue();
				}
				else if (tag.getType().equalsIgnoreCase("Bezirk"))
				{
					bezirk = tag.getValue();
				}
			}

			if (ort != null)
			{
				String newLocation = ort;

				if (bezirk != null)
				{
					newLocation += " " + bezirk;
				}

				outputEvent.setLocation(newLocation);
			}

			String rating = details.getAverageRating();
			outputEvent.setRating(new Rating(rating != null ? Double.parseDouble(rating) : 0, rating != null ? Integer.parseInt(details.getNumRatings().get(0).getValue()) : 0));

			outputEvent.setAdapterID(details.getId());
			outputEvent.setLanguages(details.getLanguages().get(0).getLanguage().stream().map(l -> l.getCode()).collect(Collectors.toList()).toArray(new String[] {}));

			int duration_minuten = 0;

			String uhr = null;

			if (details.getMinDuration().startsWith("PT"))
			{
				uhr = details.getMinDuration().substring(2);
			}
			else if (details.getMinDuration().startsWith("P"))
			{
				uhr = details.getMinDuration().substring(1);
			}

			if (uhr.contains("H"))
			{
				String[] splitH = uhr.split("H");
				duration_minuten += Integer.parseInt(splitH[0]) * 60;

				if (splitH.length == 2)
					uhr = splitH[1];
			}

			if (uhr.contains("M"))
			{
				duration_minuten += Integer.parseInt(uhr.substring(0, uhr.length() - 1));
			}

			outputEvent.setDuration(duration_minuten);


			// Preise
			Prices prices = new Prices();
			prices.setType(TYPE.SIMPLE);

			List<Price> priceList = new ArrayList<Price>();

			prices.setList(priceList);

			for (greyjoy.travelsmart.adapter.rentaguide.model.details.TourDetails.Prices pc : details.getPrices())
			{
				for (greyjoy.travelsmart.adapter.rentaguide.model.details.TourDetails.Prices.Price price : pc.getPrice())
				{
					String priceDecription = price.getNote();

					if (priceDecription == null)
					{
						if (price.getMaxParticipants() != null)
						{
							priceDecription = "Gruppenpreis für maximal " + price.getMaxParticipants() + " Personen";
						}
						else if (price.getMinParticipants() != null)
						{
							priceDecription = "Gruppenpreis für mindestens " + price.getMinParticipants() + " Personen";
						}
						else if (price.getType().equals("single"))
						{
							priceDecription = "Einzelpreis";
						}
					}

					Price p = new Price();
					p.setName(priceDecription);
					p.setValue(Double.parseDouble(price.getPrice()));

					priceList.add(p);
				}
			}

			outputEvent.setPrices(prices);
			outputEvent.setDisplayPrice(priceList.get(0).getValue());

			outputEvent.setCategory(CategoryHandler.get().getCategoryFromName("Sightseeing & Kultur"));

			return outputEvent;
		});
	}

	private void saveDescription(String tourID, String description)
	{
		Path p = FileSystems.getDefault().getPath("descriptions", tourID + ".txt");

		if (!Files.exists(p))
		{
			try
			{
				Files.write(p, description.getBytes(Charset.forName("UTF-8")), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private Mono<TagList> getTags(String tourID)
	{
		return Mono.fromCallable(() -> {

			if (Config.CACHE_RENTAGUIDE_TAGS)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					tagsSearchStatement.setString(1, tourID);

					ResultSet resultSet = tagsSearchStatement.executeQuery();

					if (resultSet.next())
					{
						String xml = resultSet.getString(1);
						resultSet.close();
						return xml;
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				return null;
			}
		}).switchIfEmpty(webClient_api.get().uri("tour/{id}/tags", tourID).header("Authorization", "Basic cHJha3Rpa3VtQHN3Yy5yd3RoLWFhY2hlbi5kZTpTUS5fRUBwQXdqTjI=").retrieve().bodyToMono(String.class).retryWhen(detailsRetry).doOnNext(xml -> {

			if (Config.CACHE_RENTAGUIDE_TAGS)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					try
					{
						tagsSaveStatement.setString(1, tourID);
						tagsSaveStatement.setString(2, xml);
						tagsSaveStatement.executeUpdate();
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		})).map(xml -> {
			try
			{
				Unmarshaller tagsUm = tagListContext.createUnmarshaller();
				return (TagList) tagsUm.unmarshal(new StringReader(xml.trim()));
			}
			catch (JAXBException e)
			{
				e.printStackTrace();
			}

			return null;
		});
	}

	private Mono<TourDetails> getDetails(String tourID)
	{
		return Mono.fromCallable(() -> {

			if (Config.CACHE_RENTAGUIDE_DETAILS)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					detailsSearchStatement.setString(1, tourID);

					ResultSet resultSet = detailsSearchStatement.executeQuery();

					if (resultSet.next())
					{
						String xml = resultSet.getString(1);
						resultSet.close();
						return xml;
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				return null;
			}
		}).switchIfEmpty(webClient_api.get().uri("tour/{id}", tourID).header("Authorization", "Basic cHJha3Rpa3VtQHN3Yy5yd3RoLWFhY2hlbi5kZTpTUS5fRUBwQXdqTjI=").retrieve().bodyToMono(String.class).retryWhen(detailsRetry).doOnNext(xml -> {

			if (Config.CACHE_RENTAGUIDE_DETAILS)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					try
					{
						detailsSaveStatement.setString(1, tourID);
						detailsSaveStatement.setString(2, xml);
						detailsSaveStatement.executeUpdate();
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		})).map(xml -> {
			Unmarshaller detailsUm;
			try
			{
				detailsUm = detailsContext.createUnmarshaller();
				return (TourDetails) detailsUm.unmarshal(new StringReader(xml.trim()));
			}
			catch (JAXBException e)
			{
				e.printStackTrace();
			}

			return null;
		});
	}

	private Mono<AvailabilityList> getAvailabilityList(String tourID)
	{
		return webClient_api.get().uri("tour/{id}/availability", tourID).header("Authorization", "Basic cHJha3Rpa3VtQHN3Yy5yd3RoLWFhY2hlbi5kZTpTUS5fRUBwQXdqTjI=").retrieve().bodyToMono(String.class).retryWhen(availabilityRetry).map(xml -> {
			try
			{
				Unmarshaller availabilitiesUm = availabilitiesContext.createUnmarshaller();
				return (AvailabilityList) availabilitiesUm.unmarshal(new StringReader(xml.trim()));
			}
			catch (JAXBException e)
			{
				e.printStackTrace();
			}

			return null;
		});
	}

	@Override
	public String getVendorID()
	{
		return "rentaguide";
	}

	@Override
	public String getVendorName()
	{
		return "rent-a-guide";
	}

	@Override
	public Mono<EventDetails> getEventDetails(String eventID)
	{
		return Mono.just(eventID).flatMap(id -> Mono.zip(getDetails(id), getAvailabilityList(id), getTags(id)).map(tuple -> {
			TourDetails details = tuple.getT1();

			String longDescription = details.getLongDescription();

			List<String> highlights = Jsoup.parse(longDescription).getElementsByTag("li").eachText();

			String description;
			if (highlights.size() == 0)
			{
				description = longDescription;
			}
			else
			{
				String descriptionPart = longDescription.split("</ul>")[1];

				description = descriptionPart;
			}

			description = description.replace("&lt;br&gt;", "<br>");

			AvailabilityList availabilityList = tuple.getT2();
			TagList tagList = tuple.getT3();

			EventDetails outputDetails = new EventDetails();
			outputDetails.setTitle(details.getTitle());
			outputDetails.setImages(details.getImages().get(0).getImage().stream().map(i -> i.getHref().replace("_small", "")).toArray(String[]::new));
			outputDetails.setLocation(details.getMeetingPoint().replaceAll("\\<.*?>", ""));
			outputDetails.setDescription(description);

			String ort = null;
			String bezirk = null;

			for (int i = 0; i < tagList.getTag().size(); i++)
			{
				TagList.Tag tag = tagList.getTag().get(i);
				if (tag.getType().equalsIgnoreCase("Ort"))
				{
					ort = tag.getValue();
				}
				else if (tag.getType().equalsIgnoreCase("Bezirk"))
				{
					bezirk = tag.getValue();
				}
			}

			if (ort != null)
			{
				String newLocation = ort;

				if (bezirk != null)
				{
					newLocation += " " + bezirk;
				}

				outputDetails.setLocation(newLocation);
			}

			outputDetails.setHighlights(highlights.toArray(new String[0]));


			String rating = details.getAverageRating();
			outputDetails.setRating(new Rating(rating != null ? Double.parseDouble(rating) : 0, rating != null ? Integer.parseInt(details.getNumRatings().get(0).getValue()) : 0));

			int duration_minuten = 0;

			String uhr = null;

			if (details.getMinDuration().startsWith("PT"))
			{
				uhr = details.getMinDuration().substring(2);
			}
			else if (details.getMinDuration().startsWith("P"))
			{
				uhr = details.getMinDuration().substring(1);
			}

			if (uhr.contains("H"))
			{
				String[] splitH = uhr.split("H");
				duration_minuten += Integer.parseInt(splitH[0]) * 60;

				if (splitH.length == 2)
					uhr = splitH[1];
			}

			if (uhr.contains("M"))
			{
				duration_minuten += Integer.parseInt(uhr.substring(0, uhr.length() - 1));
			}

			outputDetails.setDuration(duration_minuten);

			outputDetails.setAdapterID(details.getId());
			outputDetails.setDescription(description);
			outputDetails.setLanguages(details.getLanguages().get(0).getLanguage().stream().map(l -> l.getCode()).collect(Collectors.toList()).toArray(new String[] {}));
			List<Ticket> tickets = new ArrayList<Ticket>();

			for (Availability a : availabilityList.getAvailability())
			{
				Ticket t = new Ticket();

				DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
				ZonedDateTime zonedTime = ZonedDateTime.parse(a.getStartDateTime(), formatter);
				ZonedDateTime zonedTimeUTC = zonedTime.withZoneSameInstant(ZoneOffset.UTC);

				t.setAvailable_slots(Integer.parseInt(a.getSlots()));
				t.setTotal_slots(Integer.parseInt(details.getMaxParticipants()));
				t.setDate(formatter.format(zonedTimeUTC));

				tickets.add(t);
			}

			outputDetails.setTickets(tickets);

			// Preise
			Prices prices = new Prices();
			prices.setType(TYPE.SIMPLE);

			List<Price> priceList = new ArrayList<Price>();

			prices.setList(priceList);

			for (greyjoy.travelsmart.adapter.rentaguide.model.details.TourDetails.Prices pc : details.getPrices())
			{
				for (greyjoy.travelsmart.adapter.rentaguide.model.details.TourDetails.Prices.Price price : pc.getPrice())
				{
					String priceDecription = price.getNote();

					if (priceDecription == null)
					{
						if (price.getMaxParticipants() != null)
						{
							priceDecription = "Gruppenpreis für maximal " + price.getMaxParticipants() + " Personen";
						}
						else if (price.getMinParticipants() != null)
						{
							priceDecription = "Gruppenpreis für mindestens " + price.getMinParticipants() + " Personen";
						}
						else if (price.getType().equals("single"))
						{
							priceDecription = "Einzelpreis";
						}
					}

					Price p = new Price();
					p.setName(priceDecription);
					p.setValue(Double.parseDouble(price.getPrice()));

					priceList.add(p);
				}
			}

			outputDetails.setPrices(prices);
			outputDetails.setDisplayPrice(priceList.get(0).getValue());

			outputDetails.setCategory(CategoryHandler.get().getCategoryFromName("Sightseeing & Kultur"));

			return outputDetails;
		}));
	}
}
