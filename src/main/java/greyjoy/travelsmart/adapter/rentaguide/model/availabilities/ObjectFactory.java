//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.05.10 um 07:40:54 PM CEST 
//

package greyjoy.travelsmart.adapter.rentaguide.model.availabilities;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * greyjoy.travelsmart.adapter.rentaguide.model.availabilities package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package:
	 * greyjoy.travelsmart.adapter.rentaguide.model.availabilities
	 * 
	 */
	public ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link AvailabilityList }
	 * 
	 */
	public AvailabilityList createAvailabilityList()
	{
		return new AvailabilityList();
	}

	/**
	 * Create an instance of {@link NewDataSet }
	 * 
	 */
	public NewDataSet createNewDataSet()
	{
		return new NewDataSet();
	}

	/**
	 * Create an instance of {@link AvailabilityList.Availability }
	 * 
	 */
	public AvailabilityList.Availability createAvailabilityListAvailability()
	{
		return new AvailabilityList.Availability();
	}

}
