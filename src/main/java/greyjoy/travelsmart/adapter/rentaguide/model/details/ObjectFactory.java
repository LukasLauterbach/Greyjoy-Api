//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.05.10 um 04:20:18 PM CEST 
//

package greyjoy.travelsmart.adapter.rentaguide.model.details;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * greyjoy.travelsmart.adapter.rentaguide.model.details package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package:
	 * greyjoy.travelsmart.adapter.rentaguide.model.details
	 * 
	 */
	public ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link TourDetails }
	 * 
	 */
	public TourDetails createTour()
	{
		return new TourDetails();
	}

	/**
	 * Create an instance of {@link TourDetails.Images }
	 * 
	 */
	public TourDetails.Images createTourImages()
	{
		return new TourDetails.Images();
	}

	/**
	 * Create an instance of {@link TourDetails.Languages }
	 * 
	 */
	public TourDetails.Languages createTourLanguages()
	{
		return new TourDetails.Languages();
	}

	/**
	 * Create an instance of {@link TourDetails.Prices }
	 * 
	 */
	public TourDetails.Prices createTourPrices()
	{
		return new TourDetails.Prices();
	}

	/**
	 * Create an instance of {@link NewDataSet }
	 * 
	 */
	public NewDataSet createNewDataSet()
	{
		return new NewDataSet();
	}

	/**
	 * Create an instance of {@link TourDetails.NumRatings }
	 * 
	 */
	public TourDetails.NumRatings createTourNumRatings()
	{
		return new TourDetails.NumRatings();
	}

	/**
	 * Create an instance of {@link TourDetails.Guide }
	 * 
	 */
	public TourDetails.Guide createTourGuide()
	{
		return new TourDetails.Guide();
	}

	/**
	 * Create an instance of {@link TourDetails.Availability }
	 * 
	 */
	public TourDetails.Availability createTourAvailability()
	{
		return new TourDetails.Availability();
	}

	/**
	 * Create an instance of {@link TourDetails.Tags }
	 * 
	 */
	public TourDetails.Tags createTourTags()
	{
		return new TourDetails.Tags();
	}

	/**
	 * Create an instance of {@link TourDetails.Reviews }
	 * 
	 */
	public TourDetails.Reviews createTourReviews()
	{
		return new TourDetails.Reviews();
	}

	/**
	 * Create an instance of {@link TourDetails.Images.Image }
	 * 
	 */
	public TourDetails.Images.Image createTourImagesImage()
	{
		return new TourDetails.Images.Image();
	}

	/**
	 * Create an instance of {@link TourDetails.Languages.Language }
	 * 
	 */
	public TourDetails.Languages.Language createTourLanguagesLanguage()
	{
		return new TourDetails.Languages.Language();
	}

	/**
	 * Create an instance of {@link TourDetails.Prices.Price }
	 * 
	 */
	public TourDetails.Prices.Price createTourPricesPrice()
	{
		return new TourDetails.Prices.Price();
	}

}
