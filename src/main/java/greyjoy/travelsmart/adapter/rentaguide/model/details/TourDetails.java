//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.05.10 um 04:20:18 PM CEST 
//

package greyjoy.travelsmart.adapter.rentaguide.model.details;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>
 * Java-Klasse für anonymous complex type.
 * 
 * <p>
 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
 * Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="meetingPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pricingModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="minAdvBookable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maxAdvBookable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cancellationPeriod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="minDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maxDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="averageRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="minParticipants" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prices" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="price" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="numRatings" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="languages" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="language" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="guide" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="availability" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tags" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="reviews" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="images" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="image" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "created", "updated", "title", "shortDescription", "longDescription", "meetingPoint", "info", "pricingModel", "minAdvBookable", "maxAdvBookable", "cancellationPeriod", "minDuration", "maxDuration", "averageRating", "minParticipants", "maxParticipants", "prices", "numRatings", "languages", "guide", "availability", "tags", "reviews", "images" })
@XmlRootElement(name = "tour")
public class TourDetails
{

	protected String id;
	protected String created;
	protected String updated;
	protected String title;
	protected String shortDescription;
	protected String longDescription;
	protected String meetingPoint;
	protected String info;
	protected String pricingModel;
	protected String minAdvBookable;
	protected String maxAdvBookable;
	protected String cancellationPeriod;
	protected String minDuration;
	protected String maxDuration;
	protected String averageRating;
	protected String minParticipants;
	protected String maxParticipants;
	protected List<TourDetails.Prices> prices;
	@XmlElement(nillable = true)
	protected List<TourDetails.NumRatings> numRatings;
	protected List<TourDetails.Languages> languages;
	@XmlElement(nillable = true)
	protected List<TourDetails.Guide> guide;
	protected List<TourDetails.Availability> availability;
	protected List<TourDetails.Tags> tags;
	protected List<TourDetails.Reviews> reviews;
	protected List<TourDetails.Images> images;
	@XmlAttribute(name = "uri")
	protected String uri;

	/**
	 * Ruft den Wert der id-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * Legt den Wert der id-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setId(String value)
	{
		this.id = value;
	}

	/**
	 * Ruft den Wert der created-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCreated()
	{
		return created;
	}

	/**
	 * Legt den Wert der created-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCreated(String value)
	{
		this.created = value;
	}

	/**
	 * Ruft den Wert der updated-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUpdated()
	{
		return updated;
	}

	/**
	 * Legt den Wert der updated-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUpdated(String value)
	{
		this.updated = value;
	}

	/**
	 * Ruft den Wert der title-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Legt den Wert der title-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value)
	{
		this.title = value;
	}

	/**
	 * Ruft den Wert der shortDescription-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShortDescription()
	{
		return shortDescription;
	}

	/**
	 * Legt den Wert der shortDescription-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setShortDescription(String value)
	{
		this.shortDescription = value;
	}

	/**
	 * Ruft den Wert der longDescription-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLongDescription()
	{
		return longDescription;
	}

	/**
	 * Legt den Wert der longDescription-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setLongDescription(String value)
	{
		this.longDescription = value;
	}

	/**
	 * Ruft den Wert der meetingPoint-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMeetingPoint()
	{
		return meetingPoint;
	}

	/**
	 * Legt den Wert der meetingPoint-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMeetingPoint(String value)
	{
		this.meetingPoint = value;
	}

	/**
	 * Ruft den Wert der info-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInfo()
	{
		return info;
	}

	/**
	 * Legt den Wert der info-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setInfo(String value)
	{
		this.info = value;
	}

	/**
	 * Ruft den Wert der pricingModel-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPricingModel()
	{
		return pricingModel;
	}

	/**
	 * Legt den Wert der pricingModel-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPricingModel(String value)
	{
		this.pricingModel = value;
	}

	/**
	 * Ruft den Wert der minAdvBookable-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMinAdvBookable()
	{
		return minAdvBookable;
	}

	/**
	 * Legt den Wert der minAdvBookable-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMinAdvBookable(String value)
	{
		this.minAdvBookable = value;
	}

	/**
	 * Ruft den Wert der maxAdvBookable-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaxAdvBookable()
	{
		return maxAdvBookable;
	}

	/**
	 * Legt den Wert der maxAdvBookable-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaxAdvBookable(String value)
	{
		this.maxAdvBookable = value;
	}

	/**
	 * Ruft den Wert der cancellationPeriod-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCancellationPeriod()
	{
		return cancellationPeriod;
	}

	/**
	 * Legt den Wert der cancellationPeriod-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCancellationPeriod(String value)
	{
		this.cancellationPeriod = value;
	}

	/**
	 * Ruft den Wert der minDuration-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMinDuration()
	{
		return minDuration;
	}

	/**
	 * Legt den Wert der minDuration-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMinDuration(String value)
	{
		this.minDuration = value;
	}

	/**
	 * Ruft den Wert der maxDuration-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaxDuration()
	{
		return maxDuration;
	}

	/**
	 * Legt den Wert der maxDuration-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaxDuration(String value)
	{
		this.maxDuration = value;
	}

	/**
	 * Ruft den Wert der averageRating-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAverageRating()
	{
		return averageRating;
	}

	/**
	 * Legt den Wert der averageRating-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAverageRating(String value)
	{
		this.averageRating = value;
	}

	/**
	 * Ruft den Wert der minParticipants-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMinParticipants()
	{
		return minParticipants;
	}

	/**
	 * Legt den Wert der minParticipants-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMinParticipants(String value)
	{
		this.minParticipants = value;
	}

	/**
	 * Ruft den Wert der maxParticipants-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMaxParticipants()
	{
		return maxParticipants;
	}

	/**
	 * Legt den Wert der maxParticipants-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMaxParticipants(String value)
	{
		this.maxParticipants = value;
	}

	/**
	 * Gets the value of the prices property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the prices property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPrices().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Prices }
	 * 
	 * 
	 */
	public List<TourDetails.Prices> getPrices()
	{
		if (prices == null)
		{
			prices = new ArrayList<TourDetails.Prices>();
		}
		return this.prices;
	}

	/**
	 * Gets the value of the numRatings property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the numRatings property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getNumRatings().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.NumRatings }
	 * 
	 * 
	 */
	public List<TourDetails.NumRatings> getNumRatings()
	{
		if (numRatings == null)
		{
			numRatings = new ArrayList<TourDetails.NumRatings>();
		}
		return this.numRatings;
	}

	/**
	 * Gets the value of the languages property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the languages property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getLanguages().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Languages }
	 * 
	 * 
	 */
	public List<TourDetails.Languages> getLanguages()
	{
		if (languages == null)
		{
			languages = new ArrayList<TourDetails.Languages>();
		}
		return this.languages;
	}

	/**
	 * Gets the value of the guide property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the guide property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGuide().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Guide }
	 * 
	 * 
	 */
	public List<TourDetails.Guide> getGuide()
	{
		if (guide == null)
		{
			guide = new ArrayList<TourDetails.Guide>();
		}
		return this.guide;
	}

	/**
	 * Gets the value of the availability property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the availability property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAvailability().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Availability }
	 * 
	 * 
	 */
	public List<TourDetails.Availability> getAvailability()
	{
		if (availability == null)
		{
			availability = new ArrayList<TourDetails.Availability>();
		}
		return this.availability;
	}

	/**
	 * Gets the value of the tags property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the tags property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTags().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Tags }
	 * 
	 * 
	 */
	public List<TourDetails.Tags> getTags()
	{
		if (tags == null)
		{
			tags = new ArrayList<TourDetails.Tags>();
		}
		return this.tags;
	}

	/**
	 * Gets the value of the reviews property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the reviews property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getReviews().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Reviews }
	 * 
	 * 
	 */
	public List<TourDetails.Reviews> getReviews()
	{
		if (reviews == null)
		{
			reviews = new ArrayList<TourDetails.Reviews>();
		}
		return this.reviews;
	}

	/**
	 * Gets the value of the images property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the images property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getImages().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourDetails.Images }
	 * 
	 * 
	 */
	public List<TourDetails.Images> getImages()
	{
		if (images == null)
		{
			images = new ArrayList<TourDetails.Images>();
		}
		return this.images;
	}

	/**
	 * Ruft den Wert der uri-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUri()
	{
		return uri;
	}

	/**
	 * Legt den Wert der uri-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUri(String value)
	{
		this.uri = value;
	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class Availability
	{

		@XmlAttribute(name = "uri")
		protected String uri;

		/**
		 * Ruft den Wert der uri-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUri()
		{
			return uri;
		}

		/**
		 * Legt den Wert der uri-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUri(String value)
		{
			this.uri = value;
		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;simpleContent>
	 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
	 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/extension>
	 *   &lt;/simpleContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "value" })
	public static class Guide
	{

		@XmlValue
		protected String value;
		@XmlAttribute(name = "uri")
		protected String uri;
		@XmlAttribute(name = "id")
		protected String id;

		/**
		 * Ruft den Wert der value-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getValue()
		{
			return value;
		}

		/**
		 * Legt den Wert der value-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setValue(String value)
		{
			this.value = value;
		}

		/**
		 * Ruft den Wert der uri-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUri()
		{
			return uri;
		}

		/**
		 * Legt den Wert der uri-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUri(String value)
		{
			this.uri = value;
		}

		/**
		 * Ruft den Wert der id-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getId()
		{
			return id;
		}

		/**
		 * Legt den Wert der id-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value)
		{
			this.id = value;
		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="image" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "image" })
	public static class Images
	{

		protected List<TourDetails.Images.Image> image;

		/**
		 * Gets the value of the image property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the image property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getImage().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourDetails.Images.Image }
		 * 
		 * 
		 */
		public List<TourDetails.Images.Image> getImage()
		{
			if (image == null)
			{
				image = new ArrayList<TourDetails.Images.Image>();
			}
			return this.image;
		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Image
		{

			@XmlAttribute(name = "width")
			protected String width;
			@XmlAttribute(name = "height")
			protected String height;
			@XmlAttribute(name = "href")
			protected String href;

			/**
			 * Ruft den Wert der width-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getWidth()
			{
				return width;
			}

			/**
			 * Legt den Wert der width-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setWidth(String value)
			{
				this.width = value;
			}

			/**
			 * Ruft den Wert der height-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getHeight()
			{
				return height;
			}

			/**
			 * Legt den Wert der height-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setHeight(String value)
			{
				this.height = value;
			}

			/**
			 * Ruft den Wert der href-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getHref()
			{
				return href;
			}

			/**
			 * Legt den Wert der href-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setHref(String value)
			{
				this.href = value;
			}

		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="language" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;simpleContent>
	 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
	 *                 &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/extension>
	 *             &lt;/simpleContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "language" })
	public static class Languages
	{

		@XmlElement(nillable = true)
		protected List<TourDetails.Languages.Language> language;

		/**
		 * Gets the value of the language property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the language property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getLanguage().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourDetails.Languages.Language }
		 * 
		 * 
		 */
		public List<TourDetails.Languages.Language> getLanguage()
		{
			if (language == null)
			{
				language = new ArrayList<TourDetails.Languages.Language>();
			}
			return this.language;
		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;simpleContent>
		 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
		 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/extension>
		 *   &lt;/simpleContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "value" })
		public static class Language
		{

			@XmlValue
			protected String value;
			@XmlAttribute(name = "code")
			protected String code;

			/**
			 * Ruft den Wert der value-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue()
			{
				return value;
			}

			/**
			 * Legt den Wert der value-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value)
			{
				this.value = value;
			}

			/**
			 * Ruft den Wert der code-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCode()
			{
				return code;
			}

			/**
			 * Legt den Wert der code-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCode(String value)
			{
				this.code = value;
			}

		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;simpleContent>
	 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
	 *     &lt;/extension>
	 *   &lt;/simpleContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "value" })
	public static class NumRatings
	{

		@XmlValue
		protected String value;

		/**
		 * Ruft den Wert der value-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getValue()
		{
			return value;
		}

		/**
		 * Legt den Wert der value-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setValue(String value)
		{
			this.value = value;
		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="price" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                 &lt;/sequence>
	 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "price" })
	public static class Prices
	{

		protected List<TourDetails.Prices.Price> price;

		/**
		 * Gets the value of the price property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the price property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getPrice().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourDetails.Prices.Price }
		 * 
		 * 
		 */
		public List<TourDetails.Prices.Price> getPrice()
		{
			if (price == null)
			{
				price = new ArrayList<TourDetails.Prices.Price>();
			}
			return this.price;
		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *       &lt;/sequence>
		 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "note" })
		public static class Price
		{

			protected String note;
			@XmlAttribute(name = "id")
			protected String id;
			@XmlAttribute(name = "type")
			protected String type;
			@XmlAttribute(name = "reduced")
			protected String reduced;
			@XmlAttribute(name = "price")
			protected String price;
			@XmlAttribute(name = "maxParticipants")
			protected String maxParticipants;
			@XmlAttribute(name = "minParticipants")
			protected String minParticipants;
			@XmlAttribute(name = "currency")
			protected String currency;

			/**
			 * Ruft den Wert der note-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNote()
			{
				return note;
			}

			/**
			 * Legt den Wert der note-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setNote(String value)
			{
				this.note = value;
			}

			/**
			 * Ruft den Wert der id-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getId()
			{
				return id;
			}

			/**
			 * Legt den Wert der id-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setId(String value)
			{
				this.id = value;
			}

			/**
			 * Ruft den Wert der type-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getType()
			{
				return type;
			}

			/**
			 * Legt den Wert der type-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setType(String value)
			{
				this.type = value;
			}

			/**
			 * Ruft den Wert der reduced-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getReduced()
			{
				return reduced;
			}

			/**
			 * Legt den Wert der reduced-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setReduced(String value)
			{
				this.reduced = value;
			}

			/**
			 * Ruft den Wert der price-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getPrice()
			{
				return price;
			}

			/**
			 * Legt den Wert der price-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setPrice(String value)
			{
				this.price = value;
			}

			/**
			 * Ruft den Wert der maxParticipants-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getMaxParticipants()
			{
				return maxParticipants;
			}

			/**
			 * Legt den Wert der maxParticipants-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setMaxParticipants(String value)
			{
				this.maxParticipants = value;
			}
			
			public String getMinParticipants()
			{
				return minParticipants;
			}

			/**
			 * Legt den Wert der maxParticipants-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setMinParticipants(String value)
			{
				this.minParticipants = value;
			}

			/**
			 * Ruft den Wert der currency-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCurrency()
			{
				return currency;
			}

			/**
			 * Legt den Wert der currency-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setCurrency(String value)
			{
				this.currency = value;
			}

		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class Reviews
	{

		@XmlAttribute(name = "uri")
		protected String uri;

		/**
		 * Ruft den Wert der uri-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUri()
		{
			return uri;
		}

		/**
		 * Legt den Wert der uri-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUri(String value)
		{
			this.uri = value;
		}

	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "")
	public static class Tags
	{

		@XmlAttribute(name = "uri")
		protected String uri;

		/**
		 * Ruft den Wert der uri-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUri()
		{
			return uri;
		}

		/**
		 * Legt den Wert der uri-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUri(String value)
		{
			this.uri = value;
		}

	}

}
