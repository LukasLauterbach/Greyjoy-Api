package greyjoy.travelsmart.adapter.rentaguide.model.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * <p>
 * Java-Klasse für anonymous complex type.
 * 
 * <p>
 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
 * Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tour" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="shortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="pricingModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="minDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="maxDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="numRatings" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="prices" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="price" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="minParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="addPerPersonPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="languages" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="language" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="guide" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="availability" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="image" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="next_page" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="total" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "tour" })
@XmlRootElement(name = "tourList")
public class TourList
{

	protected List<TourList.Tour> tour;
	@XmlAttribute(name = "uri")
	protected String uri;
	@XmlAttribute(name = "count")
	protected String count;
	@XmlAttribute(name = "next_page")
	protected String nextPage;
	@XmlAttribute(name = "total")
	protected String total;

	/**
	 * Gets the value of the tour property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the tour property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getTour().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TourList.Tour }
	 * 
	 * 
	 */
	public List<TourList.Tour> getTour()
	{
		if (tour == null)
		{
			tour = new ArrayList<TourList.Tour>();
		}
		return this.tour;
	}

	/**
	 * Ruft den Wert der uri-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUri()
	{
		return uri;
	}

	/**
	 * Legt den Wert der uri-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUri(String value)
	{
		this.uri = value;
	}

	/**
	 * Ruft den Wert der count-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCount()
	{
		return count;
	}

	/**
	 * Legt den Wert der count-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCount(String value)
	{
		this.count = value;
	}

	/**
	 * Ruft den Wert der nextPage-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNextPage()
	{
		return nextPage;
	}

	/**
	 * Legt den Wert der nextPage-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNextPage(String value)
	{
		this.nextPage = value;
	}

	/**
	 * Ruft den Wert der total-Eigenschaft ab.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTotal()
	{
		return total;
	}

	/**
	 * Legt den Wert der total-Eigenschaft fest.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTotal(String value)
	{
		this.total = value;
	}

	/**
	 * <p>
	 * Java-Klasse für anonymous complex type.
	 * 
	 * <p>
	 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
	 * Klasse enthalten ist.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="shortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="pricingModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="minDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="maxDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="numRatings" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *         &lt;element name="prices" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="price" maxOccurs="unbounded" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                           &lt;/sequence>
	 *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="minParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                           &lt;attribute name="addPerPersonPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="languages" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="language" maxOccurs="unbounded" minOccurs="0">
	 *                     &lt;complexType>
	 *                       &lt;simpleContent>
	 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
	 *                           &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                         &lt;/extension>
	 *                       &lt;/simpleContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="guide" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;simpleContent>
	 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
	 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/extension>
	 *             &lt;/simpleContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="availability" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="image" maxOccurs="unbounded" minOccurs="0">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "title", "shortDescription", "pricingModel", "minDuration", "maxDuration", "numRatings", "prices", "languages", "guide", "availability", "image" })
	public static class Tour
	{

		protected String title;
		protected String shortDescription;
		protected String pricingModel;
		protected String minDuration;
		protected String maxDuration;
		protected String numRatings;
		protected List<TourList.Tour.Prices> prices;
		protected List<TourList.Tour.Languages> languages;
		@XmlElement(nillable = true)
		protected List<TourList.Tour.Guide> guide;
		protected List<TourList.Tour.Availability> availability;
		protected List<TourList.Tour.Image> image;
		@XmlAttribute(name = "uri")
		protected String uri;
		@XmlAttribute(name = "id")
		protected String id;

		/**
		 * Ruft den Wert der title-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getTitle()
		{
			return title;
		}

		/**
		 * Legt den Wert der title-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setTitle(String value)
		{
			this.title = value;
		}

		/**
		 * Ruft den Wert der shortDescription-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getShortDescription()
		{
			return shortDescription;
		}

		/**
		 * Legt den Wert der shortDescription-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setShortDescription(String value)
		{
			this.shortDescription = value;
		}

		/**
		 * Ruft den Wert der pricingModel-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getPricingModel()
		{
			return pricingModel;
		}

		/**
		 * Legt den Wert der pricingModel-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setPricingModel(String value)
		{
			this.pricingModel = value;
		}

		/**
		 * Ruft den Wert der minDuration-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMinDuration()
		{
			return minDuration;
		}

		/**
		 * Legt den Wert der minDuration-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMinDuration(String value)
		{
			this.minDuration = value;
		}

		/**
		 * Ruft den Wert der maxDuration-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMaxDuration()
		{
			return maxDuration;
		}

		/**
		 * Legt den Wert der maxDuration-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setMaxDuration(String value)
		{
			this.maxDuration = value;
		}

		/**
		 * Ruft den Wert der numRatings-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getNumRatings()
		{
			return numRatings;
		}

		/**
		 * Legt den Wert der numRatings-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setNumRatings(String value)
		{
			this.numRatings = value;
		}

		/**
		 * Gets the value of the prices property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the prices property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getPrices().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourList.Tour.Prices }
		 * 
		 * 
		 */
		public List<TourList.Tour.Prices> getPrices()
		{
			if (prices == null)
			{
				prices = new ArrayList<TourList.Tour.Prices>();
			}
			return this.prices;
		}

		/**
		 * Gets the value of the languages property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the languages property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getLanguages().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourList.Tour.Languages }
		 * 
		 * 
		 */
		public List<TourList.Tour.Languages> getLanguages()
		{
			if (languages == null)
			{
				languages = new ArrayList<TourList.Tour.Languages>();
			}
			return this.languages;
		}

		/**
		 * Gets the value of the guide property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the guide property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getGuide().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourList.Tour.Guide }
		 * 
		 * 
		 */
		public List<TourList.Tour.Guide> getGuide()
		{
			if (guide == null)
			{
				guide = new ArrayList<TourList.Tour.Guide>();
			}
			return this.guide;
		}

		/**
		 * Gets the value of the availability property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the availability property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getAvailability().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourList.Tour.Availability }
		 * 
		 * 
		 */
		public List<TourList.Tour.Availability> getAvailability()
		{
			if (availability == null)
			{
				availability = new ArrayList<TourList.Tour.Availability>();
			}
			return this.availability;
		}

		/**
		 * Gets the value of the image property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the image property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getImage().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link TourList.Tour.Image }
		 * 
		 * 
		 */
		public List<TourList.Tour.Image> getImage()
		{
			if (image == null)
			{
				image = new ArrayList<TourList.Tour.Image>();
			}
			return this.image;
		}

		/**
		 * Ruft den Wert der uri-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getUri()
		{
			return uri;
		}

		/**
		 * Legt den Wert der uri-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setUri(String value)
		{
			this.uri = value;
		}

		/**
		 * Ruft den Wert der id-Eigenschaft ab.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getId()
		{
			return id;
		}

		/**
		 * Legt den Wert der id-Eigenschaft fest.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setId(String value)
		{
			this.id = value;
		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Availability
		{

			@XmlAttribute(name = "uri")
			protected String uri;

			/**
			 * Ruft den Wert der uri-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getUri()
			{
				return uri;
			}

			/**
			 * Legt den Wert der uri-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setUri(String value)
			{
				this.uri = value;
			}

		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;simpleContent>
		 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
		 *       &lt;attribute name="uri" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/extension>
		 *   &lt;/simpleContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "value" })
		public static class Guide
		{

			@XmlValue
			protected String value;
			@XmlAttribute(name = "uri")
			protected String uri;
			@XmlAttribute(name = "id")
			protected String id;

			/**
			 * Ruft den Wert der value-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue()
			{
				return value;
			}

			/**
			 * Legt den Wert der value-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value)
			{
				this.value = value;
			}

			/**
			 * Ruft den Wert der uri-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getUri()
			{
				return uri;
			}

			/**
			 * Legt den Wert der uri-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setUri(String value)
			{
				this.uri = value;
			}

			/**
			 * Ruft den Wert der id-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getId()
			{
				return id;
			}

			/**
			 * Legt den Wert der id-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setId(String value)
			{
				this.id = value;
			}

		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="height" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="href" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Image
		{

			@XmlAttribute(name = "width")
			protected String width;
			@XmlAttribute(name = "height")
			protected String height;
			@XmlAttribute(name = "href")
			protected String href;

			/**
			 * Ruft den Wert der width-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getWidth()
			{
				return width;
			}

			/**
			 * Legt den Wert der width-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setWidth(String value)
			{
				this.width = value;
			}

			/**
			 * Ruft den Wert der height-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getHeight()
			{
				return height;
			}

			/**
			 * Legt den Wert der height-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setHeight(String value)
			{
				this.height = value;
			}

			/**
			 * Ruft den Wert der href-Eigenschaft ab.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getHref()
			{
				return href;
			}

			/**
			 * Legt den Wert der href-Eigenschaft fest.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setHref(String value)
			{
				this.href = value;
			}

		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="language" maxOccurs="unbounded" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;simpleContent>
		 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
		 *                 &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *               &lt;/extension>
		 *             &lt;/simpleContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "language" })
		public static class Languages
		{

			@XmlElement(nillable = true)
			protected List<TourList.Tour.Languages.Language> language;

			/**
			 * Gets the value of the language property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a snapshot.
			 * Therefore any modification you make to the returned list will be present
			 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
			 * for the language property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getLanguage().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link TourList.Tour.Languages.Language }
			 * 
			 * 
			 */
			public List<TourList.Tour.Languages.Language> getLanguage()
			{
				if (language == null)
				{
					language = new ArrayList<TourList.Tour.Languages.Language>();
				}
				return this.language;
			}

			/**
			 * <p>
			 * Java-Klasse für anonymous complex type.
			 * 
			 * <p>
			 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
			 * Klasse enthalten ist.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;simpleContent>
			 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
			 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *     &lt;/extension>
			 *   &lt;/simpleContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "value" })
			public static class Language
			{

				@XmlValue
				protected String value;
				@XmlAttribute(name = "code")
				protected String code;

				/**
				 * Ruft den Wert der value-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getValue()
				{
					return value;
				}

				/**
				 * Legt den Wert der value-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setValue(String value)
				{
					this.value = value;
				}

				/**
				 * Ruft den Wert der code-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getCode()
				{
					return code;
				}

				/**
				 * Legt den Wert der code-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setCode(String value)
				{
					this.code = value;
				}

			}

		}

		/**
		 * <p>
		 * Java-Klasse für anonymous complex type.
		 * 
		 * <p>
		 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
		 * Klasse enthalten ist.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="price" maxOccurs="unbounded" minOccurs="0">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                 &lt;/sequence>
		 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="minParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                 &lt;attribute name="addPerPersonPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "price" })
		public static class Prices
		{

			protected List<TourList.Tour.Prices.Price> price;

			/**
			 * Gets the value of the price property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a snapshot.
			 * Therefore any modification you make to the returned list will be present
			 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
			 * for the price property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getPrice().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list
			 * {@link TourList.Tour.Prices.Price }
			 * 
			 * 
			 */
			public List<TourList.Tour.Prices.Price> getPrice()
			{
				if (price == null)
				{
					price = new ArrayList<TourList.Tour.Prices.Price>();
				}
				return this.price;
			}

			/**
			 * <p>
			 * Java-Klasse für anonymous complex type.
			 * 
			 * <p>
			 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
			 * Klasse enthalten ist.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *       &lt;/sequence>
			 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="reduced" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="price" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="maxParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="currency" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="minParticipants" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *       &lt;attribute name="addPerPersonPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "note" })
			public static class Price
			{

				protected String note;
				@XmlAttribute(name = "id")
				protected String id;
				@XmlAttribute(name = "type")
				protected String type;
				@XmlAttribute(name = "reduced")
				protected String reduced;
				@XmlAttribute(name = "price")
				protected String price;
				@XmlAttribute(name = "maxParticipants")
				protected String maxParticipants;
				@XmlAttribute(name = "currency")
				protected String currency;
				@XmlAttribute(name = "minParticipants")
				protected String minParticipants;
				@XmlAttribute(name = "addPerPersonPrice")
				protected String addPerPersonPrice;

				/**
				 * Ruft den Wert der note-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getNote()
				{
					return note;
				}

				/**
				 * Legt den Wert der note-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setNote(String value)
				{
					this.note = value;
				}

				/**
				 * Ruft den Wert der id-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getId()
				{
					return id;
				}

				/**
				 * Legt den Wert der id-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setId(String value)
				{
					this.id = value;
				}

				/**
				 * Ruft den Wert der type-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getType()
				{
					return type;
				}

				/**
				 * Legt den Wert der type-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setType(String value)
				{
					this.type = value;
				}

				/**
				 * Ruft den Wert der reduced-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getReduced()
				{
					return reduced;
				}

				/**
				 * Legt den Wert der reduced-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setReduced(String value)
				{
					this.reduced = value;
				}

				/**
				 * Ruft den Wert der price-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getPrice()
				{
					return price;
				}

				/**
				 * Legt den Wert der price-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setPrice(String value)
				{
					this.price = value;
				}

				/**
				 * Ruft den Wert der maxParticipants-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getMaxParticipants()
				{
					return maxParticipants;
				}

				/**
				 * Legt den Wert der maxParticipants-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setMaxParticipants(String value)
				{
					this.maxParticipants = value;
				}

				/**
				 * Ruft den Wert der currency-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getCurrency()
				{
					return currency;
				}

				/**
				 * Legt den Wert der currency-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setCurrency(String value)
				{
					this.currency = value;
				}

				/**
				 * Ruft den Wert der minParticipants-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getMinParticipants()
				{
					return minParticipants;
				}

				/**
				 * Legt den Wert der minParticipants-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setMinParticipants(String value)
				{
					this.minParticipants = value;
				}

				/**
				 * Ruft den Wert der addPerPersonPrice-Eigenschaft ab.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getAddPerPersonPrice()
				{
					return addPerPersonPrice;
				}

				/**
				 * Legt den Wert der addPerPersonPrice-Eigenschaft fest.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 * 
				 */
				public void setAddPerPersonPrice(String value)
				{
					this.addPerPersonPrice = value;
				}

			}

		}

	}

}
