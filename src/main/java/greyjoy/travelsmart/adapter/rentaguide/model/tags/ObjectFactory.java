//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.05.14 um 06:09:56 PM CEST 
//

package greyjoy.travelsmart.adapter.rentaguide.model.tags;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * greyjoy.travelsmart.adapter.rentaguide.model.tags package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package:
	 * greyjoy.travelsmart.adapter.rentaguide.model.tags
	 * 
	 */
	public ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link TagList }
	 * 
	 */
	public TagList createTagList()
	{
		return new TagList();
	}

	/**
	 * Create an instance of {@link TagList.Tag }
	 * 
	 */
	public TagList.Tag createTagListTag()
	{
		return new TagList.Tag();
	}

	/**
	 * Create an instance of {@link NewDataSet }
	 * 
	 */
	public NewDataSet createNewDataSet()
	{
		return new NewDataSet();
	}

}
