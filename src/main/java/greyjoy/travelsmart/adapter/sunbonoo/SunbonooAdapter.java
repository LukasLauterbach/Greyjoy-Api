package greyjoy.travelsmart.adapter.sunbonoo;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import greyjoy.travelsmart.adapter.ITravelAdapter;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductReturnEntity;
import greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceStub;
import greyjoy.travelsmart.adapter.sunbonoo.regions.RegionPoint;
import greyjoy.travelsmart.adapter.sunbonoo.regions.SunbonooRegionHandler;
import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.handler.CategoryHandler;
import greyjoy.travelsmart.api.handler.DatabaseHandler;
import greyjoy.travelsmart.api.model.Category;
import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.api.model.Rating;
import greyjoy.travelsmart.api.model.Ticket;
import greyjoy.travelsmart.api.model.price.Price;
import greyjoy.travelsmart.api.model.price.PriceGroup;
import greyjoy.travelsmart.api.model.price.Prices;
import greyjoy.travelsmart.api.model.price.Prices.TYPE;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.retry.Retry;

@Service
public class SunbonooAdapter implements ITravelAdapter
{
	private @Autowired Log logger;

	MagentoServiceStub service;
	Mono<String> sessionCache;

	private final Retry<Event> retry = Retry.<Event> any().fixedBackoff(Duration.ofMillis(1000));

	SunbonooHelper helper;

	SunbonooRegionHandler regionHandler;

	DatabaseHandler databaseHandler;

	WebClient.Builder webClientBuilder;

	HashMap<Integer, Integer> categoryMap;

	public SunbonooAdapter(WebClient.Builder webClientBuilder, SunbonooRegionHandler regionHandler, DatabaseHandler databaseHandler)
	{
		try
		{
			service = new MagentoServiceStub();
		}
		catch (AxisFault e)
		{
			e.printStackTrace();
		}

		this.regionHandler = regionHandler;
		this.databaseHandler = databaseHandler;
		this.webClientBuilder = webClientBuilder;
		this.categoryMap = new HashMap<Integer, Integer>();

		categoryMap.put(176, 1);
		categoryMap.put(179, 2);
		categoryMap.put(187, 3);
		categoryMap.put(188, 4);
		categoryMap.put(189, 5);
		categoryMap.put(190, 6);
		categoryMap.put(191, 7);
		categoryMap.put(192, 8);
		categoryMap.put(193, 9);
	}

	@PostConstruct
	public void init()
	{
		sessionCache = SunbonooHelper.login(service, "APIDemo", "vXojqgCDtCNBztvp*vFTD9MfgMeJytWgXhWiR8PF").cache(Duration.ofHours(23));

		helper = new SunbonooHelper(service, sessionCache, webClientBuilder, databaseHandler);

		regionHandler.initRegions(sessionCache, service);
	}

	@Override
	public Flux<Event> getEvents(Filter filter) throws Exception
	{
		Flux<Integer> categoryIDs;

		if (filter.filterLocation())
		{
			categoryIDs = regionHandler.getOverlappingRegions(filter.location_radius(), filter.location_latitude(), filter.location_longitude());
		}
		else
		{
			categoryIDs = Flux.just(319);
		}

		if (filter.filterDate())
		{
			// categoryIDs = Flux.empty(); // TODO: Implement
		}

		return categoryIDs.flatMap(categoryID -> helper.getProducts(categoryID)).distinct(cd -> cd.getSku()).flatMap(cd -> {
			return Mono.zip(Mono.just(cd), helper.getWebsiteData(cd.getSku(), cd.getUrl_path()), helper.getBookableOptions(cd.getSku()));
		}, 4).map(tuple -> {
			CatalogProductReturnEntity cd = tuple.getT1();
			Event o = new Event();

			List<CatalogProductReturnEntity> bookableOptions = tuple.getT3();

			o.setTitle(cd.getName());
			o.setAdapterID(cd.getSku());

			String[] categoryID = cd.getCategory_ids().getComplexObjectArray();
			for (String sid : categoryID)
			{
				int id = Integer.parseInt(sid);

				if (o.getCategory().getId() == 0 && categoryMap.containsKey(id))
				{
					Category c = CategoryHandler.get().getCategoryFromID(categoryMap.get(id));

					o.setCategory(c);
				}

				RegionPoint rp = regionHandler.getRegionPoint(id);

				if (rp != null)
				{
					o.setLocation(regionHandler.getRegionName(id));
					o.setLocation_lat(rp.getLatitude());
					o.setLocation_long(rp.getLongitude());

					break;
				}
			}

			if (cd.isLatitudeSpecified())
			{
				o.setLocation_lat(Double.parseDouble(cd.getLatitude()));
			}

			if (cd.isLongitudeSpecified())
			{
				o.setLocation_long(Double.parseDouble(cd.getLongitude()));
			}

			String durationString = cd.getLength();
			int duration = 0;

			switch (durationString)
			{
				case "ganzer Tag":
					duration = 9 * 60;
					break;
				case "halber Tag":
					duration = 6 * 60;
					break;
				case "nach Wahl":
					duration = -1;
					break;
				default:
					try
					{
						if (durationString.startsWith("ca. "))
						{
							String[] caSplit = durationString.split("ca\\. ");
							String number = caSplit[1];

							boolean stunden = false;
							boolean invalid = false;

							if (number.contains("Stunden"))
							{
								number = number.split("Stunden")[0];

								stunden = true;
							}
							else if (number.contains("Stunde"))
							{
								number = number.split("Stunde")[0];

								stunden = true;
							}
							else if (number.contains("Std."))
							{
								number = number.split("Std\\.")[0];
								stunden = true;
							}
							else if (number.contains("Minuten"))
							{
								number = number.split("Minuten")[0];
							}
							else
							{
								logger.error("Invalid Number: " + number);
								invalid = true;
							}

							if (!invalid)
							{
								if (number.contains("-"))
								{
									number = number.split("-")[0].trim();
								}

								duration = (int) Math.floor(Double.parseDouble(number.replace(',', '.')) * (stunden ? 60 : 1));
							}
						}
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
					break;
			}

			o.setDuration(duration);

			o.setLanguages(Arrays.stream(cd.getLanguages().split(",")).map(s -> s.toLowerCase().trim()).collect(Collectors.toList()).toArray(new String[0]));

			// Preise
			HashMap<String, String> groupNameMap = new HashMap<String, String>();

			HashMap<String, List<Price>> priceGroupMap = new HashMap<>();

			// Preise
			for (CatalogProductReturnEntity bo : bookableOptions)
			{
				List<Price> priceList;
				if (priceGroupMap.containsKey(bo.getOption_group_id()))
				{
					priceList = priceGroupMap.get(bo.getOption_group_id());
				}
				else
				{
					priceList = new ArrayList<Price>();
					priceGroupMap.put(bo.getOption_group_id(), priceList);

					if (!groupNameMap.containsKey(bo.getOption_group_id()))
					{
						groupNameMap.put(bo.getOption_group_id(), bo.getOption_group_name());
					}
				}

				Price p = new Price();

				p.setName(bo.getName());
				p.setValue(Double.parseDouble(bo.getPrice()));

				priceList.add(p);
			}

			Prices prices = new Prices();

			if (priceGroupMap.keySet().size() == 1 && priceGroupMap.containsKey(null))
			{
				// Simple
				prices.setType(TYPE.SIMPLE);

				prices.setList(priceGroupMap.get(null));
			}
			else
			{
				// Grouped
				prices.setType(TYPE.GROUPED);

				prices.setGroups(priceGroupMap.keySet().stream().map(groupID -> {
					PriceGroup pg = new PriceGroup();
					pg.setName(groupNameMap.get(groupID));

					pg.setList(priceGroupMap.get(groupID));

					return pg;
				}).collect(Collectors.toList()));
			}

			o.setPrices(prices);

			if (cd.isShowpriceSpecified())
			{
				o.setDisplayPrice(Double.parseDouble(cd.getShowprice()));
			}
			else if (!priceGroupMap.isEmpty())
			{
				o.setDisplayPrice(priceGroupMap.values().iterator().next().get(0).getValue());
			}
			else
			{
				o.setDisplayPrice(0);
			}

			if (tuple.getT2().isEmpty())
			{

			}
			else
			{
				String html = tuple.getT2();
				Element e = Jsoup.parse(html);

				String ratingStyle = e.getElementsByClass("rating").attr("style");

				String ratingCount = e.getElementsByAttributeValue("itemProp", "reviewCount").text();

				Rating rating = new Rating(0, 0);
				if (!ratingStyle.isEmpty() && !ratingCount.isEmpty())
				{
					rating = new Rating(5D / 100 * Integer.parseInt(ratingStyle.split(":")[1].split("%")[0]), Integer.parseInt(ratingCount));
				}

				List<String> images = e.getElementsByClass("rsImg").eachAttr("href");

				o.setImages(images.toArray(new String[0]));
				o.setRating(rating);
			}

			return o;
		}).filter(e -> !filter.filterLanguage() || Arrays.stream(e.getLanguages()).anyMatch(s -> s.equals(filter.language())));
	}

	@Override
	public Mono<EventDetails> getEventDetails(String productSku)
	{
		return helper.getProductInfo(productSku).flatMap(cp -> {
			return Mono.zip(Mono.just(cp), helper.getWebsiteData(cp.getSku(), cp.getUrl_path()), helper.getBookableOptions(cp.getSku()));
		}).map(tuple -> {
			CatalogProductReturnEntity cd = tuple.getT1();
			String html = tuple.getT2();

			List<CatalogProductReturnEntity> bookableOptions = tuple.getT3();

			EventDetails ed = new EventDetails();

			ed.setTitle(cd.getName());
			ed.setAdapterID(cd.getSku());
			ed.setDescription(cd.getShort_description());

			String[] categoryID = cd.getCategory_ids().getComplexObjectArray();

			for (String sid : categoryID)
			{
				int id = Integer.parseInt(sid);

				if (ed.getCategory().getId() == 0 && categoryMap.containsKey(id))
				{
					Category c = CategoryHandler.get().getCategoryFromID(categoryMap.get(id));

					ed.setCategory(c);
				}

				RegionPoint rp = regionHandler.getRegionPoint(id);

				if (rp != null)
				{
					ed.setLocation(regionHandler.getRegionName(id));
					ed.setLocation_lat(rp.getLatitude());
					ed.setLocation_long(rp.getLongitude());

					break;
				}
			}

			if (cd.isLatitudeSpecified())
			{
				ed.setLocation_lat(Double.parseDouble(cd.getLatitude()));
			}

			if (cd.isLongitudeSpecified())
			{
				ed.setLocation_long(Double.parseDouble(cd.getLongitude()));
			}

			String durationString = cd.getLength();
			int duration = 0;

			switch (durationString)
			{
				case "ganzer Tag":
					duration = 9 * 60;
					break;
				case "halber Tag":
					duration = 6 * 60;
					break;
				case "nach Wahl":
					duration = -1;
					break;
				default:
					try
					{
						if (durationString.startsWith("ca. "))
						{
							String[] caSplit = durationString.split("ca\\. ");
							String number = caSplit[1];

							boolean stunden = false;
							boolean invalid = false;

							if (number.contains("Stunden"))
							{
								number = number.split("Stunden")[0];

								stunden = true;
							}
							else if (number.contains("Stunde"))
							{
								number = number.split("Stunde")[0];

								stunden = true;
							}
							else if (number.contains("Std."))
							{
								number = number.split("Std\\.")[0];
								stunden = true;
							}
							else if (number.contains("Minuten"))
							{
								number = number.split("Minuten")[0];
							}
							else
							{
								logger.error("Invalid Number: " + number);
								invalid = true;
							}

							if (!invalid)
							{
								if (number.contains("-"))
								{
									number = number.split("-")[0].trim();
								}

								duration = (int) Math.floor(Double.parseDouble(number.replace(',', '.')) * (stunden ? 60 : 1));
							}
						}
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
					break;
			}

			ed.setDuration(duration);

			ed.setLanguages(Arrays.stream(cd.getLanguages().split(",")).map(s -> s.toLowerCase().trim()).collect(Collectors.toList()).toArray(new String[0]));

			// Preise
			HashMap<String, String> groupNameMap = new HashMap<String, String>();

			HashMap<String, List<Price>> priceGroupMap = new HashMap<>();

			// Preise
			for (CatalogProductReturnEntity bo : bookableOptions)
			{
				List<Price> priceList;
				if (priceGroupMap.containsKey(bo.getOption_group_id()))
				{
					priceList = priceGroupMap.get(bo.getOption_group_id());
				}
				else
				{
					priceList = new ArrayList<Price>();
					priceGroupMap.put(bo.getOption_group_id(), priceList);

					if (!groupNameMap.containsKey(bo.getOption_group_id()))
					{
						groupNameMap.put(bo.getOption_group_id(), bo.getOption_group_name());
					}
				}

				Price p = new Price();

				p.setName(bo.getName());
				p.setValue(Double.parseDouble(bo.getPrice()));

				priceList.add(p);
			}

			Prices prices = new Prices();

			if (priceGroupMap.keySet().size() == 1 && priceGroupMap.containsKey(null))
			{
				// Simple
				prices.setType(TYPE.SIMPLE);

				prices.setList(priceGroupMap.get(null));
			}
			else
			{
				// Grouped
				prices.setType(TYPE.GROUPED);

				prices.setGroups(priceGroupMap.keySet().stream().map(groupID -> {
					PriceGroup pg = new PriceGroup();
					pg.setName(groupNameMap.get(groupID));

					pg.setList(priceGroupMap.get(groupID));

					return pg;
				}).collect(Collectors.toList()));
			}

			ed.setPrices(prices);

			if (cd.isShowpriceSpecified())
			{
				ed.setDisplayPrice(Double.parseDouble(cd.getShowprice()));
			}
			else if (!priceGroupMap.isEmpty())
			{
				ed.setDisplayPrice(priceGroupMap.values().iterator().next().get(0).getValue());
			}
			else
			{
				ed.setDisplayPrice(0);
			}

			if (!tuple.getT2().isEmpty())
			{
				Element e = Jsoup.parse(html);

				String title = e.getElementsByAttributeValue("itemprop", "name").text();

				String ratingStyle = e.getElementsByClass("rating").attr("style");
				String ratingCount = e.getElementsByAttributeValue("itemProp", "reviewCount").text();

				Rating rating = new Rating(0, 0);
				if (!ratingStyle.isEmpty() && !ratingCount.isEmpty())
				{
					rating = new Rating(5D / 100 * Integer.parseInt(ratingStyle.split(":")[1].split("%")[0]), Integer.parseInt(ratingCount));
				}

				List<String> images = e.getElementsByClass("rsImg").eachAttr("href");

				ed.setImages(images.toArray(new String[0]));
				ed.setRating(rating);
			}

			// Fake Tickets
			List<Ticket> fakeTickets = new ArrayList<Ticket>();

			ZonedDateTime now = ZonedDateTime.now();

			DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;

			for (int i = 0; i < 10; i++)
			{
				Ticket t = new Ticket();

				ZonedDateTime ticketTime = now.plusDays(i).withHour(i % 2 == 0 ? 14 : 18).withMinute(0).withSecond(0).withZoneSameInstant(ZoneOffset.UTC);

				t.setAvailable_slots(20);
				t.setTotal_slots(20);
				t.setDate(formatter.format(ticketTime));

				fakeTickets.add(t);
			}

			ed.setTickets(fakeTickets);

			return ed;
		});
	}

	@Override
	public String getVendorName()
	{
		return "Sunbonoo";
	}

	public String getVendorID()
	{
		return "sunbonoo";
	}
}
