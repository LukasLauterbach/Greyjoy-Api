package greyjoy.travelsmart.adapter.sunbonoo;

import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axis2.AxisFault;
import org.apache.axis2.databinding.ADBException;
import org.apache.commons.io.input.XmlStreamReader;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAssignedProduct;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAssignedProductArray;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsRequestParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoRequestParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkEntity;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListRequestParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsEntity;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsEntityArray;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsRequestParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductReturnEntity;
import greyjoy.travelsmart.adapter.sunbonoo.magento.LoginParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.LoginResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoService;
import greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler;
import greyjoy.travelsmart.api.handler.DatabaseHandler;
import greyjoy.travelsmart.config.Config;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class SunbonooHelper
{
	Mono<String> sessionCache;
	MagentoService service;

	HashMap<String, String> languageCodes;

	WebClient webClient_site;

	private PreparedStatement websiteSearchStatement;
	private PreparedStatement websiteSaveStatement;

	private PreparedStatement infoSearchStatement;
	private PreparedStatement infoSaveStatement;
	
	private PreparedStatement bookablesSearchStatement;
	private PreparedStatement bookablesSaveStatement;

	public SunbonooHelper(MagentoService service, Mono<String> sessionCache, WebClient.Builder webClientBuilder, DatabaseHandler databaseHandler)
	{
		this.sessionCache = sessionCache;
		this.service = service;

		languageCodes = new HashMap<String, String>();
		languageCodes.put("Deutsch", "de");
		languageCodes.put("Englisch", "en");
		languageCodes.put("Spanisch", "es");
		languageCodes.put("Italienisch", "it");
		languageCodes.put("Französisch", "fr");
		languageCodes.put("Russisch", "ru");
		languageCodes.put("Niederländisch", "nl");
		languageCodes.put("Katalanisch", "ca");
		languageCodes.put("Portugiesisch", "pt");
		languageCodes.put("Chinesisch", "zh");
		languageCodes.put("Schwedisch", "sv");

		this.webClient_site = webClientBuilder.baseUrl("https://www.sunbonoo.com/de/").clientConnector(new ReactorClientHttpConnector(options -> {
			options.compression(true);
			options.sslSupport();
		})).build();

		databaseHandler.executeUpdate("CREATE TABLE IF NOT EXISTS SUNBONOO_WEBSITE (\r\n" + "    sku     TEXT PRIMARY KEY,\r\n" + "    html TEXT\r\n" + ")");
		databaseHandler.executeUpdate("CREATE TABLE IF NOT EXISTS SUNBONOO_INFO (\r\n" + "    sku     TEXT PRIMARY KEY,\r\n" + "    xml TEXT\r\n" + ")");
		databaseHandler.executeUpdate("CREATE TABLE IF NOT EXISTS SUNBONOO_BOOKABLES (\r\n" + "    sku     TEXT PRIMARY KEY,\r\n" + "    xml TEXT\r\n" + ")");
		
		this.websiteSearchStatement = databaseHandler.prepare("SELECT html FROM SUNBONOO_WEBSITE WHERE sku = ?");
		this.websiteSaveStatement = databaseHandler.prepare("INSERT OR IGNORE INTO SUNBONOO_WEBSITE VALUES (?,?)");

		this.infoSearchStatement = databaseHandler.prepare("SELECT xml FROM SUNBONOO_INFO WHERE sku = ?");
		this.infoSaveStatement = databaseHandler.prepare("INSERT OR IGNORE INTO SUNBONOO_INFO VALUES (?,?)");
		
		this.bookablesSearchStatement = databaseHandler.prepare("SELECT xml FROM SUNBONOO_BOOKABLES WHERE sku = ?");
		this.bookablesSaveStatement = databaseHandler.prepare("INSERT OR IGNORE INTO SUNBONOO_BOOKABLES VALUES (?,?)");
	}

	public String getLanguageCode(String language)
	{
		boolean exists = languageCodes.containsKey(language);

		if (!exists)
		{
			System.out.println("Missing Language Code Mapping: " + language);
		}

		return exists ? languageCodes.get(language) : "inv";
	}

	public static Mono<String> login(MagentoService service, String username, String apiKey)
	{
		LoginParam rq = new LoginParam();
		rq.setUsername(username);
		rq.setApiKey(apiKey);

		return Mono.<LoginResponseParam>create(sink -> {
			MagentoServiceCallbackHandler callback = new MagentoServiceCallbackHandler()
			{
				public void receiveResultlogin(greyjoy.travelsmart.adapter.sunbonoo.magento.LoginResponseParam result)
				{
					sink.success(result);
				}

				public void receiveErrorlogin(Exception e)
				{
					sink.error(e);
				}
			};

			try
			{
				service.startlogin(rq, callback);
			}
			catch (RemoteException e1)
			{
				e1.printStackTrace();
			}
		}).map(LoginResponseParam::getResult);
	}

	public Flux<CatalogProductReturnEntity> getProducts(int categoryID)
	{
		return getProductsAssignedToCategory(categoryID).flatMap(product -> {
			return getProductInfo(product.getSku());
		});
	}

	public Flux<CatalogProductReturnEntity> getAllProducts()
	{
		return getProductsAssignedToCategory(320).flatMap(product -> {
			return getProductInfo(product.getSku());
		});
	}
	
	public Mono<List<CatalogProductOptionGroupsEntity>> getOptionGroups(String sku)
	{
		return sessionCache.flatMap(sessionID -> {
			CatalogProductOptionGroupsRequestParam rq = new CatalogProductOptionGroupsRequestParam();
			rq.setSessionId(sessionID);
			rq.setProductId(sku);

			return Mono.<CatalogProductOptionGroupsEntity[]>create(sink -> {
				MagentoServiceCallbackHandler callback = new MagentoServiceCallbackHandler()
				{
					@Override
					public void receiveResultcatalogProductOptionGroups(CatalogProductOptionGroupsResponseParam result)
					{
						sink.success(result.getResult().getComplexObjectArray());
					}

					@Override
					public void receiveErrorcatalogProductOptionGroups(Exception e)
					{
						sink.error(e);
					}
				};

				try
				{
					service.startcatalogProductOptionGroups(rq, callback);
				}
				catch (RemoteException e1)
				{
					e1.printStackTrace();
				}
			});
		}).flatMapMany(Flux::fromArray).collectList();
	}

	public Mono<List<CatalogProductReturnEntity>> getBookableOptions(String sku)
	{
		return Mono.fromCallable(() -> {
			if (Config.CACHE_SUNBONOO_BOOKABLES)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					bookablesSearchStatement.setString(1, sku);

					ResultSet resultSet = bookablesSearchStatement.executeQuery();

					if (resultSet.next())
					{
						String xml = resultSet.getString(1);
						resultSet.close();
						return xml;
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				return null;
			}
		}).flatMap(xml -> {
			try
			{
				return Mono.just(CatalogProductLinkListResponseParam.Factory.parse(XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(xml))));
			}
			catch (XMLStreamException e)
			{
				e.printStackTrace();
			}
			catch (FactoryConfigurationError e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			return Mono.empty();
		}).switchIfEmpty(sessionCache.flatMap(sessionID -> {
			CatalogProductLinkListRequestParam rq = new CatalogProductLinkListRequestParam();
			rq.setSessionId(sessionID);
			rq.setProductId(sku);
			rq.setType("grouped");

			return Mono.<CatalogProductLinkListResponseParam>create(sink -> {
				MagentoServiceCallbackHandler callback = new MagentoServiceCallbackHandler()
				{
					@Override
					public void receiveResultcatalogProductLinkList(CatalogProductLinkListResponseParam result)
					{
						sink.success(result);
					}

					@Override
					public void receiveErrorcatalogProductInfo(Exception e)
					{
						sink.error(e);
					}
				};

				try
				{
					service.startcatalogProductLinkList(rq, callback);
				}
				catch (RemoteException e1)
				{
					e1.printStackTrace();
				}
			});
		})).doOnNext(cpr -> {
			if (Config.CACHE_SUNBONOO_BOOKABLES)
			{
				StringWriter writer = new StringWriter();
				try
				{
					XMLStreamWriter xmlWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(writer);
					cpr.serialize(QName.valueOf("Test"), xmlWriter);
					xmlWriter.flush();
				}
				catch (ADBException e)
				{
					e.printStackTrace();
				}
				catch (XMLStreamException e)
				{
					e.printStackTrace();
				}
				catch (FactoryConfigurationError e)
				{
					e.printStackTrace();
				}

				writer.flush();

				if (Config.CACHE_SUNBONOO_BOOKABLES)
				{
					synchronized (DatabaseHandler.LOCK)
					{
						try
						{
							bookablesSaveStatement.setString(1, sku);
							bookablesSaveStatement.setString(2, writer.toString());
							bookablesSaveStatement.executeUpdate();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}).map(resp -> resp.getResult().getComplexObjectArray()).flatMapMany(Flux::fromArray).flatMap(p -> getProductInfo(p.getSku())).collectList().onErrorReturn(Collections.emptyList());
	}

	public Mono<CatalogProductReturnEntity> getProductInfo(String sku)
	{
		return Mono.fromCallable(() -> {
			if (Config.CACHE_SUNBONOO_INFO)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					infoSearchStatement.setString(1, sku);

					ResultSet resultSet = infoSearchStatement.executeQuery();

					if (resultSet.next())
					{
						String xml = resultSet.getString(1);
						resultSet.close();
						return xml;
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				return null;
			}
		}).flatMap(xml -> {
			try
			{
				return Mono.just(CatalogProductReturnEntity.Factory.parse(XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(xml))));
			}
			catch (XMLStreamException e)
			{
				e.printStackTrace();
			}
			catch (FactoryConfigurationError e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			return Mono.empty();
		}).switchIfEmpty(sessionCache.flatMap(sessionID -> {
			CatalogProductInfoRequestParam rq = new CatalogProductInfoRequestParam();
			rq.setSessionId(sessionID);
			rq.setProductId(sku);

			return Mono.<CatalogProductReturnEntity>create(sink -> {
				MagentoServiceCallbackHandler callback = new MagentoServiceCallbackHandler()
				{
					@Override
					public void receiveResultcatalogProductInfo(CatalogProductInfoResponseParam result)
					{
						sink.success(result.getResult());
					}

					@Override
					public void receiveErrorcatalogProductInfo(Exception e)
					{
						sink.error(e);
					}
				};

				try
				{
					service.startcatalogProductInfo(rq, callback);
				}
				catch (RemoteException e1)
				{
					e1.printStackTrace();
				}
			}).doOnNext(cpr -> {
				if (Config.CACHE_SUNBONOO_INFO)
				{
					StringWriter writer = new StringWriter();
					try
					{
						XMLStreamWriter xmlWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(writer);
						cpr.serialize(QName.valueOf("Test"), xmlWriter);
						xmlWriter.flush();
					}
					catch (ADBException e)
					{
						e.printStackTrace();
					}
					catch (XMLStreamException e)
					{
						e.printStackTrace();
					}
					catch (FactoryConfigurationError e)
					{
						e.printStackTrace();
					}

					writer.flush();

					if (Config.CACHE_SUNBONOO_WEBSITE)
					{
						synchronized (DatabaseHandler.LOCK)
						{
							try
							{
								infoSaveStatement.setString(1, sku);
								infoSaveStatement.setString(2, writer.toString());
								infoSaveStatement.executeUpdate();
							}
							catch (SQLException e)
							{
								e.printStackTrace();
							}
						}
					}
				}
			});
		})).onErrorResume(t -> Mono.empty());
	}

	public Mono<String> getWebsiteData(String sku, String path)
	{
		return Mono.fromCallable(() -> {
			if (Config.CACHE_SUNBONOO_WEBSITE)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					websiteSearchStatement.setString(1, sku);

					try
					{
						ResultSet resultSet = websiteSearchStatement.executeQuery();

						if (resultSet.next())
						{
							String html = resultSet.getString(1);
							resultSet.close();
							return html;
						}
					}
					catch (SQLException e)
					{

					}

					return null;
				}
			}
			else
			{
				return null;
			}
		}).switchIfEmpty(webClient_site.get().uri("{path}", path).retrieve().bodyToMono(String.class).onErrorReturn("").doOnNext(html -> {
			if (Config.CACHE_SUNBONOO_WEBSITE)
			{
				synchronized (DatabaseHandler.LOCK)
				{
					try
					{
						websiteSaveStatement.setString(1, sku);
						websiteSaveStatement.setString(2, html);
						websiteSaveStatement.executeUpdate();
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		}));
	}

	private Flux<CatalogAssignedProduct> getProductsAssignedToCategory(int categoryID)
	{
		return sessionCache.flatMapMany(sessionID -> {
			CatalogCategoryAssignedProductsRequestParam productRequest = new CatalogCategoryAssignedProductsRequestParam();
			productRequest.setCategoryId(categoryID);
			productRequest.setSessionId(sessionID);

			return Mono.<CatalogAssignedProductArray>create(sink -> {

				MagentoServiceCallbackHandler callback = new MagentoServiceCallbackHandler()
				{
					@Override
					public void receiveResultcatalogCategoryAssignedProducts(CatalogCategoryAssignedProductsResponseParam result)
					{
						sink.success(result.getResult());
					}

					@Override
					public void receiveErrorcatalogCategoryAssignedProducts(Exception e)
					{
						sink.error(e);
					}
				};

				try
				{
					service.startcatalogCategoryAssignedProducts(productRequest, callback);
				}
				catch (RemoteException e1)
				{
					e1.printStackTrace();
				}
			}).flatMapMany(pa -> pa.isComplexObjectArraySpecified() ? Flux.fromArray(pa.getComplexObjectArray()) : Flux.empty());
		});
	}
}
