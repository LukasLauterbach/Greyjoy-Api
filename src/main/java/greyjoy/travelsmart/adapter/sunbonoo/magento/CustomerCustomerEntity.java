
/**
 * CustomerCustomerEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * CustomerCustomerEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class CustomerCustomerEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * customerCustomerEntity Namespace URI = urn:Magento Namespace Prefix = ns1
	 */


	/**
	 * field for Customer_id
	 */


	protected int localCustomer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_idTracker = false;

	public boolean isCustomer_idSpecified()
	{
		return localCustomer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getCustomer_id()
	{
		return localCustomer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_id
	 */
	public void setCustomer_id(int param)
	{

		// setting primitive attribute tracker to true
		localCustomer_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localCustomer_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Store_id
	 */


	protected int localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(int param)
	{

		// setting primitive attribute tracker to true
		localStore_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localStore_id = param;


	}


	/**
	 * field for Website_id
	 */


	protected int localWebsite_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWebsite_idTracker = false;

	public boolean isWebsite_idSpecified()
	{
		return localWebsite_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getWebsite_id()
	{
		return localWebsite_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Website_id
	 */
	public void setWebsite_id(int param)
	{

		// setting primitive attribute tracker to true
		localWebsite_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localWebsite_id = param;


	}


	/**
	 * field for Created_in
	 */


	protected java.lang.String localCreated_in;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_inTracker = false;

	public boolean isCreated_inSpecified()
	{
		return localCreated_inTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_in()
	{
		return localCreated_in;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_in
	 */
	public void setCreated_in(java.lang.String param)
	{
		localCreated_inTracker = param != null;

		this.localCreated_in = param;


	}


	/**
	 * field for Email
	 */


	protected java.lang.String localEmail;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEmailTracker = false;

	public boolean isEmailSpecified()
	{
		return localEmailTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEmail()
	{
		return localEmail;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Email
	 */
	public void setEmail(java.lang.String param)
	{
		localEmailTracker = param != null;

		this.localEmail = param;


	}


	/**
	 * field for Firstname
	 */


	protected java.lang.String localFirstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFirstnameTracker = false;

	public boolean isFirstnameSpecified()
	{
		return localFirstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFirstname()
	{
		return localFirstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Firstname
	 */
	public void setFirstname(java.lang.String param)
	{
		localFirstnameTracker = param != null;

		this.localFirstname = param;


	}


	/**
	 * field for Middlename
	 */


	protected java.lang.String localMiddlename;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localMiddlenameTracker = false;

	public boolean isMiddlenameSpecified()
	{
		return localMiddlenameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMiddlename()
	{
		return localMiddlename;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Middlename
	 */
	public void setMiddlename(java.lang.String param)
	{
		localMiddlenameTracker = param != null;

		this.localMiddlename = param;


	}


	/**
	 * field for Lastname
	 */


	protected java.lang.String localLastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localLastnameTracker = false;

	public boolean isLastnameSpecified()
	{
		return localLastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getLastname()
	{
		return localLastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Lastname
	 */
	public void setLastname(java.lang.String param)
	{
		localLastnameTracker = param != null;

		this.localLastname = param;


	}


	/**
	 * field for Group_id
	 */


	protected int localGroup_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGroup_idTracker = false;

	public boolean isGroup_idSpecified()
	{
		return localGroup_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getGroup_id()
	{
		return localGroup_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Group_id
	 */
	public void setGroup_id(int param)
	{

		// setting primitive attribute tracker to true
		localGroup_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localGroup_id = param;


	}


	/**
	 * field for Prefix
	 */


	protected java.lang.String localPrefix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPrefixTracker = false;

	public boolean isPrefixSpecified()
	{
		return localPrefixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrefix()
	{
		return localPrefix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Prefix
	 */
	public void setPrefix(java.lang.String param)
	{
		localPrefixTracker = param != null;

		this.localPrefix = param;


	}


	/**
	 * field for Suffix
	 */


	protected java.lang.String localSuffix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSuffixTracker = false;

	public boolean isSuffixSpecified()
	{
		return localSuffixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSuffix()
	{
		return localSuffix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Suffix
	 */
	public void setSuffix(java.lang.String param)
	{
		localSuffixTracker = param != null;

		this.localSuffix = param;


	}


	/**
	 * field for Dob
	 */


	protected java.lang.String localDob;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDobTracker = false;

	public boolean isDobSpecified()
	{
		return localDobTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDob()
	{
		return localDob;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Dob
	 */
	public void setDob(java.lang.String param)
	{
		localDobTracker = param != null;

		this.localDob = param;


	}


	/**
	 * field for Taxvat
	 */


	protected java.lang.String localTaxvat;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTaxvatTracker = false;

	public boolean isTaxvatSpecified()
	{
		return localTaxvatTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTaxvat()
	{
		return localTaxvat;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Taxvat
	 */
	public void setTaxvat(java.lang.String param)
	{
		localTaxvatTracker = param != null;

		this.localTaxvat = param;


	}


	/**
	 * field for Confirmation
	 */


	protected boolean localConfirmation;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localConfirmationTracker = false;

	public boolean isConfirmationSpecified()
	{
		return localConfirmationTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return boolean
	 */
	public boolean getConfirmation()
	{
		return localConfirmation;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Confirmation
	 */
	public void setConfirmation(boolean param)
	{

		// setting primitive attribute tracker to true
		localConfirmationTracker = true;

		this.localConfirmation = param;


	}


	/**
	 * field for Password_hash
	 */


	protected java.lang.String localPassword_hash;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPassword_hashTracker = false;

	public boolean isPassword_hashSpecified()
	{
		return localPassword_hashTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPassword_hash()
	{
		return localPassword_hash;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Password_hash
	 */
	public void setPassword_hash(java.lang.String param)
	{
		localPassword_hashTracker = param != null;

		this.localPassword_hash = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":customerCustomerEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "customerCustomerEntity", xmlWriter);
			}


		}
		if (localCustomer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_id", xmlWriter);

			if (localCustomer_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("customer_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomer_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);

			if (localStore_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStore_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localWebsite_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "website_id", xmlWriter);

			if (localWebsite_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("website_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWebsite_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_inTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_in", xmlWriter);


			if (localCreated_in == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_in cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_in);

			}

			xmlWriter.writeEndElement();
		}
		if (localEmailTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "email", xmlWriter);


			if (localEmail == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEmail);

			}

			xmlWriter.writeEndElement();
		}
		if (localFirstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "firstname", xmlWriter);


			if (localFirstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFirstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localMiddlenameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "middlename", xmlWriter);


			if (localMiddlename == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("middlename cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localMiddlename);

			}

			xmlWriter.writeEndElement();
		}
		if (localLastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "lastname", xmlWriter);


			if (localLastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localLastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localGroup_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "group_id", xmlWriter);

			if (localGroup_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("group_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGroup_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localPrefixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "prefix", xmlWriter);


			if (localPrefix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("prefix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrefix);

			}

			xmlWriter.writeEndElement();
		}
		if (localSuffixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "suffix", xmlWriter);


			if (localSuffix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSuffix);

			}

			xmlWriter.writeEndElement();
		}
		if (localDobTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "dob", xmlWriter);


			if (localDob == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("dob cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDob);

			}

			xmlWriter.writeEndElement();
		}
		if (localTaxvatTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "taxvat", xmlWriter);


			if (localTaxvat == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("taxvat cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTaxvat);

			}

			xmlWriter.writeEndElement();
		}
		if (localConfirmationTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "confirmation", xmlWriter);

			if (false)
			{

				throw new org.apache.axis2.databinding.ADBException("confirmation cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localConfirmation));
			}

			xmlWriter.writeEndElement();
		}
		if (localPassword_hashTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "password_hash", xmlWriter);


			if (localPassword_hash == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("password_hash cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPassword_hash);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static CustomerCustomerEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			CustomerCustomerEntity object = new CustomerCustomerEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"customerCustomerEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (CustomerCustomerEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setCustomer_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setStore_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "website_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "website_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWebsite_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setWebsite_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_in").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_in" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_in(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "email").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "email" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEmail(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFirstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "middlename").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "middlename" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setMiddlename(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setLastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "group_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "group_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGroup_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setGroup_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "prefix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "prefix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrefix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "suffix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "suffix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSuffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "dob").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "dob" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDob(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "taxvat").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "taxvat" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTaxvat(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "confirmation").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "confirmation" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setConfirmation(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "password_hash").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "password_hash" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPassword_hash(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

