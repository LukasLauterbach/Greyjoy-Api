
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;

/**
 * ExtensionMapper class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class ExtensionMapper
{

	public static java.lang.Object getTypeObject(java.lang.String namespaceURI, java.lang.String typeName, javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
	{


		if ("urn:Magento".equals(namespaceURI) && "catalogProductAttributeMediaTypeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypeEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "giftMessageEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerCustomerEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "directoryRegionEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerCustomerEntityToCreate".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerEntityToCreate.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartLicenseEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAttributeEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAttributeEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogCategoryEntityCreate".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryEntityCreate.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTagUpdateEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagUpdateEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartPaymentMethodEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartTotalsEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "orderItemIdQtyArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.OrderItemIdQtyArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfExistsFaltures".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfExistsFaltures.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAssignedProductArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAssignedProductArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderStatusHistoryEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "storeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.StoreEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "filters".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.Filters.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartAddressEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkAddSampleEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddSampleEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfCatalogCategoryEntitiesNoChildren".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfCatalogCategoryEntitiesNoChildren.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTierPriceEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTierPriceEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartProductEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartProductResponseEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductResponseEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfCatalogCategoryEntities".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfCatalogCategoryEntities.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "orderItemIdQty".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.OrderItemIdQty.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartPaymentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfString".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfString.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderPaymentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerAddressEntityItem".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressEntityItem.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTypeEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogCategoryInfo".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryInfo.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTierPriceEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTierPriceEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentCommentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfApiMethods".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfApiMethods.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "complexFilterArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ComplexFilterArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerCustomerEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTagAddEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagAddEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTagInfoEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagInfoEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerGroupEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartShippingMethodEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductImageEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductImageEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogInventoryStockItemUpdateEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemUpdateEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductAttributeSetEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentCommentEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerAddressEntityCreate".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressEntityCreate.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderListEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductCreateEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCreateEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerAddressEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductAttributeSetEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "giftMessageAssociativeProductsEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageAssociativeProductsEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentTrackEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkSampleEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkSampleEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartCustomerEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAttributeOptionEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAttributeOptionEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "associativeArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.AssociativeArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartLicenseEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "customerGroupEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceCommentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCommentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartInfoEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartInfoEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAttributeOptionEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAttributeOptionEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "ArrayOfApis".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ArrayOfApis.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductImageFileEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductImageFileEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "associativeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.AssociativeEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "directoryCountryEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductOptionGroupsEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "directoryCountryEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderAddressEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogInventoryStockItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductLinkAttributeEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributeEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "existsFaltureEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ExistsFaltureEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductAttributeMediaTypeEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypeEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTypeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoCommentEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoCommentEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "giftMessageResponseArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageResponseArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "apiMethodEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ApiMethodEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkFileInfoEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkFileInfoEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderStatusHistoryEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogCategoryTree".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTree.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "directoryRegionEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "apiEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ApiEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductOptionGroupsEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductLinkEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkFileInfoEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkFileInfoEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAttributeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAttributeEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductLinkAttributeEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributeEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "complexFilter".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ComplexFilter.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "giftMessageAssociativeProductsEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageAssociativeProductsEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "giftMessageResponse".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageResponse.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkSampleEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkSampleEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductImageEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductImageEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogCategoryEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartCustomerAddressEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartShippingMethodEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductLinkEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTagListEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartCustomerAddressEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceCommentEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCommentEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductRequestAttributes".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductRequestAttributes.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderListEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductAttributeMediaCreateEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCreateEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkFileEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkFileEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkListEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkListEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogCategoryEntityNoChildren".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryEntityNoChildren.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "storeEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.StoreEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartTotalsEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogAssignedProduct".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogAssignedProduct.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductDownloadableLinkAddEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductTagListEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartPaymentMethodResponseEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodResponseEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartItemEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderCreditmemoData".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogInventoryStockItemEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "shoppingCartProductEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderInvoiceEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceEntityArray.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "catalogProductReturnEntity".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductReturnEntity.Factory.parse(reader);


		}


		if ("urn:Magento".equals(namespaceURI) && "salesOrderShipmentTrackEntityArray".equals(typeName))
		{

			return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray.Factory.parse(reader);


		}


		throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
	}

}
