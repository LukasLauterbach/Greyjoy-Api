

/**
 * MagentoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:20 GMT)
 */

package greyjoy.travelsmart.adapter.sunbonoo.magento;

/*
 *  MagentoService java interface
 */

public interface MagentoService
{


	/**
	 * Auto generated method signature Retrieve hierarchical tree of categories.
	 * 
	 * @param catalogCategoryLevelRequestParam1
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryLevelResponseParam catalogCategoryLevel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryLevelRequestParam catalogCategoryLevelRequestParam1) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * hierarchical tree of categories.
	 * 
	 * @param catalogCategoryLevelRequestParam1
	 * 
	 */
	public void startcatalogCategoryLevel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryLevelRequestParam catalogCategoryLevelRequestParam1,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of stores
	 * 
	 * @param storeListRequestParam3
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.StoreListResponseParam storeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.StoreListRequestParam storeListRequestParam3) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * stores
	 * 
	 * @param storeListRequestParam3
	 * 
	 */
	public void startstoreList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.StoreListRequestParam storeListRequestParam3,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of allowed carriers for
	 * order
	 * 
	 * @param salesOrderShipmentGetCarriersRequestParam5
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentGetCarriersResponseParam salesOrderShipmentGetCarriers(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentGetCarriersRequestParam salesOrderShipmentGetCarriersRequestParam5) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of allowed carriers for order
	 * 
	 * @param salesOrderShipmentGetCarriersRequestParam5
	 * 
	 */
	public void startsalesOrderShipmentGetCarriers(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentGetCarriersRequestParam salesOrderShipmentGetCarriersRequestParam5,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove links and samples from
	 * downloadable product
	 * 
	 * @param catalogProductDownloadableLinkRemoveRequestParam7
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkRemoveResponseParam catalogProductDownloadableLinkRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkRemoveRequestParam catalogProductDownloadableLinkRemoveRequestParam7) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove links
	 * and samples from downloadable product
	 * 
	 * @param catalogProductDownloadableLinkRemoveRequestParam7
	 * 
	 */
	public void startcatalogProductDownloadableLinkRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkRemoveRequestParam catalogProductDownloadableLinkRemoveRequestParam7,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get list of available shipping methods
	 * 
	 * @param shoppingCartShippingListRequestParam9
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingListResponseParam shoppingCartShippingList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingListRequestParam shoppingCartShippingListRequestParam9) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get list of
	 * available shipping methods
	 * 
	 * @param shoppingCartShippingListRequestParam9
	 * 
	 */
	public void startshoppingCartShippingList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingListRequestParam shoppingCartShippingListRequestParam9,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product link type attributes
	 * 
	 * @param catalogProductLinkAttributesRequestParam11
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributesResponseParam catalogProductLinkAttributes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributesRequestParam catalogProductLinkAttributesRequestParam11) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product link type attributes
	 * 
	 * @param catalogProductLinkAttributesRequestParam11
	 * 
	 */
	public void startcatalogProductLinkAttributes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributesRequestParam catalogProductLinkAttributesRequestParam11,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product types
	 * 
	 * @param catalogProductTypeListRequestParam13
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeListResponseParam catalogProductTypeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeListRequestParam catalogProductTypeListRequestParam13) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product types
	 * 
	 * @param catalogProductTypeListRequestParam13
	 * 
	 */
	public void startcatalogProductTypeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeListRequestParam catalogProductTypeListRequestParam13,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove tracking number
	 * 
	 * @param salesOrderShipmentRemoveTrackRequestParam15
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentRemoveTrackResponseParam salesOrderShipmentRemoveTrack(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentRemoveTrackRequestParam salesOrderShipmentRemoveTrackRequestParam15) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * tracking number
	 * 
	 * @param salesOrderShipmentRemoveTrackRequestParam15
	 * 
	 */
	public void startsalesOrderShipmentRemoveTrack(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentRemoveTrackRequestParam salesOrderShipmentRemoveTrackRequestParam15,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Capture invoice
	 * 
	 * @param salesOrderInvoiceCaptureRequestParam17
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCaptureResponseParam salesOrderInvoiceCapture(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCaptureRequestParam salesOrderInvoiceCaptureRequestParam17) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Capture
	 * invoice
	 * 
	 * @param salesOrderInvoiceCaptureRequestParam17
	 * 
	 */
	public void startsalesOrderInvoiceCapture(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCaptureRequestParam salesOrderInvoiceCaptureRequestParam17,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set/Get current store view
	 * 
	 * @param catalogCategoryAttributeCurrentStoreRequestParam19
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeCurrentStoreResponseParam catalogCategoryAttributeCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeCurrentStoreRequestParam catalogCategoryAttributeCurrentStoreRequestParam19) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set/Get
	 * current store view
	 * 
	 * @param catalogCategoryAttributeCurrentStoreRequestParam19
	 * 
	 */
	public void startcatalogCategoryAttributeCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeCurrentStoreRequestParam catalogCategoryAttributeCurrentStoreRequestParam19,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create new product and return product id
	 * 
	 * @param catalogProductCreateRequestParam21
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCreateResponseParam catalogProductCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCreateRequestParam catalogProductCreateRequestParam21) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create new
	 * product and return product id
	 * 
	 * @param catalogProductCreateRequestParam21
	 * 
	 */
	public void startcatalogProductCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCreateRequestParam catalogProductCreateRequestParam21,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of orders by filters
	 * 
	 * @param salesOrderListRequestParam23
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListResponseParam salesOrderList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListRequestParam salesOrderListRequestParam23) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of orders by filters
	 * 
	 * @param salesOrderListRequestParam23
	 * 
	 */
	public void startsalesOrderList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListRequestParam salesOrderListRequestParam23,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Cancel creditmemo
	 * 
	 * @param salesOrderCreditmemoCancelRequestParam25
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCancelResponseParam salesOrderCreditmemoCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCancelRequestParam salesOrderCreditmemoCancelRequestParam25) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Cancel
	 * creditmemo
	 * 
	 * @param salesOrderCreditmemoCancelRequestParam25
	 * 
	 */
	public void startsalesOrderCreditmemoCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCancelRequestParam salesOrderCreditmemoCancelRequestParam25,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of available resources
	 * 
	 * @param resourcesRequestParam27
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ResourcesResponseParam resources(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ResourcesRequestParam resourcesRequestParam27) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * available resources
	 * 
	 * @param resourcesRequestParam27
	 * 
	 */
	public void startresources(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ResourcesRequestParam resourcesRequestParam27,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product tier prices
	 * 
	 * @param catalogProductAttributeTierPriceInfoRequestParam29
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceInfoResponseParam catalogProductAttributeTierPriceInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceInfoRequestParam catalogProductAttributeTierPriceInfoRequestParam29) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product tier prices
	 * 
	 * @param catalogProductAttributeTierPriceInfoRequestParam29
	 * 
	 */
	public void startcatalogProductAttributeTierPriceInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceInfoRequestParam catalogProductAttributeTierPriceInfoRequestParam29,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product tag info
	 * 
	 * @param catalogProductTagInfoRequestParam31
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagInfoResponseParam catalogProductTagInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagInfoRequestParam catalogProductTagInfoRequestParam31) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product tag info
	 * 
	 * @param catalogProductTagInfoRequestParam31
	 * 
	 */
	public void startcatalogProductTagInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagInfoRequestParam catalogProductTagInfoRequestParam31,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product tier prices
	 * 
	 * @param catalogProductAttributeTierPriceUpdateRequestParam33
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceUpdateResponseParam catalogProductAttributeTierPriceUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceUpdateRequestParam catalogProductAttributeTierPriceUpdateRequestParam33) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product tier prices
	 * 
	 * @param catalogProductAttributeTierPriceUpdateRequestParam33
	 * 
	 */
	public void startcatalogProductAttributeTierPriceUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceUpdateRequestParam catalogProductAttributeTierPriceUpdateRequestParam33,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Upload new product image
	 * 
	 * @param catalogProductAttributeMediaCreateRequestParam35
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCreateResponseParam catalogProductAttributeMediaCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCreateRequestParam catalogProductAttributeMediaCreateRequestParam35) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Upload new
	 * product image
	 * 
	 * @param catalogProductAttributeMediaCreateRequestParam35
	 * 
	 */
	public void startcatalogProductAttributeMediaCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCreateRequestParam catalogProductAttributeMediaCreateRequestParam35,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of regions in specified country
	 * 
	 * @param directoryRegionListRequestParam37
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionListResponseParam directoryRegionList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionListRequestParam directoryRegionListRequestParam37) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * regions in specified country
	 * 
	 * @param directoryRegionListRequestParam37
	 * 
	 */
	public void startdirectoryRegionList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionListRequestParam directoryRegionListRequestParam37,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature End web service session
	 * 
	 * @param endSessionParam39
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.EndSessionResponseParam endSession(

			greyjoy.travelsmart.adapter.sunbonoo.magento.EndSessionParam endSessionParam39) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations End web
	 * service session
	 * 
	 * @param endSessionParam39
	 * 
	 */
	public void startendSession(

			greyjoy.travelsmart.adapter.sunbonoo.magento.EndSessionParam endSessionParam39,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add comment to order
	 * 
	 * @param salesOrderAddCommentRequestParam41
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddCommentResponseParam salesOrderAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddCommentRequestParam salesOrderAddCommentRequestParam41) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add comment
	 * to order
	 * 
	 * @param salesOrderAddCommentRequestParam41
	 * 
	 */
	public void startsalesOrderAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddCommentRequestParam salesOrderAddCommentRequestParam41,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add product(s) to shopping cart
	 * 
	 * @param shoppingCartProductAddRequestParam43
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductAddResponseParam shoppingCartProductAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductAddRequestParam shoppingCartProductAddRequestParam43) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add
	 * product(s) to shopping cart
	 * 
	 * @param shoppingCartProductAddRequestParam43
	 * 
	 */
	public void startshoppingCartProductAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductAddRequestParam shoppingCartProductAddRequestParam43,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set payment method
	 * 
	 * @param shoppingCartPaymentMethodRequestParam45
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodResponseParam shoppingCartPaymentMethod(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodRequestParam shoppingCartPaymentMethodRequestParam45) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set payment
	 * method
	 * 
	 * @param shoppingCartPaymentMethodRequestParam45
	 * 
	 */
	public void startshoppingCartPaymentMethod(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodRequestParam shoppingCartPaymentMethodRequestParam45,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of tags by product
	 * 
	 * @param catalogProductTagListRequestParam47
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListResponseParam catalogProductTagList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListRequestParam catalogProductTagListRequestParam47) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of tags by product
	 * 
	 * @param catalogProductTagListRequestParam47
	 * 
	 */
	public void startcatalogProductTagList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListRequestParam catalogProductTagListRequestParam47,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set customer's addresses in shopping cart
	 * 
	 * @param shoppingCartCustomerAddressesRequestParam49
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressesResponseParam shoppingCartCustomerAddresses(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressesRequestParam shoppingCartCustomerAddressesRequestParam49) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set
	 * customer's addresses in shopping cart
	 * 
	 * @param shoppingCartCustomerAddressesRequestParam49
	 * 
	 */
	public void startshoppingCartCustomerAddresses(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressesRequestParam shoppingCartCustomerAddressesRequestParam49,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Store view info
	 * 
	 * @param storeInfoRequestParam51
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.StoreInfoResponseParam storeInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.StoreInfoRequestParam storeInfoRequestParam51) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Store view
	 * info
	 * 
	 * @param storeInfoRequestParam51
	 * 
	 */
	public void startstoreInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.StoreInfoRequestParam storeInfoRequestParam51,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product special price
	 * 
	 * @param catalogProductSetSpecialPriceRequestParam53
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductSetSpecialPriceResponseParam catalogProductSetSpecialPrice(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductSetSpecialPriceRequestParam catalogProductSetSpecialPriceRequestParam53) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product special price
	 * 
	 * @param catalogProductSetSpecialPriceRequestParam53
	 * 
	 */
	public void startcatalogProductSetSpecialPrice(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductSetSpecialPriceRequestParam catalogProductSetSpecialPriceRequestParam53,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get list of products in shopping cart
	 * 
	 * @param shoppingCartProductListRequestParam55
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductListResponseParam shoppingCartProductList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductListRequestParam shoppingCartProductListRequestParam55) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get list of
	 * products in shopping cart
	 * 
	 * @param shoppingCartProductListRequestParam55
	 * 
	 */
	public void startshoppingCartProductList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductListRequestParam shoppingCartProductListRequestParam55,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve order information
	 * 
	 * @param salesOrderInfoRequestParam57
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInfoResponseParam salesOrderInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInfoRequestParam salesOrderInfoRequestParam57) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * order information
	 * 
	 * @param salesOrderInfoRequestParam57
	 * 
	 */
	public void startsalesOrderInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInfoRequestParam salesOrderInfoRequestParam57,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product link
	 * 
	 * @param catalogProductLinkUpdateRequestParam59
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkUpdateResponseParam catalogProductLinkUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkUpdateRequestParam catalogProductLinkUpdateRequestParam59) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product link
	 * 
	 * @param catalogProductLinkUpdateRequestParam59
	 * 
	 */
	public void startcatalogProductLinkUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkUpdateRequestParam catalogProductLinkUpdateRequestParam59,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get product special price data
	 * 
	 * @param catalogProductGetSpecialPriceRequestParam61
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductGetSpecialPriceResponseParam catalogProductGetSpecialPrice(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductGetSpecialPriceRequestParam catalogProductGetSpecialPriceRequestParam61) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get product
	 * special price data
	 * 
	 * @param catalogProductGetSpecialPriceRequestParam61
	 * 
	 */
	public void startcatalogProductGetSpecialPrice(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductGetSpecialPriceRequestParam catalogProductGetSpecialPriceRequestParam61,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve attribute list
	 * 
	 * @param catalogProductAttributeListRequestParam63
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeListResponseParam catalogProductAttributeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeListRequestParam catalogProductAttributeListRequestParam63) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * attribute list
	 * 
	 * @param catalogProductAttributeListRequestParam63
	 * 
	 */
	public void startcatalogProductAttributeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeListRequestParam catalogProductAttributeListRequestParam63,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve invoice information
	 * 
	 * @param salesOrderInvoiceInfoRequestParam65
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceInfoResponseParam salesOrderInvoiceInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceInfoRequestParam salesOrderInvoiceInfoRequestParam65) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * invoice information
	 * 
	 * @param salesOrderInvoiceInfoRequestParam65
	 * 
	 */
	public void startsalesOrderInvoiceInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceInfoRequestParam salesOrderInvoiceInfoRequestParam65,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve information about shopping cart
	 * 
	 * @param shoppingCartInfoRequestParam67
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartInfoResponseParam shoppingCartInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartInfoRequestParam shoppingCartInfoRequestParam67) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * information about shopping cart
	 * 
	 * @param shoppingCartInfoRequestParam67
	 * 
	 */
	public void startshoppingCartInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartInfoRequestParam shoppingCartInfoRequestParam67,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Move product(s) to customer quote
	 * 
	 * @param shoppingCartProductMoveToCustomerQuoteRequestParam69
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductMoveToCustomerQuoteResponseParam shoppingCartProductMoveToCustomerQuote(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductMoveToCustomerQuoteRequestParam shoppingCartProductMoveToCustomerQuoteRequestParam69) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Move
	 * product(s) to customer quote
	 * 
	 * @param shoppingCartProductMoveToCustomerQuoteRequestParam69
	 * 
	 */
	public void startshoppingCartProductMoveToCustomerQuote(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductMoveToCustomerQuoteRequestParam shoppingCartProductMoveToCustomerQuoteRequestParam69,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve hierarchical tree of categories.
	 * 
	 * @param catalogCategoryInfoRequestParam71
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryInfoResponseParam catalogCategoryInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryInfoRequestParam catalogCategoryInfoRequestParam71) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * hierarchical tree of categories.
	 * 
	 * @param catalogCategoryInfoRequestParam71
	 * 
	 */
	public void startcatalogCategoryInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryInfoRequestParam catalogCategoryInfoRequestParam71,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Assign product to category
	 * 
	 * @param catalogCategoryAssignProductRequestParam73
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignProductResponseParam catalogCategoryAssignProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignProductRequestParam catalogCategoryAssignProductRequestParam73) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Assign
	 * product to category
	 * 
	 * @param catalogCategoryAssignProductRequestParam73
	 * 
	 */
	public void startcatalogCategoryAssignProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignProductRequestParam catalogCategoryAssignProductRequestParam73,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set/Get current store view
	 * 
	 * @param catalogProductAttributeCurrentStoreRequestParam75
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeCurrentStoreResponseParam catalogProductAttributeCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeCurrentStoreRequestParam catalogProductAttributeCurrentStoreRequestParam75) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set/Get
	 * current store view
	 * 
	 * @param catalogProductAttributeCurrentStoreRequestParam75
	 * 
	 */
	public void startcatalogProductAttributeCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeCurrentStoreRequestParam catalogProductAttributeCurrentStoreRequestParam75,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set/Get current store view
	 * 
	 * @param catalogProductAttributeMediaCurrentStoreRequestParam77
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCurrentStoreResponseParam catalogProductAttributeMediaCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCurrentStoreRequestParam catalogProductAttributeMediaCurrentStoreRequestParam77) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set/Get
	 * current store view
	 * 
	 * @param catalogProductAttributeMediaCurrentStoreRequestParam77
	 * 
	 */
	public void startcatalogProductAttributeMediaCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCurrentStoreRequestParam catalogProductAttributeMediaCurrentStoreRequestParam77,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product
	 * 
	 * @param catalogProductUpdateRequestParam79
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductUpdateResponseParam catalogProductUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductUpdateRequestParam catalogProductUpdateRequestParam79) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product
	 * 
	 * @param catalogProductUpdateRequestParam79
	 * 
	 */
	public void startcatalogProductUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductUpdateRequestParam catalogProductUpdateRequestParam79,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product image list
	 * 
	 * @param catalogProductAttributeMediaListRequestParam81
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaListResponseParam catalogProductAttributeMediaList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaListRequestParam catalogProductAttributeMediaListRequestParam81) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product image list
	 * 
	 * @param catalogProductAttributeMediaListRequestParam81
	 * 
	 */
	public void startcatalogProductAttributeMediaList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaListRequestParam catalogProductAttributeMediaListRequestParam81,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Start web service session
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.StartSessionResponseParam startSession(

	) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Start web
	 * service session
	 */
	public void startstartSession(



			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of shipments by filters
	 * 
	 * @param salesOrderShipmentListRequestParam85
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentListResponseParam salesOrderShipmentList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentListRequestParam salesOrderShipmentListRequestParam85) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of shipments by filters
	 * 
	 * @param salesOrderShipmentListRequestParam85
	 * 
	 */
	public void startsalesOrderShipmentList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentListRequestParam salesOrderShipmentListRequestParam85,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Assign product link
	 * 
	 * @param catalogProductLinkAssignRequestParam87
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAssignResponseParam catalogProductLinkAssign(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAssignRequestParam catalogProductLinkAssignRequestParam87) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Assign
	 * product link
	 * 
	 * @param catalogProductLinkAssignRequestParam87
	 * 
	 */
	public void startcatalogProductLinkAssign(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAssignRequestParam catalogProductLinkAssignRequestParam87,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of invoices by filters
	 * 
	 * @param salesOrderInvoiceListRequestParam89
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceListResponseParam salesOrderInvoiceList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceListRequestParam salesOrderInvoiceListRequestParam89) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of invoices by filters
	 * 
	 * @param salesOrderInvoiceListRequestParam89
	 * 
	 */
	public void startsalesOrderInvoiceList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceListRequestParam salesOrderInvoiceListRequestParam89,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve linked products
	 * 
	 * @param catalogProductLinkListRequestParam91
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListResponseParam catalogProductLinkList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListRequestParam catalogProductLinkListRequestParam91) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * linked products
	 * 
	 * @param catalogProductLinkListRequestParam91
	 * 
	 */
	public void startcatalogProductLinkList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListRequestParam catalogProductLinkListRequestParam91,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create new invoice for order
	 * 
	 * @param salesOrderInvoiceCreateRequestParam93
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCreateResponseParam salesOrderInvoiceCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCreateRequestParam salesOrderInvoiceCreateRequestParam93) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create new
	 * invoice for order
	 * 
	 * @param salesOrderInvoiceCreateRequestParam93
	 * 
	 */
	public void startsalesOrderInvoiceCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCreateRequestParam salesOrderInvoiceCreateRequestParam93,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve customer groups
	 * 
	 * @param customerGroupListRequestParam95
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupListResponseParam customerGroupList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupListRequestParam customerGroupListRequestParam95) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * customer groups
	 * 
	 * @param customerGroupListRequestParam95
	 * 
	 */
	public void startcustomerGroupList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupListRequestParam customerGroupListRequestParam95,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product attribute sets
	 * 
	 * @param catalogProductAttributeSetListRequestParam97
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetListResponseParam catalogProductAttributeSetList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetListRequestParam catalogProductAttributeSetListRequestParam97) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product attribute sets
	 * 
	 * @param catalogProductAttributeSetListRequestParam97
	 * 
	 */
	public void startcatalogProductAttributeSetList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetListRequestParam catalogProductAttributeSetListRequestParam97,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Move category in tree
	 * 
	 * @param catalogCategoryMoveRequestParam99
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryMoveResponseParam catalogCategoryMove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryMoveRequestParam catalogCategoryMoveRequestParam99) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Move
	 * category in tree
	 * 
	 * @param catalogCategoryMoveRequestParam99
	 * 
	 */
	public void startcatalogCategoryMove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryMoveRequestParam catalogCategoryMoveRequestParam99,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update category
	 * 
	 * @param catalogCategoryUpdateRequestParam101
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateResponseParam catalogCategoryUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateRequestParam catalogCategoryUpdateRequestParam101) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * category
	 * 
	 * @param catalogCategoryUpdateRequestParam101
	 * 
	 */
	public void startcatalogCategoryUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateRequestParam catalogCategoryUpdateRequestParam101,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve category attributes
	 * 
	 * @param catalogCategoryAttributeListRequestParam103
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeListResponseParam catalogCategoryAttributeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeListRequestParam catalogCategoryAttributeListRequestParam103) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * category attributes
	 * 
	 * @param catalogCategoryAttributeListRequestParam103
	 * 
	 */
	public void startcatalogCategoryAttributeList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeListRequestParam catalogCategoryAttributeListRequestParam103,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product(s) quantities in shopping
	 * cart
	 * 
	 * @param shoppingCartProductUpdateRequestParam105
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductUpdateResponseParam shoppingCartProductUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductUpdateRequestParam shoppingCartProductUpdateRequestParam105) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product(s) quantities in shopping cart
	 * 
	 * @param shoppingCartProductUpdateRequestParam105
	 * 
	 */
	public void startshoppingCartProductUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductUpdateRequestParam shoppingCartProductUpdateRequestParam105,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Void invoice
	 * 
	 * @param salesOrderInvoiceVoidRequestParam107
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceVoidResponseParam salesOrderInvoiceVoid(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceVoidRequestParam salesOrderInvoiceVoidRequestParam107) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Void invoice
	 * 
	 * @param salesOrderInvoiceVoidRequestParam107
	 * 
	 */
	public void startsalesOrderInvoiceVoid(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceVoidRequestParam salesOrderInvoiceVoidRequestParam107,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve customer address data
	 * 
	 * @param customerAddressInfoRequestParam109
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressInfoResponseParam customerAddressInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressInfoRequestParam customerAddressInfoRequestParam109) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * customer address data
	 * 
	 * @param customerAddressInfoRequestParam109
	 * 
	 */
	public void startcustomerAddressInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressInfoRequestParam customerAddressInfoRequestParam109,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create new category and return its id.
	 * 
	 * @param catalogCategoryCreateRequestParam111
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCreateResponseParam catalogCategoryCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCreateRequestParam catalogCategoryCreateRequestParam111) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create new
	 * category and return its id.
	 * 
	 * @param catalogCategoryCreateRequestParam111
	 * 
	 */
	public void startcatalogCategoryCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCreateRequestParam catalogCategoryCreateRequestParam111,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product image data
	 * 
	 * @param catalogProductAttributeMediaInfoRequestParam113
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaInfoResponseParam catalogProductAttributeMediaInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaInfoRequestParam catalogProductAttributeMediaInfoRequestParam113) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product image data
	 * 
	 * @param catalogProductAttributeMediaInfoRequestParam113
	 * 
	 */
	public void startcatalogProductAttributeMediaInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaInfoRequestParam catalogProductAttributeMediaInfoRequestParam113,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update customer data
	 * 
	 * @param customerCustomerUpdateRequestParam115
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerUpdateResponseParam customerCustomerUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerUpdateRequestParam customerCustomerUpdateRequestParam115) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * customer data
	 * 
	 * @param customerCustomerUpdateRequestParam115
	 * 
	 */
	public void startcustomerCustomerUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerUpdateRequestParam customerCustomerUpdateRequestParam115,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create shopping cart
	 * 
	 * @param shoppingCartCreateRequestParam117
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCreateResponseParam shoppingCartCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCreateRequestParam shoppingCartCreateRequestParam117) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create
	 * shopping cart
	 * 
	 * @param shoppingCartCreateRequestParam117
	 * 
	 */
	public void startshoppingCartCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCreateRequestParam shoppingCartCreateRequestParam117,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set a gift message to the cart
	 * 
	 * @param giftMessageForQuoteRequestParam119
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteResponseParam giftMessageSetForQuote(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteRequestParam giftMessageForQuoteRequestParam119) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set a gift
	 * message to the cart
	 * 
	 * @param giftMessageForQuoteRequestParam119
	 * 
	 */
	public void startgiftMessageSetForQuote(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteRequestParam giftMessageForQuoteRequestParam119,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove product(s) from shopping cart
	 * 
	 * @param shoppingCartProductRemoveRequestParam121
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductRemoveResponseParam shoppingCartProductRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductRemoveRequestParam shoppingCartProductRemoveRequestParam121) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * product(s) from shopping cart
	 * 
	 * @param shoppingCartProductRemoveRequestParam121
	 * 
	 */
	public void startshoppingCartProductRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductRemoveRequestParam shoppingCartProductRemoveRequestParam121,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Login user and retrive session id
	 * 
	 * @param loginParam123
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.LoginResponseParam login(

			greyjoy.travelsmart.adapter.sunbonoo.magento.LoginParam loginParam123) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Login user
	 * and retrive session id
	 * 
	 * @param loginParam123
	 * 
	 */
	public void startlogin(

			greyjoy.travelsmart.adapter.sunbonoo.magento.LoginParam loginParam123,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve attribute options
	 * 
	 * @param catalogCategoryAttributeOptionsRequestParam125
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeOptionsResponseParam catalogCategoryAttributeOptions(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeOptionsRequestParam catalogCategoryAttributeOptionsRequestParam125) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * attribute options
	 * 
	 * @param catalogCategoryAttributeOptionsRequestParam125
	 * 
	 */
	public void startcatalogCategoryAttributeOptions(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeOptionsRequestParam catalogCategoryAttributeOptionsRequestParam125,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve creditmemo information
	 * 
	 * @param salesOrderCreditmemoInfoRequestParam127
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoInfoResponseParam salesOrderCreditmemoInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoInfoRequestParam salesOrderCreditmemoInfoRequestParam127) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * creditmemo information
	 * 
	 * @param salesOrderCreditmemoInfoRequestParam127
	 * 
	 */
	public void startsalesOrderCreditmemoInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoInfoRequestParam salesOrderCreditmemoInfoRequestParam127,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve customer data
	 * 
	 * @param customerCustomerInfoRequestParam129
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerInfoResponseParam customerCustomerInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerInfoRequestParam customerCustomerInfoRequestParam129) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * customer data
	 * 
	 * @param customerCustomerInfoRequestParam129
	 * 
	 */
	public void startcustomerCustomerInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerInfoRequestParam customerCustomerInfoRequestParam129,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create new creditmemo for order
	 * 
	 * @param salesOrderCreditmemoCreateRequestParam131
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCreateResponseParam salesOrderCreditmemoCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCreateRequestParam salesOrderCreditmemoCreateRequestParam131) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create new
	 * creditmemo for order
	 * 
	 * @param salesOrderCreditmemoCreateRequestParam131
	 * 
	 */
	public void startsalesOrderCreditmemoCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCreateRequestParam salesOrderCreditmemoCreateRequestParam131,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Cancel order
	 * 
	 * @param salesOrderCancelRequestParam133
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCancelResponseParam salesOrderCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCancelRequestParam salesOrderCancelRequestParam133) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Cancel order
	 * 
	 * @param salesOrderCancelRequestParam133
	 * 
	 */
	public void startsalesOrderCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCancelRequestParam salesOrderCancelRequestParam133,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove coupon code from shopping cart
	 * 
	 * @param shoppingCartCouponRemoveRequestParam135
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponRemoveResponseParam shoppingCartCouponRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponRemoveRequestParam shoppingCartCouponRemoveRequestParam135) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * coupon code from shopping cart
	 * 
	 * @param shoppingCartCouponRemoveRequestParam135
	 * 
	 */
	public void startshoppingCartCouponRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponRemoveRequestParam shoppingCartCouponRemoveRequestParam135,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set shipping method
	 * 
	 * @param shoppingCartShippingMethodRequestParam137
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodResponseParam shoppingCartShippingMethod(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodRequestParam shoppingCartShippingMethodRequestParam137) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set shipping
	 * method
	 * 
	 * @param shoppingCartShippingMethodRequestParam137
	 * 
	 */
	public void startshoppingCartShippingMethod(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodRequestParam shoppingCartShippingMethodRequestParam137,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add new tracking number
	 * 
	 * @param salesOrderShipmentAddTrackRequestParam139
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddTrackResponseParam salesOrderShipmentAddTrack(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddTrackRequestParam salesOrderShipmentAddTrackRequestParam139) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add new
	 * tracking number
	 * 
	 * @param salesOrderShipmentAddTrackRequestParam139
	 * 
	 */
	public void startsalesOrderShipmentAddTrack(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddTrackRequestParam salesOrderShipmentAddTrackRequestParam139,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product tag
	 * 
	 * @param catalogProductTagUpdateRequestParam141
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagUpdateResponseParam catalogProductTagUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagUpdateRequestParam catalogProductTagUpdateRequestParam141) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product tag
	 * 
	 * @param catalogProductTagUpdateRequestParam141
	 * 
	 */
	public void startcatalogProductTagUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagUpdateRequestParam catalogProductTagUpdateRequestParam141,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add links to downloadable product
	 * 
	 * @param catalogProductDownloadableLinkAddRequestParam143
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddResponseParam catalogProductDownloadableLinkAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddRequestParam catalogProductDownloadableLinkAddRequestParam143) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add links to
	 * downloadable product
	 * 
	 * @param catalogProductDownloadableLinkAddRequestParam143
	 * 
	 */
	public void startcatalogProductDownloadableLinkAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddRequestParam catalogProductDownloadableLinkAddRequestParam143,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of resource faults
	 * 
	 * @param resourceFaultsParam145
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ResourceFaultsResponseParam resourceFaults(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ResourceFaultsParam resourceFaultsParam145) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * resource faults
	 * 
	 * @param resourceFaultsParam145
	 * 
	 */
	public void startresourceFaults(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ResourceFaultsParam resourceFaultsParam145,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve customer addresses
	 * 
	 * @param customerAddressListRequestParam147
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressListResponseParam customerAddressList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressListRequestParam customerAddressListRequestParam147) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * customer addresses
	 * 
	 * @param customerAddressListRequestParam147
	 * 
	 */
	public void startcustomerAddressList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressListRequestParam customerAddressListRequestParam147,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product stock data
	 * 
	 * @param catalogInventoryStockItemUpdateRequestParam149
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemUpdateResponseParam catalogInventoryStockItemUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemUpdateRequestParam catalogInventoryStockItemUpdateRequestParam149) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product stock data
	 * 
	 * @param catalogInventoryStockItemUpdateRequestParam149
	 * 
	 */
	public void startcatalogInventoryStockItemUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemUpdateRequestParam catalogInventoryStockItemUpdateRequestParam149,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get list of available payment methods
	 * 
	 * @param shoppingCartPaymentListRequestParam151
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentListResponseParam shoppingCartPaymentList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentListRequestParam shoppingCartPaymentListRequestParam151) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get list of
	 * available payment methods
	 * 
	 * @param shoppingCartPaymentListRequestParam151
	 * 
	 */
	public void startshoppingCartPaymentList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentListRequestParam shoppingCartPaymentListRequestParam151,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add new comment to creditmemo
	 * 
	 * @param salesOrderCreditmemoAddCommentRequestParam153
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoAddCommentResponseParam salesOrderCreditmemoAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoAddCommentRequestParam salesOrderCreditmemoAddCommentRequestParam153) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add new
	 * comment to creditmemo
	 * 
	 * @param salesOrderCreditmemoAddCommentRequestParam153
	 * 
	 */
	public void startsalesOrderCreditmemoAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoAddCommentRequestParam salesOrderCreditmemoAddCommentRequestParam153,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove product tag
	 * 
	 * @param catalogProductTagRemoveRequestParam155
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagRemoveResponseParam catalogProductTagRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagRemoveRequestParam catalogProductTagRemoveRequestParam155) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * product tag
	 * 
	 * @param catalogProductTagRemoveRequestParam155
	 * 
	 */
	public void startcatalogProductTagRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagRemoveRequestParam catalogProductTagRemoveRequestParam155,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve shipment information
	 * 
	 * @param salesOrderShipmentInfoRequestParam157
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentInfoResponseParam salesOrderShipmentInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentInfoRequestParam salesOrderShipmentInfoRequestParam157) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * shipment information
	 * 
	 * @param salesOrderShipmentInfoRequestParam157
	 * 
	 */
	public void startsalesOrderShipmentInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentInfoRequestParam salesOrderShipmentInfoRequestParam157,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Delete category
	 * 
	 * @param catalogCategoryDeleteRequestParam159
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryDeleteResponseParam catalogCategoryDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryDeleteRequestParam catalogCategoryDeleteRequestParam159) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Delete
	 * category
	 * 
	 * @param catalogCategoryDeleteRequestParam159
	 * 
	 */
	public void startcatalogCategoryDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryDeleteRequestParam catalogCategoryDeleteRequestParam159,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve attribute options
	 * 
	 * @param catalogProductAttributeOptionsRequestParam161
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeOptionsResponseParam catalogProductAttributeOptions(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeOptionsRequestParam catalogProductAttributeOptionsRequestParam161) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * attribute options
	 * 
	 * @param catalogProductAttributeOptionsRequestParam161
	 * 
	 */
	public void startcatalogProductAttributeOptions(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeOptionsRequestParam catalogProductAttributeOptionsRequestParam161,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Delete customer address
	 * 
	 * @param customerAddressDeleteRequestParam163
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressDeleteResponseParam customerAddressDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressDeleteRequestParam customerAddressDeleteRequestParam163) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Delete
	 * customer address
	 * 
	 * @param customerAddressDeleteRequestParam163
	 * 
	 */
	public void startcustomerAddressDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressDeleteRequestParam customerAddressDeleteRequestParam163,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add coupon code for shopping cart
	 * 
	 * @param shoppingCartCouponAddRequestParam165
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponAddResponseParam shoppingCartCouponAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponAddRequestParam shoppingCartCouponAddRequestParam165) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add coupon
	 * code for shopping cart
	 * 
	 * @param shoppingCartCouponAddRequestParam165
	 * 
	 */
	public void startshoppingCartCouponAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponAddRequestParam shoppingCartCouponAddRequestParam165,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of countries
	 * 
	 * @param directoryCountryListRequestParam167
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryListResponseParam directoryCountryList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryListRequestParam directoryCountryListRequestParam167) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * countries
	 * 
	 * @param directoryCountryListRequestParam167
	 * 
	 */
	public void startdirectoryCountryList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryListRequestParam directoryCountryListRequestParam167,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set_Get current store view
	 * 
	 * @param catalogCategoryCurrentStoreRequestParam169
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCurrentStoreResponseParam catalogCategoryCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCurrentStoreRequestParam catalogCategoryCurrentStoreRequestParam169) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set_Get
	 * current store view
	 * 
	 * @param catalogCategoryCurrentStoreRequestParam169
	 * 
	 */
	public void startcatalogCategoryCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCurrentStoreRequestParam catalogCategoryCurrentStoreRequestParam169,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update assigned product
	 * 
	 * @param catalogCategoryUpdateProductRequestParam171
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateProductResponseParam catalogCategoryUpdateProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateProductRequestParam catalogCategoryUpdateProductRequestParam171) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * assigned product
	 * 
	 * @param catalogCategoryUpdateProductRequestParam171
	 * 
	 */
	public void startcatalogCategoryUpdateProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateProductRequestParam catalogCategoryUpdateProductRequestParam171,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature List of global faults
	 * 
	 * @param globalFaultsParam173
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.GlobalFaultsResponseParam globalFaults(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GlobalFaultsParam globalFaultsParam173) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations List of
	 * global faults
	 * 
	 * @param globalFaultsParam173
	 * 
	 */
	public void startglobalFaults(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GlobalFaultsParam globalFaultsParam173,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create an order from shopping cart
	 * 
	 * @param shoppingCartOrderRequestParam175
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartOrderResponseParam shoppingCartOrder(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartOrderRequestParam shoppingCartOrderRequestParam175) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create an
	 * order from shopping cart
	 * 
	 * @param shoppingCartOrderRequestParam175
	 * 
	 */
	public void startshoppingCartOrder(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartOrderRequestParam shoppingCartOrderRequestParam175,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Delete customer
	 * 
	 * @param customerCustomerDeleteRequestParam177
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerDeleteResponseParam customerCustomerDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerDeleteRequestParam customerCustomerDeleteRequestParam177) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Delete
	 * customer
	 * 
	 * @param customerCustomerDeleteRequestParam177
	 * 
	 */
	public void startcustomerCustomerDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerDeleteRequestParam customerCustomerDeleteRequestParam177,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve hierarchical tree of categories.
	 * 
	 * @param catalogCategoryTreeRequestParam179
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeResponseParam catalogCategoryTree(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeRequestParam catalogCategoryTreeRequestParam179) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * hierarchical tree of categories.
	 * 
	 * @param catalogCategoryTreeRequestParam179
	 * 
	 */
	public void startcatalogCategoryTree(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeRequestParam catalogCategoryTreeRequestParam179,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove product image
	 * 
	 * @param catalogProductAttributeMediaRemoveRequestParam181
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaRemoveResponseParam catalogProductAttributeMediaRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaRemoveRequestParam catalogProductAttributeMediaRemoveRequestParam181) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * product image
	 * 
	 * @param catalogProductAttributeMediaRemoveRequestParam181
	 * 
	 */
	public void startcatalogProductAttributeMediaRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaRemoveRequestParam catalogProductAttributeMediaRemoveRequestParam181,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Unhold order
	 * 
	 * @param salesOrderUnholdRequestParam183
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderUnholdResponseParam salesOrderUnhold(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderUnholdRequestParam salesOrderUnholdRequestParam183) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Unhold order
	 * 
	 * @param salesOrderUnholdRequestParam183
	 * 
	 */
	public void startsalesOrderUnhold(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderUnholdRequestParam salesOrderUnholdRequestParam183,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Setting a gift messages to the quote item
	 * 
	 * @param giftMessageForQuoteItemRequestParam185
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteItemResponseParam giftMessageSetForQuoteItem(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteItemRequestParam giftMessageForQuoteItemRequestParam185) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Setting a
	 * gift messages to the quote item
	 * 
	 * @param giftMessageForQuoteItemRequestParam185
	 * 
	 */
	public void startgiftMessageSetForQuoteItem(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteItemRequestParam giftMessageForQuoteItemRequestParam185,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Call api functionality
	 * 
	 * @param callParam187
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CallResponseParam call(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CallParam callParam187) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Call api
	 * functionality
	 * 
	 * @param callParam187
	 * 
	 */
	public void startcall(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CallParam callParam187,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product image types
	 * 
	 * @param catalogProductAttributeMediaTypesRequestParam189
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypesResponseParam catalogProductAttributeMediaTypes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypesRequestParam catalogProductAttributeMediaTypesRequestParam189) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product image types
	 * 
	 * @param catalogProductAttributeMediaTypesRequestParam189
	 * 
	 */
	public void startcatalogProductAttributeMediaTypes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypesRequestParam catalogProductAttributeMediaTypesRequestParam189,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update product image
	 * 
	 * @param catalogProductAttributeMediaUpdateRequestParam191
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaUpdateResponseParam catalogProductAttributeMediaUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaUpdateRequestParam catalogProductAttributeMediaUpdateRequestParam191) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * product image
	 * 
	 * @param catalogProductAttributeMediaUpdateRequestParam191
	 * 
	 */
	public void startcatalogProductAttributeMediaUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaUpdateRequestParam catalogProductAttributeMediaUpdateRequestParam191,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve stock data by product ids
	 * 
	 * @param catalogInventoryStockItemListRequestParam193
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemListResponseParam catalogInventoryStockItemList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemListRequestParam catalogInventoryStockItemListRequestParam193) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * stock data by product ids
	 * 
	 * @param catalogInventoryStockItemListRequestParam193
	 * 
	 */
	public void startcatalogInventoryStockItemList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemListRequestParam catalogInventoryStockItemListRequestParam193,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve customers
	 * 
	 * @param customerCustomerListRequestParam195
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerListResponseParam customerCustomerList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerListRequestParam customerCustomerListRequestParam195) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * customers
	 * 
	 * @param customerCustomerListRequestParam195
	 * 
	 */
	public void startcustomerCustomerList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerListRequestParam customerCustomerListRequestParam195,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove product link
	 * 
	 * @param catalogProductLinkRemoveRequestParam197
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkRemoveResponseParam catalogProductLinkRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkRemoveRequestParam catalogProductLinkRemoveRequestParam197) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * product link
	 * 
	 * @param catalogProductLinkRemoveRequestParam197
	 * 
	 */
	public void startcatalogProductLinkRemove(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkRemoveRequestParam catalogProductLinkRemoveRequestParam197,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve products list by filters
	 * 
	 * @param catalogProductListRequestParam199
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductListResponseParam catalogProductList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductListRequestParam catalogProductListRequestParam199) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * products list by filters
	 * 
	 * @param catalogProductListRequestParam199
	 * 
	 */
	public void startcatalogProductList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductListRequestParam catalogProductListRequestParam199,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of links and samples for
	 * downloadable product
	 * 
	 * @param catalogProductDownloadableLinkListRequestParam201
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkListResponseParam catalogProductDownloadableLinkList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkListRequestParam catalogProductDownloadableLinkListRequestParam201) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of links and samples for downloadable product
	 * 
	 * @param catalogProductDownloadableLinkListRequestParam201
	 * 
	 */
	public void startcatalogProductDownloadableLinkList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkListRequestParam catalogProductDownloadableLinkListRequestParam201,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Delete product
	 * 
	 * @param catalogProductDeleteRequestParam203
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDeleteResponseParam catalogProductDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDeleteRequestParam catalogProductDeleteRequestParam203) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Delete
	 * product
	 * 
	 * @param catalogProductDeleteRequestParam203
	 * 
	 */
	public void startcatalogProductDelete(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDeleteRequestParam catalogProductDeleteRequestParam203,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Hold order
	 * 
	 * @param salesOrderHoldRequestParam205
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderHoldResponseParam salesOrderHold(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderHoldRequestParam salesOrderHoldRequestParam205) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Hold order
	 * 
	 * @param salesOrderHoldRequestParam205
	 * 
	 */
	public void startsalesOrderHold(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderHoldRequestParam salesOrderHoldRequestParam205,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Update customer address data
	 * 
	 * @param customerAddressUpdateRequestParam207
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressUpdateResponseParam customerAddressUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressUpdateRequestParam customerAddressUpdateRequestParam207) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Update
	 * customer address data
	 * 
	 * @param customerAddressUpdateRequestParam207
	 * 
	 */
	public void startcustomerAddressUpdate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressUpdateRequestParam customerAddressUpdateRequestParam207,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Setting a gift messages to the quote
	 * items by products
	 * 
	 * @param giftMessageForQuoteProductRequestParam209
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteProductResponseParam giftMessageSetForQuoteProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteProductRequestParam giftMessageForQuoteProductRequestParam209) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Setting a
	 * gift messages to the quote items by products
	 * 
	 * @param giftMessageForQuoteProductRequestParam209
	 * 
	 */
	public void startgiftMessageSetForQuoteProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteProductRequestParam giftMessageForQuoteProductRequestParam209,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add new comment to shipment
	 * 
	 * @param salesOrderInvoiceAddCommentRequestParam211
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceAddCommentResponseParam salesOrderInvoiceAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceAddCommentRequestParam salesOrderInvoiceAddCommentRequestParam211) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add new
	 * comment to shipment
	 * 
	 * @param salesOrderInvoiceAddCommentRequestParam211
	 * 
	 */
	public void startsalesOrderInvoiceAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceAddCommentRequestParam salesOrderInvoiceAddCommentRequestParam211,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Remove product assignment from category
	 * 
	 * @param catalogCategoryRemoveProductRequestParam213
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryRemoveProductResponseParam catalogCategoryRemoveProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryRemoveProductRequestParam catalogCategoryRemoveProductRequestParam213) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Remove
	 * product assignment from category
	 * 
	 * @param catalogCategoryRemoveProductRequestParam213
	 * 
	 */
	public void startcatalogCategoryRemoveProduct(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryRemoveProductRequestParam catalogCategoryRemoveProductRequestParam213,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product link types
	 * 
	 * @param catalogProductLinkTypesRequestParam215
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkTypesResponseParam catalogProductLinkTypes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkTypesRequestParam catalogProductLinkTypesRequestParam215) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product link types
	 * 
	 * @param catalogProductLinkTypesRequestParam215
	 * 
	 */
	public void startcatalogProductLinkTypes(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkTypesRequestParam catalogProductLinkTypesRequestParam215,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Cancel invoice
	 * 
	 * @param salesOrderInvoiceCancelRequestParam217
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCancelResponseParam salesOrderInvoiceCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCancelRequestParam salesOrderInvoiceCancelRequestParam217) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Cancel
	 * invoice
	 * 
	 * @param salesOrderInvoiceCancelRequestParam217
	 * 
	 */
	public void startsalesOrderInvoiceCancel(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCancelRequestParam salesOrderInvoiceCancelRequestParam217,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create customer
	 * 
	 * @param customerCustomerCreateRequestParam219
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerCreateResponseParam customerCustomerCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerCreateRequestParam customerCustomerCreateRequestParam219) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create
	 * customer
	 * 
	 * @param customerCustomerCreateRequestParam219
	 * 
	 */
	public void startcustomerCustomerCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerCreateRequestParam customerCustomerCreateRequestParam219,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of assigned products
	 * 
	 * @param catalogCategoryAssignedProductsRequestParam221
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsResponseParam catalogCategoryAssignedProducts(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsRequestParam catalogCategoryAssignedProductsRequestParam221) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of assigned products
	 * 
	 * @param catalogCategoryAssignedProductsRequestParam221
	 * 
	 */
	public void startcatalogCategoryAssignedProducts(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsRequestParam catalogCategoryAssignedProductsRequestParam221,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get terms and conditions
	 * 
	 * @param shoppingCartLicenseRequestParam223
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseResponseParam shoppingCartLicense(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseRequestParam shoppingCartLicenseRequestParam223) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get terms
	 * and conditions
	 * 
	 * @param shoppingCartLicenseRequestParam223
	 * 
	 */
	public void startshoppingCartLicense(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseRequestParam shoppingCartLicenseRequestParam223,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add tag(s) to product
	 * 
	 * @param catalogProductTagAddRequestParam225
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagAddResponseParam catalogProductTagAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagAddRequestParam catalogProductTagAddRequestParam225) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add tag(s)
	 * to product
	 * 
	 * @param catalogProductTagAddRequestParam225
	 * 
	 */
	public void startcatalogProductTagAdd(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagAddRequestParam catalogProductTagAddRequestParam225,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get total prices for shopping cart
	 * 
	 * @param shoppingCartTotalsRequestParam227
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsResponseParam shoppingCartTotals(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsRequestParam shoppingCartTotalsRequestParam227) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get total
	 * prices for shopping cart
	 * 
	 * @param shoppingCartTotalsRequestParam227
	 * 
	 */
	public void startshoppingCartTotals(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsRequestParam shoppingCartTotalsRequestParam227,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create customer address
	 * 
	 * @param customerAddressCreateRequestParam229
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressCreateResponseParam customerAddressCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressCreateRequestParam customerAddressCreateRequestParam229) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create
	 * customer address
	 * 
	 * @param customerAddressCreateRequestParam229
	 * 
	 */
	public void startcustomerAddressCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressCreateRequestParam customerAddressCreateRequestParam229,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set/Get current store view
	 * 
	 * @param catalogProductCurrentStoreRequestParam231
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCurrentStoreResponseParam catalogProductCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCurrentStoreRequestParam catalogProductCurrentStoreRequestParam231) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set/Get
	 * current store view
	 * 
	 * @param catalogProductCurrentStoreRequestParam231
	 * 
	 */
	public void startcatalogProductCurrentStore(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCurrentStoreRequestParam catalogProductCurrentStoreRequestParam231,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve product
	 * 
	 * @param catalogProductInfoRequestParam233
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoResponseParam catalogProductInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoRequestParam catalogProductInfoRequestParam233) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * product
	 * 
	 * @param catalogProductInfoRequestParam233
	 * 
	 */
	public void startcatalogProductInfo(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoRequestParam catalogProductInfoRequestParam233,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Set customer for shopping cart
	 * 
	 * @param shoppingCartCustomerSetRequestParam235
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerSetResponseParam shoppingCartCustomerSet(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerSetRequestParam shoppingCartCustomerSetRequestParam235) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Set customer
	 * for shopping cart
	 * 
	 * @param shoppingCartCustomerSetRequestParam235
	 * 
	 */
	public void startshoppingCartCustomerSet(

			greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerSetRequestParam shoppingCartCustomerSetRequestParam235,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Get option groups for selected product
	 * 
	 * @param catalogProductOptionGroupsRequestParam237
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsResponseParam catalogProductOptionGroups(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsRequestParam catalogProductOptionGroupsRequestParam237) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Get option
	 * groups for selected product
	 * 
	 * @param catalogProductOptionGroupsRequestParam237
	 * 
	 */
	public void startcatalogProductOptionGroups(

			greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsRequestParam catalogProductOptionGroupsRequestParam237,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Multiple calls of resource functionality
	 * 
	 * @param multiCallParam239
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.MultiCallResponseParam multiCall(

			greyjoy.travelsmart.adapter.sunbonoo.magento.MultiCallParam multiCallParam239) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Multiple
	 * calls of resource functionality
	 * 
	 * @param multiCallParam239
	 * 
	 */
	public void startmultiCall(

			greyjoy.travelsmart.adapter.sunbonoo.magento.MultiCallParam multiCallParam239,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Add new comment to shipment
	 * 
	 * @param salesOrderShipmentAddCommentRequestParam241
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddCommentResponseParam salesOrderShipmentAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddCommentRequestParam salesOrderShipmentAddCommentRequestParam241) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Add new
	 * comment to shipment
	 * 
	 * @param salesOrderShipmentAddCommentRequestParam241
	 * 
	 */
	public void startsalesOrderShipmentAddComment(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddCommentRequestParam salesOrderShipmentAddCommentRequestParam241,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Create new shipment for order
	 * 
	 * @param salesOrderShipmentCreateRequestParam243
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCreateResponseParam salesOrderShipmentCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCreateRequestParam salesOrderShipmentCreateRequestParam243) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Create new
	 * shipment for order
	 * 
	 * @param salesOrderShipmentCreateRequestParam243
	 * 
	 */
	public void startsalesOrderShipmentCreate(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCreateRequestParam salesOrderShipmentCreateRequestParam243,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;



	/**
	 * Auto generated method signature Retrieve list of creditmemos by filters
	 * 
	 * @param salesOrderCreditmemoListRequestParam245
	 * 
	 */


	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoListResponseParam salesOrderCreditmemoList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoListRequestParam salesOrderCreditmemoListRequestParam245) throws java.rmi.RemoteException;


	/**
	 * Auto generated method signature for Asynchronous Invocations Retrieve
	 * list of creditmemos by filters
	 * 
	 * @param salesOrderCreditmemoListRequestParam245
	 * 
	 */
	public void startsalesOrderCreditmemoList(

			greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoListRequestParam salesOrderCreditmemoListRequestParam245,

			final greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoServiceCallbackHandler callback)

			throws java.rmi.RemoteException;




	//
}
