
/**
 * MagentoServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:20 GMT)
 */

package greyjoy.travelsmart.adapter.sunbonoo.magento;

/**
 * MagentoServiceCallbackHandler Callback class, Users can extend this class and
 * implement their own receiveResult and receiveError methods.
 */
public abstract class MagentoServiceCallbackHandler
{



	protected Object clientData;

	/**
	 * User can pass in any object that needs to be accessed once the
	 * NonBlocking Web service call is finished and appropriate method of this
	 * CallBack is called.
	 * 
	 * @param clientData
	 *            Object mechanism by which the user can pass in user data that
	 *            will be avilable at the time this callback is called.
	 */
	public MagentoServiceCallbackHandler(Object clientData)
	{
		this.clientData = clientData;
	}

	/**
	 * Please use this constructor if you don't want to set any clientData
	 */
	public MagentoServiceCallbackHandler()
	{
		this.clientData = null;
	}

	/**
	 * Get the client data
	 */

	public Object getClientData()
	{
		return clientData;
	}


	/**
	 * auto generated Axis2 call back method for catalogCategoryLevel method
	 * override this method for handling normal response from
	 * catalogCategoryLevel operation
	 */
	public void receiveResultcatalogCategoryLevel(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryLevelResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryLevel operation
	 */
	public void receiveErrorcatalogCategoryLevel(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for storeList method override this
	 * method for handling normal response from storeList operation
	 */
	public void receiveResultstoreList(greyjoy.travelsmart.adapter.sunbonoo.magento.StoreListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from storeList operation
	 */
	public void receiveErrorstoreList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentGetCarriers
	 * method override this method for handling normal response from
	 * salesOrderShipmentGetCarriers operation
	 */
	public void receiveResultsalesOrderShipmentGetCarriers(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentGetCarriersResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentGetCarriers operation
	 */
	public void receiveErrorsalesOrderShipmentGetCarriers(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductDownloadableLinkRemove method override this method for
	 * handling normal response from catalogProductDownloadableLinkRemove
	 * operation
	 */
	public void receiveResultcatalogProductDownloadableLinkRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductDownloadableLinkRemove operation
	 */
	public void receiveErrorcatalogProductDownloadableLinkRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartShippingList method
	 * override this method for handling normal response from
	 * shoppingCartShippingList operation
	 */
	public void receiveResultshoppingCartShippingList(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartShippingList operation
	 */
	public void receiveErrorshoppingCartShippingList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkAttributes
	 * method override this method for handling normal response from
	 * catalogProductLinkAttributes operation
	 */
	public void receiveResultcatalogProductLinkAttributes(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAttributesResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkAttributes operation
	 */
	public void receiveErrorcatalogProductLinkAttributes(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTypeList method
	 * override this method for handling normal response from
	 * catalogProductTypeList operation
	 */
	public void receiveResultcatalogProductTypeList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTypeListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTypeList operation
	 */
	public void receiveErrorcatalogProductTypeList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentRemoveTrack
	 * method override this method for handling normal response from
	 * salesOrderShipmentRemoveTrack operation
	 */
	public void receiveResultsalesOrderShipmentRemoveTrack(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentRemoveTrackResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentRemoveTrack operation
	 */
	public void receiveErrorsalesOrderShipmentRemoveTrack(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceCapture method
	 * override this method for handling normal response from
	 * salesOrderInvoiceCapture operation
	 */
	public void receiveResultsalesOrderInvoiceCapture(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCaptureResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceCapture operation
	 */
	public void receiveErrorsalesOrderInvoiceCapture(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogCategoryAttributeCurrentStore method override this method for
	 * handling normal response from catalogCategoryAttributeCurrentStore
	 * operation
	 */
	public void receiveResultcatalogCategoryAttributeCurrentStore(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeCurrentStoreResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryAttributeCurrentStore operation
	 */
	public void receiveErrorcatalogCategoryAttributeCurrentStore(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductCreate method
	 * override this method for handling normal response from
	 * catalogProductCreate operation
	 */
	public void receiveResultcatalogProductCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductCreate operation
	 */
	public void receiveErrorcatalogProductCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderList method override
	 * this method for handling normal response from salesOrderList operation
	 */
	public void receiveResultsalesOrderList(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderList operation
	 */
	public void receiveErrorsalesOrderList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCreditmemoCancel
	 * method override this method for handling normal response from
	 * salesOrderCreditmemoCancel operation
	 */
	public void receiveResultsalesOrderCreditmemoCancel(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCancelResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCreditmemoCancel operation
	 */
	public void receiveErrorsalesOrderCreditmemoCancel(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for resources method override this
	 * method for handling normal response from resources operation
	 */
	public void receiveResultresources(greyjoy.travelsmart.adapter.sunbonoo.magento.ResourcesResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from resources operation
	 */
	public void receiveErrorresources(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeTierPriceInfo method override this method for
	 * handling normal response from catalogProductAttributeTierPriceInfo
	 * operation
	 */
	public void receiveResultcatalogProductAttributeTierPriceInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeTierPriceInfo operation
	 */
	public void receiveErrorcatalogProductAttributeTierPriceInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTagInfo method
	 * override this method for handling normal response from
	 * catalogProductTagInfo operation
	 */
	public void receiveResultcatalogProductTagInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTagInfo operation
	 */
	public void receiveErrorcatalogProductTagInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeTierPriceUpdate method override this method for
	 * handling normal response from catalogProductAttributeTierPriceUpdate
	 * operation
	 */
	public void receiveResultcatalogProductAttributeTierPriceUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeTierPriceUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeTierPriceUpdate operation
	 */
	public void receiveErrorcatalogProductAttributeTierPriceUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaCreate method override this method for
	 * handling normal response from catalogProductAttributeMediaCreate
	 * operation
	 */
	public void receiveResultcatalogProductAttributeMediaCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaCreate operation
	 */
	public void receiveErrorcatalogProductAttributeMediaCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for directoryRegionList method
	 * override this method for handling normal response from
	 * directoryRegionList operation
	 */
	public void receiveResultdirectoryRegionList(greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryRegionListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from directoryRegionList operation
	 */
	public void receiveErrordirectoryRegionList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for endSession method override this
	 * method for handling normal response from endSession operation
	 */
	public void receiveResultendSession(greyjoy.travelsmart.adapter.sunbonoo.magento.EndSessionResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from endSession operation
	 */
	public void receiveErrorendSession(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderAddComment method
	 * override this method for handling normal response from
	 * salesOrderAddComment operation
	 */
	public void receiveResultsalesOrderAddComment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddCommentResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderAddComment operation
	 */
	public void receiveErrorsalesOrderAddComment(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartProductAdd method
	 * override this method for handling normal response from
	 * shoppingCartProductAdd operation
	 */
	public void receiveResultshoppingCartProductAdd(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductAddResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartProductAdd operation
	 */
	public void receiveErrorshoppingCartProductAdd(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartPaymentMethod
	 * method override this method for handling normal response from
	 * shoppingCartPaymentMethod operation
	 */
	public void receiveResultshoppingCartPaymentMethod(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentMethodResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartPaymentMethod operation
	 */
	public void receiveErrorshoppingCartPaymentMethod(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTagList method
	 * override this method for handling normal response from
	 * catalogProductTagList operation
	 */
	public void receiveResultcatalogProductTagList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTagList operation
	 */
	public void receiveErrorcatalogProductTagList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartCustomerAddresses
	 * method override this method for handling normal response from
	 * shoppingCartCustomerAddresses operation
	 */
	public void receiveResultshoppingCartCustomerAddresses(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerAddressesResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartCustomerAddresses operation
	 */
	public void receiveErrorshoppingCartCustomerAddresses(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for storeInfo method override this
	 * method for handling normal response from storeInfo operation
	 */
	public void receiveResultstoreInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.StoreInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from storeInfo operation
	 */
	public void receiveErrorstoreInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductSetSpecialPrice
	 * method override this method for handling normal response from
	 * catalogProductSetSpecialPrice operation
	 */
	public void receiveResultcatalogProductSetSpecialPrice(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductSetSpecialPriceResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductSetSpecialPrice operation
	 */
	public void receiveErrorcatalogProductSetSpecialPrice(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartProductList method
	 * override this method for handling normal response from
	 * shoppingCartProductList operation
	 */
	public void receiveResultshoppingCartProductList(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartProductList operation
	 */
	public void receiveErrorshoppingCartProductList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInfo method override
	 * this method for handling normal response from salesOrderInfo operation
	 */
	public void receiveResultsalesOrderInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInfo operation
	 */
	public void receiveErrorsalesOrderInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkUpdate method
	 * override this method for handling normal response from
	 * catalogProductLinkUpdate operation
	 */
	public void receiveResultcatalogProductLinkUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkUpdate operation
	 */
	public void receiveErrorcatalogProductLinkUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductGetSpecialPrice
	 * method override this method for handling normal response from
	 * catalogProductGetSpecialPrice operation
	 */
	public void receiveResultcatalogProductGetSpecialPrice(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductGetSpecialPriceResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductGetSpecialPrice operation
	 */
	public void receiveErrorcatalogProductGetSpecialPrice(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductAttributeList
	 * method override this method for handling normal response from
	 * catalogProductAttributeList operation
	 */
	public void receiveResultcatalogProductAttributeList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeList operation
	 */
	public void receiveErrorcatalogProductAttributeList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceInfo method
	 * override this method for handling normal response from
	 * salesOrderInvoiceInfo operation
	 */
	public void receiveResultsalesOrderInvoiceInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceInfo operation
	 */
	public void receiveErrorsalesOrderInvoiceInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartInfo method
	 * override this method for handling normal response from shoppingCartInfo
	 * operation
	 */
	public void receiveResultshoppingCartInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartInfo operation
	 */
	public void receiveErrorshoppingCartInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * shoppingCartProductMoveToCustomerQuote method override this method for
	 * handling normal response from shoppingCartProductMoveToCustomerQuote
	 * operation
	 */
	public void receiveResultshoppingCartProductMoveToCustomerQuote(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductMoveToCustomerQuoteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartProductMoveToCustomerQuote operation
	 */
	public void receiveErrorshoppingCartProductMoveToCustomerQuote(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryInfo method
	 * override this method for handling normal response from
	 * catalogCategoryInfo operation
	 */
	public void receiveResultcatalogCategoryInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryInfo operation
	 */
	public void receiveErrorcatalogCategoryInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryAssignProduct
	 * method override this method for handling normal response from
	 * catalogCategoryAssignProduct operation
	 */
	public void receiveResultcatalogCategoryAssignProduct(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignProductResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryAssignProduct operation
	 */
	public void receiveErrorcatalogCategoryAssignProduct(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeCurrentStore method override this method for
	 * handling normal response from catalogProductAttributeCurrentStore
	 * operation
	 */
	public void receiveResultcatalogProductAttributeCurrentStore(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeCurrentStoreResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeCurrentStore operation
	 */
	public void receiveErrorcatalogProductAttributeCurrentStore(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaCurrentStore method override this method for
	 * handling normal response from catalogProductAttributeMediaCurrentStore
	 * operation
	 */
	public void receiveResultcatalogProductAttributeMediaCurrentStore(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaCurrentStoreResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaCurrentStore operation
	 */
	public void receiveErrorcatalogProductAttributeMediaCurrentStore(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductUpdate method
	 * override this method for handling normal response from
	 * catalogProductUpdate operation
	 */
	public void receiveResultcatalogProductUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductUpdate operation
	 */
	public void receiveErrorcatalogProductUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaList method override this method for handling
	 * normal response from catalogProductAttributeMediaList operation
	 */
	public void receiveResultcatalogProductAttributeMediaList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaList operation
	 */
	public void receiveErrorcatalogProductAttributeMediaList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for startSession method override
	 * this method for handling normal response from startSession operation
	 */
	public void receiveResultstartSession(greyjoy.travelsmart.adapter.sunbonoo.magento.StartSessionResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from startSession operation
	 */
	public void receiveErrorstartSession(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentList method
	 * override this method for handling normal response from
	 * salesOrderShipmentList operation
	 */
	public void receiveResultsalesOrderShipmentList(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentList operation
	 */
	public void receiveErrorsalesOrderShipmentList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkAssign method
	 * override this method for handling normal response from
	 * catalogProductLinkAssign operation
	 */
	public void receiveResultcatalogProductLinkAssign(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkAssignResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkAssign operation
	 */
	public void receiveErrorcatalogProductLinkAssign(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceList method
	 * override this method for handling normal response from
	 * salesOrderInvoiceList operation
	 */
	public void receiveResultsalesOrderInvoiceList(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceList operation
	 */
	public void receiveErrorsalesOrderInvoiceList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkList method
	 * override this method for handling normal response from
	 * catalogProductLinkList operation
	 */
	public void receiveResultcatalogProductLinkList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkList operation
	 */
	public void receiveErrorcatalogProductLinkList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceCreate method
	 * override this method for handling normal response from
	 * salesOrderInvoiceCreate operation
	 */
	public void receiveResultsalesOrderInvoiceCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceCreate operation
	 */
	public void receiveErrorsalesOrderInvoiceCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerGroupList method
	 * override this method for handling normal response from customerGroupList
	 * operation
	 */
	public void receiveResultcustomerGroupList(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerGroupListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerGroupList operation
	 */
	public void receiveErrorcustomerGroupList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductAttributeSetList
	 * method override this method for handling normal response from
	 * catalogProductAttributeSetList operation
	 */
	public void receiveResultcatalogProductAttributeSetList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeSetListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeSetList operation
	 */
	public void receiveErrorcatalogProductAttributeSetList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryMove method
	 * override this method for handling normal response from
	 * catalogCategoryMove operation
	 */
	public void receiveResultcatalogCategoryMove(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryMoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryMove operation
	 */
	public void receiveErrorcatalogCategoryMove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryUpdate method
	 * override this method for handling normal response from
	 * catalogCategoryUpdate operation
	 */
	public void receiveResultcatalogCategoryUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryUpdate operation
	 */
	public void receiveErrorcatalogCategoryUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryAttributeList
	 * method override this method for handling normal response from
	 * catalogCategoryAttributeList operation
	 */
	public void receiveResultcatalogCategoryAttributeList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryAttributeList operation
	 */
	public void receiveErrorcatalogCategoryAttributeList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartProductUpdate
	 * method override this method for handling normal response from
	 * shoppingCartProductUpdate operation
	 */
	public void receiveResultshoppingCartProductUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartProductUpdate operation
	 */
	public void receiveErrorshoppingCartProductUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceVoid method
	 * override this method for handling normal response from
	 * salesOrderInvoiceVoid operation
	 */
	public void receiveResultsalesOrderInvoiceVoid(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceVoidResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceVoid operation
	 */
	public void receiveErrorsalesOrderInvoiceVoid(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerAddressInfo method
	 * override this method for handling normal response from
	 * customerAddressInfo operation
	 */
	public void receiveResultcustomerAddressInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerAddressInfo operation
	 */
	public void receiveErrorcustomerAddressInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryCreate method
	 * override this method for handling normal response from
	 * catalogCategoryCreate operation
	 */
	public void receiveResultcatalogCategoryCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryCreate operation
	 */
	public void receiveErrorcatalogCategoryCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaInfo method override this method for handling
	 * normal response from catalogProductAttributeMediaInfo operation
	 */
	public void receiveResultcatalogProductAttributeMediaInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaInfo operation
	 */
	public void receiveErrorcatalogProductAttributeMediaInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerCustomerUpdate method
	 * override this method for handling normal response from
	 * customerCustomerUpdate operation
	 */
	public void receiveResultcustomerCustomerUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerCustomerUpdate operation
	 */
	public void receiveErrorcustomerCustomerUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartCreate method
	 * override this method for handling normal response from shoppingCartCreate
	 * operation
	 */
	public void receiveResultshoppingCartCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartCreate operation
	 */
	public void receiveErrorshoppingCartCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for giftMessageSetForQuote method
	 * override this method for handling normal response from
	 * giftMessageSetForQuote operation
	 */
	public void receiveResultgiftMessageSetForQuote(greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from giftMessageSetForQuote operation
	 */
	public void receiveErrorgiftMessageSetForQuote(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartProductRemove
	 * method override this method for handling normal response from
	 * shoppingCartProductRemove operation
	 */
	public void receiveResultshoppingCartProductRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartProductRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartProductRemove operation
	 */
	public void receiveErrorshoppingCartProductRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for login method override this
	 * method for handling normal response from login operation
	 */
	public void receiveResultlogin(greyjoy.travelsmart.adapter.sunbonoo.magento.LoginResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from login operation
	 */
	public void receiveErrorlogin(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryAttributeOptions
	 * method override this method for handling normal response from
	 * catalogCategoryAttributeOptions operation
	 */
	public void receiveResultcatalogCategoryAttributeOptions(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAttributeOptionsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryAttributeOptions operation
	 */
	public void receiveErrorcatalogCategoryAttributeOptions(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCreditmemoInfo method
	 * override this method for handling normal response from
	 * salesOrderCreditmemoInfo operation
	 */
	public void receiveResultsalesOrderCreditmemoInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCreditmemoInfo operation
	 */
	public void receiveErrorsalesOrderCreditmemoInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerCustomerInfo method
	 * override this method for handling normal response from
	 * customerCustomerInfo operation
	 */
	public void receiveResultcustomerCustomerInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerCustomerInfo operation
	 */
	public void receiveErrorcustomerCustomerInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCreditmemoCreate
	 * method override this method for handling normal response from
	 * salesOrderCreditmemoCreate operation
	 */
	public void receiveResultsalesOrderCreditmemoCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCreditmemoCreate operation
	 */
	public void receiveErrorsalesOrderCreditmemoCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCancel method
	 * override this method for handling normal response from salesOrderCancel
	 * operation
	 */
	public void receiveResultsalesOrderCancel(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCancelResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCancel operation
	 */
	public void receiveErrorsalesOrderCancel(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartCouponRemove method
	 * override this method for handling normal response from
	 * shoppingCartCouponRemove operation
	 */
	public void receiveResultshoppingCartCouponRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartCouponRemove operation
	 */
	public void receiveErrorshoppingCartCouponRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartShippingMethod
	 * method override this method for handling normal response from
	 * shoppingCartShippingMethod operation
	 */
	public void receiveResultshoppingCartShippingMethod(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartShippingMethodResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartShippingMethod operation
	 */
	public void receiveErrorshoppingCartShippingMethod(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentAddTrack
	 * method override this method for handling normal response from
	 * salesOrderShipmentAddTrack operation
	 */
	public void receiveResultsalesOrderShipmentAddTrack(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddTrackResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentAddTrack operation
	 */
	public void receiveErrorsalesOrderShipmentAddTrack(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTagUpdate method
	 * override this method for handling normal response from
	 * catalogProductTagUpdate operation
	 */
	public void receiveResultcatalogProductTagUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTagUpdate operation
	 */
	public void receiveErrorcatalogProductTagUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductDownloadableLinkAdd method override this method for
	 * handling normal response from catalogProductDownloadableLinkAdd operation
	 */
	public void receiveResultcatalogProductDownloadableLinkAdd(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkAddResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductDownloadableLinkAdd operation
	 */
	public void receiveErrorcatalogProductDownloadableLinkAdd(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for resourceFaults method override
	 * this method for handling normal response from resourceFaults operation
	 */
	public void receiveResultresourceFaults(greyjoy.travelsmart.adapter.sunbonoo.magento.ResourceFaultsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from resourceFaults operation
	 */
	public void receiveErrorresourceFaults(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerAddressList method
	 * override this method for handling normal response from
	 * customerAddressList operation
	 */
	public void receiveResultcustomerAddressList(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerAddressList operation
	 */
	public void receiveErrorcustomerAddressList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogInventoryStockItemUpdate
	 * method override this method for handling normal response from
	 * catalogInventoryStockItemUpdate operation
	 */
	public void receiveResultcatalogInventoryStockItemUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogInventoryStockItemUpdate operation
	 */
	public void receiveErrorcatalogInventoryStockItemUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartPaymentList method
	 * override this method for handling normal response from
	 * shoppingCartPaymentList operation
	 */
	public void receiveResultshoppingCartPaymentList(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartPaymentList operation
	 */
	public void receiveErrorshoppingCartPaymentList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCreditmemoAddComment
	 * method override this method for handling normal response from
	 * salesOrderCreditmemoAddComment operation
	 */
	public void receiveResultsalesOrderCreditmemoAddComment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoAddCommentResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCreditmemoAddComment operation
	 */
	public void receiveErrorsalesOrderCreditmemoAddComment(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTagRemove method
	 * override this method for handling normal response from
	 * catalogProductTagRemove operation
	 */
	public void receiveResultcatalogProductTagRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTagRemove operation
	 */
	public void receiveErrorcatalogProductTagRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentInfo method
	 * override this method for handling normal response from
	 * salesOrderShipmentInfo operation
	 */
	public void receiveResultsalesOrderShipmentInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentInfo operation
	 */
	public void receiveErrorsalesOrderShipmentInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryDelete method
	 * override this method for handling normal response from
	 * catalogCategoryDelete operation
	 */
	public void receiveResultcatalogCategoryDelete(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryDeleteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryDelete operation
	 */
	public void receiveErrorcatalogCategoryDelete(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductAttributeOptions
	 * method override this method for handling normal response from
	 * catalogProductAttributeOptions operation
	 */
	public void receiveResultcatalogProductAttributeOptions(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeOptionsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeOptions operation
	 */
	public void receiveErrorcatalogProductAttributeOptions(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerAddressDelete method
	 * override this method for handling normal response from
	 * customerAddressDelete operation
	 */
	public void receiveResultcustomerAddressDelete(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressDeleteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerAddressDelete operation
	 */
	public void receiveErrorcustomerAddressDelete(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartCouponAdd method
	 * override this method for handling normal response from
	 * shoppingCartCouponAdd operation
	 */
	public void receiveResultshoppingCartCouponAdd(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCouponAddResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartCouponAdd operation
	 */
	public void receiveErrorshoppingCartCouponAdd(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for directoryCountryList method
	 * override this method for handling normal response from
	 * directoryCountryList operation
	 */
	public void receiveResultdirectoryCountryList(greyjoy.travelsmart.adapter.sunbonoo.magento.DirectoryCountryListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from directoryCountryList operation
	 */
	public void receiveErrordirectoryCountryList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryCurrentStore
	 * method override this method for handling normal response from
	 * catalogCategoryCurrentStore operation
	 */
	public void receiveResultcatalogCategoryCurrentStore(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryCurrentStoreResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryCurrentStore operation
	 */
	public void receiveErrorcatalogCategoryCurrentStore(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryUpdateProduct
	 * method override this method for handling normal response from
	 * catalogCategoryUpdateProduct operation
	 */
	public void receiveResultcatalogCategoryUpdateProduct(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryUpdateProductResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryUpdateProduct operation
	 */
	public void receiveErrorcatalogCategoryUpdateProduct(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for globalFaults method override
	 * this method for handling normal response from globalFaults operation
	 */
	public void receiveResultglobalFaults(greyjoy.travelsmart.adapter.sunbonoo.magento.GlobalFaultsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from globalFaults operation
	 */
	public void receiveErrorglobalFaults(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartOrder method
	 * override this method for handling normal response from shoppingCartOrder
	 * operation
	 */
	public void receiveResultshoppingCartOrder(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartOrderResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartOrder operation
	 */
	public void receiveErrorshoppingCartOrder(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerCustomerDelete method
	 * override this method for handling normal response from
	 * customerCustomerDelete operation
	 */
	public void receiveResultcustomerCustomerDelete(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerDeleteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerCustomerDelete operation
	 */
	public void receiveErrorcustomerCustomerDelete(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryTree method
	 * override this method for handling normal response from
	 * catalogCategoryTree operation
	 */
	public void receiveResultcatalogCategoryTree(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryTree operation
	 */
	public void receiveErrorcatalogCategoryTree(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaRemove method override this method for
	 * handling normal response from catalogProductAttributeMediaRemove
	 * operation
	 */
	public void receiveResultcatalogProductAttributeMediaRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaRemove operation
	 */
	public void receiveErrorcatalogProductAttributeMediaRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderUnhold method
	 * override this method for handling normal response from salesOrderUnhold
	 * operation
	 */
	public void receiveResultsalesOrderUnhold(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderUnholdResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderUnhold operation
	 */
	public void receiveErrorsalesOrderUnhold(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for giftMessageSetForQuoteItem
	 * method override this method for handling normal response from
	 * giftMessageSetForQuoteItem operation
	 */
	public void receiveResultgiftMessageSetForQuoteItem(greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteItemResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from giftMessageSetForQuoteItem operation
	 */
	public void receiveErrorgiftMessageSetForQuoteItem(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for call method override this
	 * method for handling normal response from call operation
	 */
	public void receiveResultcall(greyjoy.travelsmart.adapter.sunbonoo.magento.CallResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from call operation
	 */
	public void receiveErrorcall(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaTypes method override this method for
	 * handling normal response from catalogProductAttributeMediaTypes operation
	 */
	public void receiveResultcatalogProductAttributeMediaTypes(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaTypesResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaTypes operation
	 */
	public void receiveErrorcatalogProductAttributeMediaTypes(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductAttributeMediaUpdate method override this method for
	 * handling normal response from catalogProductAttributeMediaUpdate
	 * operation
	 */
	public void receiveResultcatalogProductAttributeMediaUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductAttributeMediaUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductAttributeMediaUpdate operation
	 */
	public void receiveErrorcatalogProductAttributeMediaUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogInventoryStockItemList
	 * method override this method for handling normal response from
	 * catalogInventoryStockItemList operation
	 */
	public void receiveResultcatalogInventoryStockItemList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogInventoryStockItemListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogInventoryStockItemList operation
	 */
	public void receiveErrorcatalogInventoryStockItemList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerCustomerList method
	 * override this method for handling normal response from
	 * customerCustomerList operation
	 */
	public void receiveResultcustomerCustomerList(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerCustomerList operation
	 */
	public void receiveErrorcustomerCustomerList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkRemove method
	 * override this method for handling normal response from
	 * catalogProductLinkRemove operation
	 */
	public void receiveResultcatalogProductLinkRemove(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkRemoveResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkRemove operation
	 */
	public void receiveErrorcatalogProductLinkRemove(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductList method
	 * override this method for handling normal response from catalogProductList
	 * operation
	 */
	public void receiveResultcatalogProductList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductList operation
	 */
	public void receiveErrorcatalogProductList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for
	 * catalogProductDownloadableLinkList method override this method for
	 * handling normal response from catalogProductDownloadableLinkList
	 * operation
	 */
	public void receiveResultcatalogProductDownloadableLinkList(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDownloadableLinkListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductDownloadableLinkList operation
	 */
	public void receiveErrorcatalogProductDownloadableLinkList(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductDelete method
	 * override this method for handling normal response from
	 * catalogProductDelete operation
	 */
	public void receiveResultcatalogProductDelete(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductDeleteResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductDelete operation
	 */
	public void receiveErrorcatalogProductDelete(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderHold method override
	 * this method for handling normal response from salesOrderHold operation
	 */
	public void receiveResultsalesOrderHold(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderHoldResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderHold operation
	 */
	public void receiveErrorsalesOrderHold(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerAddressUpdate method
	 * override this method for handling normal response from
	 * customerAddressUpdate operation
	 */
	public void receiveResultcustomerAddressUpdate(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressUpdateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerAddressUpdate operation
	 */
	public void receiveErrorcustomerAddressUpdate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for giftMessageSetForQuoteProduct
	 * method override this method for handling normal response from
	 * giftMessageSetForQuoteProduct operation
	 */
	public void receiveResultgiftMessageSetForQuoteProduct(greyjoy.travelsmart.adapter.sunbonoo.magento.GiftMessageForQuoteProductResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from giftMessageSetForQuoteProduct operation
	 */
	public void receiveErrorgiftMessageSetForQuoteProduct(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceAddComment
	 * method override this method for handling normal response from
	 * salesOrderInvoiceAddComment operation
	 */
	public void receiveResultsalesOrderInvoiceAddComment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceAddCommentResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceAddComment operation
	 */
	public void receiveErrorsalesOrderInvoiceAddComment(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryRemoveProduct
	 * method override this method for handling normal response from
	 * catalogCategoryRemoveProduct operation
	 */
	public void receiveResultcatalogCategoryRemoveProduct(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryRemoveProductResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryRemoveProduct operation
	 */
	public void receiveErrorcatalogCategoryRemoveProduct(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductLinkTypes method
	 * override this method for handling normal response from
	 * catalogProductLinkTypes operation
	 */
	public void receiveResultcatalogProductLinkTypes(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductLinkTypesResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductLinkTypes operation
	 */
	public void receiveErrorcatalogProductLinkTypes(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderInvoiceCancel method
	 * override this method for handling normal response from
	 * salesOrderInvoiceCancel operation
	 */
	public void receiveResultsalesOrderInvoiceCancel(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderInvoiceCancelResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderInvoiceCancel operation
	 */
	public void receiveErrorsalesOrderInvoiceCancel(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerCustomerCreate method
	 * override this method for handling normal response from
	 * customerCustomerCreate operation
	 */
	public void receiveResultcustomerCustomerCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerCustomerCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerCustomerCreate operation
	 */
	public void receiveErrorcustomerCustomerCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogCategoryAssignedProducts
	 * method override this method for handling normal response from
	 * catalogCategoryAssignedProducts operation
	 */
	public void receiveResultcatalogCategoryAssignedProducts(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryAssignedProductsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogCategoryAssignedProducts operation
	 */
	public void receiveErrorcatalogCategoryAssignedProducts(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartLicense method
	 * override this method for handling normal response from
	 * shoppingCartLicense operation
	 */
	public void receiveResultshoppingCartLicense(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartLicenseResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartLicense operation
	 */
	public void receiveErrorshoppingCartLicense(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductTagAdd method
	 * override this method for handling normal response from
	 * catalogProductTagAdd operation
	 */
	public void receiveResultcatalogProductTagAdd(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductTagAddResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductTagAdd operation
	 */
	public void receiveErrorcatalogProductTagAdd(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartTotals method
	 * override this method for handling normal response from shoppingCartTotals
	 * operation
	 */
	public void receiveResultshoppingCartTotals(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartTotalsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartTotals operation
	 */
	public void receiveErrorshoppingCartTotals(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for customerAddressCreate method
	 * override this method for handling normal response from
	 * customerAddressCreate operation
	 */
	public void receiveResultcustomerAddressCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.CustomerAddressCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from customerAddressCreate operation
	 */
	public void receiveErrorcustomerAddressCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductCurrentStore
	 * method override this method for handling normal response from
	 * catalogProductCurrentStore operation
	 */
	public void receiveResultcatalogProductCurrentStore(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductCurrentStoreResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductCurrentStore operation
	 */
	public void receiveErrorcatalogProductCurrentStore(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductInfo method
	 * override this method for handling normal response from catalogProductInfo
	 * operation
	 */
	public void receiveResultcatalogProductInfo(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductInfoResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductInfo operation
	 */
	public void receiveErrorcatalogProductInfo(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for shoppingCartCustomerSet method
	 * override this method for handling normal response from
	 * shoppingCartCustomerSet operation
	 */
	public void receiveResultshoppingCartCustomerSet(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartCustomerSetResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from shoppingCartCustomerSet operation
	 */
	public void receiveErrorshoppingCartCustomerSet(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for catalogProductOptionGroups
	 * method override this method for handling normal response from
	 * catalogProductOptionGroups operation
	 */
	public void receiveResultcatalogProductOptionGroups(greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogProductOptionGroupsResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from catalogProductOptionGroups operation
	 */
	public void receiveErrorcatalogProductOptionGroups(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for multiCall method override this
	 * method for handling normal response from multiCall operation
	 */
	public void receiveResultmultiCall(greyjoy.travelsmart.adapter.sunbonoo.magento.MultiCallResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from multiCall operation
	 */
	public void receiveErrormultiCall(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentAddComment
	 * method override this method for handling normal response from
	 * salesOrderShipmentAddComment operation
	 */
	public void receiveResultsalesOrderShipmentAddComment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentAddCommentResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentAddComment operation
	 */
	public void receiveErrorsalesOrderShipmentAddComment(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderShipmentCreate method
	 * override this method for handling normal response from
	 * salesOrderShipmentCreate operation
	 */
	public void receiveResultsalesOrderShipmentCreate(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCreateResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderShipmentCreate operation
	 */
	public void receiveErrorsalesOrderShipmentCreate(java.lang.Exception e)
	{
	}

	/**
	 * auto generated Axis2 call back method for salesOrderCreditmemoList method
	 * override this method for handling normal response from
	 * salesOrderCreditmemoList operation
	 */
	public void receiveResultsalesOrderCreditmemoList(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoListResponseParam result)
	{
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from salesOrderCreditmemoList operation
	 */
	public void receiveErrorsalesOrderCreditmemoList(java.lang.Exception e)
	{
	}



}
