
/**
 * SalesOrderAddressEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderAddressEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderAddressEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderAddressEntity Namespace URI = urn:Magento Namespace Prefix =
	 * ns1
	 */


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Parent_id
	 */


	protected java.lang.String localParent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localParent_idTracker = false;

	public boolean isParent_idSpecified()
	{
		return localParent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getParent_id()
	{
		return localParent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Parent_id
	 */
	public void setParent_id(java.lang.String param)
	{
		localParent_idTracker = param != null;

		this.localParent_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Is_active
	 */


	protected java.lang.String localIs_active;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_activeTracker = false;

	public boolean isIs_activeSpecified()
	{
		return localIs_activeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_active()
	{
		return localIs_active;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_active
	 */
	public void setIs_active(java.lang.String param)
	{
		localIs_activeTracker = param != null;

		this.localIs_active = param;


	}


	/**
	 * field for Address_type
	 */


	protected java.lang.String localAddress_type;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAddress_typeTracker = false;

	public boolean isAddress_typeSpecified()
	{
		return localAddress_typeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAddress_type()
	{
		return localAddress_type;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Address_type
	 */
	public void setAddress_type(java.lang.String param)
	{
		localAddress_typeTracker = param != null;

		this.localAddress_type = param;


	}


	/**
	 * field for Firstname
	 */


	protected java.lang.String localFirstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFirstnameTracker = false;

	public boolean isFirstnameSpecified()
	{
		return localFirstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFirstname()
	{
		return localFirstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Firstname
	 */
	public void setFirstname(java.lang.String param)
	{
		localFirstnameTracker = param != null;

		this.localFirstname = param;


	}


	/**
	 * field for Lastname
	 */


	protected java.lang.String localLastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localLastnameTracker = false;

	public boolean isLastnameSpecified()
	{
		return localLastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getLastname()
	{
		return localLastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Lastname
	 */
	public void setLastname(java.lang.String param)
	{
		localLastnameTracker = param != null;

		this.localLastname = param;


	}


	/**
	 * field for Company
	 */


	protected java.lang.String localCompany;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCompanyTracker = false;

	public boolean isCompanySpecified()
	{
		return localCompanyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCompany()
	{
		return localCompany;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Company
	 */
	public void setCompany(java.lang.String param)
	{
		localCompanyTracker = param != null;

		this.localCompany = param;


	}


	/**
	 * field for Street
	 */


	protected java.lang.String localStreet;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStreetTracker = false;

	public boolean isStreetSpecified()
	{
		return localStreetTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStreet()
	{
		return localStreet;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Street
	 */
	public void setStreet(java.lang.String param)
	{
		localStreetTracker = param != null;

		this.localStreet = param;


	}


	/**
	 * field for City
	 */


	protected java.lang.String localCity;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCityTracker = false;

	public boolean isCitySpecified()
	{
		return localCityTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCity()
	{
		return localCity;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            City
	 */
	public void setCity(java.lang.String param)
	{
		localCityTracker = param != null;

		this.localCity = param;


	}


	/**
	 * field for Region
	 */


	protected java.lang.String localRegion;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRegionTracker = false;

	public boolean isRegionSpecified()
	{
		return localRegionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRegion()
	{
		return localRegion;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Region
	 */
	public void setRegion(java.lang.String param)
	{
		localRegionTracker = param != null;

		this.localRegion = param;


	}


	/**
	 * field for Postcode
	 */


	protected java.lang.String localPostcode;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPostcodeTracker = false;

	public boolean isPostcodeSpecified()
	{
		return localPostcodeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPostcode()
	{
		return localPostcode;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Postcode
	 */
	public void setPostcode(java.lang.String param)
	{
		localPostcodeTracker = param != null;

		this.localPostcode = param;


	}


	/**
	 * field for Country_id
	 */


	protected java.lang.String localCountry_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCountry_idTracker = false;

	public boolean isCountry_idSpecified()
	{
		return localCountry_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCountry_id()
	{
		return localCountry_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Country_id
	 */
	public void setCountry_id(java.lang.String param)
	{
		localCountry_idTracker = param != null;

		this.localCountry_id = param;


	}


	/**
	 * field for Telephone
	 */


	protected java.lang.String localTelephone;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTelephoneTracker = false;

	public boolean isTelephoneSpecified()
	{
		return localTelephoneTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTelephone()
	{
		return localTelephone;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Telephone
	 */
	public void setTelephone(java.lang.String param)
	{
		localTelephoneTracker = param != null;

		this.localTelephone = param;


	}


	/**
	 * field for Fax
	 */


	protected java.lang.String localFax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFaxTracker = false;

	public boolean isFaxSpecified()
	{
		return localFaxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFax()
	{
		return localFax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Fax
	 */
	public void setFax(java.lang.String param)
	{
		localFaxTracker = param != null;

		this.localFax = param;


	}


	/**
	 * field for Region_id
	 */


	protected java.lang.String localRegion_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRegion_idTracker = false;

	public boolean isRegion_idSpecified()
	{
		return localRegion_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRegion_id()
	{
		return localRegion_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Region_id
	 */
	public void setRegion_id(java.lang.String param)
	{
		localRegion_idTracker = param != null;

		this.localRegion_id = param;


	}


	/**
	 * field for Address_id
	 */


	protected java.lang.String localAddress_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAddress_idTracker = false;

	public boolean isAddress_idSpecified()
	{
		return localAddress_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAddress_id()
	{
		return localAddress_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Address_id
	 */
	public void setAddress_id(java.lang.String param)
	{
		localAddress_idTracker = param != null;

		this.localAddress_id = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderAddressEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderAddressEntity", xmlWriter);
			}


		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localParent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "parent_id", xmlWriter);


			if (localParent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localParent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_activeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_active", xmlWriter);


			if (localIs_active == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_active cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_active);

			}

			xmlWriter.writeEndElement();
		}
		if (localAddress_typeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "address_type", xmlWriter);


			if (localAddress_type == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("address_type cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAddress_type);

			}

			xmlWriter.writeEndElement();
		}
		if (localFirstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "firstname", xmlWriter);


			if (localFirstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFirstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localLastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "lastname", xmlWriter);


			if (localLastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localLastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localCompanyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "company", xmlWriter);


			if (localCompany == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCompany);

			}

			xmlWriter.writeEndElement();
		}
		if (localStreetTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "street", xmlWriter);


			if (localStreet == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("street cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStreet);

			}

			xmlWriter.writeEndElement();
		}
		if (localCityTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "city", xmlWriter);


			if (localCity == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCity);

			}

			xmlWriter.writeEndElement();
		}
		if (localRegionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "region", xmlWriter);


			if (localRegion == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("region cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRegion);

			}

			xmlWriter.writeEndElement();
		}
		if (localPostcodeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "postcode", xmlWriter);


			if (localPostcode == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("postcode cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPostcode);

			}

			xmlWriter.writeEndElement();
		}
		if (localCountry_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "country_id", xmlWriter);


			if (localCountry_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("country_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCountry_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTelephoneTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "telephone", xmlWriter);


			if (localTelephone == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("telephone cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTelephone);

			}

			xmlWriter.writeEndElement();
		}
		if (localFaxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "fax", xmlWriter);


			if (localFax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFax);

			}

			xmlWriter.writeEndElement();
		}
		if (localRegion_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "region_id", xmlWriter);


			if (localRegion_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("region_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRegion_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localAddress_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "address_id", xmlWriter);


			if (localAddress_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAddress_id);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderAddressEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderAddressEntity object = new SalesOrderAddressEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderAddressEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderAddressEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setParent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_active").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_active" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_active(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "address_type").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "address_type" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAddress_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFirstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setLastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "company").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "company" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCompany(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "street").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "street" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStreet(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "city").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "city" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "region").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "region" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRegion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "postcode").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "postcode" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPostcode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "country_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "country_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCountry_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "telephone").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "telephone" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTelephone(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "fax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "fax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "region_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "region_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRegion_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAddress_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

