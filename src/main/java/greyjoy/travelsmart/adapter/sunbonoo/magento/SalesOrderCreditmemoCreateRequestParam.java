
/**
 * SalesOrderCreditmemoCreateRequestParam.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderCreditmemoCreateRequestParam bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderCreditmemoCreateRequestParam implements org.apache.axis2.databinding.ADBBean
{

	public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("urn:Magento", "salesOrderCreditmemoCreateRequestParam", "ns1");



	/**
	 * field for SessionId
	 */


	protected java.lang.String localSessionId;


	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSessionId()
	{
		return localSessionId;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            SessionId
	 */
	public void setSessionId(java.lang.String param)
	{

		this.localSessionId = param;


	}


	/**
	 * field for CreditmemoIncrementId
	 */


	protected java.lang.String localCreditmemoIncrementId;


	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreditmemoIncrementId()
	{
		return localCreditmemoIncrementId;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            CreditmemoIncrementId
	 */
	public void setCreditmemoIncrementId(java.lang.String param)
	{

		this.localCreditmemoIncrementId = param;


	}


	/**
	 * field for CreditmemoData
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData localCreditmemoData;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreditmemoDataTracker = false;

	public boolean isCreditmemoDataSpecified()
	{
		return localCreditmemoDataTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData getCreditmemoData()
	{
		return localCreditmemoData;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            CreditmemoData
	 */
	public void setCreditmemoData(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData param)
	{
		localCreditmemoDataTracker = param != null;

		this.localCreditmemoData = param;


	}


	/**
	 * field for Comment
	 */


	protected java.lang.String localComment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCommentTracker = false;

	public boolean isCommentSpecified()
	{
		return localCommentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getComment()
	{
		return localComment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Comment
	 */
	public void setComment(java.lang.String param)
	{
		localCommentTracker = param != null;

		this.localComment = param;


	}


	/**
	 * field for NotifyCustomer
	 */


	protected int localNotifyCustomer;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localNotifyCustomerTracker = false;

	public boolean isNotifyCustomerSpecified()
	{
		return localNotifyCustomerTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getNotifyCustomer()
	{
		return localNotifyCustomer;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            NotifyCustomer
	 */
	public void setNotifyCustomer(int param)
	{

		// setting primitive attribute tracker to true
		localNotifyCustomerTracker = param != java.lang.Integer.MIN_VALUE;

		this.localNotifyCustomer = param;


	}


	/**
	 * field for IncludeComment
	 */


	protected int localIncludeComment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncludeCommentTracker = false;

	public boolean isIncludeCommentSpecified()
	{
		return localIncludeCommentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getIncludeComment()
	{
		return localIncludeComment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            IncludeComment
	 */
	public void setIncludeComment(int param)
	{

		// setting primitive attribute tracker to true
		localIncludeCommentTracker = param != java.lang.Integer.MIN_VALUE;

		this.localIncludeComment = param;


	}


	/**
	 * field for RefundToStoreCreditAmount
	 */


	protected java.lang.String localRefundToStoreCreditAmount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRefundToStoreCreditAmountTracker = false;

	public boolean isRefundToStoreCreditAmountSpecified()
	{
		return localRefundToStoreCreditAmountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRefundToStoreCreditAmount()
	{
		return localRefundToStoreCreditAmount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            RefundToStoreCreditAmount
	 */
	public void setRefundToStoreCreditAmount(java.lang.String param)
	{
		localRefundToStoreCreditAmountTracker = param != null;

		this.localRefundToStoreCreditAmount = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderCreditmemoCreateRequestParam", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderCreditmemoCreateRequestParam", xmlWriter);
			}


		}

		namespace = "";
		writeStartElement(null, namespace, "sessionId", xmlWriter);


		if (localSessionId == null)
		{
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException("sessionId cannot be null!!");

		}
		else
		{


			xmlWriter.writeCharacters(localSessionId);

		}

		xmlWriter.writeEndElement();

		namespace = "";
		writeStartElement(null, namespace, "creditmemoIncrementId", xmlWriter);


		if (localCreditmemoIncrementId == null)
		{
			// write the nil attribute

			throw new org.apache.axis2.databinding.ADBException("creditmemoIncrementId cannot be null!!");

		}
		else
		{


			xmlWriter.writeCharacters(localCreditmemoIncrementId);

		}

		xmlWriter.writeEndElement();
		if (localCreditmemoDataTracker)
		{
			if (localCreditmemoData == null)
			{
				throw new org.apache.axis2.databinding.ADBException("creditmemoData cannot be null!!");
			}
			localCreditmemoData.serialize(new javax.xml.namespace.QName("", "creditmemoData"), xmlWriter);
		}
		if (localCommentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "comment", xmlWriter);


			if (localComment == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localComment);

			}

			xmlWriter.writeEndElement();
		}
		if (localNotifyCustomerTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "notifyCustomer", xmlWriter);

			if (localNotifyCustomer == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("notifyCustomer cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNotifyCustomer));
			}

			xmlWriter.writeEndElement();
		}
		if (localIncludeCommentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "includeComment", xmlWriter);

			if (localIncludeComment == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("includeComment cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeComment));
			}

			xmlWriter.writeEndElement();
		}
		if (localRefundToStoreCreditAmountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "refundToStoreCreditAmount", xmlWriter);


			if (localRefundToStoreCreditAmount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("refundToStoreCreditAmount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRefundToStoreCreditAmount);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderCreditmemoCreateRequestParam parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderCreditmemoCreateRequestParam object = new SalesOrderCreditmemoCreateRequestParam();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderCreditmemoCreateRequestParam".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderCreditmemoCreateRequestParam) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "sessionId").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "sessionId" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSessionId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{
					// 1 - A start element we are not expecting indicates an
					// invalid parameter was passed
					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "creditmemoIncrementId").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "creditmemoIncrementId" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreditmemoIncrementId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{
					// 1 - A start element we are not expecting indicates an
					// invalid parameter was passed
					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "creditmemoData").equals(reader.getName()))
				{

					object.setCreditmemoData(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoData.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "comment").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "comment" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setComment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "notifyCustomer").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "notifyCustomer" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setNotifyCustomer(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setNotifyCustomer(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "includeComment").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "includeComment" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncludeComment(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setIncludeComment(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "refundToStoreCreditAmount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "refundToStoreCreditAmount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRefundToStoreCreditAmount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

