
/**
 * SalesOrderCreditmemoEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderCreditmemoEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderCreditmemoEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderCreditmemoEntity Namespace URI = urn:Magento Namespace Prefix =
	 * ns1
	 */


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Transaction_id
	 */


	protected java.lang.String localTransaction_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTransaction_idTracker = false;

	public boolean isTransaction_idSpecified()
	{
		return localTransaction_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTransaction_id()
	{
		return localTransaction_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Transaction_id
	 */
	public void setTransaction_id(java.lang.String param)
	{
		localTransaction_idTracker = param != null;

		this.localTransaction_id = param;


	}


	/**
	 * field for Global_currency_code
	 */


	protected java.lang.String localGlobal_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGlobal_currency_codeTracker = false;

	public boolean isGlobal_currency_codeSpecified()
	{
		return localGlobal_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGlobal_currency_code()
	{
		return localGlobal_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Global_currency_code
	 */
	public void setGlobal_currency_code(java.lang.String param)
	{
		localGlobal_currency_codeTracker = param != null;

		this.localGlobal_currency_code = param;


	}


	/**
	 * field for Base_currency_code
	 */


	protected java.lang.String localBase_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_currency_codeTracker = false;

	public boolean isBase_currency_codeSpecified()
	{
		return localBase_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_currency_code()
	{
		return localBase_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_currency_code
	 */
	public void setBase_currency_code(java.lang.String param)
	{
		localBase_currency_codeTracker = param != null;

		this.localBase_currency_code = param;


	}


	/**
	 * field for Order_currency_code
	 */


	protected java.lang.String localOrder_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_currency_codeTracker = false;

	public boolean isOrder_currency_codeSpecified()
	{
		return localOrder_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_currency_code()
	{
		return localOrder_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_currency_code
	 */
	public void setOrder_currency_code(java.lang.String param)
	{
		localOrder_currency_codeTracker = param != null;

		this.localOrder_currency_code = param;


	}


	/**
	 * field for Store_currency_code
	 */


	protected java.lang.String localStore_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_currency_codeTracker = false;

	public boolean isStore_currency_codeSpecified()
	{
		return localStore_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_currency_code()
	{
		return localStore_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_currency_code
	 */
	public void setStore_currency_code(java.lang.String param)
	{
		localStore_currency_codeTracker = param != null;

		this.localStore_currency_code = param;


	}


	/**
	 * field for Cybersource_token
	 */


	protected java.lang.String localCybersource_token;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCybersource_tokenTracker = false;

	public boolean isCybersource_tokenSpecified()
	{
		return localCybersource_tokenTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCybersource_token()
	{
		return localCybersource_token;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cybersource_token
	 */
	public void setCybersource_token(java.lang.String param)
	{
		localCybersource_tokenTracker = param != null;

		this.localCybersource_token = param;


	}


	/**
	 * field for Invoice_id
	 */


	protected java.lang.String localInvoice_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localInvoice_idTracker = false;

	public boolean isInvoice_idSpecified()
	{
		return localInvoice_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getInvoice_id()
	{
		return localInvoice_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Invoice_id
	 */
	public void setInvoice_id(java.lang.String param)
	{
		localInvoice_idTracker = param != null;

		this.localInvoice_id = param;


	}


	/**
	 * field for Billing_address_id
	 */


	protected java.lang.String localBilling_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_address_idTracker = false;

	public boolean isBilling_address_idSpecified()
	{
		return localBilling_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_address_id()
	{
		return localBilling_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_address_id
	 */
	public void setBilling_address_id(java.lang.String param)
	{
		localBilling_address_idTracker = param != null;

		this.localBilling_address_id = param;


	}


	/**
	 * field for Shipping_address_id
	 */


	protected java.lang.String localShipping_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_address_idTracker = false;

	public boolean isShipping_address_idSpecified()
	{
		return localShipping_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_address_id()
	{
		return localShipping_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address_id
	 */
	public void setShipping_address_id(java.lang.String param)
	{
		localShipping_address_idTracker = param != null;

		this.localShipping_address_id = param;


	}


	/**
	 * field for State
	 */


	protected java.lang.String localState;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStateTracker = false;

	public boolean isStateSpecified()
	{
		return localStateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getState()
	{
		return localState;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            State
	 */
	public void setState(java.lang.String param)
	{
		localStateTracker = param != null;

		this.localState = param;


	}


	/**
	 * field for Creditmemo_status
	 */


	protected java.lang.String localCreditmemo_status;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreditmemo_statusTracker = false;

	public boolean isCreditmemo_statusSpecified()
	{
		return localCreditmemo_statusTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreditmemo_status()
	{
		return localCreditmemo_status;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Creditmemo_status
	 */
	public void setCreditmemo_status(java.lang.String param)
	{
		localCreditmemo_statusTracker = param != null;

		this.localCreditmemo_status = param;


	}


	/**
	 * field for Email_sent
	 */


	protected java.lang.String localEmail_sent;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEmail_sentTracker = false;

	public boolean isEmail_sentSpecified()
	{
		return localEmail_sentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEmail_sent()
	{
		return localEmail_sent;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Email_sent
	 */
	public void setEmail_sent(java.lang.String param)
	{
		localEmail_sentTracker = param != null;

		this.localEmail_sent = param;


	}


	/**
	 * field for Order_id
	 */


	protected java.lang.String localOrder_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_idTracker = false;

	public boolean isOrder_idSpecified()
	{
		return localOrder_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_id()
	{
		return localOrder_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_id
	 */
	public void setOrder_id(java.lang.String param)
	{
		localOrder_idTracker = param != null;

		this.localOrder_id = param;


	}


	/**
	 * field for Tax_amount
	 */


	protected java.lang.String localTax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_amountTracker = false;

	public boolean isTax_amountSpecified()
	{
		return localTax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_amount()
	{
		return localTax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_amount
	 */
	public void setTax_amount(java.lang.String param)
	{
		localTax_amountTracker = param != null;

		this.localTax_amount = param;


	}


	/**
	 * field for Shipping_tax_amount
	 */


	protected java.lang.String localShipping_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_tax_amountTracker = false;

	public boolean isShipping_tax_amountSpecified()
	{
		return localShipping_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_tax_amount()
	{
		return localShipping_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_tax_amount
	 */
	public void setShipping_tax_amount(java.lang.String param)
	{
		localShipping_tax_amountTracker = param != null;

		this.localShipping_tax_amount = param;


	}


	/**
	 * field for Base_tax_amount
	 */


	protected java.lang.String localBase_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_amountTracker = false;

	public boolean isBase_tax_amountSpecified()
	{
		return localBase_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_amount()
	{
		return localBase_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_amount
	 */
	public void setBase_tax_amount(java.lang.String param)
	{
		localBase_tax_amountTracker = param != null;

		this.localBase_tax_amount = param;


	}


	/**
	 * field for Base_adjustment_positive
	 */


	protected java.lang.String localBase_adjustment_positive;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_adjustment_positiveTracker = false;

	public boolean isBase_adjustment_positiveSpecified()
	{
		return localBase_adjustment_positiveTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_adjustment_positive()
	{
		return localBase_adjustment_positive;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_adjustment_positive
	 */
	public void setBase_adjustment_positive(java.lang.String param)
	{
		localBase_adjustment_positiveTracker = param != null;

		this.localBase_adjustment_positive = param;


	}


	/**
	 * field for Base_grand_total
	 */


	protected java.lang.String localBase_grand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_grand_totalTracker = false;

	public boolean isBase_grand_totalSpecified()
	{
		return localBase_grand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_grand_total()
	{
		return localBase_grand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_grand_total
	 */
	public void setBase_grand_total(java.lang.String param)
	{
		localBase_grand_totalTracker = param != null;

		this.localBase_grand_total = param;


	}


	/**
	 * field for Adjustment
	 */


	protected java.lang.String localAdjustment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdjustmentTracker = false;

	public boolean isAdjustmentSpecified()
	{
		return localAdjustmentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdjustment()
	{
		return localAdjustment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Adjustment
	 */
	public void setAdjustment(java.lang.String param)
	{
		localAdjustmentTracker = param != null;

		this.localAdjustment = param;


	}


	/**
	 * field for Subtotal
	 */


	protected java.lang.String localSubtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotalTracker = false;

	public boolean isSubtotalSpecified()
	{
		return localSubtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal()
	{
		return localSubtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal
	 */
	public void setSubtotal(java.lang.String param)
	{
		localSubtotalTracker = param != null;

		this.localSubtotal = param;


	}


	/**
	 * field for Discount_amount
	 */


	protected java.lang.String localDiscount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_amountTracker = false;

	public boolean isDiscount_amountSpecified()
	{
		return localDiscount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_amount()
	{
		return localDiscount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_amount
	 */
	public void setDiscount_amount(java.lang.String param)
	{
		localDiscount_amountTracker = param != null;

		this.localDiscount_amount = param;


	}


	/**
	 * field for Base_subtotal
	 */


	protected java.lang.String localBase_subtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotalTracker = false;

	public boolean isBase_subtotalSpecified()
	{
		return localBase_subtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal()
	{
		return localBase_subtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal
	 */
	public void setBase_subtotal(java.lang.String param)
	{
		localBase_subtotalTracker = param != null;

		this.localBase_subtotal = param;


	}


	/**
	 * field for Base_adjustment
	 */


	protected java.lang.String localBase_adjustment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_adjustmentTracker = false;

	public boolean isBase_adjustmentSpecified()
	{
		return localBase_adjustmentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_adjustment()
	{
		return localBase_adjustment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_adjustment
	 */
	public void setBase_adjustment(java.lang.String param)
	{
		localBase_adjustmentTracker = param != null;

		this.localBase_adjustment = param;


	}


	/**
	 * field for Base_to_global_rate
	 */


	protected java.lang.String localBase_to_global_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_global_rateTracker = false;

	public boolean isBase_to_global_rateSpecified()
	{
		return localBase_to_global_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_global_rate()
	{
		return localBase_to_global_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_global_rate
	 */
	public void setBase_to_global_rate(java.lang.String param)
	{
		localBase_to_global_rateTracker = param != null;

		this.localBase_to_global_rate = param;


	}


	/**
	 * field for Store_to_base_rate
	 */


	protected java.lang.String localStore_to_base_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_base_rateTracker = false;

	public boolean isStore_to_base_rateSpecified()
	{
		return localStore_to_base_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_base_rate()
	{
		return localStore_to_base_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_base_rate
	 */
	public void setStore_to_base_rate(java.lang.String param)
	{
		localStore_to_base_rateTracker = param != null;

		this.localStore_to_base_rate = param;


	}


	/**
	 * field for Base_shipping_amount
	 */


	protected java.lang.String localBase_shipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_amountTracker = false;

	public boolean isBase_shipping_amountSpecified()
	{
		return localBase_shipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_amount()
	{
		return localBase_shipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_amount
	 */
	public void setBase_shipping_amount(java.lang.String param)
	{
		localBase_shipping_amountTracker = param != null;

		this.localBase_shipping_amount = param;


	}


	/**
	 * field for Adjustment_negative
	 */


	protected java.lang.String localAdjustment_negative;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdjustment_negativeTracker = false;

	public boolean isAdjustment_negativeSpecified()
	{
		return localAdjustment_negativeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdjustment_negative()
	{
		return localAdjustment_negative;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Adjustment_negative
	 */
	public void setAdjustment_negative(java.lang.String param)
	{
		localAdjustment_negativeTracker = param != null;

		this.localAdjustment_negative = param;


	}


	/**
	 * field for Subtotal_incl_tax
	 */


	protected java.lang.String localSubtotal_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_incl_taxTracker = false;

	public boolean isSubtotal_incl_taxSpecified()
	{
		return localSubtotal_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal_incl_tax()
	{
		return localSubtotal_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_incl_tax
	 */
	public void setSubtotal_incl_tax(java.lang.String param)
	{
		localSubtotal_incl_taxTracker = param != null;

		this.localSubtotal_incl_tax = param;


	}


	/**
	 * field for Shipping_amount
	 */


	protected java.lang.String localShipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_amountTracker = false;

	public boolean isShipping_amountSpecified()
	{
		return localShipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_amount()
	{
		return localShipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_amount
	 */
	public void setShipping_amount(java.lang.String param)
	{
		localShipping_amountTracker = param != null;

		this.localShipping_amount = param;


	}


	/**
	 * field for Base_subtotal_incl_tax
	 */


	protected java.lang.String localBase_subtotal_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_incl_taxTracker = false;

	public boolean isBase_subtotal_incl_taxSpecified()
	{
		return localBase_subtotal_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal_incl_tax()
	{
		return localBase_subtotal_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_incl_tax
	 */
	public void setBase_subtotal_incl_tax(java.lang.String param)
	{
		localBase_subtotal_incl_taxTracker = param != null;

		this.localBase_subtotal_incl_tax = param;


	}


	/**
	 * field for Base_adjustment_negative
	 */


	protected java.lang.String localBase_adjustment_negative;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_adjustment_negativeTracker = false;

	public boolean isBase_adjustment_negativeSpecified()
	{
		return localBase_adjustment_negativeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_adjustment_negative()
	{
		return localBase_adjustment_negative;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_adjustment_negative
	 */
	public void setBase_adjustment_negative(java.lang.String param)
	{
		localBase_adjustment_negativeTracker = param != null;

		this.localBase_adjustment_negative = param;


	}


	/**
	 * field for Grand_total
	 */


	protected java.lang.String localGrand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGrand_totalTracker = false;

	public boolean isGrand_totalSpecified()
	{
		return localGrand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGrand_total()
	{
		return localGrand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Grand_total
	 */
	public void setGrand_total(java.lang.String param)
	{
		localGrand_totalTracker = param != null;

		this.localGrand_total = param;


	}


	/**
	 * field for Base_discount_amount
	 */


	protected java.lang.String localBase_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_amountTracker = false;

	public boolean isBase_discount_amountSpecified()
	{
		return localBase_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_amount()
	{
		return localBase_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_amount
	 */
	public void setBase_discount_amount(java.lang.String param)
	{
		localBase_discount_amountTracker = param != null;

		this.localBase_discount_amount = param;


	}


	/**
	 * field for Base_to_order_rate
	 */


	protected java.lang.String localBase_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_order_rateTracker = false;

	public boolean isBase_to_order_rateSpecified()
	{
		return localBase_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_order_rate()
	{
		return localBase_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_order_rate
	 */
	public void setBase_to_order_rate(java.lang.String param)
	{
		localBase_to_order_rateTracker = param != null;

		this.localBase_to_order_rate = param;


	}


	/**
	 * field for Store_to_order_rate
	 */


	protected java.lang.String localStore_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_order_rateTracker = false;

	public boolean isStore_to_order_rateSpecified()
	{
		return localStore_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_order_rate()
	{
		return localStore_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_order_rate
	 */
	public void setStore_to_order_rate(java.lang.String param)
	{
		localStore_to_order_rateTracker = param != null;

		this.localStore_to_order_rate = param;


	}


	/**
	 * field for Base_shipping_tax_amount
	 */


	protected java.lang.String localBase_shipping_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_tax_amountTracker = false;

	public boolean isBase_shipping_tax_amountSpecified()
	{
		return localBase_shipping_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_tax_amount()
	{
		return localBase_shipping_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_tax_amount
	 */
	public void setBase_shipping_tax_amount(java.lang.String param)
	{
		localBase_shipping_tax_amountTracker = param != null;

		this.localBase_shipping_tax_amount = param;


	}


	/**
	 * field for Adjustment_positive
	 */


	protected java.lang.String localAdjustment_positive;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdjustment_positiveTracker = false;

	public boolean isAdjustment_positiveSpecified()
	{
		return localAdjustment_positiveTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdjustment_positive()
	{
		return localAdjustment_positive;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Adjustment_positive
	 */
	public void setAdjustment_positive(java.lang.String param)
	{
		localAdjustment_positiveTracker = param != null;

		this.localAdjustment_positive = param;


	}


	/**
	 * field for Store_id
	 */


	protected java.lang.String localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(java.lang.String param)
	{
		localStore_idTracker = param != null;

		this.localStore_id = param;


	}


	/**
	 * field for Hidden_tax_amount
	 */


	protected java.lang.String localHidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHidden_tax_amountTracker = false;

	public boolean isHidden_tax_amountSpecified()
	{
		return localHidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHidden_tax_amount()
	{
		return localHidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hidden_tax_amount
	 */
	public void setHidden_tax_amount(java.lang.String param)
	{
		localHidden_tax_amountTracker = param != null;

		this.localHidden_tax_amount = param;


	}


	/**
	 * field for Base_hidden_tax_amount
	 */


	protected java.lang.String localBase_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_hidden_tax_amountTracker = false;

	public boolean isBase_hidden_tax_amountSpecified()
	{
		return localBase_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_hidden_tax_amount()
	{
		return localBase_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_hidden_tax_amount
	 */
	public void setBase_hidden_tax_amount(java.lang.String param)
	{
		localBase_hidden_tax_amountTracker = param != null;

		this.localBase_hidden_tax_amount = param;


	}


	/**
	 * field for Shipping_hidden_tax_amount
	 */


	protected java.lang.String localShipping_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_hidden_tax_amountTracker = false;

	public boolean isShipping_hidden_tax_amountSpecified()
	{
		return localShipping_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_hidden_tax_amount()
	{
		return localShipping_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_hidden_tax_amount
	 */
	public void setShipping_hidden_tax_amount(java.lang.String param)
	{
		localShipping_hidden_tax_amountTracker = param != null;

		this.localShipping_hidden_tax_amount = param;


	}


	/**
	 * field for Base_shipping_hidden_tax_amnt
	 */


	protected java.lang.String localBase_shipping_hidden_tax_amnt;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_hidden_tax_amntTracker = false;

	public boolean isBase_shipping_hidden_tax_amntSpecified()
	{
		return localBase_shipping_hidden_tax_amntTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_hidden_tax_amnt()
	{
		return localBase_shipping_hidden_tax_amnt;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_hidden_tax_amnt
	 */
	public void setBase_shipping_hidden_tax_amnt(java.lang.String param)
	{
		localBase_shipping_hidden_tax_amntTracker = param != null;

		this.localBase_shipping_hidden_tax_amnt = param;


	}


	/**
	 * field for Shipping_incl_tax
	 */


	protected java.lang.String localShipping_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_incl_taxTracker = false;

	public boolean isShipping_incl_taxSpecified()
	{
		return localShipping_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_incl_tax()
	{
		return localShipping_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_incl_tax
	 */
	public void setShipping_incl_tax(java.lang.String param)
	{
		localShipping_incl_taxTracker = param != null;

		this.localShipping_incl_tax = param;


	}


	/**
	 * field for Base_shipping_incl_tax
	 */


	protected java.lang.String localBase_shipping_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_incl_taxTracker = false;

	public boolean isBase_shipping_incl_taxSpecified()
	{
		return localBase_shipping_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_incl_tax()
	{
		return localBase_shipping_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_incl_tax
	 */
	public void setBase_shipping_incl_tax(java.lang.String param)
	{
		localBase_shipping_incl_taxTracker = param != null;

		this.localBase_shipping_incl_tax = param;


	}


	/**
	 * field for Base_customer_balance_amount
	 */


	protected java.lang.String localBase_customer_balance_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_amountTracker = false;

	public boolean isBase_customer_balance_amountSpecified()
	{
		return localBase_customer_balance_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_customer_balance_amount()
	{
		return localBase_customer_balance_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_amount
	 */
	public void setBase_customer_balance_amount(java.lang.String param)
	{
		localBase_customer_balance_amountTracker = param != null;

		this.localBase_customer_balance_amount = param;


	}


	/**
	 * field for Customer_balance_amount
	 */


	protected java.lang.String localCustomer_balance_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_amountTracker = false;

	public boolean isCustomer_balance_amountSpecified()
	{
		return localCustomer_balance_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_balance_amount()
	{
		return localCustomer_balance_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_amount
	 */
	public void setCustomer_balance_amount(java.lang.String param)
	{
		localCustomer_balance_amountTracker = param != null;

		this.localCustomer_balance_amount = param;


	}


	/**
	 * field for Bs_customer_bal_total_refunded
	 */


	protected java.lang.String localBs_customer_bal_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBs_customer_bal_total_refundedTracker = false;

	public boolean isBs_customer_bal_total_refundedSpecified()
	{
		return localBs_customer_bal_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBs_customer_bal_total_refunded()
	{
		return localBs_customer_bal_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Bs_customer_bal_total_refunded
	 */
	public void setBs_customer_bal_total_refunded(java.lang.String param)
	{
		localBs_customer_bal_total_refundedTracker = param != null;

		this.localBs_customer_bal_total_refunded = param;


	}


	/**
	 * field for Customer_bal_total_refunded
	 */


	protected java.lang.String localCustomer_bal_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_bal_total_refundedTracker = false;

	public boolean isCustomer_bal_total_refundedSpecified()
	{
		return localCustomer_bal_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_bal_total_refunded()
	{
		return localCustomer_bal_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_bal_total_refunded
	 */
	public void setCustomer_bal_total_refunded(java.lang.String param)
	{
		localCustomer_bal_total_refundedTracker = param != null;

		this.localCustomer_bal_total_refunded = param;


	}


	/**
	 * field for Base_gift_cards_amount
	 */


	protected java.lang.String localBase_gift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_gift_cards_amountTracker = false;

	public boolean isBase_gift_cards_amountSpecified()
	{
		return localBase_gift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_gift_cards_amount()
	{
		return localBase_gift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_gift_cards_amount
	 */
	public void setBase_gift_cards_amount(java.lang.String param)
	{
		localBase_gift_cards_amountTracker = param != null;

		this.localBase_gift_cards_amount = param;


	}


	/**
	 * field for Gift_cards_amount
	 */


	protected java.lang.String localGift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_amountTracker = false;

	public boolean isGift_cards_amountSpecified()
	{
		return localGift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_amount()
	{
		return localGift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_amount
	 */
	public void setGift_cards_amount(java.lang.String param)
	{
		localGift_cards_amountTracker = param != null;

		this.localGift_cards_amount = param;


	}


	/**
	 * field for Gw_base_price
	 */


	protected java.lang.String localGw_base_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_base_priceTracker = false;

	public boolean isGw_base_priceSpecified()
	{
		return localGw_base_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_base_price()
	{
		return localGw_base_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_base_price
	 */
	public void setGw_base_price(java.lang.String param)
	{
		localGw_base_priceTracker = param != null;

		this.localGw_base_price = param;


	}


	/**
	 * field for Gw_price
	 */


	protected java.lang.String localGw_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_priceTracker = false;

	public boolean isGw_priceSpecified()
	{
		return localGw_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_price()
	{
		return localGw_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_price
	 */
	public void setGw_price(java.lang.String param)
	{
		localGw_priceTracker = param != null;

		this.localGw_price = param;


	}


	/**
	 * field for Gw_items_base_price
	 */


	protected java.lang.String localGw_items_base_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_items_base_priceTracker = false;

	public boolean isGw_items_base_priceSpecified()
	{
		return localGw_items_base_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_items_base_price()
	{
		return localGw_items_base_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_items_base_price
	 */
	public void setGw_items_base_price(java.lang.String param)
	{
		localGw_items_base_priceTracker = param != null;

		this.localGw_items_base_price = param;


	}


	/**
	 * field for Gw_items_price
	 */


	protected java.lang.String localGw_items_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_items_priceTracker = false;

	public boolean isGw_items_priceSpecified()
	{
		return localGw_items_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_items_price()
	{
		return localGw_items_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_items_price
	 */
	public void setGw_items_price(java.lang.String param)
	{
		localGw_items_priceTracker = param != null;

		this.localGw_items_price = param;


	}


	/**
	 * field for Gw_card_base_price
	 */


	protected java.lang.String localGw_card_base_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_card_base_priceTracker = false;

	public boolean isGw_card_base_priceSpecified()
	{
		return localGw_card_base_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_card_base_price()
	{
		return localGw_card_base_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_card_base_price
	 */
	public void setGw_card_base_price(java.lang.String param)
	{
		localGw_card_base_priceTracker = param != null;

		this.localGw_card_base_price = param;


	}


	/**
	 * field for Gw_card_price
	 */


	protected java.lang.String localGw_card_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_card_priceTracker = false;

	public boolean isGw_card_priceSpecified()
	{
		return localGw_card_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_card_price()
	{
		return localGw_card_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_card_price
	 */
	public void setGw_card_price(java.lang.String param)
	{
		localGw_card_priceTracker = param != null;

		this.localGw_card_price = param;


	}


	/**
	 * field for Gw_base_tax_amount
	 */


	protected java.lang.String localGw_base_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_base_tax_amountTracker = false;

	public boolean isGw_base_tax_amountSpecified()
	{
		return localGw_base_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_base_tax_amount()
	{
		return localGw_base_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_base_tax_amount
	 */
	public void setGw_base_tax_amount(java.lang.String param)
	{
		localGw_base_tax_amountTracker = param != null;

		this.localGw_base_tax_amount = param;


	}


	/**
	 * field for Gw_tax_amount
	 */


	protected java.lang.String localGw_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_tax_amountTracker = false;

	public boolean isGw_tax_amountSpecified()
	{
		return localGw_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_tax_amount()
	{
		return localGw_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_tax_amount
	 */
	public void setGw_tax_amount(java.lang.String param)
	{
		localGw_tax_amountTracker = param != null;

		this.localGw_tax_amount = param;


	}


	/**
	 * field for Gw_items_base_tax_amount
	 */


	protected java.lang.String localGw_items_base_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_items_base_tax_amountTracker = false;

	public boolean isGw_items_base_tax_amountSpecified()
	{
		return localGw_items_base_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_items_base_tax_amount()
	{
		return localGw_items_base_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_items_base_tax_amount
	 */
	public void setGw_items_base_tax_amount(java.lang.String param)
	{
		localGw_items_base_tax_amountTracker = param != null;

		this.localGw_items_base_tax_amount = param;


	}


	/**
	 * field for Gw_items_tax_amount
	 */


	protected java.lang.String localGw_items_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_items_tax_amountTracker = false;

	public boolean isGw_items_tax_amountSpecified()
	{
		return localGw_items_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_items_tax_amount()
	{
		return localGw_items_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_items_tax_amount
	 */
	public void setGw_items_tax_amount(java.lang.String param)
	{
		localGw_items_tax_amountTracker = param != null;

		this.localGw_items_tax_amount = param;


	}


	/**
	 * field for Gw_card_base_tax_amount
	 */


	protected java.lang.String localGw_card_base_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_card_base_tax_amountTracker = false;

	public boolean isGw_card_base_tax_amountSpecified()
	{
		return localGw_card_base_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_card_base_tax_amount()
	{
		return localGw_card_base_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_card_base_tax_amount
	 */
	public void setGw_card_base_tax_amount(java.lang.String param)
	{
		localGw_card_base_tax_amountTracker = param != null;

		this.localGw_card_base_tax_amount = param;


	}


	/**
	 * field for Gw_card_tax_amount
	 */


	protected java.lang.String localGw_card_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGw_card_tax_amountTracker = false;

	public boolean isGw_card_tax_amountSpecified()
	{
		return localGw_card_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGw_card_tax_amount()
	{
		return localGw_card_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gw_card_tax_amount
	 */
	public void setGw_card_tax_amount(java.lang.String param)
	{
		localGw_card_tax_amountTracker = param != null;

		this.localGw_card_tax_amount = param;


	}


	/**
	 * field for Base_reward_currency_amount
	 */


	protected java.lang.String localBase_reward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_reward_currency_amountTracker = false;

	public boolean isBase_reward_currency_amountSpecified()
	{
		return localBase_reward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_reward_currency_amount()
	{
		return localBase_reward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_reward_currency_amount
	 */
	public void setBase_reward_currency_amount(java.lang.String param)
	{
		localBase_reward_currency_amountTracker = param != null;

		this.localBase_reward_currency_amount = param;


	}


	/**
	 * field for Reward_currency_amount
	 */


	protected java.lang.String localReward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_currency_amountTracker = false;

	public boolean isReward_currency_amountSpecified()
	{
		return localReward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_currency_amount()
	{
		return localReward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_currency_amount
	 */
	public void setReward_currency_amount(java.lang.String param)
	{
		localReward_currency_amountTracker = param != null;

		this.localReward_currency_amount = param;


	}


	/**
	 * field for Reward_points_balance
	 */


	protected java.lang.String localReward_points_balance;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balanceTracker = false;

	public boolean isReward_points_balanceSpecified()
	{
		return localReward_points_balanceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance()
	{
		return localReward_points_balance;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance
	 */
	public void setReward_points_balance(java.lang.String param)
	{
		localReward_points_balanceTracker = param != null;

		this.localReward_points_balance = param;


	}


	/**
	 * field for Reward_points_balance_refund
	 */


	protected java.lang.String localReward_points_balance_refund;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balance_refundTracker = false;

	public boolean isReward_points_balance_refundSpecified()
	{
		return localReward_points_balance_refundTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance_refund()
	{
		return localReward_points_balance_refund;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance_refund
	 */
	public void setReward_points_balance_refund(java.lang.String param)
	{
		localReward_points_balance_refundTracker = param != null;

		this.localReward_points_balance_refund = param;


	}


	/**
	 * field for Creditmemo_id
	 */


	protected java.lang.String localCreditmemo_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreditmemo_idTracker = false;

	public boolean isCreditmemo_idSpecified()
	{
		return localCreditmemo_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreditmemo_id()
	{
		return localCreditmemo_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Creditmemo_id
	 */
	public void setCreditmemo_id(java.lang.String param)
	{
		localCreditmemo_idTracker = param != null;

		this.localCreditmemo_id = param;


	}


	/**
	 * field for Items
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray localItems;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItemsTracker = false;

	public boolean isItemsSpecified()
	{
		return localItemsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray getItems()
	{
		return localItems;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items
	 */
	public void setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray param)
	{
		localItemsTracker = param != null;

		this.localItems = param;


	}


	/**
	 * field for Comments
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray localComments;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCommentsTracker = false;

	public boolean isCommentsSpecified()
	{
		return localCommentsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray getComments()
	{
		return localComments;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Comments
	 */
	public void setComments(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray param)
	{
		localCommentsTracker = param != null;

		this.localComments = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderCreditmemoEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderCreditmemoEntity", xmlWriter);
			}


		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTransaction_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "transaction_id", xmlWriter);


			if (localTransaction_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("transaction_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTransaction_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGlobal_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "global_currency_code", xmlWriter);


			if (localGlobal_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("global_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGlobal_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_currency_code", xmlWriter);


			if (localBase_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_currency_code", xmlWriter);


			if (localOrder_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_currency_code", xmlWriter);


			if (localStore_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localCybersource_tokenTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cybersource_token", xmlWriter);


			if (localCybersource_token == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cybersource_token cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCybersource_token);

			}

			xmlWriter.writeEndElement();
		}
		if (localInvoice_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "invoice_id", xmlWriter);


			if (localInvoice_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("invoice_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localInvoice_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_address_id", xmlWriter);


			if (localBilling_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_address_id", xmlWriter);


			if (localShipping_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "state", xmlWriter);


			if (localState == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localState);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreditmemo_statusTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "creditmemo_status", xmlWriter);


			if (localCreditmemo_status == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("creditmemo_status cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreditmemo_status);

			}

			xmlWriter.writeEndElement();
		}
		if (localEmail_sentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "email_sent", xmlWriter);


			if (localEmail_sent == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("email_sent cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEmail_sent);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_id", xmlWriter);


			if (localOrder_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_amount", xmlWriter);


			if (localTax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_tax_amount", xmlWriter);


			if (localShipping_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_amount", xmlWriter);


			if (localBase_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_adjustment_positiveTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_adjustment_positive", xmlWriter);


			if (localBase_adjustment_positive == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_adjustment_positive cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_adjustment_positive);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_grand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_grand_total", xmlWriter);


			if (localBase_grand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_grand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdjustmentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "adjustment", xmlWriter);


			if (localAdjustment == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("adjustment cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdjustment);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal", xmlWriter);


			if (localSubtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_amount", xmlWriter);


			if (localDiscount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal", xmlWriter);


			if (localBase_subtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_adjustmentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_adjustment", xmlWriter);


			if (localBase_adjustment == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_adjustment cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_adjustment);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_global_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_global_rate", xmlWriter);


			if (localBase_to_global_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_global_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_global_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_base_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_base_rate", xmlWriter);


			if (localStore_to_base_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_base_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_base_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_amount", xmlWriter);


			if (localBase_shipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdjustment_negativeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "adjustment_negative", xmlWriter);


			if (localAdjustment_negative == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("adjustment_negative cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdjustment_negative);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_incl_tax", xmlWriter);


			if (localSubtotal_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_amount", xmlWriter);


			if (localShipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_incl_tax", xmlWriter);


			if (localBase_subtotal_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_adjustment_negativeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_adjustment_negative", xmlWriter);


			if (localBase_adjustment_negative == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_adjustment_negative cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_adjustment_negative);

			}

			xmlWriter.writeEndElement();
		}
		if (localGrand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "grand_total", xmlWriter);


			if (localGrand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGrand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_amount", xmlWriter);


			if (localBase_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_order_rate", xmlWriter);


			if (localBase_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_order_rate", xmlWriter);


			if (localStore_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_tax_amount", xmlWriter);


			if (localBase_shipping_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdjustment_positiveTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "adjustment_positive", xmlWriter);


			if (localAdjustment_positive == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("adjustment_positive cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdjustment_positive);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);


			if (localStore_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localHidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hidden_tax_amount", xmlWriter);


			if (localHidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_hidden_tax_amount", xmlWriter);


			if (localBase_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_hidden_tax_amount", xmlWriter);


			if (localShipping_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_hidden_tax_amntTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_hidden_tax_amnt", xmlWriter);


			if (localBase_shipping_hidden_tax_amnt == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_hidden_tax_amnt cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_hidden_tax_amnt);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_incl_tax", xmlWriter);


			if (localShipping_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_incl_tax", xmlWriter);


			if (localBase_shipping_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_amount", xmlWriter);


			if (localBase_customer_balance_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_customer_balance_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_amount", xmlWriter);


			if (localCustomer_balance_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_balance_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_balance_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBs_customer_bal_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "bs_customer_bal_total_refunded", xmlWriter);


			if (localBs_customer_bal_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("bs_customer_bal_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBs_customer_bal_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_bal_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_bal_total_refunded", xmlWriter);


			if (localCustomer_bal_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_bal_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_bal_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_gift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_gift_cards_amount", xmlWriter);


			if (localBase_gift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_gift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_amount", xmlWriter);


			if (localGift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_base_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_base_price", xmlWriter);


			if (localGw_base_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_base_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_base_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_price", xmlWriter);


			if (localGw_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_items_base_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_items_base_price", xmlWriter);


			if (localGw_items_base_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_items_base_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_items_base_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_items_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_items_price", xmlWriter);


			if (localGw_items_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_items_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_items_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_card_base_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_card_base_price", xmlWriter);


			if (localGw_card_base_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_card_base_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_card_base_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_card_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_card_price", xmlWriter);


			if (localGw_card_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_card_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_card_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_base_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_base_tax_amount", xmlWriter);


			if (localGw_base_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_base_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_tax_amount", xmlWriter);


			if (localGw_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_items_base_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_items_base_tax_amount", xmlWriter);


			if (localGw_items_base_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_items_base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_items_base_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_items_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_items_tax_amount", xmlWriter);


			if (localGw_items_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_items_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_items_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_card_base_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_card_base_tax_amount", xmlWriter);


			if (localGw_card_base_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_card_base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_card_base_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGw_card_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gw_card_tax_amount", xmlWriter);


			if (localGw_card_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gw_card_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGw_card_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_reward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_reward_currency_amount", xmlWriter);


			if (localBase_reward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_reward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_currency_amount", xmlWriter);


			if (localReward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balanceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance", xmlWriter);


			if (localReward_points_balance == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balance_refundTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance_refund", xmlWriter);


			if (localReward_points_balance_refund == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance_refund cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance_refund);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreditmemo_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "creditmemo_id", xmlWriter);


			if (localCreditmemo_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("creditmemo_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreditmemo_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localItemsTracker)
		{
			if (localItems == null)
			{
				throw new org.apache.axis2.databinding.ADBException("items cannot be null!!");
			}
			localItems.serialize(new javax.xml.namespace.QName("", "items"), xmlWriter);
		}
		if (localCommentsTracker)
		{
			if (localComments == null)
			{
				throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
			}
			localComments.serialize(new javax.xml.namespace.QName("", "comments"), xmlWriter);
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderCreditmemoEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderCreditmemoEntity object = new SalesOrderCreditmemoEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderCreditmemoEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderCreditmemoEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "transaction_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "transaction_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTransaction_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "global_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "global_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGlobal_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cybersource_token").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cybersource_token" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCybersource_token(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "invoice_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "invoice_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setInvoice_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "state").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "state" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setState(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "creditmemo_status").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "creditmemo_status" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreditmemo_status(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "email_sent").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "email_sent" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEmail_sent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_adjustment_positive").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_adjustment_positive" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_adjustment_positive(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_grand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "adjustment").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "adjustment" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdjustment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_adjustment").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_adjustment" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_adjustment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_global_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_global_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_global_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_base_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_base_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_base_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "adjustment_negative").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "adjustment_negative" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdjustment_negative(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_adjustment_negative").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_adjustment_negative" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_adjustment_negative(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGrand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "adjustment_positive").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "adjustment_positive" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdjustment_positive(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_hidden_tax_amnt").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_hidden_tax_amnt" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_hidden_tax_amnt(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "bs_customer_bal_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "bs_customer_bal_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBs_customer_bal_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_bal_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_bal_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_bal_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_gift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_base_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_base_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_base_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_items_base_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_items_base_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_items_base_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_items_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_items_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_items_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_card_base_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_card_base_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_card_base_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_card_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_card_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_card_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_base_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_items_base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_items_base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_items_base_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_items_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_items_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_items_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_card_base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_card_base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_card_base_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gw_card_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gw_card_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGw_card_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_reward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance_refund").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance_refund" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance_refund(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "creditmemo_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "creditmemo_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreditmemo_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items").equals(reader.getName()))
				{

					object.setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoItemEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "comments").equals(reader.getName()))
				{

					object.setComments(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderCreditmemoCommentEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

