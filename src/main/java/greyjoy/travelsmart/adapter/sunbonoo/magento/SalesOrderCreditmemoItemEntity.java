
/**
 * SalesOrderCreditmemoItemEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderCreditmemoItemEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderCreditmemoItemEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderCreditmemoItemEntity Namespace URI = urn:Magento Namespace
	 * Prefix = ns1
	 */


	/**
	 * field for Item_id
	 */


	protected java.lang.String localItem_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItem_idTracker = false;

	public boolean isItem_idSpecified()
	{
		return localItem_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getItem_id()
	{
		return localItem_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Item_id
	 */
	public void setItem_id(java.lang.String param)
	{
		localItem_idTracker = param != null;

		this.localItem_id = param;


	}


	/**
	 * field for Parent_id
	 */


	protected java.lang.String localParent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localParent_idTracker = false;

	public boolean isParent_idSpecified()
	{
		return localParent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getParent_id()
	{
		return localParent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Parent_id
	 */
	public void setParent_id(java.lang.String param)
	{
		localParent_idTracker = param != null;

		this.localParent_id = param;


	}


	/**
	 * field for Weee_tax_applied_row_amount
	 */


	protected java.lang.String localWeee_tax_applied_row_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_applied_row_amountTracker = false;

	public boolean isWeee_tax_applied_row_amountSpecified()
	{
		return localWeee_tax_applied_row_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied_row_amount()
	{
		return localWeee_tax_applied_row_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied_row_amount
	 */
	public void setWeee_tax_applied_row_amount(java.lang.String param)
	{
		localWeee_tax_applied_row_amountTracker = param != null;

		this.localWeee_tax_applied_row_amount = param;


	}


	/**
	 * field for Base_price
	 */


	protected java.lang.String localBase_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_priceTracker = false;

	public boolean isBase_priceSpecified()
	{
		return localBase_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_price()
	{
		return localBase_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_price
	 */
	public void setBase_price(java.lang.String param)
	{
		localBase_priceTracker = param != null;

		this.localBase_price = param;


	}


	/**
	 * field for Base_weee_tax_row_disposition
	 */


	protected java.lang.String localBase_weee_tax_row_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_row_dispositionTracker = false;

	public boolean isBase_weee_tax_row_dispositionSpecified()
	{
		return localBase_weee_tax_row_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_row_disposition()
	{
		return localBase_weee_tax_row_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_row_disposition
	 */
	public void setBase_weee_tax_row_disposition(java.lang.String param)
	{
		localBase_weee_tax_row_dispositionTracker = param != null;

		this.localBase_weee_tax_row_disposition = param;


	}


	/**
	 * field for Tax_amount
	 */


	protected java.lang.String localTax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_amountTracker = false;

	public boolean isTax_amountSpecified()
	{
		return localTax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_amount()
	{
		return localTax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_amount
	 */
	public void setTax_amount(java.lang.String param)
	{
		localTax_amountTracker = param != null;

		this.localTax_amount = param;


	}


	/**
	 * field for Base_weee_tax_applied_amount
	 */


	protected java.lang.String localBase_weee_tax_applied_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_applied_amountTracker = false;

	public boolean isBase_weee_tax_applied_amountSpecified()
	{
		return localBase_weee_tax_applied_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_applied_amount()
	{
		return localBase_weee_tax_applied_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_applied_amount
	 */
	public void setBase_weee_tax_applied_amount(java.lang.String param)
	{
		localBase_weee_tax_applied_amountTracker = param != null;

		this.localBase_weee_tax_applied_amount = param;


	}


	/**
	 * field for Weee_tax_row_disposition
	 */


	protected java.lang.String localWeee_tax_row_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_row_dispositionTracker = false;

	public boolean isWeee_tax_row_dispositionSpecified()
	{
		return localWeee_tax_row_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_row_disposition()
	{
		return localWeee_tax_row_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_row_disposition
	 */
	public void setWeee_tax_row_disposition(java.lang.String param)
	{
		localWeee_tax_row_dispositionTracker = param != null;

		this.localWeee_tax_row_disposition = param;


	}


	/**
	 * field for Base_row_total
	 */


	protected java.lang.String localBase_row_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_row_totalTracker = false;

	public boolean isBase_row_totalSpecified()
	{
		return localBase_row_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_row_total()
	{
		return localBase_row_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_row_total
	 */
	public void setBase_row_total(java.lang.String param)
	{
		localBase_row_totalTracker = param != null;

		this.localBase_row_total = param;


	}


	/**
	 * field for Discount_amount
	 */


	protected java.lang.String localDiscount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_amountTracker = false;

	public boolean isDiscount_amountSpecified()
	{
		return localDiscount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_amount()
	{
		return localDiscount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_amount
	 */
	public void setDiscount_amount(java.lang.String param)
	{
		localDiscount_amountTracker = param != null;

		this.localDiscount_amount = param;


	}


	/**
	 * field for Row_total
	 */


	protected java.lang.String localRow_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRow_totalTracker = false;

	public boolean isRow_totalSpecified()
	{
		return localRow_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRow_total()
	{
		return localRow_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Row_total
	 */
	public void setRow_total(java.lang.String param)
	{
		localRow_totalTracker = param != null;

		this.localRow_total = param;


	}


	/**
	 * field for Weee_tax_applied_amount
	 */


	protected java.lang.String localWeee_tax_applied_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_applied_amountTracker = false;

	public boolean isWeee_tax_applied_amountSpecified()
	{
		return localWeee_tax_applied_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied_amount()
	{
		return localWeee_tax_applied_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied_amount
	 */
	public void setWeee_tax_applied_amount(java.lang.String param)
	{
		localWeee_tax_applied_amountTracker = param != null;

		this.localWeee_tax_applied_amount = param;


	}


	/**
	 * field for Base_discount_amount
	 */


	protected java.lang.String localBase_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_amountTracker = false;

	public boolean isBase_discount_amountSpecified()
	{
		return localBase_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_amount()
	{
		return localBase_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_amount
	 */
	public void setBase_discount_amount(java.lang.String param)
	{
		localBase_discount_amountTracker = param != null;

		this.localBase_discount_amount = param;


	}


	/**
	 * field for Base_weee_tax_disposition
	 */


	protected java.lang.String localBase_weee_tax_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_dispositionTracker = false;

	public boolean isBase_weee_tax_dispositionSpecified()
	{
		return localBase_weee_tax_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_disposition()
	{
		return localBase_weee_tax_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_disposition
	 */
	public void setBase_weee_tax_disposition(java.lang.String param)
	{
		localBase_weee_tax_dispositionTracker = param != null;

		this.localBase_weee_tax_disposition = param;


	}


	/**
	 * field for Price_incl_tax
	 */


	protected java.lang.String localPrice_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPrice_incl_taxTracker = false;

	public boolean isPrice_incl_taxSpecified()
	{
		return localPrice_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrice_incl_tax()
	{
		return localPrice_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Price_incl_tax
	 */
	public void setPrice_incl_tax(java.lang.String param)
	{
		localPrice_incl_taxTracker = param != null;

		this.localPrice_incl_tax = param;


	}


	/**
	 * field for Base_tax_amount
	 */


	protected java.lang.String localBase_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_amountTracker = false;

	public boolean isBase_tax_amountSpecified()
	{
		return localBase_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_amount()
	{
		return localBase_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_amount
	 */
	public void setBase_tax_amount(java.lang.String param)
	{
		localBase_tax_amountTracker = param != null;

		this.localBase_tax_amount = param;


	}


	/**
	 * field for Weee_tax_disposition
	 */


	protected java.lang.String localWeee_tax_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_dispositionTracker = false;

	public boolean isWeee_tax_dispositionSpecified()
	{
		return localWeee_tax_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_disposition()
	{
		return localWeee_tax_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_disposition
	 */
	public void setWeee_tax_disposition(java.lang.String param)
	{
		localWeee_tax_dispositionTracker = param != null;

		this.localWeee_tax_disposition = param;


	}


	/**
	 * field for Base_price_incl_tax
	 */


	protected java.lang.String localBase_price_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_price_incl_taxTracker = false;

	public boolean isBase_price_incl_taxSpecified()
	{
		return localBase_price_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_price_incl_tax()
	{
		return localBase_price_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_price_incl_tax
	 */
	public void setBase_price_incl_tax(java.lang.String param)
	{
		localBase_price_incl_taxTracker = param != null;

		this.localBase_price_incl_tax = param;


	}


	/**
	 * field for Qty
	 */


	protected java.lang.String localQty;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQtyTracker = false;

	public boolean isQtySpecified()
	{
		return localQtyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty()
	{
		return localQty;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty
	 */
	public void setQty(java.lang.String param)
	{
		localQtyTracker = param != null;

		this.localQty = param;


	}


	/**
	 * field for Base_cost
	 */


	protected java.lang.String localBase_cost;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_costTracker = false;

	public boolean isBase_costSpecified()
	{
		return localBase_costTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_cost()
	{
		return localBase_cost;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_cost
	 */
	public void setBase_cost(java.lang.String param)
	{
		localBase_costTracker = param != null;

		this.localBase_cost = param;


	}


	/**
	 * field for Base_weee_tax_applied_row_amount
	 */


	protected java.lang.String localBase_weee_tax_applied_row_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_applied_row_amountTracker = false;

	public boolean isBase_weee_tax_applied_row_amountSpecified()
	{
		return localBase_weee_tax_applied_row_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_applied_row_amount()
	{
		return localBase_weee_tax_applied_row_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_applied_row_amount
	 */
	public void setBase_weee_tax_applied_row_amount(java.lang.String param)
	{
		localBase_weee_tax_applied_row_amountTracker = param != null;

		this.localBase_weee_tax_applied_row_amount = param;


	}


	/**
	 * field for Price
	 */


	protected java.lang.String localPrice;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPriceTracker = false;

	public boolean isPriceSpecified()
	{
		return localPriceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrice()
	{
		return localPrice;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Price
	 */
	public void setPrice(java.lang.String param)
	{
		localPriceTracker = param != null;

		this.localPrice = param;


	}


	/**
	 * field for Base_row_total_incl_tax
	 */


	protected java.lang.String localBase_row_total_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_row_total_incl_taxTracker = false;

	public boolean isBase_row_total_incl_taxSpecified()
	{
		return localBase_row_total_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_row_total_incl_tax()
	{
		return localBase_row_total_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_row_total_incl_tax
	 */
	public void setBase_row_total_incl_tax(java.lang.String param)
	{
		localBase_row_total_incl_taxTracker = param != null;

		this.localBase_row_total_incl_tax = param;


	}


	/**
	 * field for Row_total_incl_tax
	 */


	protected java.lang.String localRow_total_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRow_total_incl_taxTracker = false;

	public boolean isRow_total_incl_taxSpecified()
	{
		return localRow_total_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRow_total_incl_tax()
	{
		return localRow_total_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Row_total_incl_tax
	 */
	public void setRow_total_incl_tax(java.lang.String param)
	{
		localRow_total_incl_taxTracker = param != null;

		this.localRow_total_incl_tax = param;


	}


	/**
	 * field for Product_id
	 */


	protected java.lang.String localProduct_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProduct_idTracker = false;

	public boolean isProduct_idSpecified()
	{
		return localProduct_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProduct_id()
	{
		return localProduct_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Product_id
	 */
	public void setProduct_id(java.lang.String param)
	{
		localProduct_idTracker = param != null;

		this.localProduct_id = param;


	}


	/**
	 * field for Order_item_id
	 */


	protected java.lang.String localOrder_item_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_item_idTracker = false;

	public boolean isOrder_item_idSpecified()
	{
		return localOrder_item_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_item_id()
	{
		return localOrder_item_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_item_id
	 */
	public void setOrder_item_id(java.lang.String param)
	{
		localOrder_item_idTracker = param != null;

		this.localOrder_item_id = param;


	}


	/**
	 * field for Additional_data
	 */


	protected java.lang.String localAdditional_data;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdditional_dataTracker = false;

	public boolean isAdditional_dataSpecified()
	{
		return localAdditional_dataTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdditional_data()
	{
		return localAdditional_data;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Additional_data
	 */
	public void setAdditional_data(java.lang.String param)
	{
		localAdditional_dataTracker = param != null;

		this.localAdditional_data = param;


	}


	/**
	 * field for Description
	 */


	protected java.lang.String localDescription;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDescriptionTracker = false;

	public boolean isDescriptionSpecified()
	{
		return localDescriptionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDescription()
	{
		return localDescription;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Description
	 */
	public void setDescription(java.lang.String param)
	{
		localDescriptionTracker = param != null;

		this.localDescription = param;


	}


	/**
	 * field for Weee_tax_applied
	 */


	protected java.lang.String localWeee_tax_applied;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_appliedTracker = false;

	public boolean isWeee_tax_appliedSpecified()
	{
		return localWeee_tax_appliedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied()
	{
		return localWeee_tax_applied;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied
	 */
	public void setWeee_tax_applied(java.lang.String param)
	{
		localWeee_tax_appliedTracker = param != null;

		this.localWeee_tax_applied = param;


	}


	/**
	 * field for Sku
	 */


	protected java.lang.String localSku;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSkuTracker = false;

	public boolean isSkuSpecified()
	{
		return localSkuTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSku()
	{
		return localSku;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Sku
	 */
	public void setSku(java.lang.String param)
	{
		localSkuTracker = param != null;

		this.localSku = param;


	}


	/**
	 * field for Name
	 */


	protected java.lang.String localName;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localNameTracker = false;

	public boolean isNameSpecified()
	{
		return localNameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getName()
	{
		return localName;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Name
	 */
	public void setName(java.lang.String param)
	{
		localNameTracker = param != null;

		this.localName = param;


	}


	/**
	 * field for Hidden_tax_amount
	 */


	protected java.lang.String localHidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHidden_tax_amountTracker = false;

	public boolean isHidden_tax_amountSpecified()
	{
		return localHidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHidden_tax_amount()
	{
		return localHidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hidden_tax_amount
	 */
	public void setHidden_tax_amount(java.lang.String param)
	{
		localHidden_tax_amountTracker = param != null;

		this.localHidden_tax_amount = param;


	}


	/**
	 * field for Base_hidden_tax_amount
	 */


	protected java.lang.String localBase_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_hidden_tax_amountTracker = false;

	public boolean isBase_hidden_tax_amountSpecified()
	{
		return localBase_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_hidden_tax_amount()
	{
		return localBase_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_hidden_tax_amount
	 */
	public void setBase_hidden_tax_amount(java.lang.String param)
	{
		localBase_hidden_tax_amountTracker = param != null;

		this.localBase_hidden_tax_amount = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderCreditmemoItemEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderCreditmemoItemEntity", xmlWriter);
			}


		}
		if (localItem_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "item_id", xmlWriter);


			if (localItem_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localItem_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localParent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "parent_id", xmlWriter);


			if (localParent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localParent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_applied_row_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied_row_amount", xmlWriter);


			if (localWeee_tax_applied_row_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied_row_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied_row_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_price", xmlWriter);


			if (localBase_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_row_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_row_disposition", xmlWriter);


			if (localBase_weee_tax_row_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_row_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_row_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_amount", xmlWriter);


			if (localTax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_applied_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_applied_amount", xmlWriter);


			if (localBase_weee_tax_applied_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_applied_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_applied_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_row_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_row_disposition", xmlWriter);


			if (localWeee_tax_row_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_row_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_row_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_row_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_row_total", xmlWriter);


			if (localBase_row_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_row_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_row_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_amount", xmlWriter);


			if (localDiscount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localRow_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "row_total", xmlWriter);


			if (localRow_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("row_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRow_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_applied_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied_amount", xmlWriter);


			if (localWeee_tax_applied_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_amount", xmlWriter);


			if (localBase_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_disposition", xmlWriter);


			if (localBase_weee_tax_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localPrice_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "price_incl_tax", xmlWriter);


			if (localPrice_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("price_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrice_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_amount", xmlWriter);


			if (localBase_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_disposition", xmlWriter);


			if (localWeee_tax_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_price_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_price_incl_tax", xmlWriter);


			if (localBase_price_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_price_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_price_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localQtyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty", xmlWriter);


			if (localQty == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_costTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_cost", xmlWriter);


			if (localBase_cost == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_cost cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_cost);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_applied_row_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_applied_row_amount", xmlWriter);


			if (localBase_weee_tax_applied_row_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_applied_row_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_applied_row_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localPriceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "price", xmlWriter);


			if (localPrice == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrice);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_row_total_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_row_total_incl_tax", xmlWriter);


			if (localBase_row_total_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_row_total_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_row_total_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localRow_total_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "row_total_incl_tax", xmlWriter);


			if (localRow_total_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("row_total_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRow_total_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localProduct_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "product_id", xmlWriter);


			if (localProduct_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("product_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProduct_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_item_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_item_id", xmlWriter);


			if (localOrder_item_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_item_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdditional_dataTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "additional_data", xmlWriter);


			if (localAdditional_data == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("additional_data cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdditional_data);

			}

			xmlWriter.writeEndElement();
		}
		if (localDescriptionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "description", xmlWriter);


			if (localDescription == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDescription);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_appliedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied", xmlWriter);


			if (localWeee_tax_applied == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied);

			}

			xmlWriter.writeEndElement();
		}
		if (localSkuTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "sku", xmlWriter);


			if (localSku == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("sku cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSku);

			}

			xmlWriter.writeEndElement();
		}
		if (localNameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "name", xmlWriter);


			if (localName == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localName);

			}

			xmlWriter.writeEndElement();
		}
		if (localHidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hidden_tax_amount", xmlWriter);


			if (localHidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_hidden_tax_amount", xmlWriter);


			if (localBase_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderCreditmemoItemEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderCreditmemoItemEntity object = new SalesOrderCreditmemoItemEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderCreditmemoItemEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderCreditmemoItemEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setItem_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setParent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied_row_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied_row_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied_row_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_row_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_row_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_row_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_applied_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_applied_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_applied_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_row_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_row_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_row_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_row_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_row_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_row_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "row_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "row_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRow_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "price_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "price_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrice_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_price_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_price_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_price_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_cost").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_cost" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_cost(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_applied_row_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_applied_row_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_applied_row_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_row_total_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_row_total_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_row_total_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "row_total_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "row_total_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRow_total_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "product_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "product_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProduct_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_item_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "additional_data").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "additional_data" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdditional_data(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "description").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "description" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "sku").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "sku" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSku(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

