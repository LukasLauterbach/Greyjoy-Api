
/**
 * SalesOrderEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderEntity Namespace URI = urn:Magento Namespace Prefix = ns1
	 */


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Parent_id
	 */


	protected java.lang.String localParent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localParent_idTracker = false;

	public boolean isParent_idSpecified()
	{
		return localParent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getParent_id()
	{
		return localParent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Parent_id
	 */
	public void setParent_id(java.lang.String param)
	{
		localParent_idTracker = param != null;

		this.localParent_id = param;


	}


	/**
	 * field for Store_id
	 */


	protected java.lang.String localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(java.lang.String param)
	{
		localStore_idTracker = param != null;

		this.localStore_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Is_active
	 */


	protected java.lang.String localIs_active;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_activeTracker = false;

	public boolean isIs_activeSpecified()
	{
		return localIs_activeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_active()
	{
		return localIs_active;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_active
	 */
	public void setIs_active(java.lang.String param)
	{
		localIs_activeTracker = param != null;

		this.localIs_active = param;


	}


	/**
	 * field for Customer_id
	 */


	protected java.lang.String localCustomer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_idTracker = false;

	public boolean isCustomer_idSpecified()
	{
		return localCustomer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_id()
	{
		return localCustomer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_id
	 */
	public void setCustomer_id(java.lang.String param)
	{
		localCustomer_idTracker = param != null;

		this.localCustomer_id = param;


	}


	/**
	 * field for Tax_amount
	 */


	protected java.lang.String localTax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_amountTracker = false;

	public boolean isTax_amountSpecified()
	{
		return localTax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_amount()
	{
		return localTax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_amount
	 */
	public void setTax_amount(java.lang.String param)
	{
		localTax_amountTracker = param != null;

		this.localTax_amount = param;


	}


	/**
	 * field for Shipping_amount
	 */


	protected java.lang.String localShipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_amountTracker = false;

	public boolean isShipping_amountSpecified()
	{
		return localShipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_amount()
	{
		return localShipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_amount
	 */
	public void setShipping_amount(java.lang.String param)
	{
		localShipping_amountTracker = param != null;

		this.localShipping_amount = param;


	}


	/**
	 * field for Discount_amount
	 */


	protected java.lang.String localDiscount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_amountTracker = false;

	public boolean isDiscount_amountSpecified()
	{
		return localDiscount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_amount()
	{
		return localDiscount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_amount
	 */
	public void setDiscount_amount(java.lang.String param)
	{
		localDiscount_amountTracker = param != null;

		this.localDiscount_amount = param;


	}


	/**
	 * field for Subtotal
	 */


	protected java.lang.String localSubtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotalTracker = false;

	public boolean isSubtotalSpecified()
	{
		return localSubtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal()
	{
		return localSubtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal
	 */
	public void setSubtotal(java.lang.String param)
	{
		localSubtotalTracker = param != null;

		this.localSubtotal = param;


	}


	/**
	 * field for Grand_total
	 */


	protected java.lang.String localGrand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGrand_totalTracker = false;

	public boolean isGrand_totalSpecified()
	{
		return localGrand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGrand_total()
	{
		return localGrand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Grand_total
	 */
	public void setGrand_total(java.lang.String param)
	{
		localGrand_totalTracker = param != null;

		this.localGrand_total = param;


	}


	/**
	 * field for Total_paid
	 */


	protected java.lang.String localTotal_paid;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_paidTracker = false;

	public boolean isTotal_paidSpecified()
	{
		return localTotal_paidTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_paid()
	{
		return localTotal_paid;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_paid
	 */
	public void setTotal_paid(java.lang.String param)
	{
		localTotal_paidTracker = param != null;

		this.localTotal_paid = param;


	}


	/**
	 * field for Total_refunded
	 */


	protected java.lang.String localTotal_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_refundedTracker = false;

	public boolean isTotal_refundedSpecified()
	{
		return localTotal_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_refunded()
	{
		return localTotal_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_refunded
	 */
	public void setTotal_refunded(java.lang.String param)
	{
		localTotal_refundedTracker = param != null;

		this.localTotal_refunded = param;


	}


	/**
	 * field for Total_qty_ordered
	 */


	protected java.lang.String localTotal_qty_ordered;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_qty_orderedTracker = false;

	public boolean isTotal_qty_orderedSpecified()
	{
		return localTotal_qty_orderedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_qty_ordered()
	{
		return localTotal_qty_ordered;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_qty_ordered
	 */
	public void setTotal_qty_ordered(java.lang.String param)
	{
		localTotal_qty_orderedTracker = param != null;

		this.localTotal_qty_ordered = param;


	}


	/**
	 * field for Total_canceled
	 */


	protected java.lang.String localTotal_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_canceledTracker = false;

	public boolean isTotal_canceledSpecified()
	{
		return localTotal_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_canceled()
	{
		return localTotal_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_canceled
	 */
	public void setTotal_canceled(java.lang.String param)
	{
		localTotal_canceledTracker = param != null;

		this.localTotal_canceled = param;


	}


	/**
	 * field for Total_invoiced
	 */


	protected java.lang.String localTotal_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_invoicedTracker = false;

	public boolean isTotal_invoicedSpecified()
	{
		return localTotal_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_invoiced()
	{
		return localTotal_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_invoiced
	 */
	public void setTotal_invoiced(java.lang.String param)
	{
		localTotal_invoicedTracker = param != null;

		this.localTotal_invoiced = param;


	}


	/**
	 * field for Total_online_refunded
	 */


	protected java.lang.String localTotal_online_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_online_refundedTracker = false;

	public boolean isTotal_online_refundedSpecified()
	{
		return localTotal_online_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_online_refunded()
	{
		return localTotal_online_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_online_refunded
	 */
	public void setTotal_online_refunded(java.lang.String param)
	{
		localTotal_online_refundedTracker = param != null;

		this.localTotal_online_refunded = param;


	}


	/**
	 * field for Total_offline_refunded
	 */


	protected java.lang.String localTotal_offline_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_offline_refundedTracker = false;

	public boolean isTotal_offline_refundedSpecified()
	{
		return localTotal_offline_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_offline_refunded()
	{
		return localTotal_offline_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_offline_refunded
	 */
	public void setTotal_offline_refunded(java.lang.String param)
	{
		localTotal_offline_refundedTracker = param != null;

		this.localTotal_offline_refunded = param;


	}


	/**
	 * field for Base_tax_amount
	 */


	protected java.lang.String localBase_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_amountTracker = false;

	public boolean isBase_tax_amountSpecified()
	{
		return localBase_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_amount()
	{
		return localBase_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_amount
	 */
	public void setBase_tax_amount(java.lang.String param)
	{
		localBase_tax_amountTracker = param != null;

		this.localBase_tax_amount = param;


	}


	/**
	 * field for Base_shipping_amount
	 */


	protected java.lang.String localBase_shipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_amountTracker = false;

	public boolean isBase_shipping_amountSpecified()
	{
		return localBase_shipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_amount()
	{
		return localBase_shipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_amount
	 */
	public void setBase_shipping_amount(java.lang.String param)
	{
		localBase_shipping_amountTracker = param != null;

		this.localBase_shipping_amount = param;


	}


	/**
	 * field for Base_discount_amount
	 */


	protected java.lang.String localBase_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_amountTracker = false;

	public boolean isBase_discount_amountSpecified()
	{
		return localBase_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_amount()
	{
		return localBase_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_amount
	 */
	public void setBase_discount_amount(java.lang.String param)
	{
		localBase_discount_amountTracker = param != null;

		this.localBase_discount_amount = param;


	}


	/**
	 * field for Base_subtotal
	 */


	protected java.lang.String localBase_subtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotalTracker = false;

	public boolean isBase_subtotalSpecified()
	{
		return localBase_subtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal()
	{
		return localBase_subtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal
	 */
	public void setBase_subtotal(java.lang.String param)
	{
		localBase_subtotalTracker = param != null;

		this.localBase_subtotal = param;


	}


	/**
	 * field for Base_grand_total
	 */


	protected java.lang.String localBase_grand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_grand_totalTracker = false;

	public boolean isBase_grand_totalSpecified()
	{
		return localBase_grand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_grand_total()
	{
		return localBase_grand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_grand_total
	 */
	public void setBase_grand_total(java.lang.String param)
	{
		localBase_grand_totalTracker = param != null;

		this.localBase_grand_total = param;


	}


	/**
	 * field for Base_total_paid
	 */


	protected java.lang.String localBase_total_paid;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_paidTracker = false;

	public boolean isBase_total_paidSpecified()
	{
		return localBase_total_paidTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_paid()
	{
		return localBase_total_paid;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_paid
	 */
	public void setBase_total_paid(java.lang.String param)
	{
		localBase_total_paidTracker = param != null;

		this.localBase_total_paid = param;


	}


	/**
	 * field for Base_total_refunded
	 */


	protected java.lang.String localBase_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_refundedTracker = false;

	public boolean isBase_total_refundedSpecified()
	{
		return localBase_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_refunded()
	{
		return localBase_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_refunded
	 */
	public void setBase_total_refunded(java.lang.String param)
	{
		localBase_total_refundedTracker = param != null;

		this.localBase_total_refunded = param;


	}


	/**
	 * field for Base_total_qty_ordered
	 */


	protected java.lang.String localBase_total_qty_ordered;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_qty_orderedTracker = false;

	public boolean isBase_total_qty_orderedSpecified()
	{
		return localBase_total_qty_orderedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_qty_ordered()
	{
		return localBase_total_qty_ordered;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_qty_ordered
	 */
	public void setBase_total_qty_ordered(java.lang.String param)
	{
		localBase_total_qty_orderedTracker = param != null;

		this.localBase_total_qty_ordered = param;


	}


	/**
	 * field for Base_total_canceled
	 */


	protected java.lang.String localBase_total_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_canceledTracker = false;

	public boolean isBase_total_canceledSpecified()
	{
		return localBase_total_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_canceled()
	{
		return localBase_total_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_canceled
	 */
	public void setBase_total_canceled(java.lang.String param)
	{
		localBase_total_canceledTracker = param != null;

		this.localBase_total_canceled = param;


	}


	/**
	 * field for Base_total_invoiced
	 */


	protected java.lang.String localBase_total_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_invoicedTracker = false;

	public boolean isBase_total_invoicedSpecified()
	{
		return localBase_total_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_invoiced()
	{
		return localBase_total_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_invoiced
	 */
	public void setBase_total_invoiced(java.lang.String param)
	{
		localBase_total_invoicedTracker = param != null;

		this.localBase_total_invoiced = param;


	}


	/**
	 * field for Base_total_online_refunded
	 */


	protected java.lang.String localBase_total_online_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_online_refundedTracker = false;

	public boolean isBase_total_online_refundedSpecified()
	{
		return localBase_total_online_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_online_refunded()
	{
		return localBase_total_online_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_online_refunded
	 */
	public void setBase_total_online_refunded(java.lang.String param)
	{
		localBase_total_online_refundedTracker = param != null;

		this.localBase_total_online_refunded = param;


	}


	/**
	 * field for Base_total_offline_refunded
	 */


	protected java.lang.String localBase_total_offline_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_offline_refundedTracker = false;

	public boolean isBase_total_offline_refundedSpecified()
	{
		return localBase_total_offline_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_offline_refunded()
	{
		return localBase_total_offline_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_offline_refunded
	 */
	public void setBase_total_offline_refunded(java.lang.String param)
	{
		localBase_total_offline_refundedTracker = param != null;

		this.localBase_total_offline_refunded = param;


	}


	/**
	 * field for Billing_address_id
	 */


	protected java.lang.String localBilling_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_address_idTracker = false;

	public boolean isBilling_address_idSpecified()
	{
		return localBilling_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_address_id()
	{
		return localBilling_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_address_id
	 */
	public void setBilling_address_id(java.lang.String param)
	{
		localBilling_address_idTracker = param != null;

		this.localBilling_address_id = param;


	}


	/**
	 * field for Billing_firstname
	 */


	protected java.lang.String localBilling_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_firstnameTracker = false;

	public boolean isBilling_firstnameSpecified()
	{
		return localBilling_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_firstname()
	{
		return localBilling_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_firstname
	 */
	public void setBilling_firstname(java.lang.String param)
	{
		localBilling_firstnameTracker = param != null;

		this.localBilling_firstname = param;


	}


	/**
	 * field for Billing_lastname
	 */


	protected java.lang.String localBilling_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_lastnameTracker = false;

	public boolean isBilling_lastnameSpecified()
	{
		return localBilling_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_lastname()
	{
		return localBilling_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_lastname
	 */
	public void setBilling_lastname(java.lang.String param)
	{
		localBilling_lastnameTracker = param != null;

		this.localBilling_lastname = param;


	}


	/**
	 * field for Shipping_address_id
	 */


	protected java.lang.String localShipping_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_address_idTracker = false;

	public boolean isShipping_address_idSpecified()
	{
		return localShipping_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_address_id()
	{
		return localShipping_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address_id
	 */
	public void setShipping_address_id(java.lang.String param)
	{
		localShipping_address_idTracker = param != null;

		this.localShipping_address_id = param;


	}


	/**
	 * field for Shipping_firstname
	 */


	protected java.lang.String localShipping_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_firstnameTracker = false;

	public boolean isShipping_firstnameSpecified()
	{
		return localShipping_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_firstname()
	{
		return localShipping_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_firstname
	 */
	public void setShipping_firstname(java.lang.String param)
	{
		localShipping_firstnameTracker = param != null;

		this.localShipping_firstname = param;


	}


	/**
	 * field for Shipping_lastname
	 */


	protected java.lang.String localShipping_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_lastnameTracker = false;

	public boolean isShipping_lastnameSpecified()
	{
		return localShipping_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_lastname()
	{
		return localShipping_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_lastname
	 */
	public void setShipping_lastname(java.lang.String param)
	{
		localShipping_lastnameTracker = param != null;

		this.localShipping_lastname = param;


	}


	/**
	 * field for Billing_name
	 */


	protected java.lang.String localBilling_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_nameTracker = false;

	public boolean isBilling_nameSpecified()
	{
		return localBilling_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_name()
	{
		return localBilling_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_name
	 */
	public void setBilling_name(java.lang.String param)
	{
		localBilling_nameTracker = param != null;

		this.localBilling_name = param;


	}


	/**
	 * field for Shipping_name
	 */


	protected java.lang.String localShipping_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_nameTracker = false;

	public boolean isShipping_nameSpecified()
	{
		return localShipping_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_name()
	{
		return localShipping_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_name
	 */
	public void setShipping_name(java.lang.String param)
	{
		localShipping_nameTracker = param != null;

		this.localShipping_name = param;


	}


	/**
	 * field for Store_to_base_rate
	 */


	protected java.lang.String localStore_to_base_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_base_rateTracker = false;

	public boolean isStore_to_base_rateSpecified()
	{
		return localStore_to_base_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_base_rate()
	{
		return localStore_to_base_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_base_rate
	 */
	public void setStore_to_base_rate(java.lang.String param)
	{
		localStore_to_base_rateTracker = param != null;

		this.localStore_to_base_rate = param;


	}


	/**
	 * field for Store_to_order_rate
	 */


	protected java.lang.String localStore_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_order_rateTracker = false;

	public boolean isStore_to_order_rateSpecified()
	{
		return localStore_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_order_rate()
	{
		return localStore_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_order_rate
	 */
	public void setStore_to_order_rate(java.lang.String param)
	{
		localStore_to_order_rateTracker = param != null;

		this.localStore_to_order_rate = param;


	}


	/**
	 * field for Base_to_global_rate
	 */


	protected java.lang.String localBase_to_global_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_global_rateTracker = false;

	public boolean isBase_to_global_rateSpecified()
	{
		return localBase_to_global_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_global_rate()
	{
		return localBase_to_global_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_global_rate
	 */
	public void setBase_to_global_rate(java.lang.String param)
	{
		localBase_to_global_rateTracker = param != null;

		this.localBase_to_global_rate = param;


	}


	/**
	 * field for Base_to_order_rate
	 */


	protected java.lang.String localBase_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_order_rateTracker = false;

	public boolean isBase_to_order_rateSpecified()
	{
		return localBase_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_order_rate()
	{
		return localBase_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_order_rate
	 */
	public void setBase_to_order_rate(java.lang.String param)
	{
		localBase_to_order_rateTracker = param != null;

		this.localBase_to_order_rate = param;


	}


	/**
	 * field for Weight
	 */


	protected java.lang.String localWeight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeightTracker = false;

	public boolean isWeightSpecified()
	{
		return localWeightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeight()
	{
		return localWeight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weight
	 */
	public void setWeight(java.lang.String param)
	{
		localWeightTracker = param != null;

		this.localWeight = param;


	}


	/**
	 * field for Store_name
	 */


	protected java.lang.String localStore_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_nameTracker = false;

	public boolean isStore_nameSpecified()
	{
		return localStore_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_name()
	{
		return localStore_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_name
	 */
	public void setStore_name(java.lang.String param)
	{
		localStore_nameTracker = param != null;

		this.localStore_name = param;


	}


	/**
	 * field for Remote_ip
	 */


	protected java.lang.String localRemote_ip;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRemote_ipTracker = false;

	public boolean isRemote_ipSpecified()
	{
		return localRemote_ipTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRemote_ip()
	{
		return localRemote_ip;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Remote_ip
	 */
	public void setRemote_ip(java.lang.String param)
	{
		localRemote_ipTracker = param != null;

		this.localRemote_ip = param;


	}


	/**
	 * field for Status
	 */


	protected java.lang.String localStatus;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStatusTracker = false;

	public boolean isStatusSpecified()
	{
		return localStatusTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStatus()
	{
		return localStatus;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Status
	 */
	public void setStatus(java.lang.String param)
	{
		localStatusTracker = param != null;

		this.localStatus = param;


	}


	/**
	 * field for State
	 */


	protected java.lang.String localState;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStateTracker = false;

	public boolean isStateSpecified()
	{
		return localStateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getState()
	{
		return localState;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            State
	 */
	public void setState(java.lang.String param)
	{
		localStateTracker = param != null;

		this.localState = param;


	}


	/**
	 * field for Applied_rule_ids
	 */


	protected java.lang.String localApplied_rule_ids;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localApplied_rule_idsTracker = false;

	public boolean isApplied_rule_idsSpecified()
	{
		return localApplied_rule_idsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getApplied_rule_ids()
	{
		return localApplied_rule_ids;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Applied_rule_ids
	 */
	public void setApplied_rule_ids(java.lang.String param)
	{
		localApplied_rule_idsTracker = param != null;

		this.localApplied_rule_ids = param;


	}


	/**
	 * field for Global_currency_code
	 */


	protected java.lang.String localGlobal_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGlobal_currency_codeTracker = false;

	public boolean isGlobal_currency_codeSpecified()
	{
		return localGlobal_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGlobal_currency_code()
	{
		return localGlobal_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Global_currency_code
	 */
	public void setGlobal_currency_code(java.lang.String param)
	{
		localGlobal_currency_codeTracker = param != null;

		this.localGlobal_currency_code = param;


	}


	/**
	 * field for Base_currency_code
	 */


	protected java.lang.String localBase_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_currency_codeTracker = false;

	public boolean isBase_currency_codeSpecified()
	{
		return localBase_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_currency_code()
	{
		return localBase_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_currency_code
	 */
	public void setBase_currency_code(java.lang.String param)
	{
		localBase_currency_codeTracker = param != null;

		this.localBase_currency_code = param;


	}


	/**
	 * field for Store_currency_code
	 */


	protected java.lang.String localStore_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_currency_codeTracker = false;

	public boolean isStore_currency_codeSpecified()
	{
		return localStore_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_currency_code()
	{
		return localStore_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_currency_code
	 */
	public void setStore_currency_code(java.lang.String param)
	{
		localStore_currency_codeTracker = param != null;

		this.localStore_currency_code = param;


	}


	/**
	 * field for Order_currency_code
	 */


	protected java.lang.String localOrder_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_currency_codeTracker = false;

	public boolean isOrder_currency_codeSpecified()
	{
		return localOrder_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_currency_code()
	{
		return localOrder_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_currency_code
	 */
	public void setOrder_currency_code(java.lang.String param)
	{
		localOrder_currency_codeTracker = param != null;

		this.localOrder_currency_code = param;


	}


	/**
	 * field for Shipping_method
	 */


	protected java.lang.String localShipping_method;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_methodTracker = false;

	public boolean isShipping_methodSpecified()
	{
		return localShipping_methodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_method()
	{
		return localShipping_method;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_method
	 */
	public void setShipping_method(java.lang.String param)
	{
		localShipping_methodTracker = param != null;

		this.localShipping_method = param;


	}


	/**
	 * field for Shipping_description
	 */


	protected java.lang.String localShipping_description;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_descriptionTracker = false;

	public boolean isShipping_descriptionSpecified()
	{
		return localShipping_descriptionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_description()
	{
		return localShipping_description;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_description
	 */
	public void setShipping_description(java.lang.String param)
	{
		localShipping_descriptionTracker = param != null;

		this.localShipping_description = param;


	}


	/**
	 * field for Customer_email
	 */


	protected java.lang.String localCustomer_email;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_emailTracker = false;

	public boolean isCustomer_emailSpecified()
	{
		return localCustomer_emailTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_email()
	{
		return localCustomer_email;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_email
	 */
	public void setCustomer_email(java.lang.String param)
	{
		localCustomer_emailTracker = param != null;

		this.localCustomer_email = param;


	}


	/**
	 * field for Customer_firstname
	 */


	protected java.lang.String localCustomer_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_firstnameTracker = false;

	public boolean isCustomer_firstnameSpecified()
	{
		return localCustomer_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_firstname()
	{
		return localCustomer_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_firstname
	 */
	public void setCustomer_firstname(java.lang.String param)
	{
		localCustomer_firstnameTracker = param != null;

		this.localCustomer_firstname = param;


	}


	/**
	 * field for Customer_lastname
	 */


	protected java.lang.String localCustomer_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_lastnameTracker = false;

	public boolean isCustomer_lastnameSpecified()
	{
		return localCustomer_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_lastname()
	{
		return localCustomer_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_lastname
	 */
	public void setCustomer_lastname(java.lang.String param)
	{
		localCustomer_lastnameTracker = param != null;

		this.localCustomer_lastname = param;


	}


	/**
	 * field for Quote_id
	 */


	protected java.lang.String localQuote_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_idTracker = false;

	public boolean isQuote_idSpecified()
	{
		return localQuote_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQuote_id()
	{
		return localQuote_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_id
	 */
	public void setQuote_id(java.lang.String param)
	{
		localQuote_idTracker = param != null;

		this.localQuote_id = param;


	}


	/**
	 * field for Is_virtual
	 */


	protected java.lang.String localIs_virtual;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_virtualTracker = false;

	public boolean isIs_virtualSpecified()
	{
		return localIs_virtualTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_virtual()
	{
		return localIs_virtual;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_virtual
	 */
	public void setIs_virtual(java.lang.String param)
	{
		localIs_virtualTracker = param != null;

		this.localIs_virtual = param;


	}


	/**
	 * field for Customer_group_id
	 */


	protected java.lang.String localCustomer_group_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_group_idTracker = false;

	public boolean isCustomer_group_idSpecified()
	{
		return localCustomer_group_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_group_id()
	{
		return localCustomer_group_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_group_id
	 */
	public void setCustomer_group_id(java.lang.String param)
	{
		localCustomer_group_idTracker = param != null;

		this.localCustomer_group_id = param;


	}


	/**
	 * field for Customer_note_notify
	 */


	protected java.lang.String localCustomer_note_notify;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_note_notifyTracker = false;

	public boolean isCustomer_note_notifySpecified()
	{
		return localCustomer_note_notifyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_note_notify()
	{
		return localCustomer_note_notify;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_note_notify
	 */
	public void setCustomer_note_notify(java.lang.String param)
	{
		localCustomer_note_notifyTracker = param != null;

		this.localCustomer_note_notify = param;


	}


	/**
	 * field for Customer_is_guest
	 */


	protected java.lang.String localCustomer_is_guest;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_is_guestTracker = false;

	public boolean isCustomer_is_guestSpecified()
	{
		return localCustomer_is_guestTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_is_guest()
	{
		return localCustomer_is_guest;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_is_guest
	 */
	public void setCustomer_is_guest(java.lang.String param)
	{
		localCustomer_is_guestTracker = param != null;

		this.localCustomer_is_guest = param;


	}


	/**
	 * field for Email_sent
	 */


	protected java.lang.String localEmail_sent;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEmail_sentTracker = false;

	public boolean isEmail_sentSpecified()
	{
		return localEmail_sentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEmail_sent()
	{
		return localEmail_sent;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Email_sent
	 */
	public void setEmail_sent(java.lang.String param)
	{
		localEmail_sentTracker = param != null;

		this.localEmail_sent = param;


	}


	/**
	 * field for Order_id
	 */


	protected java.lang.String localOrder_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_idTracker = false;

	public boolean isOrder_idSpecified()
	{
		return localOrder_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_id()
	{
		return localOrder_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_id
	 */
	public void setOrder_id(java.lang.String param)
	{
		localOrder_idTracker = param != null;

		this.localOrder_id = param;


	}


	/**
	 * field for Gift_message_id
	 */


	protected java.lang.String localGift_message_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_message_idTracker = false;

	public boolean isGift_message_idSpecified()
	{
		return localGift_message_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message_id()
	{
		return localGift_message_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message_id
	 */
	public void setGift_message_id(java.lang.String param)
	{
		localGift_message_idTracker = param != null;

		this.localGift_message_id = param;


	}


	/**
	 * field for Gift_message
	 */


	protected java.lang.String localGift_message;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_messageTracker = false;

	public boolean isGift_messageSpecified()
	{
		return localGift_messageTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message()
	{
		return localGift_message;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message
	 */
	public void setGift_message(java.lang.String param)
	{
		localGift_messageTracker = param != null;

		this.localGift_message = param;


	}


	/**
	 * field for Shipping_address
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity localShipping_address;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_addressTracker = false;

	public boolean isShipping_addressSpecified()
	{
		return localShipping_addressTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity getShipping_address()
	{
		return localShipping_address;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address
	 */
	public void setShipping_address(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity param)
	{
		localShipping_addressTracker = param != null;

		this.localShipping_address = param;


	}


	/**
	 * field for Billing_address
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity localBilling_address;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_addressTracker = false;

	public boolean isBilling_addressSpecified()
	{
		return localBilling_addressTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity getBilling_address()
	{
		return localBilling_address;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_address
	 */
	public void setBilling_address(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity param)
	{
		localBilling_addressTracker = param != null;

		this.localBilling_address = param;


	}


	/**
	 * field for Items
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray localItems;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItemsTracker = false;

	public boolean isItemsSpecified()
	{
		return localItemsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray getItems()
	{
		return localItems;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items
	 */
	public void setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray param)
	{
		localItemsTracker = param != null;

		this.localItems = param;


	}


	/**
	 * field for Payment
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity localPayment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPaymentTracker = false;

	public boolean isPaymentSpecified()
	{
		return localPaymentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity getPayment()
	{
		return localPayment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Payment
	 */
	public void setPayment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity param)
	{
		localPaymentTracker = param != null;

		this.localPayment = param;


	}


	/**
	 * field for Status_history
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray localStatus_history;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStatus_historyTracker = false;

	public boolean isStatus_historySpecified()
	{
		return localStatus_historyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray getStatus_history()
	{
		return localStatus_history;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Status_history
	 */
	public void setStatus_history(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray param)
	{
		localStatus_historyTracker = param != null;

		this.localStatus_history = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderEntity", xmlWriter);
			}


		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localParent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "parent_id", xmlWriter);


			if (localParent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localParent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);


			if (localStore_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_activeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_active", xmlWriter);


			if (localIs_active == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_active cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_active);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_id", xmlWriter);


			if (localCustomer_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_amount", xmlWriter);


			if (localTax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_amount", xmlWriter);


			if (localShipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_amount", xmlWriter);


			if (localDiscount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal", xmlWriter);


			if (localSubtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localGrand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "grand_total", xmlWriter);


			if (localGrand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGrand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_paidTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_paid", xmlWriter);


			if (localTotal_paid == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_paid cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_paid);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_refunded", xmlWriter);


			if (localTotal_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_qty_orderedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_qty_ordered", xmlWriter);


			if (localTotal_qty_ordered == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_qty_ordered cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_qty_ordered);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_canceled", xmlWriter);


			if (localTotal_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_invoiced", xmlWriter);


			if (localTotal_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_online_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_online_refunded", xmlWriter);


			if (localTotal_online_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_online_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_online_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_offline_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_offline_refunded", xmlWriter);


			if (localTotal_offline_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_offline_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_offline_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_amount", xmlWriter);


			if (localBase_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_amount", xmlWriter);


			if (localBase_shipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_amount", xmlWriter);


			if (localBase_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal", xmlWriter);


			if (localBase_subtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_grand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_grand_total", xmlWriter);


			if (localBase_grand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_grand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_paidTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_paid", xmlWriter);


			if (localBase_total_paid == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_paid cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_paid);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_refunded", xmlWriter);


			if (localBase_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_qty_orderedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_qty_ordered", xmlWriter);


			if (localBase_total_qty_ordered == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_qty_ordered cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_qty_ordered);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_canceled", xmlWriter);


			if (localBase_total_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_invoiced", xmlWriter);


			if (localBase_total_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_online_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_online_refunded", xmlWriter);


			if (localBase_total_online_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_online_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_online_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_offline_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_offline_refunded", xmlWriter);


			if (localBase_total_offline_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_offline_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_offline_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_address_id", xmlWriter);


			if (localBilling_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_firstname", xmlWriter);


			if (localBilling_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_lastname", xmlWriter);


			if (localBilling_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_address_id", xmlWriter);


			if (localShipping_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_firstname", xmlWriter);


			if (localShipping_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_lastname", xmlWriter);


			if (localShipping_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_name", xmlWriter);


			if (localBilling_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_name", xmlWriter);


			if (localShipping_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_base_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_base_rate", xmlWriter);


			if (localStore_to_base_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_base_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_base_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_order_rate", xmlWriter);


			if (localStore_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_global_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_global_rate", xmlWriter);


			if (localBase_to_global_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_global_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_global_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_order_rate", xmlWriter);


			if (localBase_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weight", xmlWriter);


			if (localWeight == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeight);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_name", xmlWriter);


			if (localStore_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localRemote_ipTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "remote_ip", xmlWriter);


			if (localRemote_ip == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("remote_ip cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRemote_ip);

			}

			xmlWriter.writeEndElement();
		}
		if (localStatusTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "status", xmlWriter);


			if (localStatus == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStatus);

			}

			xmlWriter.writeEndElement();
		}
		if (localStateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "state", xmlWriter);


			if (localState == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localState);

			}

			xmlWriter.writeEndElement();
		}
		if (localApplied_rule_idsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "applied_rule_ids", xmlWriter);


			if (localApplied_rule_ids == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("applied_rule_ids cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localApplied_rule_ids);

			}

			xmlWriter.writeEndElement();
		}
		if (localGlobal_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "global_currency_code", xmlWriter);


			if (localGlobal_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("global_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGlobal_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_currency_code", xmlWriter);


			if (localBase_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_currency_code", xmlWriter);


			if (localStore_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_currency_code", xmlWriter);


			if (localOrder_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_methodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_method", xmlWriter);


			if (localShipping_method == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_method);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_descriptionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_description", xmlWriter);


			if (localShipping_description == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_description cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_description);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_emailTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_email", xmlWriter);


			if (localCustomer_email == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_email cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_email);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_firstname", xmlWriter);


			if (localCustomer_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_lastname", xmlWriter);


			if (localCustomer_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_id", xmlWriter);


			if (localQuote_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("quote_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQuote_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_virtualTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_virtual", xmlWriter);


			if (localIs_virtual == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_virtual cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_virtual);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_group_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_group_id", xmlWriter);


			if (localCustomer_group_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_group_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_group_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_note_notifyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_note_notify", xmlWriter);


			if (localCustomer_note_notify == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_note_notify cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_note_notify);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_is_guestTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_is_guest", xmlWriter);


			if (localCustomer_is_guest == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_is_guest cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_is_guest);

			}

			xmlWriter.writeEndElement();
		}
		if (localEmail_sentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "email_sent", xmlWriter);


			if (localEmail_sent == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("email_sent cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEmail_sent);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_id", xmlWriter);


			if (localOrder_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_message_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message_id", xmlWriter);


			if (localGift_message_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_messageTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message", xmlWriter);


			if (localGift_message == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_addressTracker)
		{
			if (localShipping_address == null)
			{
				throw new org.apache.axis2.databinding.ADBException("shipping_address cannot be null!!");
			}
			localShipping_address.serialize(new javax.xml.namespace.QName("", "shipping_address"), xmlWriter);
		}
		if (localBilling_addressTracker)
		{
			if (localBilling_address == null)
			{
				throw new org.apache.axis2.databinding.ADBException("billing_address cannot be null!!");
			}
			localBilling_address.serialize(new javax.xml.namespace.QName("", "billing_address"), xmlWriter);
		}
		if (localItemsTracker)
		{
			if (localItems == null)
			{
				throw new org.apache.axis2.databinding.ADBException("items cannot be null!!");
			}
			localItems.serialize(new javax.xml.namespace.QName("", "items"), xmlWriter);
		}
		if (localPaymentTracker)
		{
			if (localPayment == null)
			{
				throw new org.apache.axis2.databinding.ADBException("payment cannot be null!!");
			}
			localPayment.serialize(new javax.xml.namespace.QName("", "payment"), xmlWriter);
		}
		if (localStatus_historyTracker)
		{
			if (localStatus_history == null)
			{
				throw new org.apache.axis2.databinding.ADBException("status_history cannot be null!!");
			}
			localStatus_history.serialize(new javax.xml.namespace.QName("", "status_history"), xmlWriter);
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderEntity object = new SalesOrderEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setParent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_active").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_active" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_active(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGrand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_paid").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_paid" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_paid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_qty_ordered").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_qty_ordered" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_qty_ordered(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_online_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_online_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_online_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_offline_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_offline_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_offline_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_grand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_paid").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_paid" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_paid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_qty_ordered").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_qty_ordered" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_qty_ordered(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_online_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_online_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_online_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_offline_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_offline_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_offline_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_base_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_base_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_base_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_global_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_global_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_global_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeight(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "remote_ip").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "remote_ip" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRemote_ip(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "status").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "status" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStatus(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "state").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "state" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setState(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "applied_rule_ids").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "applied_rule_ids" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setApplied_rule_ids(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "global_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "global_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGlobal_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_method(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_description").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_description" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_description(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_email").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_email" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_email(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_virtual").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_virtual" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_virtual(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_group_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_group_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_group_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_note_notify").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_note_notify" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_note_notify(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_is_guest").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_is_guest" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_is_guest(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "email_sent").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "email_sent" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEmail_sent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address").equals(reader.getName()))
				{

					object.setShipping_address(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_address").equals(reader.getName()))
				{

					object.setBilling_address(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderAddressEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items").equals(reader.getName()))
				{

					object.setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderItemEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "payment").equals(reader.getName()))
				{

					object.setPayment(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderPaymentEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "status_history").equals(reader.getName()))
				{

					object.setStatus_history(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderStatusHistoryEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

