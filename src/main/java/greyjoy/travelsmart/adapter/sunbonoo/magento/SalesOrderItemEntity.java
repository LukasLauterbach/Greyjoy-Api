
/**
 * SalesOrderItemEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderItemEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderItemEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderItemEntity Namespace URI = urn:Magento Namespace Prefix = ns1
	 */


	/**
	 * field for Item_id
	 */


	protected java.lang.String localItem_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItem_idTracker = false;

	public boolean isItem_idSpecified()
	{
		return localItem_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getItem_id()
	{
		return localItem_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Item_id
	 */
	public void setItem_id(java.lang.String param)
	{
		localItem_idTracker = param != null;

		this.localItem_id = param;


	}


	/**
	 * field for Order_id
	 */


	protected java.lang.String localOrder_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_idTracker = false;

	public boolean isOrder_idSpecified()
	{
		return localOrder_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_id()
	{
		return localOrder_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_id
	 */
	public void setOrder_id(java.lang.String param)
	{
		localOrder_idTracker = param != null;

		this.localOrder_id = param;


	}


	/**
	 * field for Quote_item_id
	 */


	protected java.lang.String localQuote_item_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_item_idTracker = false;

	public boolean isQuote_item_idSpecified()
	{
		return localQuote_item_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQuote_item_id()
	{
		return localQuote_item_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_item_id
	 */
	public void setQuote_item_id(java.lang.String param)
	{
		localQuote_item_idTracker = param != null;

		this.localQuote_item_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Product_id
	 */


	protected java.lang.String localProduct_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProduct_idTracker = false;

	public boolean isProduct_idSpecified()
	{
		return localProduct_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProduct_id()
	{
		return localProduct_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Product_id
	 */
	public void setProduct_id(java.lang.String param)
	{
		localProduct_idTracker = param != null;

		this.localProduct_id = param;


	}


	/**
	 * field for Product_type
	 */


	protected java.lang.String localProduct_type;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProduct_typeTracker = false;

	public boolean isProduct_typeSpecified()
	{
		return localProduct_typeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProduct_type()
	{
		return localProduct_type;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Product_type
	 */
	public void setProduct_type(java.lang.String param)
	{
		localProduct_typeTracker = param != null;

		this.localProduct_type = param;


	}


	/**
	 * field for Product_options
	 */


	protected java.lang.String localProduct_options;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProduct_optionsTracker = false;

	public boolean isProduct_optionsSpecified()
	{
		return localProduct_optionsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProduct_options()
	{
		return localProduct_options;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Product_options
	 */
	public void setProduct_options(java.lang.String param)
	{
		localProduct_optionsTracker = param != null;

		this.localProduct_options = param;


	}


	/**
	 * field for Weight
	 */


	protected java.lang.String localWeight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeightTracker = false;

	public boolean isWeightSpecified()
	{
		return localWeightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeight()
	{
		return localWeight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weight
	 */
	public void setWeight(java.lang.String param)
	{
		localWeightTracker = param != null;

		this.localWeight = param;


	}


	/**
	 * field for Is_virtual
	 */


	protected java.lang.String localIs_virtual;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_virtualTracker = false;

	public boolean isIs_virtualSpecified()
	{
		return localIs_virtualTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_virtual()
	{
		return localIs_virtual;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_virtual
	 */
	public void setIs_virtual(java.lang.String param)
	{
		localIs_virtualTracker = param != null;

		this.localIs_virtual = param;


	}


	/**
	 * field for Sku
	 */


	protected java.lang.String localSku;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSkuTracker = false;

	public boolean isSkuSpecified()
	{
		return localSkuTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSku()
	{
		return localSku;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Sku
	 */
	public void setSku(java.lang.String param)
	{
		localSkuTracker = param != null;

		this.localSku = param;


	}


	/**
	 * field for Name
	 */


	protected java.lang.String localName;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localNameTracker = false;

	public boolean isNameSpecified()
	{
		return localNameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getName()
	{
		return localName;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Name
	 */
	public void setName(java.lang.String param)
	{
		localNameTracker = param != null;

		this.localName = param;


	}


	/**
	 * field for Applied_rule_ids
	 */


	protected java.lang.String localApplied_rule_ids;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localApplied_rule_idsTracker = false;

	public boolean isApplied_rule_idsSpecified()
	{
		return localApplied_rule_idsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getApplied_rule_ids()
	{
		return localApplied_rule_ids;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Applied_rule_ids
	 */
	public void setApplied_rule_ids(java.lang.String param)
	{
		localApplied_rule_idsTracker = param != null;

		this.localApplied_rule_ids = param;


	}


	/**
	 * field for Free_shipping
	 */


	protected java.lang.String localFree_shipping;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFree_shippingTracker = false;

	public boolean isFree_shippingSpecified()
	{
		return localFree_shippingTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFree_shipping()
	{
		return localFree_shipping;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Free_shipping
	 */
	public void setFree_shipping(java.lang.String param)
	{
		localFree_shippingTracker = param != null;

		this.localFree_shipping = param;


	}


	/**
	 * field for Is_qty_decimal
	 */


	protected java.lang.String localIs_qty_decimal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_qty_decimalTracker = false;

	public boolean isIs_qty_decimalSpecified()
	{
		return localIs_qty_decimalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_qty_decimal()
	{
		return localIs_qty_decimal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_qty_decimal
	 */
	public void setIs_qty_decimal(java.lang.String param)
	{
		localIs_qty_decimalTracker = param != null;

		this.localIs_qty_decimal = param;


	}


	/**
	 * field for No_discount
	 */


	protected java.lang.String localNo_discount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localNo_discountTracker = false;

	public boolean isNo_discountSpecified()
	{
		return localNo_discountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getNo_discount()
	{
		return localNo_discount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            No_discount
	 */
	public void setNo_discount(java.lang.String param)
	{
		localNo_discountTracker = param != null;

		this.localNo_discount = param;


	}


	/**
	 * field for Qty_canceled
	 */


	protected java.lang.String localQty_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQty_canceledTracker = false;

	public boolean isQty_canceledSpecified()
	{
		return localQty_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty_canceled()
	{
		return localQty_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty_canceled
	 */
	public void setQty_canceled(java.lang.String param)
	{
		localQty_canceledTracker = param != null;

		this.localQty_canceled = param;


	}


	/**
	 * field for Qty_invoiced
	 */


	protected java.lang.String localQty_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQty_invoicedTracker = false;

	public boolean isQty_invoicedSpecified()
	{
		return localQty_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty_invoiced()
	{
		return localQty_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty_invoiced
	 */
	public void setQty_invoiced(java.lang.String param)
	{
		localQty_invoicedTracker = param != null;

		this.localQty_invoiced = param;


	}


	/**
	 * field for Qty_ordered
	 */


	protected java.lang.String localQty_ordered;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQty_orderedTracker = false;

	public boolean isQty_orderedSpecified()
	{
		return localQty_orderedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty_ordered()
	{
		return localQty_ordered;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty_ordered
	 */
	public void setQty_ordered(java.lang.String param)
	{
		localQty_orderedTracker = param != null;

		this.localQty_ordered = param;


	}


	/**
	 * field for Qty_refunded
	 */


	protected java.lang.String localQty_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQty_refundedTracker = false;

	public boolean isQty_refundedSpecified()
	{
		return localQty_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty_refunded()
	{
		return localQty_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty_refunded
	 */
	public void setQty_refunded(java.lang.String param)
	{
		localQty_refundedTracker = param != null;

		this.localQty_refunded = param;


	}


	/**
	 * field for Qty_shipped
	 */


	protected java.lang.String localQty_shipped;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQty_shippedTracker = false;

	public boolean isQty_shippedSpecified()
	{
		return localQty_shippedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty_shipped()
	{
		return localQty_shipped;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty_shipped
	 */
	public void setQty_shipped(java.lang.String param)
	{
		localQty_shippedTracker = param != null;

		this.localQty_shipped = param;


	}


	/**
	 * field for Cost
	 */


	protected java.lang.String localCost;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCostTracker = false;

	public boolean isCostSpecified()
	{
		return localCostTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCost()
	{
		return localCost;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cost
	 */
	public void setCost(java.lang.String param)
	{
		localCostTracker = param != null;

		this.localCost = param;


	}


	/**
	 * field for Price
	 */


	protected java.lang.String localPrice;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPriceTracker = false;

	public boolean isPriceSpecified()
	{
		return localPriceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrice()
	{
		return localPrice;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Price
	 */
	public void setPrice(java.lang.String param)
	{
		localPriceTracker = param != null;

		this.localPrice = param;


	}


	/**
	 * field for Base_price
	 */


	protected java.lang.String localBase_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_priceTracker = false;

	public boolean isBase_priceSpecified()
	{
		return localBase_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_price()
	{
		return localBase_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_price
	 */
	public void setBase_price(java.lang.String param)
	{
		localBase_priceTracker = param != null;

		this.localBase_price = param;


	}


	/**
	 * field for Original_price
	 */


	protected java.lang.String localOriginal_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOriginal_priceTracker = false;

	public boolean isOriginal_priceSpecified()
	{
		return localOriginal_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOriginal_price()
	{
		return localOriginal_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Original_price
	 */
	public void setOriginal_price(java.lang.String param)
	{
		localOriginal_priceTracker = param != null;

		this.localOriginal_price = param;


	}


	/**
	 * field for Base_original_price
	 */


	protected java.lang.String localBase_original_price;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_original_priceTracker = false;

	public boolean isBase_original_priceSpecified()
	{
		return localBase_original_priceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_original_price()
	{
		return localBase_original_price;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_original_price
	 */
	public void setBase_original_price(java.lang.String param)
	{
		localBase_original_priceTracker = param != null;

		this.localBase_original_price = param;


	}


	/**
	 * field for Tax_percent
	 */


	protected java.lang.String localTax_percent;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_percentTracker = false;

	public boolean isTax_percentSpecified()
	{
		return localTax_percentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_percent()
	{
		return localTax_percent;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_percent
	 */
	public void setTax_percent(java.lang.String param)
	{
		localTax_percentTracker = param != null;

		this.localTax_percent = param;


	}


	/**
	 * field for Tax_amount
	 */


	protected java.lang.String localTax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_amountTracker = false;

	public boolean isTax_amountSpecified()
	{
		return localTax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_amount()
	{
		return localTax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_amount
	 */
	public void setTax_amount(java.lang.String param)
	{
		localTax_amountTracker = param != null;

		this.localTax_amount = param;


	}


	/**
	 * field for Base_tax_amount
	 */


	protected java.lang.String localBase_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_amountTracker = false;

	public boolean isBase_tax_amountSpecified()
	{
		return localBase_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_amount()
	{
		return localBase_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_amount
	 */
	public void setBase_tax_amount(java.lang.String param)
	{
		localBase_tax_amountTracker = param != null;

		this.localBase_tax_amount = param;


	}


	/**
	 * field for Tax_invoiced
	 */


	protected java.lang.String localTax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_invoicedTracker = false;

	public boolean isTax_invoicedSpecified()
	{
		return localTax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_invoiced()
	{
		return localTax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_invoiced
	 */
	public void setTax_invoiced(java.lang.String param)
	{
		localTax_invoicedTracker = param != null;

		this.localTax_invoiced = param;


	}


	/**
	 * field for Base_tax_invoiced
	 */


	protected java.lang.String localBase_tax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_invoicedTracker = false;

	public boolean isBase_tax_invoicedSpecified()
	{
		return localBase_tax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_invoiced()
	{
		return localBase_tax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_invoiced
	 */
	public void setBase_tax_invoiced(java.lang.String param)
	{
		localBase_tax_invoicedTracker = param != null;

		this.localBase_tax_invoiced = param;


	}


	/**
	 * field for Discount_percent
	 */


	protected java.lang.String localDiscount_percent;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_percentTracker = false;

	public boolean isDiscount_percentSpecified()
	{
		return localDiscount_percentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_percent()
	{
		return localDiscount_percent;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_percent
	 */
	public void setDiscount_percent(java.lang.String param)
	{
		localDiscount_percentTracker = param != null;

		this.localDiscount_percent = param;


	}


	/**
	 * field for Discount_amount
	 */


	protected java.lang.String localDiscount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_amountTracker = false;

	public boolean isDiscount_amountSpecified()
	{
		return localDiscount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_amount()
	{
		return localDiscount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_amount
	 */
	public void setDiscount_amount(java.lang.String param)
	{
		localDiscount_amountTracker = param != null;

		this.localDiscount_amount = param;


	}


	/**
	 * field for Base_discount_amount
	 */


	protected java.lang.String localBase_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_amountTracker = false;

	public boolean isBase_discount_amountSpecified()
	{
		return localBase_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_amount()
	{
		return localBase_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_amount
	 */
	public void setBase_discount_amount(java.lang.String param)
	{
		localBase_discount_amountTracker = param != null;

		this.localBase_discount_amount = param;


	}


	/**
	 * field for Discount_invoiced
	 */


	protected java.lang.String localDiscount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_invoicedTracker = false;

	public boolean isDiscount_invoicedSpecified()
	{
		return localDiscount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_invoiced()
	{
		return localDiscount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_invoiced
	 */
	public void setDiscount_invoiced(java.lang.String param)
	{
		localDiscount_invoicedTracker = param != null;

		this.localDiscount_invoiced = param;


	}


	/**
	 * field for Base_discount_invoiced
	 */


	protected java.lang.String localBase_discount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_invoicedTracker = false;

	public boolean isBase_discount_invoicedSpecified()
	{
		return localBase_discount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_invoiced()
	{
		return localBase_discount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_invoiced
	 */
	public void setBase_discount_invoiced(java.lang.String param)
	{
		localBase_discount_invoicedTracker = param != null;

		this.localBase_discount_invoiced = param;


	}


	/**
	 * field for Amount_refunded
	 */


	protected java.lang.String localAmount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAmount_refundedTracker = false;

	public boolean isAmount_refundedSpecified()
	{
		return localAmount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAmount_refunded()
	{
		return localAmount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Amount_refunded
	 */
	public void setAmount_refunded(java.lang.String param)
	{
		localAmount_refundedTracker = param != null;

		this.localAmount_refunded = param;


	}


	/**
	 * field for Base_amount_refunded
	 */


	protected java.lang.String localBase_amount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_amount_refundedTracker = false;

	public boolean isBase_amount_refundedSpecified()
	{
		return localBase_amount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_amount_refunded()
	{
		return localBase_amount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_amount_refunded
	 */
	public void setBase_amount_refunded(java.lang.String param)
	{
		localBase_amount_refundedTracker = param != null;

		this.localBase_amount_refunded = param;


	}


	/**
	 * field for Row_total
	 */


	protected java.lang.String localRow_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRow_totalTracker = false;

	public boolean isRow_totalSpecified()
	{
		return localRow_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRow_total()
	{
		return localRow_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Row_total
	 */
	public void setRow_total(java.lang.String param)
	{
		localRow_totalTracker = param != null;

		this.localRow_total = param;


	}


	/**
	 * field for Base_row_total
	 */


	protected java.lang.String localBase_row_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_row_totalTracker = false;

	public boolean isBase_row_totalSpecified()
	{
		return localBase_row_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_row_total()
	{
		return localBase_row_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_row_total
	 */
	public void setBase_row_total(java.lang.String param)
	{
		localBase_row_totalTracker = param != null;

		this.localBase_row_total = param;


	}


	/**
	 * field for Row_invoiced
	 */


	protected java.lang.String localRow_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRow_invoicedTracker = false;

	public boolean isRow_invoicedSpecified()
	{
		return localRow_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRow_invoiced()
	{
		return localRow_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Row_invoiced
	 */
	public void setRow_invoiced(java.lang.String param)
	{
		localRow_invoicedTracker = param != null;

		this.localRow_invoiced = param;


	}


	/**
	 * field for Base_row_invoiced
	 */


	protected java.lang.String localBase_row_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_row_invoicedTracker = false;

	public boolean isBase_row_invoicedSpecified()
	{
		return localBase_row_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_row_invoiced()
	{
		return localBase_row_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_row_invoiced
	 */
	public void setBase_row_invoiced(java.lang.String param)
	{
		localBase_row_invoicedTracker = param != null;

		this.localBase_row_invoiced = param;


	}


	/**
	 * field for Row_weight
	 */


	protected java.lang.String localRow_weight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRow_weightTracker = false;

	public boolean isRow_weightSpecified()
	{
		return localRow_weightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRow_weight()
	{
		return localRow_weight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Row_weight
	 */
	public void setRow_weight(java.lang.String param)
	{
		localRow_weightTracker = param != null;

		this.localRow_weight = param;


	}


	/**
	 * field for Gift_message_id
	 */


	protected java.lang.String localGift_message_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_message_idTracker = false;

	public boolean isGift_message_idSpecified()
	{
		return localGift_message_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message_id()
	{
		return localGift_message_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message_id
	 */
	public void setGift_message_id(java.lang.String param)
	{
		localGift_message_idTracker = param != null;

		this.localGift_message_id = param;


	}


	/**
	 * field for Gift_message
	 */


	protected java.lang.String localGift_message;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_messageTracker = false;

	public boolean isGift_messageSpecified()
	{
		return localGift_messageTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message()
	{
		return localGift_message;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message
	 */
	public void setGift_message(java.lang.String param)
	{
		localGift_messageTracker = param != null;

		this.localGift_message = param;


	}


	/**
	 * field for Gift_message_available
	 */


	protected java.lang.String localGift_message_available;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_message_availableTracker = false;

	public boolean isGift_message_availableSpecified()
	{
		return localGift_message_availableTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message_available()
	{
		return localGift_message_available;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message_available
	 */
	public void setGift_message_available(java.lang.String param)
	{
		localGift_message_availableTracker = param != null;

		this.localGift_message_available = param;


	}


	/**
	 * field for Base_tax_before_discount
	 */


	protected java.lang.String localBase_tax_before_discount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_before_discountTracker = false;

	public boolean isBase_tax_before_discountSpecified()
	{
		return localBase_tax_before_discountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_before_discount()
	{
		return localBase_tax_before_discount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_before_discount
	 */
	public void setBase_tax_before_discount(java.lang.String param)
	{
		localBase_tax_before_discountTracker = param != null;

		this.localBase_tax_before_discount = param;


	}


	/**
	 * field for Tax_before_discount
	 */


	protected java.lang.String localTax_before_discount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_before_discountTracker = false;

	public boolean isTax_before_discountSpecified()
	{
		return localTax_before_discountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_before_discount()
	{
		return localTax_before_discount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_before_discount
	 */
	public void setTax_before_discount(java.lang.String param)
	{
		localTax_before_discountTracker = param != null;

		this.localTax_before_discount = param;


	}


	/**
	 * field for Weee_tax_applied
	 */


	protected java.lang.String localWeee_tax_applied;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_appliedTracker = false;

	public boolean isWeee_tax_appliedSpecified()
	{
		return localWeee_tax_appliedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied()
	{
		return localWeee_tax_applied;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied
	 */
	public void setWeee_tax_applied(java.lang.String param)
	{
		localWeee_tax_appliedTracker = param != null;

		this.localWeee_tax_applied = param;


	}


	/**
	 * field for Weee_tax_applied_amount
	 */


	protected java.lang.String localWeee_tax_applied_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_applied_amountTracker = false;

	public boolean isWeee_tax_applied_amountSpecified()
	{
		return localWeee_tax_applied_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied_amount()
	{
		return localWeee_tax_applied_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied_amount
	 */
	public void setWeee_tax_applied_amount(java.lang.String param)
	{
		localWeee_tax_applied_amountTracker = param != null;

		this.localWeee_tax_applied_amount = param;


	}


	/**
	 * field for Weee_tax_applied_row_amount
	 */


	protected java.lang.String localWeee_tax_applied_row_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_applied_row_amountTracker = false;

	public boolean isWeee_tax_applied_row_amountSpecified()
	{
		return localWeee_tax_applied_row_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_applied_row_amount()
	{
		return localWeee_tax_applied_row_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_applied_row_amount
	 */
	public void setWeee_tax_applied_row_amount(java.lang.String param)
	{
		localWeee_tax_applied_row_amountTracker = param != null;

		this.localWeee_tax_applied_row_amount = param;


	}


	/**
	 * field for Base_weee_tax_applied_amount
	 */


	protected java.lang.String localBase_weee_tax_applied_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_applied_amountTracker = false;

	public boolean isBase_weee_tax_applied_amountSpecified()
	{
		return localBase_weee_tax_applied_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_applied_amount()
	{
		return localBase_weee_tax_applied_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_applied_amount
	 */
	public void setBase_weee_tax_applied_amount(java.lang.String param)
	{
		localBase_weee_tax_applied_amountTracker = param != null;

		this.localBase_weee_tax_applied_amount = param;


	}


	/**
	 * field for Base_weee_tax_applied_row_amount
	 */


	protected java.lang.String localBase_weee_tax_applied_row_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_applied_row_amountTracker = false;

	public boolean isBase_weee_tax_applied_row_amountSpecified()
	{
		return localBase_weee_tax_applied_row_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_applied_row_amount()
	{
		return localBase_weee_tax_applied_row_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_applied_row_amount
	 */
	public void setBase_weee_tax_applied_row_amount(java.lang.String param)
	{
		localBase_weee_tax_applied_row_amountTracker = param != null;

		this.localBase_weee_tax_applied_row_amount = param;


	}


	/**
	 * field for Weee_tax_disposition
	 */


	protected java.lang.String localWeee_tax_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_dispositionTracker = false;

	public boolean isWeee_tax_dispositionSpecified()
	{
		return localWeee_tax_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_disposition()
	{
		return localWeee_tax_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_disposition
	 */
	public void setWeee_tax_disposition(java.lang.String param)
	{
		localWeee_tax_dispositionTracker = param != null;

		this.localWeee_tax_disposition = param;


	}


	/**
	 * field for Weee_tax_row_disposition
	 */


	protected java.lang.String localWeee_tax_row_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeee_tax_row_dispositionTracker = false;

	public boolean isWeee_tax_row_dispositionSpecified()
	{
		return localWeee_tax_row_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeee_tax_row_disposition()
	{
		return localWeee_tax_row_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weee_tax_row_disposition
	 */
	public void setWeee_tax_row_disposition(java.lang.String param)
	{
		localWeee_tax_row_dispositionTracker = param != null;

		this.localWeee_tax_row_disposition = param;


	}


	/**
	 * field for Base_weee_tax_disposition
	 */


	protected java.lang.String localBase_weee_tax_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_dispositionTracker = false;

	public boolean isBase_weee_tax_dispositionSpecified()
	{
		return localBase_weee_tax_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_disposition()
	{
		return localBase_weee_tax_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_disposition
	 */
	public void setBase_weee_tax_disposition(java.lang.String param)
	{
		localBase_weee_tax_dispositionTracker = param != null;

		this.localBase_weee_tax_disposition = param;


	}


	/**
	 * field for Base_weee_tax_row_disposition
	 */


	protected java.lang.String localBase_weee_tax_row_disposition;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_weee_tax_row_dispositionTracker = false;

	public boolean isBase_weee_tax_row_dispositionSpecified()
	{
		return localBase_weee_tax_row_dispositionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_weee_tax_row_disposition()
	{
		return localBase_weee_tax_row_disposition;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_weee_tax_row_disposition
	 */
	public void setBase_weee_tax_row_disposition(java.lang.String param)
	{
		localBase_weee_tax_row_dispositionTracker = param != null;

		this.localBase_weee_tax_row_disposition = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderItemEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderItemEntity", xmlWriter);
			}


		}
		if (localItem_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "item_id", xmlWriter);


			if (localItem_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localItem_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_id", xmlWriter);


			if (localOrder_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_item_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_item_id", xmlWriter);


			if (localQuote_item_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("quote_item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQuote_item_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localProduct_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "product_id", xmlWriter);


			if (localProduct_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("product_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProduct_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localProduct_typeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "product_type", xmlWriter);


			if (localProduct_type == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("product_type cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProduct_type);

			}

			xmlWriter.writeEndElement();
		}
		if (localProduct_optionsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "product_options", xmlWriter);


			if (localProduct_options == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("product_options cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProduct_options);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weight", xmlWriter);


			if (localWeight == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeight);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_virtualTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_virtual", xmlWriter);


			if (localIs_virtual == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_virtual cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_virtual);

			}

			xmlWriter.writeEndElement();
		}
		if (localSkuTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "sku", xmlWriter);


			if (localSku == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("sku cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSku);

			}

			xmlWriter.writeEndElement();
		}
		if (localNameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "name", xmlWriter);


			if (localName == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localName);

			}

			xmlWriter.writeEndElement();
		}
		if (localApplied_rule_idsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "applied_rule_ids", xmlWriter);


			if (localApplied_rule_ids == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("applied_rule_ids cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localApplied_rule_ids);

			}

			xmlWriter.writeEndElement();
		}
		if (localFree_shippingTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "free_shipping", xmlWriter);


			if (localFree_shipping == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("free_shipping cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFree_shipping);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_qty_decimalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_qty_decimal", xmlWriter);


			if (localIs_qty_decimal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_qty_decimal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_qty_decimal);

			}

			xmlWriter.writeEndElement();
		}
		if (localNo_discountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "no_discount", xmlWriter);


			if (localNo_discount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("no_discount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localNo_discount);

			}

			xmlWriter.writeEndElement();
		}
		if (localQty_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty_canceled", xmlWriter);


			if (localQty_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localQty_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty_invoiced", xmlWriter);


			if (localQty_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localQty_orderedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty_ordered", xmlWriter);


			if (localQty_ordered == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty_ordered cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty_ordered);

			}

			xmlWriter.writeEndElement();
		}
		if (localQty_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty_refunded", xmlWriter);


			if (localQty_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localQty_shippedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty_shipped", xmlWriter);


			if (localQty_shipped == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty_shipped cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty_shipped);

			}

			xmlWriter.writeEndElement();
		}
		if (localCostTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cost", xmlWriter);


			if (localCost == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cost cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCost);

			}

			xmlWriter.writeEndElement();
		}
		if (localPriceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "price", xmlWriter);


			if (localPrice == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrice);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_price", xmlWriter);


			if (localBase_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localOriginal_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "original_price", xmlWriter);


			if (localOriginal_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("original_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOriginal_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_original_priceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_original_price", xmlWriter);


			if (localBase_original_price == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_original_price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_original_price);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_percentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_percent", xmlWriter);


			if (localTax_percent == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_percent cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_percent);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_amount", xmlWriter);


			if (localTax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_amount", xmlWriter);


			if (localBase_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_invoiced", xmlWriter);


			if (localTax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_invoiced", xmlWriter);


			if (localBase_tax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_percentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_percent", xmlWriter);


			if (localDiscount_percent == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_percent cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_percent);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_amount", xmlWriter);


			if (localDiscount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_amount", xmlWriter);


			if (localBase_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_invoiced", xmlWriter);


			if (localDiscount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_invoiced", xmlWriter);


			if (localBase_discount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localAmount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "amount_refunded", xmlWriter);


			if (localAmount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("amount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAmount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_amount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_amount_refunded", xmlWriter);


			if (localBase_amount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_amount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_amount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localRow_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "row_total", xmlWriter);


			if (localRow_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("row_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRow_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_row_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_row_total", xmlWriter);


			if (localBase_row_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_row_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_row_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localRow_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "row_invoiced", xmlWriter);


			if (localRow_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("row_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRow_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_row_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_row_invoiced", xmlWriter);


			if (localBase_row_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_row_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_row_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localRow_weightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "row_weight", xmlWriter);


			if (localRow_weight == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("row_weight cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRow_weight);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_message_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message_id", xmlWriter);


			if (localGift_message_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_messageTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message", xmlWriter);


			if (localGift_message == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_message_availableTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message_available", xmlWriter);


			if (localGift_message_available == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message_available cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message_available);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_before_discountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_before_discount", xmlWriter);


			if (localBase_tax_before_discount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_before_discount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_before_discount);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_before_discountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_before_discount", xmlWriter);


			if (localTax_before_discount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_before_discount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_before_discount);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_appliedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied", xmlWriter);


			if (localWeee_tax_applied == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_applied_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied_amount", xmlWriter);


			if (localWeee_tax_applied_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_applied_row_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_applied_row_amount", xmlWriter);


			if (localWeee_tax_applied_row_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_applied_row_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_applied_row_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_applied_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_applied_amount", xmlWriter);


			if (localBase_weee_tax_applied_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_applied_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_applied_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_applied_row_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_applied_row_amount", xmlWriter);


			if (localBase_weee_tax_applied_row_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_applied_row_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_applied_row_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_disposition", xmlWriter);


			if (localWeee_tax_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeee_tax_row_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weee_tax_row_disposition", xmlWriter);


			if (localWeee_tax_row_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weee_tax_row_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeee_tax_row_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_disposition", xmlWriter);


			if (localBase_weee_tax_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_disposition);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_weee_tax_row_dispositionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_weee_tax_row_disposition", xmlWriter);


			if (localBase_weee_tax_row_disposition == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_weee_tax_row_disposition cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_weee_tax_row_disposition);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderItemEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderItemEntity object = new SalesOrderItemEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderItemEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderItemEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setItem_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_item_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "product_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "product_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProduct_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "product_type").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "product_type" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProduct_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "product_options").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "product_options" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProduct_options(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeight(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_virtual").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_virtual" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_virtual(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "sku").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "sku" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSku(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "applied_rule_ids").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "applied_rule_ids" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setApplied_rule_ids(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "free_shipping").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "free_shipping" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFree_shipping(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_qty_decimal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_qty_decimal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_qty_decimal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "no_discount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "no_discount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setNo_discount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty_ordered").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty_ordered" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty_ordered(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty_shipped").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty_shipped" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty_shipped(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cost").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cost" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCost(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "original_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "original_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOriginal_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_original_price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_original_price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_original_price(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_percent").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_percent" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_percent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_percent").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_percent" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_percent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "amount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "amount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAmount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_amount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_amount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_amount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "row_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "row_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRow_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_row_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_row_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_row_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "row_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "row_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRow_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_row_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_row_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_row_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "row_weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "row_weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRow_weight(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message_available").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message_available" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message_available(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_before_discount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_before_discount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_before_discount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_before_discount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_before_discount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_before_discount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_applied_row_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_applied_row_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_applied_row_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_applied_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_applied_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_applied_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_applied_row_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_applied_row_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_applied_row_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weee_tax_row_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weee_tax_row_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeee_tax_row_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_weee_tax_row_disposition").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_weee_tax_row_disposition" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_weee_tax_row_disposition(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

