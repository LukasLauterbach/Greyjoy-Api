
/**
 * SalesOrderListEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderListEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderListEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderListEntity Namespace URI = urn:Magento Namespace Prefix = ns1
	 */


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Store_id
	 */


	protected java.lang.String localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(java.lang.String param)
	{
		localStore_idTracker = param != null;

		this.localStore_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Customer_id
	 */


	protected java.lang.String localCustomer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_idTracker = false;

	public boolean isCustomer_idSpecified()
	{
		return localCustomer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_id()
	{
		return localCustomer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_id
	 */
	public void setCustomer_id(java.lang.String param)
	{
		localCustomer_idTracker = param != null;

		this.localCustomer_id = param;


	}


	/**
	 * field for Tax_amount
	 */


	protected java.lang.String localTax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_amountTracker = false;

	public boolean isTax_amountSpecified()
	{
		return localTax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_amount()
	{
		return localTax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_amount
	 */
	public void setTax_amount(java.lang.String param)
	{
		localTax_amountTracker = param != null;

		this.localTax_amount = param;


	}


	/**
	 * field for Shipping_amount
	 */


	protected java.lang.String localShipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_amountTracker = false;

	public boolean isShipping_amountSpecified()
	{
		return localShipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_amount()
	{
		return localShipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_amount
	 */
	public void setShipping_amount(java.lang.String param)
	{
		localShipping_amountTracker = param != null;

		this.localShipping_amount = param;


	}


	/**
	 * field for Discount_amount
	 */


	protected java.lang.String localDiscount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_amountTracker = false;

	public boolean isDiscount_amountSpecified()
	{
		return localDiscount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_amount()
	{
		return localDiscount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_amount
	 */
	public void setDiscount_amount(java.lang.String param)
	{
		localDiscount_amountTracker = param != null;

		this.localDiscount_amount = param;


	}


	/**
	 * field for Subtotal
	 */


	protected java.lang.String localSubtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotalTracker = false;

	public boolean isSubtotalSpecified()
	{
		return localSubtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal()
	{
		return localSubtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal
	 */
	public void setSubtotal(java.lang.String param)
	{
		localSubtotalTracker = param != null;

		this.localSubtotal = param;


	}


	/**
	 * field for Grand_total
	 */


	protected java.lang.String localGrand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGrand_totalTracker = false;

	public boolean isGrand_totalSpecified()
	{
		return localGrand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGrand_total()
	{
		return localGrand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Grand_total
	 */
	public void setGrand_total(java.lang.String param)
	{
		localGrand_totalTracker = param != null;

		this.localGrand_total = param;


	}


	/**
	 * field for Total_paid
	 */


	protected java.lang.String localTotal_paid;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_paidTracker = false;

	public boolean isTotal_paidSpecified()
	{
		return localTotal_paidTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_paid()
	{
		return localTotal_paid;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_paid
	 */
	public void setTotal_paid(java.lang.String param)
	{
		localTotal_paidTracker = param != null;

		this.localTotal_paid = param;


	}


	/**
	 * field for Total_refunded
	 */


	protected java.lang.String localTotal_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_refundedTracker = false;

	public boolean isTotal_refundedSpecified()
	{
		return localTotal_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_refunded()
	{
		return localTotal_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_refunded
	 */
	public void setTotal_refunded(java.lang.String param)
	{
		localTotal_refundedTracker = param != null;

		this.localTotal_refunded = param;


	}


	/**
	 * field for Total_qty_ordered
	 */


	protected java.lang.String localTotal_qty_ordered;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_qty_orderedTracker = false;

	public boolean isTotal_qty_orderedSpecified()
	{
		return localTotal_qty_orderedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_qty_ordered()
	{
		return localTotal_qty_ordered;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_qty_ordered
	 */
	public void setTotal_qty_ordered(java.lang.String param)
	{
		localTotal_qty_orderedTracker = param != null;

		this.localTotal_qty_ordered = param;


	}


	/**
	 * field for Total_canceled
	 */


	protected java.lang.String localTotal_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_canceledTracker = false;

	public boolean isTotal_canceledSpecified()
	{
		return localTotal_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_canceled()
	{
		return localTotal_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_canceled
	 */
	public void setTotal_canceled(java.lang.String param)
	{
		localTotal_canceledTracker = param != null;

		this.localTotal_canceled = param;


	}


	/**
	 * field for Total_invoiced
	 */


	protected java.lang.String localTotal_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_invoicedTracker = false;

	public boolean isTotal_invoicedSpecified()
	{
		return localTotal_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_invoiced()
	{
		return localTotal_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_invoiced
	 */
	public void setTotal_invoiced(java.lang.String param)
	{
		localTotal_invoicedTracker = param != null;

		this.localTotal_invoiced = param;


	}


	/**
	 * field for Total_online_refunded
	 */


	protected java.lang.String localTotal_online_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_online_refundedTracker = false;

	public boolean isTotal_online_refundedSpecified()
	{
		return localTotal_online_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_online_refunded()
	{
		return localTotal_online_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_online_refunded
	 */
	public void setTotal_online_refunded(java.lang.String param)
	{
		localTotal_online_refundedTracker = param != null;

		this.localTotal_online_refunded = param;


	}


	/**
	 * field for Total_offline_refunded
	 */


	protected java.lang.String localTotal_offline_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_offline_refundedTracker = false;

	public boolean isTotal_offline_refundedSpecified()
	{
		return localTotal_offline_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_offline_refunded()
	{
		return localTotal_offline_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_offline_refunded
	 */
	public void setTotal_offline_refunded(java.lang.String param)
	{
		localTotal_offline_refundedTracker = param != null;

		this.localTotal_offline_refunded = param;


	}


	/**
	 * field for Base_tax_amount
	 */


	protected java.lang.String localBase_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_amountTracker = false;

	public boolean isBase_tax_amountSpecified()
	{
		return localBase_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_amount()
	{
		return localBase_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_amount
	 */
	public void setBase_tax_amount(java.lang.String param)
	{
		localBase_tax_amountTracker = param != null;

		this.localBase_tax_amount = param;


	}


	/**
	 * field for Base_shipping_amount
	 */


	protected java.lang.String localBase_shipping_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_amountTracker = false;

	public boolean isBase_shipping_amountSpecified()
	{
		return localBase_shipping_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_amount()
	{
		return localBase_shipping_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_amount
	 */
	public void setBase_shipping_amount(java.lang.String param)
	{
		localBase_shipping_amountTracker = param != null;

		this.localBase_shipping_amount = param;


	}


	/**
	 * field for Base_discount_amount
	 */


	protected java.lang.String localBase_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_amountTracker = false;

	public boolean isBase_discount_amountSpecified()
	{
		return localBase_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_amount()
	{
		return localBase_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_amount
	 */
	public void setBase_discount_amount(java.lang.String param)
	{
		localBase_discount_amountTracker = param != null;

		this.localBase_discount_amount = param;


	}


	/**
	 * field for Base_subtotal
	 */


	protected java.lang.String localBase_subtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotalTracker = false;

	public boolean isBase_subtotalSpecified()
	{
		return localBase_subtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal()
	{
		return localBase_subtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal
	 */
	public void setBase_subtotal(java.lang.String param)
	{
		localBase_subtotalTracker = param != null;

		this.localBase_subtotal = param;


	}


	/**
	 * field for Base_grand_total
	 */


	protected java.lang.String localBase_grand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_grand_totalTracker = false;

	public boolean isBase_grand_totalSpecified()
	{
		return localBase_grand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_grand_total()
	{
		return localBase_grand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_grand_total
	 */
	public void setBase_grand_total(java.lang.String param)
	{
		localBase_grand_totalTracker = param != null;

		this.localBase_grand_total = param;


	}


	/**
	 * field for Base_total_paid
	 */


	protected java.lang.String localBase_total_paid;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_paidTracker = false;

	public boolean isBase_total_paidSpecified()
	{
		return localBase_total_paidTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_paid()
	{
		return localBase_total_paid;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_paid
	 */
	public void setBase_total_paid(java.lang.String param)
	{
		localBase_total_paidTracker = param != null;

		this.localBase_total_paid = param;


	}


	/**
	 * field for Base_total_refunded
	 */


	protected java.lang.String localBase_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_refundedTracker = false;

	public boolean isBase_total_refundedSpecified()
	{
		return localBase_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_refunded()
	{
		return localBase_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_refunded
	 */
	public void setBase_total_refunded(java.lang.String param)
	{
		localBase_total_refundedTracker = param != null;

		this.localBase_total_refunded = param;


	}


	/**
	 * field for Base_total_qty_ordered
	 */


	protected java.lang.String localBase_total_qty_ordered;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_qty_orderedTracker = false;

	public boolean isBase_total_qty_orderedSpecified()
	{
		return localBase_total_qty_orderedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_qty_ordered()
	{
		return localBase_total_qty_ordered;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_qty_ordered
	 */
	public void setBase_total_qty_ordered(java.lang.String param)
	{
		localBase_total_qty_orderedTracker = param != null;

		this.localBase_total_qty_ordered = param;


	}


	/**
	 * field for Base_total_canceled
	 */


	protected java.lang.String localBase_total_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_canceledTracker = false;

	public boolean isBase_total_canceledSpecified()
	{
		return localBase_total_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_canceled()
	{
		return localBase_total_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_canceled
	 */
	public void setBase_total_canceled(java.lang.String param)
	{
		localBase_total_canceledTracker = param != null;

		this.localBase_total_canceled = param;


	}


	/**
	 * field for Base_total_invoiced
	 */


	protected java.lang.String localBase_total_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_invoicedTracker = false;

	public boolean isBase_total_invoicedSpecified()
	{
		return localBase_total_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_invoiced()
	{
		return localBase_total_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_invoiced
	 */
	public void setBase_total_invoiced(java.lang.String param)
	{
		localBase_total_invoicedTracker = param != null;

		this.localBase_total_invoiced = param;


	}


	/**
	 * field for Base_total_online_refunded
	 */


	protected java.lang.String localBase_total_online_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_online_refundedTracker = false;

	public boolean isBase_total_online_refundedSpecified()
	{
		return localBase_total_online_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_online_refunded()
	{
		return localBase_total_online_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_online_refunded
	 */
	public void setBase_total_online_refunded(java.lang.String param)
	{
		localBase_total_online_refundedTracker = param != null;

		this.localBase_total_online_refunded = param;


	}


	/**
	 * field for Base_total_offline_refunded
	 */


	protected java.lang.String localBase_total_offline_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_offline_refundedTracker = false;

	public boolean isBase_total_offline_refundedSpecified()
	{
		return localBase_total_offline_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_offline_refunded()
	{
		return localBase_total_offline_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_offline_refunded
	 */
	public void setBase_total_offline_refunded(java.lang.String param)
	{
		localBase_total_offline_refundedTracker = param != null;

		this.localBase_total_offline_refunded = param;


	}


	/**
	 * field for Billing_address_id
	 */


	protected java.lang.String localBilling_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_address_idTracker = false;

	public boolean isBilling_address_idSpecified()
	{
		return localBilling_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_address_id()
	{
		return localBilling_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_address_id
	 */
	public void setBilling_address_id(java.lang.String param)
	{
		localBilling_address_idTracker = param != null;

		this.localBilling_address_id = param;


	}


	/**
	 * field for Billing_firstname
	 */


	protected java.lang.String localBilling_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_firstnameTracker = false;

	public boolean isBilling_firstnameSpecified()
	{
		return localBilling_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_firstname()
	{
		return localBilling_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_firstname
	 */
	public void setBilling_firstname(java.lang.String param)
	{
		localBilling_firstnameTracker = param != null;

		this.localBilling_firstname = param;


	}


	/**
	 * field for Billing_lastname
	 */


	protected java.lang.String localBilling_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_lastnameTracker = false;

	public boolean isBilling_lastnameSpecified()
	{
		return localBilling_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_lastname()
	{
		return localBilling_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_lastname
	 */
	public void setBilling_lastname(java.lang.String param)
	{
		localBilling_lastnameTracker = param != null;

		this.localBilling_lastname = param;


	}


	/**
	 * field for Shipping_address_id
	 */


	protected java.lang.String localShipping_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_address_idTracker = false;

	public boolean isShipping_address_idSpecified()
	{
		return localShipping_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_address_id()
	{
		return localShipping_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address_id
	 */
	public void setShipping_address_id(java.lang.String param)
	{
		localShipping_address_idTracker = param != null;

		this.localShipping_address_id = param;


	}


	/**
	 * field for Shipping_firstname
	 */


	protected java.lang.String localShipping_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_firstnameTracker = false;

	public boolean isShipping_firstnameSpecified()
	{
		return localShipping_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_firstname()
	{
		return localShipping_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_firstname
	 */
	public void setShipping_firstname(java.lang.String param)
	{
		localShipping_firstnameTracker = param != null;

		this.localShipping_firstname = param;


	}


	/**
	 * field for Shipping_lastname
	 */


	protected java.lang.String localShipping_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_lastnameTracker = false;

	public boolean isShipping_lastnameSpecified()
	{
		return localShipping_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_lastname()
	{
		return localShipping_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_lastname
	 */
	public void setShipping_lastname(java.lang.String param)
	{
		localShipping_lastnameTracker = param != null;

		this.localShipping_lastname = param;


	}


	/**
	 * field for Billing_name
	 */


	protected java.lang.String localBilling_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_nameTracker = false;

	public boolean isBilling_nameSpecified()
	{
		return localBilling_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBilling_name()
	{
		return localBilling_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_name
	 */
	public void setBilling_name(java.lang.String param)
	{
		localBilling_nameTracker = param != null;

		this.localBilling_name = param;


	}


	/**
	 * field for Shipping_name
	 */


	protected java.lang.String localShipping_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_nameTracker = false;

	public boolean isShipping_nameSpecified()
	{
		return localShipping_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_name()
	{
		return localShipping_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_name
	 */
	public void setShipping_name(java.lang.String param)
	{
		localShipping_nameTracker = param != null;

		this.localShipping_name = param;


	}


	/**
	 * field for Store_to_base_rate
	 */


	protected java.lang.String localStore_to_base_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_base_rateTracker = false;

	public boolean isStore_to_base_rateSpecified()
	{
		return localStore_to_base_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_base_rate()
	{
		return localStore_to_base_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_base_rate
	 */
	public void setStore_to_base_rate(java.lang.String param)
	{
		localStore_to_base_rateTracker = param != null;

		this.localStore_to_base_rate = param;


	}


	/**
	 * field for Store_to_order_rate
	 */


	protected java.lang.String localStore_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_order_rateTracker = false;

	public boolean isStore_to_order_rateSpecified()
	{
		return localStore_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_order_rate()
	{
		return localStore_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_order_rate
	 */
	public void setStore_to_order_rate(java.lang.String param)
	{
		localStore_to_order_rateTracker = param != null;

		this.localStore_to_order_rate = param;


	}


	/**
	 * field for Base_to_global_rate
	 */


	protected java.lang.String localBase_to_global_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_global_rateTracker = false;

	public boolean isBase_to_global_rateSpecified()
	{
		return localBase_to_global_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_global_rate()
	{
		return localBase_to_global_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_global_rate
	 */
	public void setBase_to_global_rate(java.lang.String param)
	{
		localBase_to_global_rateTracker = param != null;

		this.localBase_to_global_rate = param;


	}


	/**
	 * field for Base_to_order_rate
	 */


	protected java.lang.String localBase_to_order_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_order_rateTracker = false;

	public boolean isBase_to_order_rateSpecified()
	{
		return localBase_to_order_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_to_order_rate()
	{
		return localBase_to_order_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_order_rate
	 */
	public void setBase_to_order_rate(java.lang.String param)
	{
		localBase_to_order_rateTracker = param != null;

		this.localBase_to_order_rate = param;


	}


	/**
	 * field for Weight
	 */


	protected java.lang.String localWeight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeightTracker = false;

	public boolean isWeightSpecified()
	{
		return localWeightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeight()
	{
		return localWeight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weight
	 */
	public void setWeight(java.lang.String param)
	{
		localWeightTracker = param != null;

		this.localWeight = param;


	}


	/**
	 * field for Store_name
	 */


	protected java.lang.String localStore_name;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_nameTracker = false;

	public boolean isStore_nameSpecified()
	{
		return localStore_nameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_name()
	{
		return localStore_name;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_name
	 */
	public void setStore_name(java.lang.String param)
	{
		localStore_nameTracker = param != null;

		this.localStore_name = param;


	}


	/**
	 * field for Remote_ip
	 */


	protected java.lang.String localRemote_ip;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRemote_ipTracker = false;

	public boolean isRemote_ipSpecified()
	{
		return localRemote_ipTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRemote_ip()
	{
		return localRemote_ip;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Remote_ip
	 */
	public void setRemote_ip(java.lang.String param)
	{
		localRemote_ipTracker = param != null;

		this.localRemote_ip = param;


	}


	/**
	 * field for Status
	 */


	protected java.lang.String localStatus;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStatusTracker = false;

	public boolean isStatusSpecified()
	{
		return localStatusTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStatus()
	{
		return localStatus;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Status
	 */
	public void setStatus(java.lang.String param)
	{
		localStatusTracker = param != null;

		this.localStatus = param;


	}


	/**
	 * field for State
	 */


	protected java.lang.String localState;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStateTracker = false;

	public boolean isStateSpecified()
	{
		return localStateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getState()
	{
		return localState;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            State
	 */
	public void setState(java.lang.String param)
	{
		localStateTracker = param != null;

		this.localState = param;


	}


	/**
	 * field for Applied_rule_ids
	 */


	protected java.lang.String localApplied_rule_ids;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localApplied_rule_idsTracker = false;

	public boolean isApplied_rule_idsSpecified()
	{
		return localApplied_rule_idsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getApplied_rule_ids()
	{
		return localApplied_rule_ids;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Applied_rule_ids
	 */
	public void setApplied_rule_ids(java.lang.String param)
	{
		localApplied_rule_idsTracker = param != null;

		this.localApplied_rule_ids = param;


	}


	/**
	 * field for Global_currency_code
	 */


	protected java.lang.String localGlobal_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGlobal_currency_codeTracker = false;

	public boolean isGlobal_currency_codeSpecified()
	{
		return localGlobal_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGlobal_currency_code()
	{
		return localGlobal_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Global_currency_code
	 */
	public void setGlobal_currency_code(java.lang.String param)
	{
		localGlobal_currency_codeTracker = param != null;

		this.localGlobal_currency_code = param;


	}


	/**
	 * field for Base_currency_code
	 */


	protected java.lang.String localBase_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_currency_codeTracker = false;

	public boolean isBase_currency_codeSpecified()
	{
		return localBase_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_currency_code()
	{
		return localBase_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_currency_code
	 */
	public void setBase_currency_code(java.lang.String param)
	{
		localBase_currency_codeTracker = param != null;

		this.localBase_currency_code = param;


	}


	/**
	 * field for Store_currency_code
	 */


	protected java.lang.String localStore_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_currency_codeTracker = false;

	public boolean isStore_currency_codeSpecified()
	{
		return localStore_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_currency_code()
	{
		return localStore_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_currency_code
	 */
	public void setStore_currency_code(java.lang.String param)
	{
		localStore_currency_codeTracker = param != null;

		this.localStore_currency_code = param;


	}


	/**
	 * field for Order_currency_code
	 */


	protected java.lang.String localOrder_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_currency_codeTracker = false;

	public boolean isOrder_currency_codeSpecified()
	{
		return localOrder_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_currency_code()
	{
		return localOrder_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_currency_code
	 */
	public void setOrder_currency_code(java.lang.String param)
	{
		localOrder_currency_codeTracker = param != null;

		this.localOrder_currency_code = param;


	}


	/**
	 * field for Shipping_method
	 */


	protected java.lang.String localShipping_method;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_methodTracker = false;

	public boolean isShipping_methodSpecified()
	{
		return localShipping_methodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_method()
	{
		return localShipping_method;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_method
	 */
	public void setShipping_method(java.lang.String param)
	{
		localShipping_methodTracker = param != null;

		this.localShipping_method = param;


	}


	/**
	 * field for Shipping_description
	 */


	protected java.lang.String localShipping_description;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_descriptionTracker = false;

	public boolean isShipping_descriptionSpecified()
	{
		return localShipping_descriptionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_description()
	{
		return localShipping_description;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_description
	 */
	public void setShipping_description(java.lang.String param)
	{
		localShipping_descriptionTracker = param != null;

		this.localShipping_description = param;


	}


	/**
	 * field for Customer_email
	 */


	protected java.lang.String localCustomer_email;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_emailTracker = false;

	public boolean isCustomer_emailSpecified()
	{
		return localCustomer_emailTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_email()
	{
		return localCustomer_email;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_email
	 */
	public void setCustomer_email(java.lang.String param)
	{
		localCustomer_emailTracker = param != null;

		this.localCustomer_email = param;


	}


	/**
	 * field for Customer_firstname
	 */


	protected java.lang.String localCustomer_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_firstnameTracker = false;

	public boolean isCustomer_firstnameSpecified()
	{
		return localCustomer_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_firstname()
	{
		return localCustomer_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_firstname
	 */
	public void setCustomer_firstname(java.lang.String param)
	{
		localCustomer_firstnameTracker = param != null;

		this.localCustomer_firstname = param;


	}


	/**
	 * field for Customer_lastname
	 */


	protected java.lang.String localCustomer_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_lastnameTracker = false;

	public boolean isCustomer_lastnameSpecified()
	{
		return localCustomer_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_lastname()
	{
		return localCustomer_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_lastname
	 */
	public void setCustomer_lastname(java.lang.String param)
	{
		localCustomer_lastnameTracker = param != null;

		this.localCustomer_lastname = param;


	}


	/**
	 * field for Quote_id
	 */


	protected java.lang.String localQuote_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_idTracker = false;

	public boolean isQuote_idSpecified()
	{
		return localQuote_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQuote_id()
	{
		return localQuote_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_id
	 */
	public void setQuote_id(java.lang.String param)
	{
		localQuote_idTracker = param != null;

		this.localQuote_id = param;


	}


	/**
	 * field for Is_virtual
	 */


	protected java.lang.String localIs_virtual;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_virtualTracker = false;

	public boolean isIs_virtualSpecified()
	{
		return localIs_virtualTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_virtual()
	{
		return localIs_virtual;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_virtual
	 */
	public void setIs_virtual(java.lang.String param)
	{
		localIs_virtualTracker = param != null;

		this.localIs_virtual = param;


	}


	/**
	 * field for Customer_group_id
	 */


	protected java.lang.String localCustomer_group_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_group_idTracker = false;

	public boolean isCustomer_group_idSpecified()
	{
		return localCustomer_group_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_group_id()
	{
		return localCustomer_group_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_group_id
	 */
	public void setCustomer_group_id(java.lang.String param)
	{
		localCustomer_group_idTracker = param != null;

		this.localCustomer_group_id = param;


	}


	/**
	 * field for Customer_note_notify
	 */


	protected java.lang.String localCustomer_note_notify;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_note_notifyTracker = false;

	public boolean isCustomer_note_notifySpecified()
	{
		return localCustomer_note_notifyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_note_notify()
	{
		return localCustomer_note_notify;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_note_notify
	 */
	public void setCustomer_note_notify(java.lang.String param)
	{
		localCustomer_note_notifyTracker = param != null;

		this.localCustomer_note_notify = param;


	}


	/**
	 * field for Customer_is_guest
	 */


	protected java.lang.String localCustomer_is_guest;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_is_guestTracker = false;

	public boolean isCustomer_is_guestSpecified()
	{
		return localCustomer_is_guestTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_is_guest()
	{
		return localCustomer_is_guest;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_is_guest
	 */
	public void setCustomer_is_guest(java.lang.String param)
	{
		localCustomer_is_guestTracker = param != null;

		this.localCustomer_is_guest = param;


	}


	/**
	 * field for Email_sent
	 */


	protected java.lang.String localEmail_sent;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEmail_sentTracker = false;

	public boolean isEmail_sentSpecified()
	{
		return localEmail_sentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEmail_sent()
	{
		return localEmail_sent;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Email_sent
	 */
	public void setEmail_sent(java.lang.String param)
	{
		localEmail_sentTracker = param != null;

		this.localEmail_sent = param;


	}


	/**
	 * field for Order_id
	 */


	protected java.lang.String localOrder_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_idTracker = false;

	public boolean isOrder_idSpecified()
	{
		return localOrder_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_id()
	{
		return localOrder_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_id
	 */
	public void setOrder_id(java.lang.String param)
	{
		localOrder_idTracker = param != null;

		this.localOrder_id = param;


	}


	/**
	 * field for Gift_message_id
	 */


	protected java.lang.String localGift_message_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_message_idTracker = false;

	public boolean isGift_message_idSpecified()
	{
		return localGift_message_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message_id()
	{
		return localGift_message_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message_id
	 */
	public void setGift_message_id(java.lang.String param)
	{
		localGift_message_idTracker = param != null;

		this.localGift_message_id = param;


	}


	/**
	 * field for Coupon_code
	 */


	protected java.lang.String localCoupon_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCoupon_codeTracker = false;

	public boolean isCoupon_codeSpecified()
	{
		return localCoupon_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCoupon_code()
	{
		return localCoupon_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Coupon_code
	 */
	public void setCoupon_code(java.lang.String param)
	{
		localCoupon_codeTracker = param != null;

		this.localCoupon_code = param;


	}


	/**
	 * field for Protect_code
	 */


	protected java.lang.String localProtect_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProtect_codeTracker = false;

	public boolean isProtect_codeSpecified()
	{
		return localProtect_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProtect_code()
	{
		return localProtect_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Protect_code
	 */
	public void setProtect_code(java.lang.String param)
	{
		localProtect_codeTracker = param != null;

		this.localProtect_code = param;


	}


	/**
	 * field for Base_discount_canceled
	 */


	protected java.lang.String localBase_discount_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_canceledTracker = false;

	public boolean isBase_discount_canceledSpecified()
	{
		return localBase_discount_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_canceled()
	{
		return localBase_discount_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_canceled
	 */
	public void setBase_discount_canceled(java.lang.String param)
	{
		localBase_discount_canceledTracker = param != null;

		this.localBase_discount_canceled = param;


	}


	/**
	 * field for Base_discount_invoiced
	 */


	protected java.lang.String localBase_discount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_invoicedTracker = false;

	public boolean isBase_discount_invoicedSpecified()
	{
		return localBase_discount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_invoiced()
	{
		return localBase_discount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_invoiced
	 */
	public void setBase_discount_invoiced(java.lang.String param)
	{
		localBase_discount_invoicedTracker = param != null;

		this.localBase_discount_invoiced = param;


	}


	/**
	 * field for Base_discount_refunded
	 */


	protected java.lang.String localBase_discount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_discount_refundedTracker = false;

	public boolean isBase_discount_refundedSpecified()
	{
		return localBase_discount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_discount_refunded()
	{
		return localBase_discount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_discount_refunded
	 */
	public void setBase_discount_refunded(java.lang.String param)
	{
		localBase_discount_refundedTracker = param != null;

		this.localBase_discount_refunded = param;


	}


	/**
	 * field for Base_shipping_canceled
	 */


	protected java.lang.String localBase_shipping_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_canceledTracker = false;

	public boolean isBase_shipping_canceledSpecified()
	{
		return localBase_shipping_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_canceled()
	{
		return localBase_shipping_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_canceled
	 */
	public void setBase_shipping_canceled(java.lang.String param)
	{
		localBase_shipping_canceledTracker = param != null;

		this.localBase_shipping_canceled = param;


	}


	/**
	 * field for Base_shipping_invoiced
	 */


	protected java.lang.String localBase_shipping_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_invoicedTracker = false;

	public boolean isBase_shipping_invoicedSpecified()
	{
		return localBase_shipping_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_invoiced()
	{
		return localBase_shipping_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_invoiced
	 */
	public void setBase_shipping_invoiced(java.lang.String param)
	{
		localBase_shipping_invoicedTracker = param != null;

		this.localBase_shipping_invoiced = param;


	}


	/**
	 * field for Base_shipping_refunded
	 */


	protected java.lang.String localBase_shipping_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_refundedTracker = false;

	public boolean isBase_shipping_refundedSpecified()
	{
		return localBase_shipping_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_refunded()
	{
		return localBase_shipping_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_refunded
	 */
	public void setBase_shipping_refunded(java.lang.String param)
	{
		localBase_shipping_refundedTracker = param != null;

		this.localBase_shipping_refunded = param;


	}


	/**
	 * field for Base_shipping_tax_amount
	 */


	protected java.lang.String localBase_shipping_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_tax_amountTracker = false;

	public boolean isBase_shipping_tax_amountSpecified()
	{
		return localBase_shipping_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_tax_amount()
	{
		return localBase_shipping_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_tax_amount
	 */
	public void setBase_shipping_tax_amount(java.lang.String param)
	{
		localBase_shipping_tax_amountTracker = param != null;

		this.localBase_shipping_tax_amount = param;


	}


	/**
	 * field for Base_shipping_tax_refunded
	 */


	protected java.lang.String localBase_shipping_tax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_tax_refundedTracker = false;

	public boolean isBase_shipping_tax_refundedSpecified()
	{
		return localBase_shipping_tax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_tax_refunded()
	{
		return localBase_shipping_tax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_tax_refunded
	 */
	public void setBase_shipping_tax_refunded(java.lang.String param)
	{
		localBase_shipping_tax_refundedTracker = param != null;

		this.localBase_shipping_tax_refunded = param;


	}


	/**
	 * field for Base_subtotal_canceled
	 */


	protected java.lang.String localBase_subtotal_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_canceledTracker = false;

	public boolean isBase_subtotal_canceledSpecified()
	{
		return localBase_subtotal_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal_canceled()
	{
		return localBase_subtotal_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_canceled
	 */
	public void setBase_subtotal_canceled(java.lang.String param)
	{
		localBase_subtotal_canceledTracker = param != null;

		this.localBase_subtotal_canceled = param;


	}


	/**
	 * field for Base_subtotal_invoiced
	 */


	protected java.lang.String localBase_subtotal_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_invoicedTracker = false;

	public boolean isBase_subtotal_invoicedSpecified()
	{
		return localBase_subtotal_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal_invoiced()
	{
		return localBase_subtotal_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_invoiced
	 */
	public void setBase_subtotal_invoiced(java.lang.String param)
	{
		localBase_subtotal_invoicedTracker = param != null;

		this.localBase_subtotal_invoiced = param;


	}


	/**
	 * field for Base_subtotal_refunded
	 */


	protected java.lang.String localBase_subtotal_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_refundedTracker = false;

	public boolean isBase_subtotal_refundedSpecified()
	{
		return localBase_subtotal_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal_refunded()
	{
		return localBase_subtotal_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_refunded
	 */
	public void setBase_subtotal_refunded(java.lang.String param)
	{
		localBase_subtotal_refundedTracker = param != null;

		this.localBase_subtotal_refunded = param;


	}


	/**
	 * field for Base_tax_canceled
	 */


	protected java.lang.String localBase_tax_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_canceledTracker = false;

	public boolean isBase_tax_canceledSpecified()
	{
		return localBase_tax_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_canceled()
	{
		return localBase_tax_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_canceled
	 */
	public void setBase_tax_canceled(java.lang.String param)
	{
		localBase_tax_canceledTracker = param != null;

		this.localBase_tax_canceled = param;


	}


	/**
	 * field for Base_tax_invoiced
	 */


	protected java.lang.String localBase_tax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_invoicedTracker = false;

	public boolean isBase_tax_invoicedSpecified()
	{
		return localBase_tax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_invoiced()
	{
		return localBase_tax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_invoiced
	 */
	public void setBase_tax_invoiced(java.lang.String param)
	{
		localBase_tax_invoicedTracker = param != null;

		this.localBase_tax_invoiced = param;


	}


	/**
	 * field for Base_tax_refunded
	 */


	protected java.lang.String localBase_tax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_tax_refundedTracker = false;

	public boolean isBase_tax_refundedSpecified()
	{
		return localBase_tax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_tax_refunded()
	{
		return localBase_tax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_tax_refunded
	 */
	public void setBase_tax_refunded(java.lang.String param)
	{
		localBase_tax_refundedTracker = param != null;

		this.localBase_tax_refunded = param;


	}


	/**
	 * field for Base_total_invoiced_cost
	 */


	protected java.lang.String localBase_total_invoiced_cost;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_invoiced_costTracker = false;

	public boolean isBase_total_invoiced_costSpecified()
	{
		return localBase_total_invoiced_costTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_invoiced_cost()
	{
		return localBase_total_invoiced_cost;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_invoiced_cost
	 */
	public void setBase_total_invoiced_cost(java.lang.String param)
	{
		localBase_total_invoiced_costTracker = param != null;

		this.localBase_total_invoiced_cost = param;


	}


	/**
	 * field for Discount_canceled
	 */


	protected java.lang.String localDiscount_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_canceledTracker = false;

	public boolean isDiscount_canceledSpecified()
	{
		return localDiscount_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_canceled()
	{
		return localDiscount_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_canceled
	 */
	public void setDiscount_canceled(java.lang.String param)
	{
		localDiscount_canceledTracker = param != null;

		this.localDiscount_canceled = param;


	}


	/**
	 * field for Discount_invoiced
	 */


	protected java.lang.String localDiscount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_invoicedTracker = false;

	public boolean isDiscount_invoicedSpecified()
	{
		return localDiscount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_invoiced()
	{
		return localDiscount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_invoiced
	 */
	public void setDiscount_invoiced(java.lang.String param)
	{
		localDiscount_invoicedTracker = param != null;

		this.localDiscount_invoiced = param;


	}


	/**
	 * field for Discount_refunded
	 */


	protected java.lang.String localDiscount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_refundedTracker = false;

	public boolean isDiscount_refundedSpecified()
	{
		return localDiscount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_refunded()
	{
		return localDiscount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_refunded
	 */
	public void setDiscount_refunded(java.lang.String param)
	{
		localDiscount_refundedTracker = param != null;

		this.localDiscount_refunded = param;


	}


	/**
	 * field for Shipping_canceled
	 */


	protected java.lang.String localShipping_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_canceledTracker = false;

	public boolean isShipping_canceledSpecified()
	{
		return localShipping_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_canceled()
	{
		return localShipping_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_canceled
	 */
	public void setShipping_canceled(java.lang.String param)
	{
		localShipping_canceledTracker = param != null;

		this.localShipping_canceled = param;


	}


	/**
	 * field for Shipping_invoiced
	 */


	protected java.lang.String localShipping_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_invoicedTracker = false;

	public boolean isShipping_invoicedSpecified()
	{
		return localShipping_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_invoiced()
	{
		return localShipping_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_invoiced
	 */
	public void setShipping_invoiced(java.lang.String param)
	{
		localShipping_invoicedTracker = param != null;

		this.localShipping_invoiced = param;


	}


	/**
	 * field for Shipping_refunded
	 */


	protected java.lang.String localShipping_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_refundedTracker = false;

	public boolean isShipping_refundedSpecified()
	{
		return localShipping_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_refunded()
	{
		return localShipping_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_refunded
	 */
	public void setShipping_refunded(java.lang.String param)
	{
		localShipping_refundedTracker = param != null;

		this.localShipping_refunded = param;


	}


	/**
	 * field for Shipping_tax_amount
	 */


	protected java.lang.String localShipping_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_tax_amountTracker = false;

	public boolean isShipping_tax_amountSpecified()
	{
		return localShipping_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_tax_amount()
	{
		return localShipping_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_tax_amount
	 */
	public void setShipping_tax_amount(java.lang.String param)
	{
		localShipping_tax_amountTracker = param != null;

		this.localShipping_tax_amount = param;


	}


	/**
	 * field for Shipping_tax_refunded
	 */


	protected java.lang.String localShipping_tax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_tax_refundedTracker = false;

	public boolean isShipping_tax_refundedSpecified()
	{
		return localShipping_tax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_tax_refunded()
	{
		return localShipping_tax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_tax_refunded
	 */
	public void setShipping_tax_refunded(java.lang.String param)
	{
		localShipping_tax_refundedTracker = param != null;

		this.localShipping_tax_refunded = param;


	}


	/**
	 * field for Subtotal_canceled
	 */


	protected java.lang.String localSubtotal_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_canceledTracker = false;

	public boolean isSubtotal_canceledSpecified()
	{
		return localSubtotal_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal_canceled()
	{
		return localSubtotal_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_canceled
	 */
	public void setSubtotal_canceled(java.lang.String param)
	{
		localSubtotal_canceledTracker = param != null;

		this.localSubtotal_canceled = param;


	}


	/**
	 * field for Subtotal_invoiced
	 */


	protected java.lang.String localSubtotal_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_invoicedTracker = false;

	public boolean isSubtotal_invoicedSpecified()
	{
		return localSubtotal_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal_invoiced()
	{
		return localSubtotal_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_invoiced
	 */
	public void setSubtotal_invoiced(java.lang.String param)
	{
		localSubtotal_invoicedTracker = param != null;

		this.localSubtotal_invoiced = param;


	}


	/**
	 * field for Subtotal_refunded
	 */


	protected java.lang.String localSubtotal_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_refundedTracker = false;

	public boolean isSubtotal_refundedSpecified()
	{
		return localSubtotal_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal_refunded()
	{
		return localSubtotal_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_refunded
	 */
	public void setSubtotal_refunded(java.lang.String param)
	{
		localSubtotal_refundedTracker = param != null;

		this.localSubtotal_refunded = param;


	}


	/**
	 * field for Tax_canceled
	 */


	protected java.lang.String localTax_canceled;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_canceledTracker = false;

	public boolean isTax_canceledSpecified()
	{
		return localTax_canceledTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_canceled()
	{
		return localTax_canceled;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_canceled
	 */
	public void setTax_canceled(java.lang.String param)
	{
		localTax_canceledTracker = param != null;

		this.localTax_canceled = param;


	}


	/**
	 * field for Tax_invoiced
	 */


	protected java.lang.String localTax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_invoicedTracker = false;

	public boolean isTax_invoicedSpecified()
	{
		return localTax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_invoiced()
	{
		return localTax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_invoiced
	 */
	public void setTax_invoiced(java.lang.String param)
	{
		localTax_invoicedTracker = param != null;

		this.localTax_invoiced = param;


	}


	/**
	 * field for Tax_refunded
	 */


	protected java.lang.String localTax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTax_refundedTracker = false;

	public boolean isTax_refundedSpecified()
	{
		return localTax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTax_refunded()
	{
		return localTax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tax_refunded
	 */
	public void setTax_refunded(java.lang.String param)
	{
		localTax_refundedTracker = param != null;

		this.localTax_refunded = param;


	}


	/**
	 * field for Can_ship_partially
	 */


	protected java.lang.String localCan_ship_partially;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCan_ship_partiallyTracker = false;

	public boolean isCan_ship_partiallySpecified()
	{
		return localCan_ship_partiallyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCan_ship_partially()
	{
		return localCan_ship_partially;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Can_ship_partially
	 */
	public void setCan_ship_partially(java.lang.String param)
	{
		localCan_ship_partiallyTracker = param != null;

		this.localCan_ship_partially = param;


	}


	/**
	 * field for Can_ship_partially_item
	 */


	protected java.lang.String localCan_ship_partially_item;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCan_ship_partially_itemTracker = false;

	public boolean isCan_ship_partially_itemSpecified()
	{
		return localCan_ship_partially_itemTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCan_ship_partially_item()
	{
		return localCan_ship_partially_item;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Can_ship_partially_item
	 */
	public void setCan_ship_partially_item(java.lang.String param)
	{
		localCan_ship_partially_itemTracker = param != null;

		this.localCan_ship_partially_item = param;


	}


	/**
	 * field for Edit_increment
	 */


	protected java.lang.String localEdit_increment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEdit_incrementTracker = false;

	public boolean isEdit_incrementSpecified()
	{
		return localEdit_incrementTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEdit_increment()
	{
		return localEdit_increment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Edit_increment
	 */
	public void setEdit_increment(java.lang.String param)
	{
		localEdit_incrementTracker = param != null;

		this.localEdit_increment = param;


	}


	/**
	 * field for Forced_do_shipment_with_invoice
	 */


	protected java.lang.String localForced_do_shipment_with_invoice;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localForced_do_shipment_with_invoiceTracker = false;

	public boolean isForced_do_shipment_with_invoiceSpecified()
	{
		return localForced_do_shipment_with_invoiceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getForced_do_shipment_with_invoice()
	{
		return localForced_do_shipment_with_invoice;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Forced_do_shipment_with_invoice
	 */
	public void setForced_do_shipment_with_invoice(java.lang.String param)
	{
		localForced_do_shipment_with_invoiceTracker = param != null;

		this.localForced_do_shipment_with_invoice = param;


	}


	/**
	 * field for Payment_authorization_expiration
	 */


	protected java.lang.String localPayment_authorization_expiration;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPayment_authorization_expirationTracker = false;

	public boolean isPayment_authorization_expirationSpecified()
	{
		return localPayment_authorization_expirationTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPayment_authorization_expiration()
	{
		return localPayment_authorization_expiration;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Payment_authorization_expiration
	 */
	public void setPayment_authorization_expiration(java.lang.String param)
	{
		localPayment_authorization_expirationTracker = param != null;

		this.localPayment_authorization_expiration = param;


	}


	/**
	 * field for Paypal_ipn_customer_notified
	 */


	protected java.lang.String localPaypal_ipn_customer_notified;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPaypal_ipn_customer_notifiedTracker = false;

	public boolean isPaypal_ipn_customer_notifiedSpecified()
	{
		return localPaypal_ipn_customer_notifiedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPaypal_ipn_customer_notified()
	{
		return localPaypal_ipn_customer_notified;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Paypal_ipn_customer_notified
	 */
	public void setPaypal_ipn_customer_notified(java.lang.String param)
	{
		localPaypal_ipn_customer_notifiedTracker = param != null;

		this.localPaypal_ipn_customer_notified = param;


	}


	/**
	 * field for Quote_address_id
	 */


	protected java.lang.String localQuote_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_address_idTracker = false;

	public boolean isQuote_address_idSpecified()
	{
		return localQuote_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQuote_address_id()
	{
		return localQuote_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_address_id
	 */
	public void setQuote_address_id(java.lang.String param)
	{
		localQuote_address_idTracker = param != null;

		this.localQuote_address_id = param;


	}


	/**
	 * field for Adjustment_negative
	 */


	protected java.lang.String localAdjustment_negative;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdjustment_negativeTracker = false;

	public boolean isAdjustment_negativeSpecified()
	{
		return localAdjustment_negativeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdjustment_negative()
	{
		return localAdjustment_negative;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Adjustment_negative
	 */
	public void setAdjustment_negative(java.lang.String param)
	{
		localAdjustment_negativeTracker = param != null;

		this.localAdjustment_negative = param;


	}


	/**
	 * field for Adjustment_positive
	 */


	protected java.lang.String localAdjustment_positive;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdjustment_positiveTracker = false;

	public boolean isAdjustment_positiveSpecified()
	{
		return localAdjustment_positiveTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdjustment_positive()
	{
		return localAdjustment_positive;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Adjustment_positive
	 */
	public void setAdjustment_positive(java.lang.String param)
	{
		localAdjustment_positiveTracker = param != null;

		this.localAdjustment_positive = param;


	}


	/**
	 * field for Base_adjustment_negative
	 */


	protected java.lang.String localBase_adjustment_negative;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_adjustment_negativeTracker = false;

	public boolean isBase_adjustment_negativeSpecified()
	{
		return localBase_adjustment_negativeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_adjustment_negative()
	{
		return localBase_adjustment_negative;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_adjustment_negative
	 */
	public void setBase_adjustment_negative(java.lang.String param)
	{
		localBase_adjustment_negativeTracker = param != null;

		this.localBase_adjustment_negative = param;


	}


	/**
	 * field for Base_adjustment_positive
	 */


	protected java.lang.String localBase_adjustment_positive;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_adjustment_positiveTracker = false;

	public boolean isBase_adjustment_positiveSpecified()
	{
		return localBase_adjustment_positiveTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_adjustment_positive()
	{
		return localBase_adjustment_positive;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_adjustment_positive
	 */
	public void setBase_adjustment_positive(java.lang.String param)
	{
		localBase_adjustment_positiveTracker = param != null;

		this.localBase_adjustment_positive = param;


	}


	/**
	 * field for Base_shipping_discount_amount
	 */


	protected java.lang.String localBase_shipping_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_discount_amountTracker = false;

	public boolean isBase_shipping_discount_amountSpecified()
	{
		return localBase_shipping_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_discount_amount()
	{
		return localBase_shipping_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_discount_amount
	 */
	public void setBase_shipping_discount_amount(java.lang.String param)
	{
		localBase_shipping_discount_amountTracker = param != null;

		this.localBase_shipping_discount_amount = param;


	}


	/**
	 * field for Base_subtotal_incl_tax
	 */


	protected java.lang.String localBase_subtotal_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_incl_taxTracker = false;

	public boolean isBase_subtotal_incl_taxSpecified()
	{
		return localBase_subtotal_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_subtotal_incl_tax()
	{
		return localBase_subtotal_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_incl_tax
	 */
	public void setBase_subtotal_incl_tax(java.lang.String param)
	{
		localBase_subtotal_incl_taxTracker = param != null;

		this.localBase_subtotal_incl_tax = param;


	}


	/**
	 * field for Base_total_due
	 */


	protected java.lang.String localBase_total_due;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_total_dueTracker = false;

	public boolean isBase_total_dueSpecified()
	{
		return localBase_total_dueTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_total_due()
	{
		return localBase_total_due;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_total_due
	 */
	public void setBase_total_due(java.lang.String param)
	{
		localBase_total_dueTracker = param != null;

		this.localBase_total_due = param;


	}


	/**
	 * field for Payment_authorization_amount
	 */


	protected java.lang.String localPayment_authorization_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPayment_authorization_amountTracker = false;

	public boolean isPayment_authorization_amountSpecified()
	{
		return localPayment_authorization_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPayment_authorization_amount()
	{
		return localPayment_authorization_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Payment_authorization_amount
	 */
	public void setPayment_authorization_amount(java.lang.String param)
	{
		localPayment_authorization_amountTracker = param != null;

		this.localPayment_authorization_amount = param;


	}


	/**
	 * field for Shipping_discount_amount
	 */


	protected java.lang.String localShipping_discount_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_discount_amountTracker = false;

	public boolean isShipping_discount_amountSpecified()
	{
		return localShipping_discount_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_discount_amount()
	{
		return localShipping_discount_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_discount_amount
	 */
	public void setShipping_discount_amount(java.lang.String param)
	{
		localShipping_discount_amountTracker = param != null;

		this.localShipping_discount_amount = param;


	}


	/**
	 * field for Subtotal_incl_tax
	 */


	protected java.lang.String localSubtotal_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_incl_taxTracker = false;

	public boolean isSubtotal_incl_taxSpecified()
	{
		return localSubtotal_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSubtotal_incl_tax()
	{
		return localSubtotal_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_incl_tax
	 */
	public void setSubtotal_incl_tax(java.lang.String param)
	{
		localSubtotal_incl_taxTracker = param != null;

		this.localSubtotal_incl_tax = param;


	}


	/**
	 * field for Total_due
	 */


	protected java.lang.String localTotal_due;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_dueTracker = false;

	public boolean isTotal_dueSpecified()
	{
		return localTotal_dueTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_due()
	{
		return localTotal_due;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_due
	 */
	public void setTotal_due(java.lang.String param)
	{
		localTotal_dueTracker = param != null;

		this.localTotal_due = param;


	}


	/**
	 * field for Customer_dob
	 */


	protected java.lang.String localCustomer_dob;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_dobTracker = false;

	public boolean isCustomer_dobSpecified()
	{
		return localCustomer_dobTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_dob()
	{
		return localCustomer_dob;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_dob
	 */
	public void setCustomer_dob(java.lang.String param)
	{
		localCustomer_dobTracker = param != null;

		this.localCustomer_dob = param;


	}


	/**
	 * field for Customer_middlename
	 */


	protected java.lang.String localCustomer_middlename;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_middlenameTracker = false;

	public boolean isCustomer_middlenameSpecified()
	{
		return localCustomer_middlenameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_middlename()
	{
		return localCustomer_middlename;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_middlename
	 */
	public void setCustomer_middlename(java.lang.String param)
	{
		localCustomer_middlenameTracker = param != null;

		this.localCustomer_middlename = param;


	}


	/**
	 * field for Customer_prefix
	 */


	protected java.lang.String localCustomer_prefix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_prefixTracker = false;

	public boolean isCustomer_prefixSpecified()
	{
		return localCustomer_prefixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_prefix()
	{
		return localCustomer_prefix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_prefix
	 */
	public void setCustomer_prefix(java.lang.String param)
	{
		localCustomer_prefixTracker = param != null;

		this.localCustomer_prefix = param;


	}


	/**
	 * field for Customer_suffix
	 */


	protected java.lang.String localCustomer_suffix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_suffixTracker = false;

	public boolean isCustomer_suffixSpecified()
	{
		return localCustomer_suffixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_suffix()
	{
		return localCustomer_suffix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_suffix
	 */
	public void setCustomer_suffix(java.lang.String param)
	{
		localCustomer_suffixTracker = param != null;

		this.localCustomer_suffix = param;


	}


	/**
	 * field for Customer_taxvat
	 */


	protected java.lang.String localCustomer_taxvat;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_taxvatTracker = false;

	public boolean isCustomer_taxvatSpecified()
	{
		return localCustomer_taxvatTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_taxvat()
	{
		return localCustomer_taxvat;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_taxvat
	 */
	public void setCustomer_taxvat(java.lang.String param)
	{
		localCustomer_taxvatTracker = param != null;

		this.localCustomer_taxvat = param;


	}


	/**
	 * field for Discount_description
	 */


	protected java.lang.String localDiscount_description;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localDiscount_descriptionTracker = false;

	public boolean isDiscount_descriptionSpecified()
	{
		return localDiscount_descriptionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getDiscount_description()
	{
		return localDiscount_description;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Discount_description
	 */
	public void setDiscount_description(java.lang.String param)
	{
		localDiscount_descriptionTracker = param != null;

		this.localDiscount_description = param;


	}


	/**
	 * field for Ext_customer_id
	 */


	protected java.lang.String localExt_customer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localExt_customer_idTracker = false;

	public boolean isExt_customer_idSpecified()
	{
		return localExt_customer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getExt_customer_id()
	{
		return localExt_customer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Ext_customer_id
	 */
	public void setExt_customer_id(java.lang.String param)
	{
		localExt_customer_idTracker = param != null;

		this.localExt_customer_id = param;


	}


	/**
	 * field for Ext_order_id
	 */


	protected java.lang.String localExt_order_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localExt_order_idTracker = false;

	public boolean isExt_order_idSpecified()
	{
		return localExt_order_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getExt_order_id()
	{
		return localExt_order_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Ext_order_id
	 */
	public void setExt_order_id(java.lang.String param)
	{
		localExt_order_idTracker = param != null;

		this.localExt_order_id = param;


	}


	/**
	 * field for Hold_before_state
	 */


	protected java.lang.String localHold_before_state;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHold_before_stateTracker = false;

	public boolean isHold_before_stateSpecified()
	{
		return localHold_before_stateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHold_before_state()
	{
		return localHold_before_state;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hold_before_state
	 */
	public void setHold_before_state(java.lang.String param)
	{
		localHold_before_stateTracker = param != null;

		this.localHold_before_state = param;


	}


	/**
	 * field for Hold_before_status
	 */


	protected java.lang.String localHold_before_status;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHold_before_statusTracker = false;

	public boolean isHold_before_statusSpecified()
	{
		return localHold_before_statusTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHold_before_status()
	{
		return localHold_before_status;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hold_before_status
	 */
	public void setHold_before_status(java.lang.String param)
	{
		localHold_before_statusTracker = param != null;

		this.localHold_before_status = param;


	}


	/**
	 * field for Original_increment_id
	 */


	protected java.lang.String localOriginal_increment_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOriginal_increment_idTracker = false;

	public boolean isOriginal_increment_idSpecified()
	{
		return localOriginal_increment_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOriginal_increment_id()
	{
		return localOriginal_increment_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Original_increment_id
	 */
	public void setOriginal_increment_id(java.lang.String param)
	{
		localOriginal_increment_idTracker = param != null;

		this.localOriginal_increment_id = param;


	}


	/**
	 * field for Relation_child_id
	 */


	protected java.lang.String localRelation_child_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRelation_child_idTracker = false;

	public boolean isRelation_child_idSpecified()
	{
		return localRelation_child_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRelation_child_id()
	{
		return localRelation_child_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Relation_child_id
	 */
	public void setRelation_child_id(java.lang.String param)
	{
		localRelation_child_idTracker = param != null;

		this.localRelation_child_id = param;


	}


	/**
	 * field for Relation_child_real_id
	 */


	protected java.lang.String localRelation_child_real_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRelation_child_real_idTracker = false;

	public boolean isRelation_child_real_idSpecified()
	{
		return localRelation_child_real_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRelation_child_real_id()
	{
		return localRelation_child_real_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Relation_child_real_id
	 */
	public void setRelation_child_real_id(java.lang.String param)
	{
		localRelation_child_real_idTracker = param != null;

		this.localRelation_child_real_id = param;


	}


	/**
	 * field for Relation_parent_id
	 */


	protected java.lang.String localRelation_parent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRelation_parent_idTracker = false;

	public boolean isRelation_parent_idSpecified()
	{
		return localRelation_parent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRelation_parent_id()
	{
		return localRelation_parent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Relation_parent_id
	 */
	public void setRelation_parent_id(java.lang.String param)
	{
		localRelation_parent_idTracker = param != null;

		this.localRelation_parent_id = param;


	}


	/**
	 * field for Relation_parent_real_id
	 */


	protected java.lang.String localRelation_parent_real_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRelation_parent_real_idTracker = false;

	public boolean isRelation_parent_real_idSpecified()
	{
		return localRelation_parent_real_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRelation_parent_real_id()
	{
		return localRelation_parent_real_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Relation_parent_real_id
	 */
	public void setRelation_parent_real_id(java.lang.String param)
	{
		localRelation_parent_real_idTracker = param != null;

		this.localRelation_parent_real_id = param;


	}


	/**
	 * field for X_forwarded_for
	 */


	protected java.lang.String localX_forwarded_for;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localX_forwarded_forTracker = false;

	public boolean isX_forwarded_forSpecified()
	{
		return localX_forwarded_forTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getX_forwarded_for()
	{
		return localX_forwarded_for;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            X_forwarded_for
	 */
	public void setX_forwarded_for(java.lang.String param)
	{
		localX_forwarded_forTracker = param != null;

		this.localX_forwarded_for = param;


	}


	/**
	 * field for Customer_note
	 */


	protected java.lang.String localCustomer_note;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_noteTracker = false;

	public boolean isCustomer_noteSpecified()
	{
		return localCustomer_noteTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_note()
	{
		return localCustomer_note;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_note
	 */
	public void setCustomer_note(java.lang.String param)
	{
		localCustomer_noteTracker = param != null;

		this.localCustomer_note = param;


	}


	/**
	 * field for Total_item_count
	 */


	protected java.lang.String localTotal_item_count;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_item_countTracker = false;

	public boolean isTotal_item_countSpecified()
	{
		return localTotal_item_countTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_item_count()
	{
		return localTotal_item_count;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_item_count
	 */
	public void setTotal_item_count(java.lang.String param)
	{
		localTotal_item_countTracker = param != null;

		this.localTotal_item_count = param;


	}


	/**
	 * field for Customer_gender
	 */


	protected java.lang.String localCustomer_gender;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_genderTracker = false;

	public boolean isCustomer_genderSpecified()
	{
		return localCustomer_genderTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_gender()
	{
		return localCustomer_gender;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_gender
	 */
	public void setCustomer_gender(java.lang.String param)
	{
		localCustomer_genderTracker = param != null;

		this.localCustomer_gender = param;


	}


	/**
	 * field for Hidden_tax_amount
	 */


	protected java.lang.String localHidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHidden_tax_amountTracker = false;

	public boolean isHidden_tax_amountSpecified()
	{
		return localHidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHidden_tax_amount()
	{
		return localHidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hidden_tax_amount
	 */
	public void setHidden_tax_amount(java.lang.String param)
	{
		localHidden_tax_amountTracker = param != null;

		this.localHidden_tax_amount = param;


	}


	/**
	 * field for Base_hidden_tax_amount
	 */


	protected java.lang.String localBase_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_hidden_tax_amountTracker = false;

	public boolean isBase_hidden_tax_amountSpecified()
	{
		return localBase_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_hidden_tax_amount()
	{
		return localBase_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_hidden_tax_amount
	 */
	public void setBase_hidden_tax_amount(java.lang.String param)
	{
		localBase_hidden_tax_amountTracker = param != null;

		this.localBase_hidden_tax_amount = param;


	}


	/**
	 * field for Shipping_hidden_tax_amount
	 */


	protected java.lang.String localShipping_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_hidden_tax_amountTracker = false;

	public boolean isShipping_hidden_tax_amountSpecified()
	{
		return localShipping_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_hidden_tax_amount()
	{
		return localShipping_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_hidden_tax_amount
	 */
	public void setShipping_hidden_tax_amount(java.lang.String param)
	{
		localShipping_hidden_tax_amountTracker = param != null;

		this.localShipping_hidden_tax_amount = param;


	}


	/**
	 * field for Base_shipping_hidden_tax_amount
	 */


	protected java.lang.String localBase_shipping_hidden_tax_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_hidden_tax_amountTracker = false;

	public boolean isBase_shipping_hidden_tax_amountSpecified()
	{
		return localBase_shipping_hidden_tax_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_hidden_tax_amount()
	{
		return localBase_shipping_hidden_tax_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_hidden_tax_amount
	 */
	public void setBase_shipping_hidden_tax_amount(java.lang.String param)
	{
		localBase_shipping_hidden_tax_amountTracker = param != null;

		this.localBase_shipping_hidden_tax_amount = param;


	}


	/**
	 * field for Hidden_tax_invoiced
	 */


	protected java.lang.String localHidden_tax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHidden_tax_invoicedTracker = false;

	public boolean isHidden_tax_invoicedSpecified()
	{
		return localHidden_tax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHidden_tax_invoiced()
	{
		return localHidden_tax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hidden_tax_invoiced
	 */
	public void setHidden_tax_invoiced(java.lang.String param)
	{
		localHidden_tax_invoicedTracker = param != null;

		this.localHidden_tax_invoiced = param;


	}


	/**
	 * field for Base_hidden_tax_invoiced
	 */


	protected java.lang.String localBase_hidden_tax_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_hidden_tax_invoicedTracker = false;

	public boolean isBase_hidden_tax_invoicedSpecified()
	{
		return localBase_hidden_tax_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_hidden_tax_invoiced()
	{
		return localBase_hidden_tax_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_hidden_tax_invoiced
	 */
	public void setBase_hidden_tax_invoiced(java.lang.String param)
	{
		localBase_hidden_tax_invoicedTracker = param != null;

		this.localBase_hidden_tax_invoiced = param;


	}


	/**
	 * field for Hidden_tax_refunded
	 */


	protected java.lang.String localHidden_tax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localHidden_tax_refundedTracker = false;

	public boolean isHidden_tax_refundedSpecified()
	{
		return localHidden_tax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getHidden_tax_refunded()
	{
		return localHidden_tax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Hidden_tax_refunded
	 */
	public void setHidden_tax_refunded(java.lang.String param)
	{
		localHidden_tax_refundedTracker = param != null;

		this.localHidden_tax_refunded = param;


	}


	/**
	 * field for Base_hidden_tax_refunded
	 */


	protected java.lang.String localBase_hidden_tax_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_hidden_tax_refundedTracker = false;

	public boolean isBase_hidden_tax_refundedSpecified()
	{
		return localBase_hidden_tax_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_hidden_tax_refunded()
	{
		return localBase_hidden_tax_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_hidden_tax_refunded
	 */
	public void setBase_hidden_tax_refunded(java.lang.String param)
	{
		localBase_hidden_tax_refundedTracker = param != null;

		this.localBase_hidden_tax_refunded = param;


	}


	/**
	 * field for Shipping_incl_tax
	 */


	protected java.lang.String localShipping_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_incl_taxTracker = false;

	public boolean isShipping_incl_taxSpecified()
	{
		return localShipping_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_incl_tax()
	{
		return localShipping_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_incl_tax
	 */
	public void setShipping_incl_tax(java.lang.String param)
	{
		localShipping_incl_taxTracker = param != null;

		this.localShipping_incl_tax = param;


	}


	/**
	 * field for Base_shipping_incl_tax
	 */


	protected java.lang.String localBase_shipping_incl_tax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_shipping_incl_taxTracker = false;

	public boolean isBase_shipping_incl_taxSpecified()
	{
		return localBase_shipping_incl_taxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_shipping_incl_tax()
	{
		return localBase_shipping_incl_tax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_shipping_incl_tax
	 */
	public void setBase_shipping_incl_tax(java.lang.String param)
	{
		localBase_shipping_incl_taxTracker = param != null;

		this.localBase_shipping_incl_tax = param;


	}


	/**
	 * field for Base_customer_balance_amount
	 */


	protected java.lang.String localBase_customer_balance_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_amountTracker = false;

	public boolean isBase_customer_balance_amountSpecified()
	{
		return localBase_customer_balance_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_customer_balance_amount()
	{
		return localBase_customer_balance_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_amount
	 */
	public void setBase_customer_balance_amount(java.lang.String param)
	{
		localBase_customer_balance_amountTracker = param != null;

		this.localBase_customer_balance_amount = param;


	}


	/**
	 * field for Customer_balance_amount
	 */


	protected java.lang.String localCustomer_balance_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_amountTracker = false;

	public boolean isCustomer_balance_amountSpecified()
	{
		return localCustomer_balance_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_balance_amount()
	{
		return localCustomer_balance_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_amount
	 */
	public void setCustomer_balance_amount(java.lang.String param)
	{
		localCustomer_balance_amountTracker = param != null;

		this.localCustomer_balance_amount = param;


	}


	/**
	 * field for Base_customer_balance_invoiced
	 */


	protected java.lang.String localBase_customer_balance_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_invoicedTracker = false;

	public boolean isBase_customer_balance_invoicedSpecified()
	{
		return localBase_customer_balance_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_customer_balance_invoiced()
	{
		return localBase_customer_balance_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_invoiced
	 */
	public void setBase_customer_balance_invoiced(java.lang.String param)
	{
		localBase_customer_balance_invoicedTracker = param != null;

		this.localBase_customer_balance_invoiced = param;


	}


	/**
	 * field for Customer_balance_invoiced
	 */


	protected java.lang.String localCustomer_balance_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_invoicedTracker = false;

	public boolean isCustomer_balance_invoicedSpecified()
	{
		return localCustomer_balance_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_balance_invoiced()
	{
		return localCustomer_balance_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_invoiced
	 */
	public void setCustomer_balance_invoiced(java.lang.String param)
	{
		localCustomer_balance_invoicedTracker = param != null;

		this.localCustomer_balance_invoiced = param;


	}


	/**
	 * field for Base_customer_balance_refunded
	 */


	protected java.lang.String localBase_customer_balance_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_refundedTracker = false;

	public boolean isBase_customer_balance_refundedSpecified()
	{
		return localBase_customer_balance_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_customer_balance_refunded()
	{
		return localBase_customer_balance_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_refunded
	 */
	public void setBase_customer_balance_refunded(java.lang.String param)
	{
		localBase_customer_balance_refundedTracker = param != null;

		this.localBase_customer_balance_refunded = param;


	}


	/**
	 * field for Customer_balance_refunded
	 */


	protected java.lang.String localCustomer_balance_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_refundedTracker = false;

	public boolean isCustomer_balance_refundedSpecified()
	{
		return localCustomer_balance_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_balance_refunded()
	{
		return localCustomer_balance_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_refunded
	 */
	public void setCustomer_balance_refunded(java.lang.String param)
	{
		localCustomer_balance_refundedTracker = param != null;

		this.localCustomer_balance_refunded = param;


	}


	/**
	 * field for Base_customer_balance_total_refunded
	 */


	protected java.lang.String localBase_customer_balance_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_total_refundedTracker = false;

	public boolean isBase_customer_balance_total_refundedSpecified()
	{
		return localBase_customer_balance_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_customer_balance_total_refunded()
	{
		return localBase_customer_balance_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_total_refunded
	 */
	public void setBase_customer_balance_total_refunded(java.lang.String param)
	{
		localBase_customer_balance_total_refundedTracker = param != null;

		this.localBase_customer_balance_total_refunded = param;


	}


	/**
	 * field for Customer_balance_total_refunded
	 */


	protected java.lang.String localCustomer_balance_total_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_total_refundedTracker = false;

	public boolean isCustomer_balance_total_refundedSpecified()
	{
		return localCustomer_balance_total_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_balance_total_refunded()
	{
		return localCustomer_balance_total_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_total_refunded
	 */
	public void setCustomer_balance_total_refunded(java.lang.String param)
	{
		localCustomer_balance_total_refundedTracker = param != null;

		this.localCustomer_balance_total_refunded = param;


	}


	/**
	 * field for Gift_cards
	 */


	protected java.lang.String localGift_cards;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cardsTracker = false;

	public boolean isGift_cardsSpecified()
	{
		return localGift_cardsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards()
	{
		return localGift_cards;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards
	 */
	public void setGift_cards(java.lang.String param)
	{
		localGift_cardsTracker = param != null;

		this.localGift_cards = param;


	}


	/**
	 * field for Base_gift_cards_amount
	 */


	protected java.lang.String localBase_gift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_gift_cards_amountTracker = false;

	public boolean isBase_gift_cards_amountSpecified()
	{
		return localBase_gift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_gift_cards_amount()
	{
		return localBase_gift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_gift_cards_amount
	 */
	public void setBase_gift_cards_amount(java.lang.String param)
	{
		localBase_gift_cards_amountTracker = param != null;

		this.localBase_gift_cards_amount = param;


	}


	/**
	 * field for Gift_cards_amount
	 */


	protected java.lang.String localGift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_amountTracker = false;

	public boolean isGift_cards_amountSpecified()
	{
		return localGift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_amount()
	{
		return localGift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_amount
	 */
	public void setGift_cards_amount(java.lang.String param)
	{
		localGift_cards_amountTracker = param != null;

		this.localGift_cards_amount = param;


	}


	/**
	 * field for Base_gift_cards_invoiced
	 */


	protected java.lang.String localBase_gift_cards_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_gift_cards_invoicedTracker = false;

	public boolean isBase_gift_cards_invoicedSpecified()
	{
		return localBase_gift_cards_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_gift_cards_invoiced()
	{
		return localBase_gift_cards_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_gift_cards_invoiced
	 */
	public void setBase_gift_cards_invoiced(java.lang.String param)
	{
		localBase_gift_cards_invoicedTracker = param != null;

		this.localBase_gift_cards_invoiced = param;


	}


	/**
	 * field for Gift_cards_invoiced
	 */


	protected java.lang.String localGift_cards_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_invoicedTracker = false;

	public boolean isGift_cards_invoicedSpecified()
	{
		return localGift_cards_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_invoiced()
	{
		return localGift_cards_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_invoiced
	 */
	public void setGift_cards_invoiced(java.lang.String param)
	{
		localGift_cards_invoicedTracker = param != null;

		this.localGift_cards_invoiced = param;


	}


	/**
	 * field for Base_gift_cards_refunded
	 */


	protected java.lang.String localBase_gift_cards_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_gift_cards_refundedTracker = false;

	public boolean isBase_gift_cards_refundedSpecified()
	{
		return localBase_gift_cards_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_gift_cards_refunded()
	{
		return localBase_gift_cards_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_gift_cards_refunded
	 */
	public void setBase_gift_cards_refunded(java.lang.String param)
	{
		localBase_gift_cards_refundedTracker = param != null;

		this.localBase_gift_cards_refunded = param;


	}


	/**
	 * field for Gift_cards_refunded
	 */


	protected java.lang.String localGift_cards_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_refundedTracker = false;

	public boolean isGift_cards_refundedSpecified()
	{
		return localGift_cards_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_refunded()
	{
		return localGift_cards_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_refunded
	 */
	public void setGift_cards_refunded(java.lang.String param)
	{
		localGift_cards_refundedTracker = param != null;

		this.localGift_cards_refunded = param;


	}


	/**
	 * field for Reward_points_balance
	 */


	protected java.lang.String localReward_points_balance;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balanceTracker = false;

	public boolean isReward_points_balanceSpecified()
	{
		return localReward_points_balanceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance()
	{
		return localReward_points_balance;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance
	 */
	public void setReward_points_balance(java.lang.String param)
	{
		localReward_points_balanceTracker = param != null;

		this.localReward_points_balance = param;


	}


	/**
	 * field for Base_reward_currency_amount
	 */


	protected java.lang.String localBase_reward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_reward_currency_amountTracker = false;

	public boolean isBase_reward_currency_amountSpecified()
	{
		return localBase_reward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_reward_currency_amount()
	{
		return localBase_reward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_reward_currency_amount
	 */
	public void setBase_reward_currency_amount(java.lang.String param)
	{
		localBase_reward_currency_amountTracker = param != null;

		this.localBase_reward_currency_amount = param;


	}


	/**
	 * field for Reward_currency_amount
	 */


	protected java.lang.String localReward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_currency_amountTracker = false;

	public boolean isReward_currency_amountSpecified()
	{
		return localReward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_currency_amount()
	{
		return localReward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_currency_amount
	 */
	public void setReward_currency_amount(java.lang.String param)
	{
		localReward_currency_amountTracker = param != null;

		this.localReward_currency_amount = param;


	}


	/**
	 * field for Base_reward_currency_amount_invoiced
	 */


	protected java.lang.String localBase_reward_currency_amount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_reward_currency_amount_invoicedTracker = false;

	public boolean isBase_reward_currency_amount_invoicedSpecified()
	{
		return localBase_reward_currency_amount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_reward_currency_amount_invoiced()
	{
		return localBase_reward_currency_amount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_reward_currency_amount_invoiced
	 */
	public void setBase_reward_currency_amount_invoiced(java.lang.String param)
	{
		localBase_reward_currency_amount_invoicedTracker = param != null;

		this.localBase_reward_currency_amount_invoiced = param;


	}


	/**
	 * field for Reward_currency_amount_invoiced
	 */


	protected java.lang.String localReward_currency_amount_invoiced;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_currency_amount_invoicedTracker = false;

	public boolean isReward_currency_amount_invoicedSpecified()
	{
		return localReward_currency_amount_invoicedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_currency_amount_invoiced()
	{
		return localReward_currency_amount_invoiced;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_currency_amount_invoiced
	 */
	public void setReward_currency_amount_invoiced(java.lang.String param)
	{
		localReward_currency_amount_invoicedTracker = param != null;

		this.localReward_currency_amount_invoiced = param;


	}


	/**
	 * field for Base_reward_currency_amount_refunded
	 */


	protected java.lang.String localBase_reward_currency_amount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_reward_currency_amount_refundedTracker = false;

	public boolean isBase_reward_currency_amount_refundedSpecified()
	{
		return localBase_reward_currency_amount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_reward_currency_amount_refunded()
	{
		return localBase_reward_currency_amount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_reward_currency_amount_refunded
	 */
	public void setBase_reward_currency_amount_refunded(java.lang.String param)
	{
		localBase_reward_currency_amount_refundedTracker = param != null;

		this.localBase_reward_currency_amount_refunded = param;


	}


	/**
	 * field for Reward_currency_amount_refunded
	 */


	protected java.lang.String localReward_currency_amount_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_currency_amount_refundedTracker = false;

	public boolean isReward_currency_amount_refundedSpecified()
	{
		return localReward_currency_amount_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_currency_amount_refunded()
	{
		return localReward_currency_amount_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_currency_amount_refunded
	 */
	public void setReward_currency_amount_refunded(java.lang.String param)
	{
		localReward_currency_amount_refundedTracker = param != null;

		this.localReward_currency_amount_refunded = param;


	}


	/**
	 * field for Reward_points_balance_refunded
	 */


	protected java.lang.String localReward_points_balance_refunded;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balance_refundedTracker = false;

	public boolean isReward_points_balance_refundedSpecified()
	{
		return localReward_points_balance_refundedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance_refunded()
	{
		return localReward_points_balance_refunded;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance_refunded
	 */
	public void setReward_points_balance_refunded(java.lang.String param)
	{
		localReward_points_balance_refundedTracker = param != null;

		this.localReward_points_balance_refunded = param;


	}


	/**
	 * field for Reward_points_balance_to_refund
	 */


	protected java.lang.String localReward_points_balance_to_refund;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balance_to_refundTracker = false;

	public boolean isReward_points_balance_to_refundSpecified()
	{
		return localReward_points_balance_to_refundTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance_to_refund()
	{
		return localReward_points_balance_to_refund;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance_to_refund
	 */
	public void setReward_points_balance_to_refund(java.lang.String param)
	{
		localReward_points_balance_to_refundTracker = param != null;

		this.localReward_points_balance_to_refund = param;


	}


	/**
	 * field for Reward_salesrule_points
	 */


	protected java.lang.String localReward_salesrule_points;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_salesrule_pointsTracker = false;

	public boolean isReward_salesrule_pointsSpecified()
	{
		return localReward_salesrule_pointsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_salesrule_points()
	{
		return localReward_salesrule_points;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_salesrule_points
	 */
	public void setReward_salesrule_points(java.lang.String param)
	{
		localReward_salesrule_pointsTracker = param != null;

		this.localReward_salesrule_points = param;


	}


	/**
	 * field for Firstname
	 */


	protected java.lang.String localFirstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFirstnameTracker = false;

	public boolean isFirstnameSpecified()
	{
		return localFirstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFirstname()
	{
		return localFirstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Firstname
	 */
	public void setFirstname(java.lang.String param)
	{
		localFirstnameTracker = param != null;

		this.localFirstname = param;


	}


	/**
	 * field for Lastname
	 */


	protected java.lang.String localLastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localLastnameTracker = false;

	public boolean isLastnameSpecified()
	{
		return localLastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getLastname()
	{
		return localLastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Lastname
	 */
	public void setLastname(java.lang.String param)
	{
		localLastnameTracker = param != null;

		this.localLastname = param;


	}


	/**
	 * field for Telephone
	 */


	protected java.lang.String localTelephone;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTelephoneTracker = false;

	public boolean isTelephoneSpecified()
	{
		return localTelephoneTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTelephone()
	{
		return localTelephone;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Telephone
	 */
	public void setTelephone(java.lang.String param)
	{
		localTelephoneTracker = param != null;

		this.localTelephone = param;


	}


	/**
	 * field for Postcode
	 */


	protected java.lang.String localPostcode;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPostcodeTracker = false;

	public boolean isPostcodeSpecified()
	{
		return localPostcodeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPostcode()
	{
		return localPostcode;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Postcode
	 */
	public void setPostcode(java.lang.String param)
	{
		localPostcodeTracker = param != null;

		this.localPostcode = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderListEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderListEntity", xmlWriter);
			}


		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);


			if (localStore_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_id", xmlWriter);


			if (localCustomer_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_amount", xmlWriter);


			if (localTax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_amount", xmlWriter);


			if (localShipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_amount", xmlWriter);


			if (localDiscount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal", xmlWriter);


			if (localSubtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localGrand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "grand_total", xmlWriter);


			if (localGrand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGrand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_paidTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_paid", xmlWriter);


			if (localTotal_paid == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_paid cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_paid);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_refunded", xmlWriter);


			if (localTotal_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_qty_orderedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_qty_ordered", xmlWriter);


			if (localTotal_qty_ordered == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_qty_ordered cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_qty_ordered);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_canceled", xmlWriter);


			if (localTotal_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_invoiced", xmlWriter);


			if (localTotal_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_online_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_online_refunded", xmlWriter);


			if (localTotal_online_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_online_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_online_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_offline_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_offline_refunded", xmlWriter);


			if (localTotal_offline_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_offline_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_offline_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_amount", xmlWriter);


			if (localBase_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_amount", xmlWriter);


			if (localBase_shipping_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_amount", xmlWriter);


			if (localBase_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal", xmlWriter);


			if (localBase_subtotal == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_grand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_grand_total", xmlWriter);


			if (localBase_grand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_grand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_paidTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_paid", xmlWriter);


			if (localBase_total_paid == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_paid cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_paid);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_refunded", xmlWriter);


			if (localBase_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_qty_orderedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_qty_ordered", xmlWriter);


			if (localBase_total_qty_ordered == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_qty_ordered cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_qty_ordered);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_canceled", xmlWriter);


			if (localBase_total_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_invoiced", xmlWriter);


			if (localBase_total_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_online_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_online_refunded", xmlWriter);


			if (localBase_total_online_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_online_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_online_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_offline_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_offline_refunded", xmlWriter);


			if (localBase_total_offline_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_offline_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_offline_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_address_id", xmlWriter);


			if (localBilling_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_firstname", xmlWriter);


			if (localBilling_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_lastname", xmlWriter);


			if (localBilling_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_address_id", xmlWriter);


			if (localShipping_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_firstname", xmlWriter);


			if (localShipping_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_lastname", xmlWriter);


			if (localShipping_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localBilling_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "billing_name", xmlWriter);


			if (localBilling_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("billing_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBilling_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_name", xmlWriter);


			if (localShipping_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_base_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_base_rate", xmlWriter);


			if (localStore_to_base_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_base_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_base_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_order_rate", xmlWriter);


			if (localStore_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_global_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_global_rate", xmlWriter);


			if (localBase_to_global_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_global_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_global_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_order_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_order_rate", xmlWriter);


			if (localBase_to_order_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_to_order_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_to_order_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weight", xmlWriter);


			if (localWeight == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeight);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_nameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_name", xmlWriter);


			if (localStore_name == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_name);

			}

			xmlWriter.writeEndElement();
		}
		if (localRemote_ipTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "remote_ip", xmlWriter);


			if (localRemote_ip == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("remote_ip cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRemote_ip);

			}

			xmlWriter.writeEndElement();
		}
		if (localStatusTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "status", xmlWriter);


			if (localStatus == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStatus);

			}

			xmlWriter.writeEndElement();
		}
		if (localStateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "state", xmlWriter);


			if (localState == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localState);

			}

			xmlWriter.writeEndElement();
		}
		if (localApplied_rule_idsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "applied_rule_ids", xmlWriter);


			if (localApplied_rule_ids == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("applied_rule_ids cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localApplied_rule_ids);

			}

			xmlWriter.writeEndElement();
		}
		if (localGlobal_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "global_currency_code", xmlWriter);


			if (localGlobal_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("global_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGlobal_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_currency_code", xmlWriter);


			if (localBase_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_currency_code", xmlWriter);


			if (localStore_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_currency_code", xmlWriter);


			if (localOrder_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_methodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_method", xmlWriter);


			if (localShipping_method == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_method);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_descriptionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_description", xmlWriter);


			if (localShipping_description == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_description cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_description);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_emailTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_email", xmlWriter);


			if (localCustomer_email == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_email cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_email);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_firstname", xmlWriter);


			if (localCustomer_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_lastname", xmlWriter);


			if (localCustomer_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_id", xmlWriter);


			if (localQuote_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("quote_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQuote_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_virtualTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_virtual", xmlWriter);


			if (localIs_virtual == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_virtual cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_virtual);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_group_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_group_id", xmlWriter);


			if (localCustomer_group_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_group_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_group_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_note_notifyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_note_notify", xmlWriter);


			if (localCustomer_note_notify == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_note_notify cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_note_notify);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_is_guestTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_is_guest", xmlWriter);


			if (localCustomer_is_guest == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_is_guest cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_is_guest);

			}

			xmlWriter.writeEndElement();
		}
		if (localEmail_sentTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "email_sent", xmlWriter);


			if (localEmail_sent == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("email_sent cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEmail_sent);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_id", xmlWriter);


			if (localOrder_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_message_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message_id", xmlWriter);


			if (localGift_message_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCoupon_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "coupon_code", xmlWriter);


			if (localCoupon_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("coupon_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCoupon_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localProtect_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "protect_code", xmlWriter);


			if (localProtect_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("protect_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProtect_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_canceled", xmlWriter);


			if (localBase_discount_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_invoiced", xmlWriter);


			if (localBase_discount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_discount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_discount_refunded", xmlWriter);


			if (localBase_discount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_discount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_discount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_canceled", xmlWriter);


			if (localBase_shipping_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_invoiced", xmlWriter);


			if (localBase_shipping_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_refunded", xmlWriter);


			if (localBase_shipping_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_tax_amount", xmlWriter);


			if (localBase_shipping_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_tax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_tax_refunded", xmlWriter);


			if (localBase_shipping_tax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_tax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_canceled", xmlWriter);


			if (localBase_subtotal_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_invoiced", xmlWriter);


			if (localBase_subtotal_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_refunded", xmlWriter);


			if (localBase_subtotal_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_canceled", xmlWriter);


			if (localBase_tax_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_invoiced", xmlWriter);


			if (localBase_tax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_tax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_tax_refunded", xmlWriter);


			if (localBase_tax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_tax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_invoiced_costTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_invoiced_cost", xmlWriter);


			if (localBase_total_invoiced_cost == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_invoiced_cost cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_invoiced_cost);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_canceled", xmlWriter);


			if (localDiscount_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_invoiced", xmlWriter);


			if (localDiscount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_refunded", xmlWriter);


			if (localDiscount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_canceled", xmlWriter);


			if (localShipping_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_invoiced", xmlWriter);


			if (localShipping_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_refunded", xmlWriter);


			if (localShipping_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_tax_amount", xmlWriter);


			if (localShipping_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_tax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_tax_refunded", xmlWriter);


			if (localShipping_tax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_tax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_canceled", xmlWriter);


			if (localSubtotal_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_invoiced", xmlWriter);


			if (localSubtotal_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_refunded", xmlWriter);


			if (localSubtotal_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_canceledTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_canceled", xmlWriter);


			if (localTax_canceled == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_canceled cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_canceled);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_invoiced", xmlWriter);


			if (localTax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localTax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "tax_refunded", xmlWriter);


			if (localTax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localCan_ship_partiallyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "can_ship_partially", xmlWriter);


			if (localCan_ship_partially == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("can_ship_partially cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCan_ship_partially);

			}

			xmlWriter.writeEndElement();
		}
		if (localCan_ship_partially_itemTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "can_ship_partially_item", xmlWriter);


			if (localCan_ship_partially_item == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("can_ship_partially_item cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCan_ship_partially_item);

			}

			xmlWriter.writeEndElement();
		}
		if (localEdit_incrementTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "edit_increment", xmlWriter);


			if (localEdit_increment == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("edit_increment cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEdit_increment);

			}

			xmlWriter.writeEndElement();
		}
		if (localForced_do_shipment_with_invoiceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "forced_do_shipment_with_invoice", xmlWriter);


			if (localForced_do_shipment_with_invoice == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("forced_do_shipment_with_invoice cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localForced_do_shipment_with_invoice);

			}

			xmlWriter.writeEndElement();
		}
		if (localPayment_authorization_expirationTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "payment_authorization_expiration", xmlWriter);


			if (localPayment_authorization_expiration == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("payment_authorization_expiration cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPayment_authorization_expiration);

			}

			xmlWriter.writeEndElement();
		}
		if (localPaypal_ipn_customer_notifiedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "paypal_ipn_customer_notified", xmlWriter);


			if (localPaypal_ipn_customer_notified == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("paypal_ipn_customer_notified cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPaypal_ipn_customer_notified);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_address_id", xmlWriter);


			if (localQuote_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("quote_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQuote_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdjustment_negativeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "adjustment_negative", xmlWriter);


			if (localAdjustment_negative == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("adjustment_negative cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdjustment_negative);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdjustment_positiveTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "adjustment_positive", xmlWriter);


			if (localAdjustment_positive == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("adjustment_positive cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdjustment_positive);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_adjustment_negativeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_adjustment_negative", xmlWriter);


			if (localBase_adjustment_negative == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_adjustment_negative cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_adjustment_negative);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_adjustment_positiveTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_adjustment_positive", xmlWriter);


			if (localBase_adjustment_positive == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_adjustment_positive cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_adjustment_positive);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_discount_amount", xmlWriter);


			if (localBase_shipping_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_incl_tax", xmlWriter);


			if (localBase_subtotal_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_subtotal_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_total_dueTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_total_due", xmlWriter);


			if (localBase_total_due == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_total_due cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_total_due);

			}

			xmlWriter.writeEndElement();
		}
		if (localPayment_authorization_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "payment_authorization_amount", xmlWriter);


			if (localPayment_authorization_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("payment_authorization_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPayment_authorization_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_discount_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_discount_amount", xmlWriter);


			if (localShipping_discount_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_discount_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_discount_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_incl_tax", xmlWriter);


			if (localSubtotal_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("subtotal_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSubtotal_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_dueTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_due", xmlWriter);


			if (localTotal_due == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_due cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_due);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_dobTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_dob", xmlWriter);


			if (localCustomer_dob == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_dob cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_dob);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_middlenameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_middlename", xmlWriter);


			if (localCustomer_middlename == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_middlename cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_middlename);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_prefixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_prefix", xmlWriter);


			if (localCustomer_prefix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_prefix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_prefix);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_suffixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_suffix", xmlWriter);


			if (localCustomer_suffix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_suffix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_suffix);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_taxvatTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_taxvat", xmlWriter);


			if (localCustomer_taxvat == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_taxvat cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_taxvat);

			}

			xmlWriter.writeEndElement();
		}
		if (localDiscount_descriptionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "discount_description", xmlWriter);


			if (localDiscount_description == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("discount_description cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localDiscount_description);

			}

			xmlWriter.writeEndElement();
		}
		if (localExt_customer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "ext_customer_id", xmlWriter);


			if (localExt_customer_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("ext_customer_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localExt_customer_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localExt_order_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "ext_order_id", xmlWriter);


			if (localExt_order_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("ext_order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localExt_order_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localHold_before_stateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hold_before_state", xmlWriter);


			if (localHold_before_state == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hold_before_state cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHold_before_state);

			}

			xmlWriter.writeEndElement();
		}
		if (localHold_before_statusTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hold_before_status", xmlWriter);


			if (localHold_before_status == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hold_before_status cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHold_before_status);

			}

			xmlWriter.writeEndElement();
		}
		if (localOriginal_increment_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "original_increment_id", xmlWriter);


			if (localOriginal_increment_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("original_increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOriginal_increment_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localRelation_child_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "relation_child_id", xmlWriter);


			if (localRelation_child_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("relation_child_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRelation_child_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localRelation_child_real_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "relation_child_real_id", xmlWriter);


			if (localRelation_child_real_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("relation_child_real_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRelation_child_real_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localRelation_parent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "relation_parent_id", xmlWriter);


			if (localRelation_parent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("relation_parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRelation_parent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localRelation_parent_real_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "relation_parent_real_id", xmlWriter);


			if (localRelation_parent_real_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("relation_parent_real_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRelation_parent_real_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localX_forwarded_forTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "x_forwarded_for", xmlWriter);


			if (localX_forwarded_for == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("x_forwarded_for cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localX_forwarded_for);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_noteTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_note", xmlWriter);


			if (localCustomer_note == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_note cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_note);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_item_countTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_item_count", xmlWriter);


			if (localTotal_item_count == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_item_count cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_item_count);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_genderTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_gender", xmlWriter);


			if (localCustomer_gender == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_gender cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_gender);

			}

			xmlWriter.writeEndElement();
		}
		if (localHidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hidden_tax_amount", xmlWriter);


			if (localHidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_hidden_tax_amount", xmlWriter);


			if (localBase_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_hidden_tax_amount", xmlWriter);


			if (localShipping_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_hidden_tax_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_hidden_tax_amount", xmlWriter);


			if (localBase_shipping_hidden_tax_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_hidden_tax_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_hidden_tax_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localHidden_tax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hidden_tax_invoiced", xmlWriter);


			if (localHidden_tax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hidden_tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHidden_tax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_hidden_tax_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_hidden_tax_invoiced", xmlWriter);


			if (localBase_hidden_tax_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_hidden_tax_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_hidden_tax_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localHidden_tax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "hidden_tax_refunded", xmlWriter);


			if (localHidden_tax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("hidden_tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localHidden_tax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_hidden_tax_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_hidden_tax_refunded", xmlWriter);


			if (localBase_hidden_tax_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_hidden_tax_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_hidden_tax_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_incl_tax", xmlWriter);


			if (localShipping_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_shipping_incl_taxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_shipping_incl_tax", xmlWriter);


			if (localBase_shipping_incl_tax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_shipping_incl_tax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_shipping_incl_tax);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_amount", xmlWriter);


			if (localBase_customer_balance_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_customer_balance_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_amount", xmlWriter);


			if (localCustomer_balance_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_balance_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_balance_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_invoiced", xmlWriter);


			if (localBase_customer_balance_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_customer_balance_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_invoiced", xmlWriter);


			if (localCustomer_balance_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_balance_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_balance_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_refunded", xmlWriter);


			if (localBase_customer_balance_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_customer_balance_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_refunded", xmlWriter);


			if (localCustomer_balance_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_balance_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_balance_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_total_refunded", xmlWriter);


			if (localBase_customer_balance_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_customer_balance_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_total_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_total_refunded", xmlWriter);


			if (localCustomer_balance_total_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_balance_total_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_balance_total_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cardsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards", xmlWriter);


			if (localGift_cards == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_gift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_gift_cards_amount", xmlWriter);


			if (localBase_gift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_gift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_amount", xmlWriter);


			if (localGift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_gift_cards_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_gift_cards_invoiced", xmlWriter);


			if (localBase_gift_cards_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_gift_cards_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_gift_cards_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_invoiced", xmlWriter);


			if (localGift_cards_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_gift_cards_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_gift_cards_refunded", xmlWriter);


			if (localBase_gift_cards_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_gift_cards_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_gift_cards_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_refunded", xmlWriter);


			if (localGift_cards_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balanceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance", xmlWriter);


			if (localReward_points_balance == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_reward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_reward_currency_amount", xmlWriter);


			if (localBase_reward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_reward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_currency_amount", xmlWriter);


			if (localReward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_reward_currency_amount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_reward_currency_amount_invoiced", xmlWriter);


			if (localBase_reward_currency_amount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_reward_currency_amount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_reward_currency_amount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_currency_amount_invoicedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_currency_amount_invoiced", xmlWriter);


			if (localReward_currency_amount_invoiced == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_currency_amount_invoiced cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_currency_amount_invoiced);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_reward_currency_amount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_reward_currency_amount_refunded", xmlWriter);


			if (localBase_reward_currency_amount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_reward_currency_amount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_reward_currency_amount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_currency_amount_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_currency_amount_refunded", xmlWriter);


			if (localReward_currency_amount_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_currency_amount_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_currency_amount_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balance_refundedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance_refunded", xmlWriter);


			if (localReward_points_balance_refunded == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance_refunded cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance_refunded);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balance_to_refundTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance_to_refund", xmlWriter);


			if (localReward_points_balance_to_refund == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance_to_refund cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance_to_refund);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_salesrule_pointsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_salesrule_points", xmlWriter);


			if (localReward_salesrule_points == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_salesrule_points cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_salesrule_points);

			}

			xmlWriter.writeEndElement();
		}
		if (localFirstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "firstname", xmlWriter);


			if (localFirstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFirstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localLastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "lastname", xmlWriter);


			if (localLastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localLastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localTelephoneTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "telephone", xmlWriter);


			if (localTelephone == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("telephone cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTelephone);

			}

			xmlWriter.writeEndElement();
		}
		if (localPostcodeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "postcode", xmlWriter);


			if (localPostcode == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("postcode cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPostcode);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderListEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderListEntity object = new SalesOrderListEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderListEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderListEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGrand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_paid").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_paid" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_paid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_qty_ordered").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_qty_ordered" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_qty_ordered(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_online_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_online_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_online_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_offline_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_offline_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_offline_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_grand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_paid").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_paid" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_paid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_qty_ordered").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_qty_ordered" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_qty_ordered(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_online_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_online_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_online_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_offline_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_offline_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_offline_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "billing_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBilling_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_base_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_base_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_base_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_global_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_global_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_global_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_order_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_order_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_order_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeight(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_name(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "remote_ip").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "remote_ip" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRemote_ip(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "status").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "status" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStatus(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "state").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "state" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setState(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "applied_rule_ids").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "applied_rule_ids" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setApplied_rule_ids(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "global_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "global_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGlobal_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_method(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_description").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_description" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_description(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_email").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_email" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_email(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_virtual").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_virtual" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_virtual(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_group_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_group_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_group_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_note_notify").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_note_notify" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_note_notify(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_is_guest").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_is_guest" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_is_guest(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "email_sent").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "email_sent" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEmail_sent(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "coupon_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "coupon_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCoupon_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "protect_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "protect_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProtect_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_discount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_discount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_discount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_tax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_tax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_invoiced_cost").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_invoiced_cost" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_invoiced_cost(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_tax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_canceled").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_canceled" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_canceled(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "can_ship_partially").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "can_ship_partially" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCan_ship_partially(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "can_ship_partially_item").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "can_ship_partially_item" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCan_ship_partially_item(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "edit_increment").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "edit_increment" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEdit_increment(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "forced_do_shipment_with_invoice").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "forced_do_shipment_with_invoice" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setForced_do_shipment_with_invoice(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "payment_authorization_expiration").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "payment_authorization_expiration" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPayment_authorization_expiration(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "paypal_ipn_customer_notified").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "paypal_ipn_customer_notified" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPaypal_ipn_customer_notified(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "adjustment_negative").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "adjustment_negative" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdjustment_negative(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "adjustment_positive").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "adjustment_positive" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdjustment_positive(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_adjustment_negative").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_adjustment_negative" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_adjustment_negative(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_adjustment_positive").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_adjustment_positive" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_adjustment_positive(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_total_due").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_total_due" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_total_due(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "payment_authorization_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "payment_authorization_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPayment_authorization_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_discount_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_discount_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_discount_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_due").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_due" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_due(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_dob").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_dob" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_dob(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_middlename").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_middlename" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_middlename(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_prefix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_prefix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_prefix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_suffix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_suffix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_suffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_taxvat").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_taxvat" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_taxvat(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "discount_description").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "discount_description" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setDiscount_description(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "ext_customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "ext_customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setExt_customer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "ext_order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "ext_order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setExt_order_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hold_before_state").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hold_before_state" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHold_before_state(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hold_before_status").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hold_before_status" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHold_before_status(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "original_increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "original_increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOriginal_increment_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "relation_child_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "relation_child_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRelation_child_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "relation_child_real_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "relation_child_real_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRelation_child_real_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "relation_parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "relation_parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRelation_parent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "relation_parent_real_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "relation_parent_real_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRelation_parent_real_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "x_forwarded_for").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "x_forwarded_for" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setX_forwarded_for(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_note").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_note" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_note(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_item_count").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_item_count" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_item_count(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_gender").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_gender" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_gender(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_hidden_tax_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_hidden_tax_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_hidden_tax_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hidden_tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hidden_tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHidden_tax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_hidden_tax_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_hidden_tax_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_hidden_tax_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "hidden_tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "hidden_tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setHidden_tax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_hidden_tax_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_hidden_tax_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_hidden_tax_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_shipping_incl_tax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_shipping_incl_tax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_shipping_incl_tax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_total_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_total_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_total_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_gift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_gift_cards_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_gift_cards_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_gift_cards_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_gift_cards_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_gift_cards_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_gift_cards_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_reward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_reward_currency_amount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_reward_currency_amount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_reward_currency_amount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_currency_amount_invoiced").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_currency_amount_invoiced" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_currency_amount_invoiced(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_reward_currency_amount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_reward_currency_amount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_reward_currency_amount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_currency_amount_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_currency_amount_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_currency_amount_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance_refunded").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance_refunded" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance_refunded(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance_to_refund").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance_to_refund" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance_to_refund(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_salesrule_points").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_salesrule_points" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_salesrule_points(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFirstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setLastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "telephone").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "telephone" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTelephone(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "postcode").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "postcode" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPostcode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

