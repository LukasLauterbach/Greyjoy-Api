
/**
 * SalesOrderShipmentEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderShipmentEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderShipmentEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderShipmentEntity Namespace URI = urn:Magento Namespace Prefix =
	 * ns1
	 */


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Parent_id
	 */


	protected java.lang.String localParent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localParent_idTracker = false;

	public boolean isParent_idSpecified()
	{
		return localParent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getParent_id()
	{
		return localParent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Parent_id
	 */
	public void setParent_id(java.lang.String param)
	{
		localParent_idTracker = param != null;

		this.localParent_id = param;


	}


	/**
	 * field for Store_id
	 */


	protected java.lang.String localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(java.lang.String param)
	{
		localStore_idTracker = param != null;

		this.localStore_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Is_active
	 */


	protected java.lang.String localIs_active;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_activeTracker = false;

	public boolean isIs_activeSpecified()
	{
		return localIs_activeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_active()
	{
		return localIs_active;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_active
	 */
	public void setIs_active(java.lang.String param)
	{
		localIs_activeTracker = param != null;

		this.localIs_active = param;


	}


	/**
	 * field for Shipping_address_id
	 */


	protected java.lang.String localShipping_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_address_idTracker = false;

	public boolean isShipping_address_idSpecified()
	{
		return localShipping_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_address_id()
	{
		return localShipping_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address_id
	 */
	public void setShipping_address_id(java.lang.String param)
	{
		localShipping_address_idTracker = param != null;

		this.localShipping_address_id = param;


	}


	/**
	 * field for Shipping_firstname
	 */


	protected java.lang.String localShipping_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_firstnameTracker = false;

	public boolean isShipping_firstnameSpecified()
	{
		return localShipping_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_firstname()
	{
		return localShipping_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_firstname
	 */
	public void setShipping_firstname(java.lang.String param)
	{
		localShipping_firstnameTracker = param != null;

		this.localShipping_firstname = param;


	}


	/**
	 * field for Shipping_lastname
	 */


	protected java.lang.String localShipping_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_lastnameTracker = false;

	public boolean isShipping_lastnameSpecified()
	{
		return localShipping_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_lastname()
	{
		return localShipping_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_lastname
	 */
	public void setShipping_lastname(java.lang.String param)
	{
		localShipping_lastnameTracker = param != null;

		this.localShipping_lastname = param;


	}


	/**
	 * field for Order_id
	 */


	protected java.lang.String localOrder_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_idTracker = false;

	public boolean isOrder_idSpecified()
	{
		return localOrder_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_id()
	{
		return localOrder_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_id
	 */
	public void setOrder_id(java.lang.String param)
	{
		localOrder_idTracker = param != null;

		this.localOrder_id = param;


	}


	/**
	 * field for Order_increment_id
	 */


	protected java.lang.String localOrder_increment_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_increment_idTracker = false;

	public boolean isOrder_increment_idSpecified()
	{
		return localOrder_increment_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_increment_id()
	{
		return localOrder_increment_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_increment_id
	 */
	public void setOrder_increment_id(java.lang.String param)
	{
		localOrder_increment_idTracker = param != null;

		this.localOrder_increment_id = param;


	}


	/**
	 * field for Order_created_at
	 */


	protected java.lang.String localOrder_created_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_created_atTracker = false;

	public boolean isOrder_created_atSpecified()
	{
		return localOrder_created_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_created_at()
	{
		return localOrder_created_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_created_at
	 */
	public void setOrder_created_at(java.lang.String param)
	{
		localOrder_created_atTracker = param != null;

		this.localOrder_created_at = param;


	}


	/**
	 * field for Total_qty
	 */


	protected java.lang.String localTotal_qty;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTotal_qtyTracker = false;

	public boolean isTotal_qtySpecified()
	{
		return localTotal_qtyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTotal_qty()
	{
		return localTotal_qty;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Total_qty
	 */
	public void setTotal_qty(java.lang.String param)
	{
		localTotal_qtyTracker = param != null;

		this.localTotal_qty = param;


	}


	/**
	 * field for Shipment_id
	 */


	protected java.lang.String localShipment_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipment_idTracker = false;

	public boolean isShipment_idSpecified()
	{
		return localShipment_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipment_id()
	{
		return localShipment_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipment_id
	 */
	public void setShipment_id(java.lang.String param)
	{
		localShipment_idTracker = param != null;

		this.localShipment_id = param;


	}


	/**
	 * field for Items
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray localItems;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItemsTracker = false;

	public boolean isItemsSpecified()
	{
		return localItemsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray getItems()
	{
		return localItems;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items
	 */
	public void setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray param)
	{
		localItemsTracker = param != null;

		this.localItems = param;


	}


	/**
	 * field for Tracks
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray localTracks;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTracksTracker = false;

	public boolean isTracksSpecified()
	{
		return localTracksTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray getTracks()
	{
		return localTracks;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Tracks
	 */
	public void setTracks(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray param)
	{
		localTracksTracker = param != null;

		this.localTracks = param;


	}


	/**
	 * field for Comments
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray localComments;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCommentsTracker = false;

	public boolean isCommentsSpecified()
	{
		return localCommentsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray getComments()
	{
		return localComments;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Comments
	 */
	public void setComments(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray param)
	{
		localCommentsTracker = param != null;

		this.localComments = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderShipmentEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderShipmentEntity", xmlWriter);
			}


		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localParent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "parent_id", xmlWriter);


			if (localParent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localParent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);


			if (localStore_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_activeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_active", xmlWriter);


			if (localIs_active == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_active cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_active);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_address_id", xmlWriter);


			if (localShipping_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_firstname", xmlWriter);


			if (localShipping_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_lastname", xmlWriter);


			if (localShipping_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_id", xmlWriter);


			if (localOrder_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_increment_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_increment_id", xmlWriter);


			if (localOrder_increment_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_increment_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_created_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_created_at", xmlWriter);


			if (localOrder_created_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_created_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localTotal_qtyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "total_qty", xmlWriter);


			if (localTotal_qty == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("total_qty cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTotal_qty);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipment_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipment_id", xmlWriter);


			if (localShipment_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipment_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localItemsTracker)
		{
			if (localItems == null)
			{
				throw new org.apache.axis2.databinding.ADBException("items cannot be null!!");
			}
			localItems.serialize(new javax.xml.namespace.QName("", "items"), xmlWriter);
		}
		if (localTracksTracker)
		{
			if (localTracks == null)
			{
				throw new org.apache.axis2.databinding.ADBException("tracks cannot be null!!");
			}
			localTracks.serialize(new javax.xml.namespace.QName("", "tracks"), xmlWriter);
		}
		if (localCommentsTracker)
		{
			if (localComments == null)
			{
				throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
			}
			localComments.serialize(new javax.xml.namespace.QName("", "comments"), xmlWriter);
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderShipmentEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderShipmentEntity object = new SalesOrderShipmentEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderShipmentEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderShipmentEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setParent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_active").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_active" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_active(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_increment_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_created_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "total_qty").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "total_qty" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTotal_qty(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipment_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items").equals(reader.getName()))
				{

					object.setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentItemEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "tracks").equals(reader.getName()))
				{

					object.setTracks(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentTrackEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "comments").equals(reader.getName()))
				{

					object.setComments(greyjoy.travelsmart.adapter.sunbonoo.magento.SalesOrderShipmentCommentEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

