
/**
 * SalesOrderShipmentItemEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * SalesOrderShipmentItemEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class SalesOrderShipmentItemEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * salesOrderShipmentItemEntity Namespace URI = urn:Magento Namespace Prefix
	 * = ns1
	 */


	/**
	 * field for Increment_id
	 */


	protected java.lang.String localIncrement_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIncrement_idTracker = false;

	public boolean isIncrement_idSpecified()
	{
		return localIncrement_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIncrement_id()
	{
		return localIncrement_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Increment_id
	 */
	public void setIncrement_id(java.lang.String param)
	{
		localIncrement_idTracker = param != null;

		this.localIncrement_id = param;


	}


	/**
	 * field for Parent_id
	 */


	protected java.lang.String localParent_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localParent_idTracker = false;

	public boolean isParent_idSpecified()
	{
		return localParent_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getParent_id()
	{
		return localParent_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Parent_id
	 */
	public void setParent_id(java.lang.String param)
	{
		localParent_idTracker = param != null;

		this.localParent_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Is_active
	 */


	protected java.lang.String localIs_active;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_activeTracker = false;

	public boolean isIs_activeSpecified()
	{
		return localIs_activeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIs_active()
	{
		return localIs_active;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_active
	 */
	public void setIs_active(java.lang.String param)
	{
		localIs_activeTracker = param != null;

		this.localIs_active = param;


	}


	/**
	 * field for Sku
	 */


	protected java.lang.String localSku;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSkuTracker = false;

	public boolean isSkuSpecified()
	{
		return localSkuTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSku()
	{
		return localSku;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Sku
	 */
	public void setSku(java.lang.String param)
	{
		localSkuTracker = param != null;

		this.localSku = param;


	}


	/**
	 * field for Name
	 */


	protected java.lang.String localName;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localNameTracker = false;

	public boolean isNameSpecified()
	{
		return localNameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getName()
	{
		return localName;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Name
	 */
	public void setName(java.lang.String param)
	{
		localNameTracker = param != null;

		this.localName = param;


	}


	/**
	 * field for Order_item_id
	 */


	protected java.lang.String localOrder_item_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrder_item_idTracker = false;

	public boolean isOrder_item_idSpecified()
	{
		return localOrder_item_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrder_item_id()
	{
		return localOrder_item_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Order_item_id
	 */
	public void setOrder_item_id(java.lang.String param)
	{
		localOrder_item_idTracker = param != null;

		this.localOrder_item_id = param;


	}


	/**
	 * field for Product_id
	 */


	protected java.lang.String localProduct_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localProduct_idTracker = false;

	public boolean isProduct_idSpecified()
	{
		return localProduct_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getProduct_id()
	{
		return localProduct_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Product_id
	 */
	public void setProduct_id(java.lang.String param)
	{
		localProduct_idTracker = param != null;

		this.localProduct_id = param;


	}


	/**
	 * field for Weight
	 */


	protected java.lang.String localWeight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeightTracker = false;

	public boolean isWeightSpecified()
	{
		return localWeightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getWeight()
	{
		return localWeight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weight
	 */
	public void setWeight(java.lang.String param)
	{
		localWeightTracker = param != null;

		this.localWeight = param;


	}


	/**
	 * field for Price
	 */


	protected java.lang.String localPrice;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPriceTracker = false;

	public boolean isPriceSpecified()
	{
		return localPriceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrice()
	{
		return localPrice;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Price
	 */
	public void setPrice(java.lang.String param)
	{
		localPriceTracker = param != null;

		this.localPrice = param;


	}


	/**
	 * field for Qty
	 */


	protected java.lang.String localQty;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQtyTracker = false;

	public boolean isQtySpecified()
	{
		return localQtyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQty()
	{
		return localQty;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Qty
	 */
	public void setQty(java.lang.String param)
	{
		localQtyTracker = param != null;

		this.localQty = param;


	}


	/**
	 * field for Item_id
	 */


	protected java.lang.String localItem_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItem_idTracker = false;

	public boolean isItem_idSpecified()
	{
		return localItem_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getItem_id()
	{
		return localItem_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Item_id
	 */
	public void setItem_id(java.lang.String param)
	{
		localItem_idTracker = param != null;

		this.localItem_id = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":salesOrderShipmentItemEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "salesOrderShipmentItemEntity", xmlWriter);
			}


		}
		if (localIncrement_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "increment_id", xmlWriter);


			if (localIncrement_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("increment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIncrement_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localParent_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "parent_id", xmlWriter);


			if (localParent_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("parent_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localParent_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localIs_activeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_active", xmlWriter);


			if (localIs_active == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("is_active cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localIs_active);

			}

			xmlWriter.writeEndElement();
		}
		if (localSkuTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "sku", xmlWriter);


			if (localSku == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("sku cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSku);

			}

			xmlWriter.writeEndElement();
		}
		if (localNameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "name", xmlWriter);


			if (localName == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localName);

			}

			xmlWriter.writeEndElement();
		}
		if (localOrder_item_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "order_item_id", xmlWriter);


			if (localOrder_item_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("order_item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrder_item_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localProduct_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "product_id", xmlWriter);


			if (localProduct_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("product_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localProduct_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weight", xmlWriter);


			if (localWeight == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localWeight);

			}

			xmlWriter.writeEndElement();
		}
		if (localPriceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "price", xmlWriter);


			if (localPrice == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrice);

			}

			xmlWriter.writeEndElement();
		}
		if (localQtyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "qty", xmlWriter);


			if (localQty == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("qty cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQty);

			}

			xmlWriter.writeEndElement();
		}
		if (localItem_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "item_id", xmlWriter);


			if (localItem_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("item_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localItem_id);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static SalesOrderShipmentItemEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			SalesOrderShipmentItemEntity object = new SalesOrderShipmentItemEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"salesOrderShipmentItemEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (SalesOrderShipmentItemEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "increment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "increment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIncrement_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "parent_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "parent_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setParent_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_active").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_active" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_active(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "sku").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "sku" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSku(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "name").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "name" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "order_item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "order_item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrder_item_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "product_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "product_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setProduct_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeight(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "price").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "price" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrice(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "qty").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "qty" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQty(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "item_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "item_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setItem_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

