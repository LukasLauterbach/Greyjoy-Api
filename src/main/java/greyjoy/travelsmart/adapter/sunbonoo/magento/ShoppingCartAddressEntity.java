
/**
 * ShoppingCartAddressEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * ShoppingCartAddressEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class ShoppingCartAddressEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * shoppingCartAddressEntity Namespace URI = urn:Magento Namespace Prefix =
	 * ns1
	 */


	/**
	 * field for Address_id
	 */


	protected java.lang.String localAddress_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAddress_idTracker = false;

	public boolean isAddress_idSpecified()
	{
		return localAddress_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAddress_id()
	{
		return localAddress_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Address_id
	 */
	public void setAddress_id(java.lang.String param)
	{
		localAddress_idTracker = param != null;

		this.localAddress_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Customer_id
	 */


	protected java.lang.String localCustomer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_idTracker = false;

	public boolean isCustomer_idSpecified()
	{
		return localCustomer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_id()
	{
		return localCustomer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_id
	 */
	public void setCustomer_id(java.lang.String param)
	{
		localCustomer_idTracker = param != null;

		this.localCustomer_id = param;


	}


	/**
	 * field for Save_in_address_book
	 */


	protected int localSave_in_address_book;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSave_in_address_bookTracker = false;

	public boolean isSave_in_address_bookSpecified()
	{
		return localSave_in_address_bookTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getSave_in_address_book()
	{
		return localSave_in_address_book;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Save_in_address_book
	 */
	public void setSave_in_address_book(int param)
	{

		// setting primitive attribute tracker to true
		localSave_in_address_bookTracker = param != java.lang.Integer.MIN_VALUE;

		this.localSave_in_address_book = param;


	}


	/**
	 * field for Customer_address_id
	 */


	protected java.lang.String localCustomer_address_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_address_idTracker = false;

	public boolean isCustomer_address_idSpecified()
	{
		return localCustomer_address_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_address_id()
	{
		return localCustomer_address_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_address_id
	 */
	public void setCustomer_address_id(java.lang.String param)
	{
		localCustomer_address_idTracker = param != null;

		this.localCustomer_address_id = param;


	}


	/**
	 * field for Address_type
	 */


	protected java.lang.String localAddress_type;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAddress_typeTracker = false;

	public boolean isAddress_typeSpecified()
	{
		return localAddress_typeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAddress_type()
	{
		return localAddress_type;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Address_type
	 */
	public void setAddress_type(java.lang.String param)
	{
		localAddress_typeTracker = param != null;

		this.localAddress_type = param;


	}


	/**
	 * field for Email
	 */


	protected java.lang.String localEmail;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localEmailTracker = false;

	public boolean isEmailSpecified()
	{
		return localEmailTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getEmail()
	{
		return localEmail;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Email
	 */
	public void setEmail(java.lang.String param)
	{
		localEmailTracker = param != null;

		this.localEmail = param;


	}


	/**
	 * field for Prefix
	 */


	protected java.lang.String localPrefix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPrefixTracker = false;

	public boolean isPrefixSpecified()
	{
		return localPrefixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPrefix()
	{
		return localPrefix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Prefix
	 */
	public void setPrefix(java.lang.String param)
	{
		localPrefixTracker = param != null;

		this.localPrefix = param;


	}


	/**
	 * field for Firstname
	 */


	protected java.lang.String localFirstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFirstnameTracker = false;

	public boolean isFirstnameSpecified()
	{
		return localFirstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFirstname()
	{
		return localFirstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Firstname
	 */
	public void setFirstname(java.lang.String param)
	{
		localFirstnameTracker = param != null;

		this.localFirstname = param;


	}


	/**
	 * field for Middlename
	 */


	protected java.lang.String localMiddlename;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localMiddlenameTracker = false;

	public boolean isMiddlenameSpecified()
	{
		return localMiddlenameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMiddlename()
	{
		return localMiddlename;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Middlename
	 */
	public void setMiddlename(java.lang.String param)
	{
		localMiddlenameTracker = param != null;

		this.localMiddlename = param;


	}


	/**
	 * field for Lastname
	 */


	protected java.lang.String localLastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localLastnameTracker = false;

	public boolean isLastnameSpecified()
	{
		return localLastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getLastname()
	{
		return localLastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Lastname
	 */
	public void setLastname(java.lang.String param)
	{
		localLastnameTracker = param != null;

		this.localLastname = param;


	}


	/**
	 * field for Suffix
	 */


	protected java.lang.String localSuffix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSuffixTracker = false;

	public boolean isSuffixSpecified()
	{
		return localSuffixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getSuffix()
	{
		return localSuffix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Suffix
	 */
	public void setSuffix(java.lang.String param)
	{
		localSuffixTracker = param != null;

		this.localSuffix = param;


	}


	/**
	 * field for Company
	 */


	protected java.lang.String localCompany;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCompanyTracker = false;

	public boolean isCompanySpecified()
	{
		return localCompanyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCompany()
	{
		return localCompany;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Company
	 */
	public void setCompany(java.lang.String param)
	{
		localCompanyTracker = param != null;

		this.localCompany = param;


	}


	/**
	 * field for Street
	 */


	protected java.lang.String localStreet;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStreetTracker = false;

	public boolean isStreetSpecified()
	{
		return localStreetTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStreet()
	{
		return localStreet;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Street
	 */
	public void setStreet(java.lang.String param)
	{
		localStreetTracker = param != null;

		this.localStreet = param;


	}


	/**
	 * field for City
	 */


	protected java.lang.String localCity;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCityTracker = false;

	public boolean isCitySpecified()
	{
		return localCityTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCity()
	{
		return localCity;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            City
	 */
	public void setCity(java.lang.String param)
	{
		localCityTracker = param != null;

		this.localCity = param;


	}


	/**
	 * field for Region
	 */


	protected java.lang.String localRegion;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRegionTracker = false;

	public boolean isRegionSpecified()
	{
		return localRegionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRegion()
	{
		return localRegion;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Region
	 */
	public void setRegion(java.lang.String param)
	{
		localRegionTracker = param != null;

		this.localRegion = param;


	}


	/**
	 * field for Region_id
	 */


	protected java.lang.String localRegion_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localRegion_idTracker = false;

	public boolean isRegion_idSpecified()
	{
		return localRegion_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getRegion_id()
	{
		return localRegion_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Region_id
	 */
	public void setRegion_id(java.lang.String param)
	{
		localRegion_idTracker = param != null;

		this.localRegion_id = param;


	}


	/**
	 * field for Postcode
	 */


	protected java.lang.String localPostcode;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPostcodeTracker = false;

	public boolean isPostcodeSpecified()
	{
		return localPostcodeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPostcode()
	{
		return localPostcode;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Postcode
	 */
	public void setPostcode(java.lang.String param)
	{
		localPostcodeTracker = param != null;

		this.localPostcode = param;


	}


	/**
	 * field for Country_id
	 */


	protected java.lang.String localCountry_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCountry_idTracker = false;

	public boolean isCountry_idSpecified()
	{
		return localCountry_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCountry_id()
	{
		return localCountry_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Country_id
	 */
	public void setCountry_id(java.lang.String param)
	{
		localCountry_idTracker = param != null;

		this.localCountry_id = param;


	}


	/**
	 * field for Telephone
	 */


	protected java.lang.String localTelephone;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localTelephoneTracker = false;

	public boolean isTelephoneSpecified()
	{
		return localTelephoneTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTelephone()
	{
		return localTelephone;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Telephone
	 */
	public void setTelephone(java.lang.String param)
	{
		localTelephoneTracker = param != null;

		this.localTelephone = param;


	}


	/**
	 * field for Fax
	 */


	protected java.lang.String localFax;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFaxTracker = false;

	public boolean isFaxSpecified()
	{
		return localFaxTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getFax()
	{
		return localFax;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Fax
	 */
	public void setFax(java.lang.String param)
	{
		localFaxTracker = param != null;

		this.localFax = param;


	}


	/**
	 * field for Same_as_billing
	 */


	protected int localSame_as_billing;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSame_as_billingTracker = false;

	public boolean isSame_as_billingSpecified()
	{
		return localSame_as_billingTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getSame_as_billing()
	{
		return localSame_as_billing;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Same_as_billing
	 */
	public void setSame_as_billing(int param)
	{

		// setting primitive attribute tracker to true
		localSame_as_billingTracker = param != java.lang.Integer.MIN_VALUE;

		this.localSame_as_billing = param;


	}


	/**
	 * field for Free_shipping
	 */


	protected int localFree_shipping;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localFree_shippingTracker = false;

	public boolean isFree_shippingSpecified()
	{
		return localFree_shippingTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getFree_shipping()
	{
		return localFree_shipping;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Free_shipping
	 */
	public void setFree_shipping(int param)
	{

		// setting primitive attribute tracker to true
		localFree_shippingTracker = param != java.lang.Integer.MIN_VALUE;

		this.localFree_shipping = param;


	}


	/**
	 * field for Shipping_method
	 */


	protected java.lang.String localShipping_method;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_methodTracker = false;

	public boolean isShipping_methodSpecified()
	{
		return localShipping_methodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_method()
	{
		return localShipping_method;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_method
	 */
	public void setShipping_method(java.lang.String param)
	{
		localShipping_methodTracker = param != null;

		this.localShipping_method = param;


	}


	/**
	 * field for Shipping_description
	 */


	protected java.lang.String localShipping_description;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_descriptionTracker = false;

	public boolean isShipping_descriptionSpecified()
	{
		return localShipping_descriptionTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getShipping_description()
	{
		return localShipping_description;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_description
	 */
	public void setShipping_description(java.lang.String param)
	{
		localShipping_descriptionTracker = param != null;

		this.localShipping_description = param;


	}


	/**
	 * field for Weight
	 */


	protected double localWeight;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localWeightTracker = false;

	public boolean isWeightSpecified()
	{
		return localWeightTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getWeight()
	{
		return localWeight;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Weight
	 */
	public void setWeight(double param)
	{

		// setting primitive attribute tracker to true
		localWeightTracker = !java.lang.Double.isNaN(param);

		this.localWeight = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":shoppingCartAddressEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "shoppingCartAddressEntity", xmlWriter);
			}


		}
		if (localAddress_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "address_id", xmlWriter);


			if (localAddress_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAddress_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_id", xmlWriter);


			if (localCustomer_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localSave_in_address_bookTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "save_in_address_book", xmlWriter);

			if (localSave_in_address_book == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("save_in_address_book cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSave_in_address_book));
			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_address_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_address_id", xmlWriter);


			if (localCustomer_address_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_address_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_address_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localAddress_typeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "address_type", xmlWriter);


			if (localAddress_type == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("address_type cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAddress_type);

			}

			xmlWriter.writeEndElement();
		}
		if (localEmailTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "email", xmlWriter);


			if (localEmail == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localEmail);

			}

			xmlWriter.writeEndElement();
		}
		if (localPrefixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "prefix", xmlWriter);


			if (localPrefix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("prefix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPrefix);

			}

			xmlWriter.writeEndElement();
		}
		if (localFirstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "firstname", xmlWriter);


			if (localFirstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFirstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localMiddlenameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "middlename", xmlWriter);


			if (localMiddlename == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("middlename cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localMiddlename);

			}

			xmlWriter.writeEndElement();
		}
		if (localLastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "lastname", xmlWriter);


			if (localLastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localLastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localSuffixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "suffix", xmlWriter);


			if (localSuffix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localSuffix);

			}

			xmlWriter.writeEndElement();
		}
		if (localCompanyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "company", xmlWriter);


			if (localCompany == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCompany);

			}

			xmlWriter.writeEndElement();
		}
		if (localStreetTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "street", xmlWriter);


			if (localStreet == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("street cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStreet);

			}

			xmlWriter.writeEndElement();
		}
		if (localCityTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "city", xmlWriter);


			if (localCity == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCity);

			}

			xmlWriter.writeEndElement();
		}
		if (localRegionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "region", xmlWriter);


			if (localRegion == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("region cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRegion);

			}

			xmlWriter.writeEndElement();
		}
		if (localRegion_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "region_id", xmlWriter);


			if (localRegion_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("region_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localRegion_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localPostcodeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "postcode", xmlWriter);


			if (localPostcode == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("postcode cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPostcode);

			}

			xmlWriter.writeEndElement();
		}
		if (localCountry_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "country_id", xmlWriter);


			if (localCountry_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("country_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCountry_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localTelephoneTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "telephone", xmlWriter);


			if (localTelephone == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("telephone cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localTelephone);

			}

			xmlWriter.writeEndElement();
		}
		if (localFaxTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "fax", xmlWriter);


			if (localFax == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localFax);

			}

			xmlWriter.writeEndElement();
		}
		if (localSame_as_billingTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "same_as_billing", xmlWriter);

			if (localSame_as_billing == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("same_as_billing cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSame_as_billing));
			}

			xmlWriter.writeEndElement();
		}
		if (localFree_shippingTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "free_shipping", xmlWriter);

			if (localFree_shipping == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("free_shipping cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFree_shipping));
			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_methodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_method", xmlWriter);


			if (localShipping_method == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_method);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_descriptionTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "shipping_description", xmlWriter);


			if (localShipping_description == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("shipping_description cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localShipping_description);

			}

			xmlWriter.writeEndElement();
		}
		if (localWeightTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "weight", xmlWriter);

			if (java.lang.Double.isNaN(localWeight))
			{

				throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static ShoppingCartAddressEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			ShoppingCartAddressEntity object = new ShoppingCartAddressEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"shoppingCartAddressEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (ShoppingCartAddressEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAddress_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "save_in_address_book").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "save_in_address_book" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSave_in_address_book(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setSave_in_address_book(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_address_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_address_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_address_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "address_type").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "address_type" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAddress_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "email").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "email" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setEmail(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "prefix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "prefix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPrefix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFirstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "middlename").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "middlename" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setMiddlename(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setLastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "suffix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "suffix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSuffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "company").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "company" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCompany(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "street").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "street" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStreet(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "city").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "city" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "region").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "region" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRegion(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "region_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "region_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setRegion_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "postcode").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "postcode" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPostcode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "country_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "country_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCountry_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "telephone").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "telephone" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setTelephone(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "fax").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "fax" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFax(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "same_as_billing").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "same_as_billing" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSame_as_billing(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setSame_as_billing(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "free_shipping").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "free_shipping" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setFree_shipping(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setFree_shipping(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_method(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_description").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "shipping_description" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setShipping_description(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "weight").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "weight" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setWeight(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setWeight(java.lang.Double.NaN);

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

