
/**
 * ShoppingCartInfoEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * ShoppingCartInfoEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class ShoppingCartInfoEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * shoppingCartInfoEntity Namespace URI = urn:Magento Namespace Prefix = ns1
	 */


	/**
	 * field for Store_id
	 */


	protected java.lang.String localStore_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_idTracker = false;

	public boolean isStore_idSpecified()
	{
		return localStore_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_id()
	{
		return localStore_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_id
	 */
	public void setStore_id(java.lang.String param)
	{
		localStore_idTracker = param != null;

		this.localStore_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Converted_at
	 */


	protected java.lang.String localConverted_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localConverted_atTracker = false;

	public boolean isConverted_atSpecified()
	{
		return localConverted_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getConverted_at()
	{
		return localConverted_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Converted_at
	 */
	public void setConverted_at(java.lang.String param)
	{
		localConverted_atTracker = param != null;

		this.localConverted_at = param;


	}


	/**
	 * field for Quote_id
	 */


	protected int localQuote_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_idTracker = false;

	public boolean isQuote_idSpecified()
	{
		return localQuote_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getQuote_id()
	{
		return localQuote_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_id
	 */
	public void setQuote_id(int param)
	{

		// setting primitive attribute tracker to true
		localQuote_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localQuote_id = param;


	}


	/**
	 * field for Is_active
	 */


	protected int localIs_active;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_activeTracker = false;

	public boolean isIs_activeSpecified()
	{
		return localIs_activeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getIs_active()
	{
		return localIs_active;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_active
	 */
	public void setIs_active(int param)
	{

		// setting primitive attribute tracker to true
		localIs_activeTracker = param != java.lang.Integer.MIN_VALUE;

		this.localIs_active = param;


	}


	/**
	 * field for Is_virtual
	 */


	protected int localIs_virtual;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_virtualTracker = false;

	public boolean isIs_virtualSpecified()
	{
		return localIs_virtualTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getIs_virtual()
	{
		return localIs_virtual;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_virtual
	 */
	public void setIs_virtual(int param)
	{

		// setting primitive attribute tracker to true
		localIs_virtualTracker = param != java.lang.Integer.MIN_VALUE;

		this.localIs_virtual = param;


	}


	/**
	 * field for Is_multi_shipping
	 */


	protected int localIs_multi_shipping;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localIs_multi_shippingTracker = false;

	public boolean isIs_multi_shippingSpecified()
	{
		return localIs_multi_shippingTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getIs_multi_shipping()
	{
		return localIs_multi_shipping;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Is_multi_shipping
	 */
	public void setIs_multi_shipping(int param)
	{

		// setting primitive attribute tracker to true
		localIs_multi_shippingTracker = param != java.lang.Integer.MIN_VALUE;

		this.localIs_multi_shipping = param;


	}


	/**
	 * field for Items_count
	 */


	protected double localItems_count;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItems_countTracker = false;

	public boolean isItems_countSpecified()
	{
		return localItems_countTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getItems_count()
	{
		return localItems_count;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items_count
	 */
	public void setItems_count(double param)
	{

		// setting primitive attribute tracker to true
		localItems_countTracker = !java.lang.Double.isNaN(param);

		this.localItems_count = param;


	}


	/**
	 * field for Items_qty
	 */


	protected double localItems_qty;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItems_qtyTracker = false;

	public boolean isItems_qtySpecified()
	{
		return localItems_qtyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getItems_qty()
	{
		return localItems_qty;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items_qty
	 */
	public void setItems_qty(double param)
	{

		// setting primitive attribute tracker to true
		localItems_qtyTracker = !java.lang.Double.isNaN(param);

		this.localItems_qty = param;


	}


	/**
	 * field for Orig_order_id
	 */


	protected java.lang.String localOrig_order_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localOrig_order_idTracker = false;

	public boolean isOrig_order_idSpecified()
	{
		return localOrig_order_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getOrig_order_id()
	{
		return localOrig_order_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Orig_order_id
	 */
	public void setOrig_order_id(java.lang.String param)
	{
		localOrig_order_idTracker = param != null;

		this.localOrig_order_id = param;


	}


	/**
	 * field for Store_to_base_rate
	 */


	protected java.lang.String localStore_to_base_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_base_rateTracker = false;

	public boolean isStore_to_base_rateSpecified()
	{
		return localStore_to_base_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_base_rate()
	{
		return localStore_to_base_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_base_rate
	 */
	public void setStore_to_base_rate(java.lang.String param)
	{
		localStore_to_base_rateTracker = param != null;

		this.localStore_to_base_rate = param;


	}


	/**
	 * field for Store_to_quote_rate
	 */


	protected java.lang.String localStore_to_quote_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_to_quote_rateTracker = false;

	public boolean isStore_to_quote_rateSpecified()
	{
		return localStore_to_quote_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_to_quote_rate()
	{
		return localStore_to_quote_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_to_quote_rate
	 */
	public void setStore_to_quote_rate(java.lang.String param)
	{
		localStore_to_quote_rateTracker = param != null;

		this.localStore_to_quote_rate = param;


	}


	/**
	 * field for Base_currency_code
	 */


	protected java.lang.String localBase_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_currency_codeTracker = false;

	public boolean isBase_currency_codeSpecified()
	{
		return localBase_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_currency_code()
	{
		return localBase_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_currency_code
	 */
	public void setBase_currency_code(java.lang.String param)
	{
		localBase_currency_codeTracker = param != null;

		this.localBase_currency_code = param;


	}


	/**
	 * field for Store_currency_code
	 */


	protected java.lang.String localStore_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localStore_currency_codeTracker = false;

	public boolean isStore_currency_codeSpecified()
	{
		return localStore_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getStore_currency_code()
	{
		return localStore_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Store_currency_code
	 */
	public void setStore_currency_code(java.lang.String param)
	{
		localStore_currency_codeTracker = param != null;

		this.localStore_currency_code = param;


	}


	/**
	 * field for Quote_currency_code
	 */


	protected java.lang.String localQuote_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localQuote_currency_codeTracker = false;

	public boolean isQuote_currency_codeSpecified()
	{
		return localQuote_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getQuote_currency_code()
	{
		return localQuote_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Quote_currency_code
	 */
	public void setQuote_currency_code(java.lang.String param)
	{
		localQuote_currency_codeTracker = param != null;

		this.localQuote_currency_code = param;


	}


	/**
	 * field for Grand_total
	 */


	protected java.lang.String localGrand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGrand_totalTracker = false;

	public boolean isGrand_totalSpecified()
	{
		return localGrand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGrand_total()
	{
		return localGrand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Grand_total
	 */
	public void setGrand_total(java.lang.String param)
	{
		localGrand_totalTracker = param != null;

		this.localGrand_total = param;


	}


	/**
	 * field for Base_grand_total
	 */


	protected java.lang.String localBase_grand_total;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_grand_totalTracker = false;

	public boolean isBase_grand_totalSpecified()
	{
		return localBase_grand_totalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_grand_total()
	{
		return localBase_grand_total;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_grand_total
	 */
	public void setBase_grand_total(java.lang.String param)
	{
		localBase_grand_totalTracker = param != null;

		this.localBase_grand_total = param;


	}


	/**
	 * field for Checkout_method
	 */


	protected java.lang.String localCheckout_method;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCheckout_methodTracker = false;

	public boolean isCheckout_methodSpecified()
	{
		return localCheckout_methodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCheckout_method()
	{
		return localCheckout_method;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Checkout_method
	 */
	public void setCheckout_method(java.lang.String param)
	{
		localCheckout_methodTracker = param != null;

		this.localCheckout_method = param;


	}


	/**
	 * field for Customer_id
	 */


	protected java.lang.String localCustomer_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_idTracker = false;

	public boolean isCustomer_idSpecified()
	{
		return localCustomer_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_id()
	{
		return localCustomer_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_id
	 */
	public void setCustomer_id(java.lang.String param)
	{
		localCustomer_idTracker = param != null;

		this.localCustomer_id = param;


	}


	/**
	 * field for Customer_tax_class_id
	 */


	protected java.lang.String localCustomer_tax_class_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_tax_class_idTracker = false;

	public boolean isCustomer_tax_class_idSpecified()
	{
		return localCustomer_tax_class_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_tax_class_id()
	{
		return localCustomer_tax_class_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_tax_class_id
	 */
	public void setCustomer_tax_class_id(java.lang.String param)
	{
		localCustomer_tax_class_idTracker = param != null;

		this.localCustomer_tax_class_id = param;


	}


	/**
	 * field for Customer_group_id
	 */


	protected int localCustomer_group_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_group_idTracker = false;

	public boolean isCustomer_group_idSpecified()
	{
		return localCustomer_group_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return int
	 */
	public int getCustomer_group_id()
	{
		return localCustomer_group_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_group_id
	 */
	public void setCustomer_group_id(int param)
	{

		// setting primitive attribute tracker to true
		localCustomer_group_idTracker = param != java.lang.Integer.MIN_VALUE;

		this.localCustomer_group_id = param;


	}


	/**
	 * field for Customer_email
	 */


	protected java.lang.String localCustomer_email;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_emailTracker = false;

	public boolean isCustomer_emailSpecified()
	{
		return localCustomer_emailTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_email()
	{
		return localCustomer_email;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_email
	 */
	public void setCustomer_email(java.lang.String param)
	{
		localCustomer_emailTracker = param != null;

		this.localCustomer_email = param;


	}


	/**
	 * field for Customer_prefix
	 */


	protected java.lang.String localCustomer_prefix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_prefixTracker = false;

	public boolean isCustomer_prefixSpecified()
	{
		return localCustomer_prefixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_prefix()
	{
		return localCustomer_prefix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_prefix
	 */
	public void setCustomer_prefix(java.lang.String param)
	{
		localCustomer_prefixTracker = param != null;

		this.localCustomer_prefix = param;


	}


	/**
	 * field for Customer_firstname
	 */


	protected java.lang.String localCustomer_firstname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_firstnameTracker = false;

	public boolean isCustomer_firstnameSpecified()
	{
		return localCustomer_firstnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_firstname()
	{
		return localCustomer_firstname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_firstname
	 */
	public void setCustomer_firstname(java.lang.String param)
	{
		localCustomer_firstnameTracker = param != null;

		this.localCustomer_firstname = param;


	}


	/**
	 * field for Customer_middlename
	 */


	protected java.lang.String localCustomer_middlename;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_middlenameTracker = false;

	public boolean isCustomer_middlenameSpecified()
	{
		return localCustomer_middlenameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_middlename()
	{
		return localCustomer_middlename;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_middlename
	 */
	public void setCustomer_middlename(java.lang.String param)
	{
		localCustomer_middlenameTracker = param != null;

		this.localCustomer_middlename = param;


	}


	/**
	 * field for Customer_lastname
	 */


	protected java.lang.String localCustomer_lastname;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_lastnameTracker = false;

	public boolean isCustomer_lastnameSpecified()
	{
		return localCustomer_lastnameTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_lastname()
	{
		return localCustomer_lastname;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_lastname
	 */
	public void setCustomer_lastname(java.lang.String param)
	{
		localCustomer_lastnameTracker = param != null;

		this.localCustomer_lastname = param;


	}


	/**
	 * field for Customer_suffix
	 */


	protected java.lang.String localCustomer_suffix;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_suffixTracker = false;

	public boolean isCustomer_suffixSpecified()
	{
		return localCustomer_suffixTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_suffix()
	{
		return localCustomer_suffix;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_suffix
	 */
	public void setCustomer_suffix(java.lang.String param)
	{
		localCustomer_suffixTracker = param != null;

		this.localCustomer_suffix = param;


	}


	/**
	 * field for Customer_note
	 */


	protected java.lang.String localCustomer_note;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_noteTracker = false;

	public boolean isCustomer_noteSpecified()
	{
		return localCustomer_noteTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_note()
	{
		return localCustomer_note;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_note
	 */
	public void setCustomer_note(java.lang.String param)
	{
		localCustomer_noteTracker = param != null;

		this.localCustomer_note = param;


	}


	/**
	 * field for Customer_note_notify
	 */


	protected java.lang.String localCustomer_note_notify;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_note_notifyTracker = false;

	public boolean isCustomer_note_notifySpecified()
	{
		return localCustomer_note_notifyTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_note_notify()
	{
		return localCustomer_note_notify;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_note_notify
	 */
	public void setCustomer_note_notify(java.lang.String param)
	{
		localCustomer_note_notifyTracker = param != null;

		this.localCustomer_note_notify = param;


	}


	/**
	 * field for Customer_is_guest
	 */


	protected java.lang.String localCustomer_is_guest;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_is_guestTracker = false;

	public boolean isCustomer_is_guestSpecified()
	{
		return localCustomer_is_guestTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_is_guest()
	{
		return localCustomer_is_guest;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_is_guest
	 */
	public void setCustomer_is_guest(java.lang.String param)
	{
		localCustomer_is_guestTracker = param != null;

		this.localCustomer_is_guest = param;


	}


	/**
	 * field for Applied_rule_ids
	 */


	protected java.lang.String localApplied_rule_ids;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localApplied_rule_idsTracker = false;

	public boolean isApplied_rule_idsSpecified()
	{
		return localApplied_rule_idsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getApplied_rule_ids()
	{
		return localApplied_rule_ids;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Applied_rule_ids
	 */
	public void setApplied_rule_ids(java.lang.String param)
	{
		localApplied_rule_idsTracker = param != null;

		this.localApplied_rule_ids = param;


	}


	/**
	 * field for Reserved_order_id
	 */


	protected java.lang.String localReserved_order_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReserved_order_idTracker = false;

	public boolean isReserved_order_idSpecified()
	{
		return localReserved_order_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReserved_order_id()
	{
		return localReserved_order_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reserved_order_id
	 */
	public void setReserved_order_id(java.lang.String param)
	{
		localReserved_order_idTracker = param != null;

		this.localReserved_order_id = param;


	}


	/**
	 * field for Password_hash
	 */


	protected java.lang.String localPassword_hash;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPassword_hashTracker = false;

	public boolean isPassword_hashSpecified()
	{
		return localPassword_hashTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPassword_hash()
	{
		return localPassword_hash;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Password_hash
	 */
	public void setPassword_hash(java.lang.String param)
	{
		localPassword_hashTracker = param != null;

		this.localPassword_hash = param;


	}


	/**
	 * field for Coupon_code
	 */


	protected java.lang.String localCoupon_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCoupon_codeTracker = false;

	public boolean isCoupon_codeSpecified()
	{
		return localCoupon_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCoupon_code()
	{
		return localCoupon_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Coupon_code
	 */
	public void setCoupon_code(java.lang.String param)
	{
		localCoupon_codeTracker = param != null;

		this.localCoupon_code = param;


	}


	/**
	 * field for Global_currency_code
	 */


	protected java.lang.String localGlobal_currency_code;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGlobal_currency_codeTracker = false;

	public boolean isGlobal_currency_codeSpecified()
	{
		return localGlobal_currency_codeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGlobal_currency_code()
	{
		return localGlobal_currency_code;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Global_currency_code
	 */
	public void setGlobal_currency_code(java.lang.String param)
	{
		localGlobal_currency_codeTracker = param != null;

		this.localGlobal_currency_code = param;


	}


	/**
	 * field for Base_to_global_rate
	 */


	protected double localBase_to_global_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_global_rateTracker = false;

	public boolean isBase_to_global_rateSpecified()
	{
		return localBase_to_global_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getBase_to_global_rate()
	{
		return localBase_to_global_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_global_rate
	 */
	public void setBase_to_global_rate(double param)
	{

		// setting primitive attribute tracker to true
		localBase_to_global_rateTracker = !java.lang.Double.isNaN(param);

		this.localBase_to_global_rate = param;


	}


	/**
	 * field for Base_to_quote_rate
	 */


	protected double localBase_to_quote_rate;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_to_quote_rateTracker = false;

	public boolean isBase_to_quote_rateSpecified()
	{
		return localBase_to_quote_rateTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getBase_to_quote_rate()
	{
		return localBase_to_quote_rate;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_to_quote_rate
	 */
	public void setBase_to_quote_rate(double param)
	{

		// setting primitive attribute tracker to true
		localBase_to_quote_rateTracker = !java.lang.Double.isNaN(param);

		this.localBase_to_quote_rate = param;


	}


	/**
	 * field for Customer_taxvat
	 */


	protected java.lang.String localCustomer_taxvat;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_taxvatTracker = false;

	public boolean isCustomer_taxvatSpecified()
	{
		return localCustomer_taxvatTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_taxvat()
	{
		return localCustomer_taxvat;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_taxvat
	 */
	public void setCustomer_taxvat(java.lang.String param)
	{
		localCustomer_taxvatTracker = param != null;

		this.localCustomer_taxvat = param;


	}


	/**
	 * field for Customer_gender
	 */


	protected java.lang.String localCustomer_gender;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_genderTracker = false;

	public boolean isCustomer_genderSpecified()
	{
		return localCustomer_genderTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCustomer_gender()
	{
		return localCustomer_gender;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_gender
	 */
	public void setCustomer_gender(java.lang.String param)
	{
		localCustomer_genderTracker = param != null;

		this.localCustomer_gender = param;


	}


	/**
	 * field for Subtotal
	 */


	protected double localSubtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotalTracker = false;

	public boolean isSubtotalSpecified()
	{
		return localSubtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getSubtotal()
	{
		return localSubtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal
	 */
	public void setSubtotal(double param)
	{

		// setting primitive attribute tracker to true
		localSubtotalTracker = !java.lang.Double.isNaN(param);

		this.localSubtotal = param;


	}


	/**
	 * field for Base_subtotal
	 */


	protected double localBase_subtotal;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotalTracker = false;

	public boolean isBase_subtotalSpecified()
	{
		return localBase_subtotalTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getBase_subtotal()
	{
		return localBase_subtotal;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal
	 */
	public void setBase_subtotal(double param)
	{

		// setting primitive attribute tracker to true
		localBase_subtotalTracker = !java.lang.Double.isNaN(param);

		this.localBase_subtotal = param;


	}


	/**
	 * field for Subtotal_with_discount
	 */


	protected double localSubtotal_with_discount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localSubtotal_with_discountTracker = false;

	public boolean isSubtotal_with_discountSpecified()
	{
		return localSubtotal_with_discountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getSubtotal_with_discount()
	{
		return localSubtotal_with_discount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Subtotal_with_discount
	 */
	public void setSubtotal_with_discount(double param)
	{

		// setting primitive attribute tracker to true
		localSubtotal_with_discountTracker = !java.lang.Double.isNaN(param);

		this.localSubtotal_with_discount = param;


	}


	/**
	 * field for Base_subtotal_with_discount
	 */


	protected double localBase_subtotal_with_discount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_subtotal_with_discountTracker = false;

	public boolean isBase_subtotal_with_discountSpecified()
	{
		return localBase_subtotal_with_discountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getBase_subtotal_with_discount()
	{
		return localBase_subtotal_with_discount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_subtotal_with_discount
	 */
	public void setBase_subtotal_with_discount(double param)
	{

		// setting primitive attribute tracker to true
		localBase_subtotal_with_discountTracker = !java.lang.Double.isNaN(param);

		this.localBase_subtotal_with_discount = param;


	}


	/**
	 * field for Ext_shipping_info
	 */


	protected java.lang.String localExt_shipping_info;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localExt_shipping_infoTracker = false;

	public boolean isExt_shipping_infoSpecified()
	{
		return localExt_shipping_infoTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getExt_shipping_info()
	{
		return localExt_shipping_info;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Ext_shipping_info
	 */
	public void setExt_shipping_info(java.lang.String param)
	{
		localExt_shipping_infoTracker = param != null;

		this.localExt_shipping_info = param;


	}


	/**
	 * field for Gift_message_id
	 */


	protected java.lang.String localGift_message_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_message_idTracker = false;

	public boolean isGift_message_idSpecified()
	{
		return localGift_message_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message_id()
	{
		return localGift_message_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message_id
	 */
	public void setGift_message_id(java.lang.String param)
	{
		localGift_message_idTracker = param != null;

		this.localGift_message_id = param;


	}


	/**
	 * field for Gift_message
	 */


	protected java.lang.String localGift_message;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_messageTracker = false;

	public boolean isGift_messageSpecified()
	{
		return localGift_messageTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_message()
	{
		return localGift_message;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_message
	 */
	public void setGift_message(java.lang.String param)
	{
		localGift_messageTracker = param != null;

		this.localGift_message = param;


	}


	/**
	 * field for Customer_balance_amount_used
	 */


	protected double localCustomer_balance_amount_used;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCustomer_balance_amount_usedTracker = false;

	public boolean isCustomer_balance_amount_usedSpecified()
	{
		return localCustomer_balance_amount_usedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getCustomer_balance_amount_used()
	{
		return localCustomer_balance_amount_used;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Customer_balance_amount_used
	 */
	public void setCustomer_balance_amount_used(double param)
	{

		// setting primitive attribute tracker to true
		localCustomer_balance_amount_usedTracker = !java.lang.Double.isNaN(param);

		this.localCustomer_balance_amount_used = param;


	}


	/**
	 * field for Base_customer_balance_amount_used
	 */


	protected double localBase_customer_balance_amount_used;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_customer_balance_amount_usedTracker = false;

	public boolean isBase_customer_balance_amount_usedSpecified()
	{
		return localBase_customer_balance_amount_usedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return double
	 */
	public double getBase_customer_balance_amount_used()
	{
		return localBase_customer_balance_amount_used;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_customer_balance_amount_used
	 */
	public void setBase_customer_balance_amount_used(double param)
	{

		// setting primitive attribute tracker to true
		localBase_customer_balance_amount_usedTracker = !java.lang.Double.isNaN(param);

		this.localBase_customer_balance_amount_used = param;


	}


	/**
	 * field for Use_customer_balance
	 */


	protected java.lang.String localUse_customer_balance;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUse_customer_balanceTracker = false;

	public boolean isUse_customer_balanceSpecified()
	{
		return localUse_customer_balanceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUse_customer_balance()
	{
		return localUse_customer_balance;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Use_customer_balance
	 */
	public void setUse_customer_balance(java.lang.String param)
	{
		localUse_customer_balanceTracker = param != null;

		this.localUse_customer_balance = param;


	}


	/**
	 * field for Gift_cards_amount
	 */


	protected java.lang.String localGift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_amountTracker = false;

	public boolean isGift_cards_amountSpecified()
	{
		return localGift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_amount()
	{
		return localGift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_amount
	 */
	public void setGift_cards_amount(java.lang.String param)
	{
		localGift_cards_amountTracker = param != null;

		this.localGift_cards_amount = param;


	}


	/**
	 * field for Base_gift_cards_amount
	 */


	protected java.lang.String localBase_gift_cards_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_gift_cards_amountTracker = false;

	public boolean isBase_gift_cards_amountSpecified()
	{
		return localBase_gift_cards_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_gift_cards_amount()
	{
		return localBase_gift_cards_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_gift_cards_amount
	 */
	public void setBase_gift_cards_amount(java.lang.String param)
	{
		localBase_gift_cards_amountTracker = param != null;

		this.localBase_gift_cards_amount = param;


	}


	/**
	 * field for Gift_cards_amount_used
	 */


	protected java.lang.String localGift_cards_amount_used;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localGift_cards_amount_usedTracker = false;

	public boolean isGift_cards_amount_usedSpecified()
	{
		return localGift_cards_amount_usedTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getGift_cards_amount_used()
	{
		return localGift_cards_amount_used;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Gift_cards_amount_used
	 */
	public void setGift_cards_amount_used(java.lang.String param)
	{
		localGift_cards_amount_usedTracker = param != null;

		this.localGift_cards_amount_used = param;


	}


	/**
	 * field for Use_reward_points
	 */


	protected java.lang.String localUse_reward_points;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUse_reward_pointsTracker = false;

	public boolean isUse_reward_pointsSpecified()
	{
		return localUse_reward_pointsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUse_reward_points()
	{
		return localUse_reward_points;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Use_reward_points
	 */
	public void setUse_reward_points(java.lang.String param)
	{
		localUse_reward_pointsTracker = param != null;

		this.localUse_reward_points = param;


	}


	/**
	 * field for Reward_points_balance
	 */


	protected java.lang.String localReward_points_balance;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_points_balanceTracker = false;

	public boolean isReward_points_balanceSpecified()
	{
		return localReward_points_balanceTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_points_balance()
	{
		return localReward_points_balance;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_points_balance
	 */
	public void setReward_points_balance(java.lang.String param)
	{
		localReward_points_balanceTracker = param != null;

		this.localReward_points_balance = param;


	}


	/**
	 * field for Base_reward_currency_amount
	 */


	protected java.lang.String localBase_reward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBase_reward_currency_amountTracker = false;

	public boolean isBase_reward_currency_amountSpecified()
	{
		return localBase_reward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getBase_reward_currency_amount()
	{
		return localBase_reward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Base_reward_currency_amount
	 */
	public void setBase_reward_currency_amount(java.lang.String param)
	{
		localBase_reward_currency_amountTracker = param != null;

		this.localBase_reward_currency_amount = param;


	}


	/**
	 * field for Reward_currency_amount
	 */


	protected java.lang.String localReward_currency_amount;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localReward_currency_amountTracker = false;

	public boolean isReward_currency_amountSpecified()
	{
		return localReward_currency_amountTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getReward_currency_amount()
	{
		return localReward_currency_amount;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Reward_currency_amount
	 */
	public void setReward_currency_amount(java.lang.String param)
	{
		localReward_currency_amountTracker = param != null;

		this.localReward_currency_amount = param;


	}


	/**
	 * field for Shipping_address
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity localShipping_address;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localShipping_addressTracker = false;

	public boolean isShipping_addressSpecified()
	{
		return localShipping_addressTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity getShipping_address()
	{
		return localShipping_address;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Shipping_address
	 */
	public void setShipping_address(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity param)
	{
		localShipping_addressTracker = param != null;

		this.localShipping_address = param;


	}


	/**
	 * field for Billing_address
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity localBilling_address;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localBilling_addressTracker = false;

	public boolean isBilling_addressSpecified()
	{
		return localBilling_addressTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity getBilling_address()
	{
		return localBilling_address;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Billing_address
	 */
	public void setBilling_address(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity param)
	{
		localBilling_addressTracker = param != null;

		this.localBilling_address = param;


	}


	/**
	 * field for Items
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray localItems;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localItemsTracker = false;

	public boolean isItemsSpecified()
	{
		return localItemsTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray getItems()
	{
		return localItems;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Items
	 */
	public void setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray param)
	{
		localItemsTracker = param != null;

		this.localItems = param;


	}


	/**
	 * field for Payment
	 */


	protected greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity localPayment;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPaymentTracker = false;

	public boolean isPaymentSpecified()
	{
		return localPaymentTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity
	 */
	public greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity getPayment()
	{
		return localPayment;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Payment
	 */
	public void setPayment(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity param)
	{
		localPaymentTracker = param != null;

		this.localPayment = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":shoppingCartInfoEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "shoppingCartInfoEntity", xmlWriter);
			}


		}
		if (localStore_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_id", xmlWriter);


			if (localStore_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localConverted_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "converted_at", xmlWriter);


			if (localConverted_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("converted_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localConverted_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_id", xmlWriter);

			if (localQuote_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("quote_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuote_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localIs_activeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_active", xmlWriter);

			if (localIs_active == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("is_active cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIs_active));
			}

			xmlWriter.writeEndElement();
		}
		if (localIs_virtualTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_virtual", xmlWriter);

			if (localIs_virtual == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("is_virtual cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIs_virtual));
			}

			xmlWriter.writeEndElement();
		}
		if (localIs_multi_shippingTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "is_multi_shipping", xmlWriter);

			if (localIs_multi_shipping == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("is_multi_shipping cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIs_multi_shipping));
			}

			xmlWriter.writeEndElement();
		}
		if (localItems_countTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "items_count", xmlWriter);

			if (java.lang.Double.isNaN(localItems_count))
			{

				throw new org.apache.axis2.databinding.ADBException("items_count cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItems_count));
			}

			xmlWriter.writeEndElement();
		}
		if (localItems_qtyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "items_qty", xmlWriter);

			if (java.lang.Double.isNaN(localItems_qty))
			{

				throw new org.apache.axis2.databinding.ADBException("items_qty cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItems_qty));
			}

			xmlWriter.writeEndElement();
		}
		if (localOrig_order_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "orig_order_id", xmlWriter);


			if (localOrig_order_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("orig_order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localOrig_order_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_base_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_base_rate", xmlWriter);


			if (localStore_to_base_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_base_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_base_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_to_quote_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_to_quote_rate", xmlWriter);


			if (localStore_to_quote_rate == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_to_quote_rate cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_to_quote_rate);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_currency_code", xmlWriter);


			if (localBase_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localStore_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "store_currency_code", xmlWriter);


			if (localStore_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("store_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localStore_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localQuote_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "quote_currency_code", xmlWriter);


			if (localQuote_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("quote_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localQuote_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localGrand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "grand_total", xmlWriter);


			if (localGrand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGrand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_grand_totalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_grand_total", xmlWriter);


			if (localBase_grand_total == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_grand_total cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_grand_total);

			}

			xmlWriter.writeEndElement();
		}
		if (localCheckout_methodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "checkout_method", xmlWriter);


			if (localCheckout_method == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("checkout_method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCheckout_method);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_id", xmlWriter);


			if (localCustomer_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_tax_class_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_tax_class_id", xmlWriter);


			if (localCustomer_tax_class_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_tax_class_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_tax_class_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_group_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_group_id", xmlWriter);

			if (localCustomer_group_id == java.lang.Integer.MIN_VALUE)
			{

				throw new org.apache.axis2.databinding.ADBException("customer_group_id cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomer_group_id));
			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_emailTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_email", xmlWriter);


			if (localCustomer_email == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_email cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_email);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_prefixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_prefix", xmlWriter);


			if (localCustomer_prefix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_prefix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_prefix);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_firstnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_firstname", xmlWriter);


			if (localCustomer_firstname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_firstname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_firstname);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_middlenameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_middlename", xmlWriter);


			if (localCustomer_middlename == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_middlename cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_middlename);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_lastnameTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_lastname", xmlWriter);


			if (localCustomer_lastname == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_lastname cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_lastname);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_suffixTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_suffix", xmlWriter);


			if (localCustomer_suffix == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_suffix cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_suffix);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_noteTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_note", xmlWriter);


			if (localCustomer_note == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_note cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_note);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_note_notifyTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_note_notify", xmlWriter);


			if (localCustomer_note_notify == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_note_notify cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_note_notify);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_is_guestTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_is_guest", xmlWriter);


			if (localCustomer_is_guest == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_is_guest cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_is_guest);

			}

			xmlWriter.writeEndElement();
		}
		if (localApplied_rule_idsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "applied_rule_ids", xmlWriter);


			if (localApplied_rule_ids == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("applied_rule_ids cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localApplied_rule_ids);

			}

			xmlWriter.writeEndElement();
		}
		if (localReserved_order_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reserved_order_id", xmlWriter);


			if (localReserved_order_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reserved_order_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReserved_order_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localPassword_hashTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "password_hash", xmlWriter);


			if (localPassword_hash == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("password_hash cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPassword_hash);

			}

			xmlWriter.writeEndElement();
		}
		if (localCoupon_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "coupon_code", xmlWriter);


			if (localCoupon_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("coupon_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCoupon_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localGlobal_currency_codeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "global_currency_code", xmlWriter);


			if (localGlobal_currency_code == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("global_currency_code cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGlobal_currency_code);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_global_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_global_rate", xmlWriter);

			if (java.lang.Double.isNaN(localBase_to_global_rate))
			{

				throw new org.apache.axis2.databinding.ADBException("base_to_global_rate cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBase_to_global_rate));
			}

			xmlWriter.writeEndElement();
		}
		if (localBase_to_quote_rateTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_to_quote_rate", xmlWriter);

			if (java.lang.Double.isNaN(localBase_to_quote_rate))
			{

				throw new org.apache.axis2.databinding.ADBException("base_to_quote_rate cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBase_to_quote_rate));
			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_taxvatTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_taxvat", xmlWriter);


			if (localCustomer_taxvat == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_taxvat cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_taxvat);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_genderTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_gender", xmlWriter);


			if (localCustomer_gender == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("customer_gender cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCustomer_gender);

			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal", xmlWriter);

			if (java.lang.Double.isNaN(localSubtotal))
			{

				throw new org.apache.axis2.databinding.ADBException("subtotal cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubtotal));
			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotalTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal", xmlWriter);

			if (java.lang.Double.isNaN(localBase_subtotal))
			{

				throw new org.apache.axis2.databinding.ADBException("base_subtotal cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBase_subtotal));
			}

			xmlWriter.writeEndElement();
		}
		if (localSubtotal_with_discountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "subtotal_with_discount", xmlWriter);

			if (java.lang.Double.isNaN(localSubtotal_with_discount))
			{

				throw new org.apache.axis2.databinding.ADBException("subtotal_with_discount cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubtotal_with_discount));
			}

			xmlWriter.writeEndElement();
		}
		if (localBase_subtotal_with_discountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_subtotal_with_discount", xmlWriter);

			if (java.lang.Double.isNaN(localBase_subtotal_with_discount))
			{

				throw new org.apache.axis2.databinding.ADBException("base_subtotal_with_discount cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBase_subtotal_with_discount));
			}

			xmlWriter.writeEndElement();
		}
		if (localExt_shipping_infoTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "ext_shipping_info", xmlWriter);


			if (localExt_shipping_info == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("ext_shipping_info cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localExt_shipping_info);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_message_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message_id", xmlWriter);


			if (localGift_message_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_messageTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_message", xmlWriter);


			if (localGift_message == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_message cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_message);

			}

			xmlWriter.writeEndElement();
		}
		if (localCustomer_balance_amount_usedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "customer_balance_amount_used", xmlWriter);

			if (java.lang.Double.isNaN(localCustomer_balance_amount_used))
			{

				throw new org.apache.axis2.databinding.ADBException("customer_balance_amount_used cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomer_balance_amount_used));
			}

			xmlWriter.writeEndElement();
		}
		if (localBase_customer_balance_amount_usedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_customer_balance_amount_used", xmlWriter);

			if (java.lang.Double.isNaN(localBase_customer_balance_amount_used))
			{

				throw new org.apache.axis2.databinding.ADBException("base_customer_balance_amount_used cannot be null!!");

			}
			else
			{
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBase_customer_balance_amount_used));
			}

			xmlWriter.writeEndElement();
		}
		if (localUse_customer_balanceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "use_customer_balance", xmlWriter);


			if (localUse_customer_balance == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("use_customer_balance cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUse_customer_balance);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_amount", xmlWriter);


			if (localGift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_gift_cards_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_gift_cards_amount", xmlWriter);


			if (localBase_gift_cards_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_gift_cards_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_gift_cards_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localGift_cards_amount_usedTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "gift_cards_amount_used", xmlWriter);


			if (localGift_cards_amount_used == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("gift_cards_amount_used cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localGift_cards_amount_used);

			}

			xmlWriter.writeEndElement();
		}
		if (localUse_reward_pointsTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "use_reward_points", xmlWriter);


			if (localUse_reward_points == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("use_reward_points cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUse_reward_points);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_points_balanceTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_points_balance", xmlWriter);


			if (localReward_points_balance == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_points_balance cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_points_balance);

			}

			xmlWriter.writeEndElement();
		}
		if (localBase_reward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "base_reward_currency_amount", xmlWriter);


			if (localBase_reward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("base_reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localBase_reward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localReward_currency_amountTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "reward_currency_amount", xmlWriter);


			if (localReward_currency_amount == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("reward_currency_amount cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localReward_currency_amount);

			}

			xmlWriter.writeEndElement();
		}
		if (localShipping_addressTracker)
		{
			if (localShipping_address == null)
			{
				throw new org.apache.axis2.databinding.ADBException("shipping_address cannot be null!!");
			}
			localShipping_address.serialize(new javax.xml.namespace.QName("", "shipping_address"), xmlWriter);
		}
		if (localBilling_addressTracker)
		{
			if (localBilling_address == null)
			{
				throw new org.apache.axis2.databinding.ADBException("billing_address cannot be null!!");
			}
			localBilling_address.serialize(new javax.xml.namespace.QName("", "billing_address"), xmlWriter);
		}
		if (localItemsTracker)
		{
			if (localItems == null)
			{
				throw new org.apache.axis2.databinding.ADBException("items cannot be null!!");
			}
			localItems.serialize(new javax.xml.namespace.QName("", "items"), xmlWriter);
		}
		if (localPaymentTracker)
		{
			if (localPayment == null)
			{
				throw new org.apache.axis2.databinding.ADBException("payment cannot be null!!");
			}
			localPayment.serialize(new javax.xml.namespace.QName("", "payment"), xmlWriter);
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static ShoppingCartInfoEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			ShoppingCartInfoEntity object = new ShoppingCartInfoEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"shoppingCartInfoEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (ShoppingCartInfoEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "converted_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "converted_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setConverted_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setQuote_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_active").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_active" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_active(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setIs_active(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_virtual").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_virtual" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_virtual(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setIs_virtual(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "is_multi_shipping").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "is_multi_shipping" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setIs_multi_shipping(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setIs_multi_shipping(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items_count").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "items_count" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setItems_count(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setItems_count(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items_qty").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "items_qty" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setItems_qty(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setItems_qty(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "orig_order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "orig_order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setOrig_order_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_base_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_base_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_base_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_to_quote_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_to_quote_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_to_quote_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "store_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "store_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setStore_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "quote_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "quote_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setQuote_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGrand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_grand_total").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_grand_total" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_grand_total(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "checkout_method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "checkout_method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCheckout_method(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_tax_class_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_tax_class_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_tax_class_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_group_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_group_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_group_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setCustomer_group_id(java.lang.Integer.MIN_VALUE);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_email").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_email" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_email(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_prefix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_prefix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_prefix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_firstname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_firstname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_firstname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_middlename").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_middlename" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_middlename(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_lastname").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_lastname" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_lastname(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_suffix").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_suffix" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_suffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_note").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_note" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_note(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_note_notify").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_note_notify" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_note_notify(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_is_guest").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_is_guest" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_is_guest(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "applied_rule_ids").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "applied_rule_ids" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setApplied_rule_ids(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reserved_order_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reserved_order_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReserved_order_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "password_hash").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "password_hash" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPassword_hash(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "coupon_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "coupon_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCoupon_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "global_currency_code").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "global_currency_code" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGlobal_currency_code(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_global_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_global_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_global_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setBase_to_global_rate(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_to_quote_rate").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_to_quote_rate" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_to_quote_rate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setBase_to_quote_rate(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_taxvat").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_taxvat" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_taxvat(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_gender").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_gender" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_gender(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setSubtotal(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setBase_subtotal(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "subtotal_with_discount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "subtotal_with_discount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setSubtotal_with_discount(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setSubtotal_with_discount(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_subtotal_with_discount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_subtotal_with_discount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_subtotal_with_discount(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setBase_subtotal_with_discount(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "ext_shipping_info").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "ext_shipping_info" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setExt_shipping_info(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_message").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_message" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_message(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "customer_balance_amount_used").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "customer_balance_amount_used" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCustomer_balance_amount_used(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setCustomer_balance_amount_used(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_customer_balance_amount_used").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_customer_balance_amount_used" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_customer_balance_amount_used(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

					object.setBase_customer_balance_amount_used(java.lang.Double.NaN);

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "use_customer_balance").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "use_customer_balance" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUse_customer_balance(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_gift_cards_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_gift_cards_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_gift_cards_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "gift_cards_amount_used").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "gift_cards_amount_used" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setGift_cards_amount_used(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "use_reward_points").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "use_reward_points" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUse_reward_points(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_points_balance").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_points_balance" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_points_balance(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "base_reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "base_reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setBase_reward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "reward_currency_amount").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "reward_currency_amount" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setReward_currency_amount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "shipping_address").equals(reader.getName()))
				{

					object.setShipping_address(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "billing_address").equals(reader.getName()))
				{

					object.setBilling_address(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartAddressEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "items").equals(reader.getName()))
				{

					object.setItems(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartItemEntityArray.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "payment").equals(reader.getName()))
				{

					object.setPayment(greyjoy.travelsmart.adapter.sunbonoo.magento.ShoppingCartPaymentEntity.Factory.parse(reader));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

