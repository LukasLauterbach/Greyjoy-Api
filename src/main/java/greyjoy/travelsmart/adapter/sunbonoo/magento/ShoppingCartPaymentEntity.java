
/**
 * ShoppingCartPaymentEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * ShoppingCartPaymentEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class ShoppingCartPaymentEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * shoppingCartPaymentEntity Namespace URI = urn:Magento Namespace Prefix =
	 * ns1
	 */


	/**
	 * field for Payment_id
	 */


	protected java.lang.String localPayment_id;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPayment_idTracker = false;

	public boolean isPayment_idSpecified()
	{
		return localPayment_idTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPayment_id()
	{
		return localPayment_id;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Payment_id
	 */
	public void setPayment_id(java.lang.String param)
	{
		localPayment_idTracker = param != null;

		this.localPayment_id = param;


	}


	/**
	 * field for Created_at
	 */


	protected java.lang.String localCreated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCreated_atTracker = false;

	public boolean isCreated_atSpecified()
	{
		return localCreated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCreated_at()
	{
		return localCreated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Created_at
	 */
	public void setCreated_at(java.lang.String param)
	{
		localCreated_atTracker = param != null;

		this.localCreated_at = param;


	}


	/**
	 * field for Updated_at
	 */


	protected java.lang.String localUpdated_at;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localUpdated_atTracker = false;

	public boolean isUpdated_atSpecified()
	{
		return localUpdated_atTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getUpdated_at()
	{
		return localUpdated_at;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Updated_at
	 */
	public void setUpdated_at(java.lang.String param)
	{
		localUpdated_atTracker = param != null;

		this.localUpdated_at = param;


	}


	/**
	 * field for Method
	 */


	protected java.lang.String localMethod;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localMethodTracker = false;

	public boolean isMethodSpecified()
	{
		return localMethodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMethod()
	{
		return localMethod;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Method
	 */
	public void setMethod(java.lang.String param)
	{
		localMethodTracker = param != null;

		this.localMethod = param;


	}


	/**
	 * field for Cc_type
	 */


	protected java.lang.String localCc_type;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_typeTracker = false;

	public boolean isCc_typeSpecified()
	{
		return localCc_typeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_type()
	{
		return localCc_type;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_type
	 */
	public void setCc_type(java.lang.String param)
	{
		localCc_typeTracker = param != null;

		this.localCc_type = param;


	}


	/**
	 * field for Cc_number_enc
	 */


	protected java.lang.String localCc_number_enc;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_number_encTracker = false;

	public boolean isCc_number_encSpecified()
	{
		return localCc_number_encTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_number_enc()
	{
		return localCc_number_enc;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_number_enc
	 */
	public void setCc_number_enc(java.lang.String param)
	{
		localCc_number_encTracker = param != null;

		this.localCc_number_enc = param;


	}


	/**
	 * field for Cc_last4
	 */


	protected java.lang.String localCc_last4;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_last4Tracker = false;

	public boolean isCc_last4Specified()
	{
		return localCc_last4Tracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_last4()
	{
		return localCc_last4;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_last4
	 */
	public void setCc_last4(java.lang.String param)
	{
		localCc_last4Tracker = param != null;

		this.localCc_last4 = param;


	}


	/**
	 * field for Cc_cid_enc
	 */


	protected java.lang.String localCc_cid_enc;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_cid_encTracker = false;

	public boolean isCc_cid_encSpecified()
	{
		return localCc_cid_encTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_cid_enc()
	{
		return localCc_cid_enc;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_cid_enc
	 */
	public void setCc_cid_enc(java.lang.String param)
	{
		localCc_cid_encTracker = param != null;

		this.localCc_cid_enc = param;


	}


	/**
	 * field for Cc_owner
	 */


	protected java.lang.String localCc_owner;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ownerTracker = false;

	public boolean isCc_ownerSpecified()
	{
		return localCc_ownerTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_owner()
	{
		return localCc_owner;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_owner
	 */
	public void setCc_owner(java.lang.String param)
	{
		localCc_ownerTracker = param != null;

		this.localCc_owner = param;


	}


	/**
	 * field for Cc_exp_month
	 */


	protected java.lang.String localCc_exp_month;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_exp_monthTracker = false;

	public boolean isCc_exp_monthSpecified()
	{
		return localCc_exp_monthTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_exp_month()
	{
		return localCc_exp_month;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_exp_month
	 */
	public void setCc_exp_month(java.lang.String param)
	{
		localCc_exp_monthTracker = param != null;

		this.localCc_exp_month = param;


	}


	/**
	 * field for Cc_exp_year
	 */


	protected java.lang.String localCc_exp_year;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_exp_yearTracker = false;

	public boolean isCc_exp_yearSpecified()
	{
		return localCc_exp_yearTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_exp_year()
	{
		return localCc_exp_year;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_exp_year
	 */
	public void setCc_exp_year(java.lang.String param)
	{
		localCc_exp_yearTracker = param != null;

		this.localCc_exp_year = param;


	}


	/**
	 * field for Cc_ss_owner
	 */


	protected java.lang.String localCc_ss_owner;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ss_ownerTracker = false;

	public boolean isCc_ss_ownerSpecified()
	{
		return localCc_ss_ownerTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_ss_owner()
	{
		return localCc_ss_owner;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_ss_owner
	 */
	public void setCc_ss_owner(java.lang.String param)
	{
		localCc_ss_ownerTracker = param != null;

		this.localCc_ss_owner = param;


	}


	/**
	 * field for Cc_ss_start_month
	 */


	protected java.lang.String localCc_ss_start_month;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ss_start_monthTracker = false;

	public boolean isCc_ss_start_monthSpecified()
	{
		return localCc_ss_start_monthTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_ss_start_month()
	{
		return localCc_ss_start_month;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_ss_start_month
	 */
	public void setCc_ss_start_month(java.lang.String param)
	{
		localCc_ss_start_monthTracker = param != null;

		this.localCc_ss_start_month = param;


	}


	/**
	 * field for Cc_ss_start_year
	 */


	protected java.lang.String localCc_ss_start_year;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ss_start_yearTracker = false;

	public boolean isCc_ss_start_yearSpecified()
	{
		return localCc_ss_start_yearTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_ss_start_year()
	{
		return localCc_ss_start_year;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_ss_start_year
	 */
	public void setCc_ss_start_year(java.lang.String param)
	{
		localCc_ss_start_yearTracker = param != null;

		this.localCc_ss_start_year = param;


	}


	/**
	 * field for Cc_ss_issue
	 */


	protected java.lang.String localCc_ss_issue;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ss_issueTracker = false;

	public boolean isCc_ss_issueSpecified()
	{
		return localCc_ss_issueTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_ss_issue()
	{
		return localCc_ss_issue;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_ss_issue
	 */
	public void setCc_ss_issue(java.lang.String param)
	{
		localCc_ss_issueTracker = param != null;

		this.localCc_ss_issue = param;


	}


	/**
	 * field for Po_number
	 */


	protected java.lang.String localPo_number;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPo_numberTracker = false;

	public boolean isPo_numberSpecified()
	{
		return localPo_numberTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPo_number()
	{
		return localPo_number;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Po_number
	 */
	public void setPo_number(java.lang.String param)
	{
		localPo_numberTracker = param != null;

		this.localPo_number = param;


	}


	/**
	 * field for Additional_data
	 */


	protected java.lang.String localAdditional_data;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdditional_dataTracker = false;

	public boolean isAdditional_dataSpecified()
	{
		return localAdditional_dataTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdditional_data()
	{
		return localAdditional_data;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Additional_data
	 */
	public void setAdditional_data(java.lang.String param)
	{
		localAdditional_dataTracker = param != null;

		this.localAdditional_data = param;


	}


	/**
	 * field for Additional_information
	 */


	protected java.lang.String localAdditional_information;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localAdditional_informationTracker = false;

	public boolean isAdditional_informationSpecified()
	{
		return localAdditional_informationTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getAdditional_information()
	{
		return localAdditional_information;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Additional_information
	 */
	public void setAdditional_information(java.lang.String param)
	{
		localAdditional_informationTracker = param != null;

		this.localAdditional_information = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":shoppingCartPaymentEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "shoppingCartPaymentEntity", xmlWriter);
			}


		}
		if (localPayment_idTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "payment_id", xmlWriter);


			if (localPayment_id == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("payment_id cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPayment_id);

			}

			xmlWriter.writeEndElement();
		}
		if (localCreated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "created_at", xmlWriter);


			if (localCreated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("created_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCreated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localUpdated_atTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "updated_at", xmlWriter);


			if (localUpdated_at == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("updated_at cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localUpdated_at);

			}

			xmlWriter.writeEndElement();
		}
		if (localMethodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "method", xmlWriter);


			if (localMethod == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localMethod);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_typeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_type", xmlWriter);


			if (localCc_type == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_type cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_type);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_number_encTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_number_enc", xmlWriter);


			if (localCc_number_enc == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_number_enc cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_number_enc);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_last4Tracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_last4", xmlWriter);


			if (localCc_last4 == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_last4 cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_last4);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_cid_encTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_cid_enc", xmlWriter);


			if (localCc_cid_enc == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_cid_enc cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_cid_enc);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ownerTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_owner", xmlWriter);


			if (localCc_owner == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_owner cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_owner);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_exp_monthTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_exp_month", xmlWriter);


			if (localCc_exp_month == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_exp_month cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_exp_month);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_exp_yearTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_exp_year", xmlWriter);


			if (localCc_exp_year == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_exp_year cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_exp_year);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ss_ownerTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_ss_owner", xmlWriter);


			if (localCc_ss_owner == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_ss_owner cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_ss_owner);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ss_start_monthTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_ss_start_month", xmlWriter);


			if (localCc_ss_start_month == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_ss_start_month cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_ss_start_month);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ss_start_yearTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_ss_start_year", xmlWriter);


			if (localCc_ss_start_year == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_ss_start_year cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_ss_start_year);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ss_issueTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_ss_issue", xmlWriter);


			if (localCc_ss_issue == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_ss_issue cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_ss_issue);

			}

			xmlWriter.writeEndElement();
		}
		if (localPo_numberTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "po_number", xmlWriter);


			if (localPo_number == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("po_number cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPo_number);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdditional_dataTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "additional_data", xmlWriter);


			if (localAdditional_data == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("additional_data cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdditional_data);

			}

			xmlWriter.writeEndElement();
		}
		if (localAdditional_informationTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "additional_information", xmlWriter);


			if (localAdditional_information == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("additional_information cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localAdditional_information);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static ShoppingCartPaymentEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			ShoppingCartPaymentEntity object = new ShoppingCartPaymentEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"shoppingCartPaymentEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (ShoppingCartPaymentEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "payment_id").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "payment_id" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPayment_id(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "created_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "created_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCreated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "updated_at").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "updated_at" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setUpdated_at(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setMethod(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_type").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_type" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_number_enc").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_number_enc" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_number_enc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_last4").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_last4" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_last4(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_cid_enc").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_cid_enc" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_cid_enc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_owner").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_owner" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_owner(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_exp_month").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_exp_month" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_exp_month(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_exp_year").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_exp_year" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_exp_year(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_ss_owner").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_ss_owner" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_ss_owner(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_ss_start_month").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_ss_start_month" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_ss_start_month(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_ss_start_year").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_ss_start_year" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_ss_start_year(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_ss_issue").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_ss_issue" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_ss_issue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "po_number").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "po_number" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPo_number(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "additional_data").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "additional_data" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdditional_data(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "additional_information").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "additional_information" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setAdditional_information(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

