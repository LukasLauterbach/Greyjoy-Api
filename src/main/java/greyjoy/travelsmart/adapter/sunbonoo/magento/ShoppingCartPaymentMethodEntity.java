
/**
 * ShoppingCartPaymentMethodEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.7  Built on : Nov 20, 2017 (11:41:50 GMT)
 */


package greyjoy.travelsmart.adapter.sunbonoo.magento;


/**
 * ShoppingCartPaymentMethodEntity bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class ShoppingCartPaymentMethodEntity implements org.apache.axis2.databinding.ADBBean
{
	/*
	 * This type was generated from the piece of schema that had name =
	 * shoppingCartPaymentMethodEntity Namespace URI = urn:Magento Namespace
	 * Prefix = ns1
	 */


	/**
	 * field for Po_number
	 */


	protected java.lang.String localPo_number;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localPo_numberTracker = false;

	public boolean isPo_numberSpecified()
	{
		return localPo_numberTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getPo_number()
	{
		return localPo_number;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Po_number
	 */
	public void setPo_number(java.lang.String param)
	{
		localPo_numberTracker = param != null;

		this.localPo_number = param;


	}


	/**
	 * field for Method
	 */


	protected java.lang.String localMethod;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localMethodTracker = false;

	public boolean isMethodSpecified()
	{
		return localMethodTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getMethod()
	{
		return localMethod;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Method
	 */
	public void setMethod(java.lang.String param)
	{
		localMethodTracker = param != null;

		this.localMethod = param;


	}


	/**
	 * field for Cc_cid
	 */


	protected java.lang.String localCc_cid;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_cidTracker = false;

	public boolean isCc_cidSpecified()
	{
		return localCc_cidTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_cid()
	{
		return localCc_cid;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_cid
	 */
	public void setCc_cid(java.lang.String param)
	{
		localCc_cidTracker = param != null;

		this.localCc_cid = param;


	}


	/**
	 * field for Cc_owner
	 */


	protected java.lang.String localCc_owner;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_ownerTracker = false;

	public boolean isCc_ownerSpecified()
	{
		return localCc_ownerTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_owner()
	{
		return localCc_owner;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_owner
	 */
	public void setCc_owner(java.lang.String param)
	{
		localCc_ownerTracker = param != null;

		this.localCc_owner = param;


	}


	/**
	 * field for Cc_number
	 */


	protected java.lang.String localCc_number;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_numberTracker = false;

	public boolean isCc_numberSpecified()
	{
		return localCc_numberTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_number()
	{
		return localCc_number;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_number
	 */
	public void setCc_number(java.lang.String param)
	{
		localCc_numberTracker = param != null;

		this.localCc_number = param;


	}


	/**
	 * field for Cc_type
	 */


	protected java.lang.String localCc_type;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_typeTracker = false;

	public boolean isCc_typeSpecified()
	{
		return localCc_typeTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_type()
	{
		return localCc_type;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_type
	 */
	public void setCc_type(java.lang.String param)
	{
		localCc_typeTracker = param != null;

		this.localCc_type = param;


	}


	/**
	 * field for Cc_exp_year
	 */


	protected java.lang.String localCc_exp_year;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_exp_yearTracker = false;

	public boolean isCc_exp_yearSpecified()
	{
		return localCc_exp_yearTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_exp_year()
	{
		return localCc_exp_year;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_exp_year
	 */
	public void setCc_exp_year(java.lang.String param)
	{
		localCc_exp_yearTracker = param != null;

		this.localCc_exp_year = param;


	}


	/**
	 * field for Cc_exp_month
	 */


	protected java.lang.String localCc_exp_month;

	/*
	 * This tracker boolean wil be used to detect whether the user called the
	 * set method for this attribute. It will be used to determine whether to
	 * include this field in the serialized XML
	 */
	protected boolean localCc_exp_monthTracker = false;

	public boolean isCc_exp_monthSpecified()
	{
		return localCc_exp_monthTracker;
	}



	/**
	 * Auto generated getter method
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getCc_exp_month()
	{
		return localCc_exp_month;
	}



	/**
	 * Auto generated setter method
	 * 
	 * @param param
	 *            Cc_exp_month
	 */
	public void setCc_exp_month(java.lang.String param)
	{
		localCc_exp_monthTracker = param != null;

		this.localCc_exp_month = param;


	}




	/**
	 *
	 * @param parentQName
	 * @param factory
	 * @return org.apache.axiom.om.OMElement
	 */
	@Override
	public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
	{



		return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, parentQName));

	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{
		serialize(parentQName, xmlWriter, false);
	}

	@Override
	public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType) throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
	{




		java.lang.String prefix = null;
		java.lang.String namespace = null;


		prefix = parentQName.getPrefix();
		namespace = parentQName.getNamespaceURI();
		writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

		if (serializeType)
		{


			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":shoppingCartPaymentMethodEntity", xmlWriter);
			}
			else
			{
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "shoppingCartPaymentMethodEntity", xmlWriter);
			}


		}
		if (localPo_numberTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "po_number", xmlWriter);


			if (localPo_number == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("po_number cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localPo_number);

			}

			xmlWriter.writeEndElement();
		}
		if (localMethodTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "method", xmlWriter);


			if (localMethod == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("method cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localMethod);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_cidTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_cid", xmlWriter);


			if (localCc_cid == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_cid cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_cid);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_ownerTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_owner", xmlWriter);


			if (localCc_owner == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_owner cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_owner);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_numberTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_number", xmlWriter);


			if (localCc_number == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_number cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_number);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_typeTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_type", xmlWriter);


			if (localCc_type == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_type cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_type);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_exp_yearTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_exp_year", xmlWriter);


			if (localCc_exp_year == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_exp_year cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_exp_year);

			}

			xmlWriter.writeEndElement();
		}
		if (localCc_exp_monthTracker)
		{
			namespace = "";
			writeStartElement(null, namespace, "cc_exp_month", xmlWriter);


			if (localCc_exp_month == null)
			{
				// write the nil attribute

				throw new org.apache.axis2.databinding.ADBException("cc_exp_month cannot be null!!");

			}
			else
			{


				xmlWriter.writeCharacters(localCc_exp_month);

			}

			xmlWriter.writeEndElement();
		}
		xmlWriter.writeEndElement();


	}

	private static java.lang.String generatePrefix(java.lang.String namespace)
	{
		if (namespace.equals("urn:Magento"))
		{
			return "ns1";
		}
		return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
	}

	/**
	 * Utility method to write an element start tag.
	 */
	private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
		}
		else
		{
			if (namespace.length() == 0)
			{
				prefix = "";
			}
			else if (prefix == null)
			{
				prefix = generatePrefix(namespace);
			}

			xmlWriter.writeStartElement(prefix, localPart, namespace);
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
	}

	/**
	 * Util method to write an attribute with the ns prefix
	 */
	private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
		if (writerPrefix != null)
		{
			xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
		}
		else
		{
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
			xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
		}
	}

	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attValue);
		}
		else
		{
			xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
		}
	}


	/**
	 * Util method to write an attribute without the ns prefix
	 */
	private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		java.lang.String attributeNamespace = qname.getNamespaceURI();
		java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
		if (attributePrefix == null)
		{
			attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
		}
		java.lang.String attributeValue;
		if (attributePrefix.trim().length() > 0)
		{
			attributeValue = attributePrefix + ":" + qname.getLocalPart();
		}
		else
		{
			attributeValue = qname.getLocalPart();
		}

		if (namespace.equals(""))
		{
			xmlWriter.writeAttribute(attName, attributeValue);
		}
		else
		{
			registerPrefix(xmlWriter, namespace);
			xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
		}
	}

	/**
	 * method to handle Qnames
	 */

	private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String namespaceURI = qname.getNamespaceURI();
		if (namespaceURI != null)
		{
			java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
			if (prefix == null)
			{
				prefix = generatePrefix(namespaceURI);
				xmlWriter.writeNamespace(prefix, namespaceURI);
				xmlWriter.setPrefix(prefix, namespaceURI);
			}

			if (prefix.trim().length() > 0)
			{
				xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
			else
			{
				// i.e this is the default namespace
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}

		}
		else
		{
			xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
		}
	}

	private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
	{

		if (qnames != null)
		{
			// we have to store this data until last moment since it is not
			// possible to write any
			// namespace data after writing the charactor data
			java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
			java.lang.String namespaceURI = null;
			java.lang.String prefix = null;

			for (int i = 0; i < qnames.length; i++)
			{
				if (i > 0)
				{
					stringToWrite.append(" ");
				}
				namespaceURI = qnames[i].getNamespaceURI();
				if (namespaceURI != null)
				{
					prefix = xmlWriter.getPrefix(namespaceURI);
					if ((prefix == null) || (prefix.length() == 0))
					{
						prefix = generatePrefix(namespaceURI);
						xmlWriter.writeNamespace(prefix, namespaceURI);
						xmlWriter.setPrefix(prefix, namespaceURI);
					}

					if (prefix.trim().length() > 0)
					{
						stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
					else
					{
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}
				else
				{
					stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
				}
			}
			xmlWriter.writeCharacters(stringToWrite.toString());
		}

	}


	/**
	 * Register a namespace prefix
	 */
	private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
	{
		java.lang.String prefix = xmlWriter.getPrefix(namespace);
		if (prefix == null)
		{
			prefix = generatePrefix(namespace);
			javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
			while (true)
			{
				java.lang.String uri = nsContext.getNamespaceURI(prefix);
				if (uri == null || uri.length() == 0)
				{
					break;
				}
				prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
			}
			xmlWriter.writeNamespace(prefix, namespace);
			xmlWriter.setPrefix(prefix, namespace);
		}
		return prefix;
	}




	/**
	 * Factory class that keeps the parse method
	 */
	public static class Factory
	{
		private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);




		/**
		 * static method to create the object Precondition: If this object is an
		 * element, the current or next start element starts this object and any
		 * intervening reader events are ignorable If this object is not an
		 * element, it is a complex type and the reader is at the event just
		 * after the outer start element Postcondition: If this object is an
		 * element, the reader is positioned at its end element If this object
		 * is a complex type, the reader is positioned at the end element of its
		 * outer element
		 */
		public static ShoppingCartPaymentMethodEntity parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
		{
			ShoppingCartPaymentMethodEntity object = new ShoppingCartPaymentMethodEntity();

			int event;
			javax.xml.namespace.QName currentQName = null;
			java.lang.String nillableValue = null;
			java.lang.String prefix = "";
			java.lang.String namespaceuri = "";
			try
			{

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				currentQName = reader.getName();

				if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
				{
					java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
					if (fullTypeName != null)
					{
						java.lang.String nsPrefix = null;
						if (fullTypeName.indexOf(":") > -1)
						{
							nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
						}
						nsPrefix = nsPrefix == null ? "" : nsPrefix;

						java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

						if (!"shoppingCartPaymentMethodEntity".equals(type))
						{
							// find namespace for the prefix
							java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
							return (ShoppingCartPaymentMethodEntity) greyjoy.travelsmart.adapter.sunbonoo.magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
						}


					}


				}




				// Note all attributes that were handled. Used to differ normal
				// attributes
				// from anyAttributes.
				java.util.Vector handledAttributes = new java.util.Vector();




				reader.next();


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "po_number").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "po_number" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setPo_number(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "method").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "method" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setMethod(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_cid").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_cid" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_cid(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_owner").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_owner" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_owner(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_number").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_number" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_number(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_type").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_type" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_exp_year").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_exp_year" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_exp_year(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}


				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement() && new javax.xml.namespace.QName("", "cc_exp_month").equals(reader.getName()))
				{

					nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
					if ("true".equals(nillableValue) || "1".equals(nillableValue))
					{
						throw new org.apache.axis2.databinding.ADBException("The element: " + "cc_exp_month" + "  cannot be null");
					}


					java.lang.String content = reader.getElementText();

					object.setCc_exp_month(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

					reader.next();

				} // End of if for expected property start element

				else
				{

				}

				while (!reader.isStartElement() && !reader.isEndElement())
					reader.next();

				if (reader.isStartElement())
					// 2 - A start element we are not expecting indicates a
					// trailing invalid property

					throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());




			}
			catch (javax.xml.stream.XMLStreamException e)
			{
				throw new java.lang.Exception(e);
			}

			return object;
		}

	}// end of factory class



}

