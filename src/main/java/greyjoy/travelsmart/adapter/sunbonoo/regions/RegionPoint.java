package greyjoy.travelsmart.adapter.sunbonoo.regions;

import greyjoy.travelsmart.util.MathUtil;

public class RegionPoint
{
	int regionID;

	double radius;
	double latitude;
	double longitude;

	public RegionPoint(int regionID, double radius, double latitude, double longitude)
	{
		this.regionID = regionID;
		this.radius = radius;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public boolean overlaps(double location_radius, double location_latitude, double location_longitude)
	{
		double distance = MathUtil.distance(latitude, longitude, location_latitude, location_longitude);

		return distance <= this.radius + location_radius;
	}

	public int getRegionID()
	{
		return regionID;
	}

	public double getRadius()
	{
		return radius;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

}
