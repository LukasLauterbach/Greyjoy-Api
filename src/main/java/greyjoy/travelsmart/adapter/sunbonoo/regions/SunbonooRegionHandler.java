package greyjoy.travelsmart.adapter.sunbonoo.regions;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.google.maps.model.GeocodingResult;
import com.google.maps.model.Geometry;

import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryEntity;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTree;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeRequestParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.CatalogCategoryTreeResponseParam;
import greyjoy.travelsmart.adapter.sunbonoo.magento.MagentoService;
import greyjoy.travelsmart.api.handler.GeoHandler;
import greyjoy.travelsmart.util.MathUtil;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class SunbonooRegionHandler
{
	private Log logger;

	HashMap<String, Integer> allRegions;
	HashMap<Integer, String> allRegions_reversed;

	List<RegionPoint> regionPoints;
	HashMap<Integer, RegionPoint> regionPointMap;

	GeoHandler geoHandler;

	public SunbonooRegionHandler(GeoHandler geoHandler)
	{
		regionPoints = new ArrayList<RegionPoint>();

		logger = LogFactory.getLog(SunbonooRegionHandler.class);

		this.geoHandler = geoHandler;

		allRegions = new HashMap<String, Integer>();
		allRegions_reversed = new HashMap<Integer, String>();
		regionPointMap = new HashMap<Integer, RegionPoint>();
	}

	public String getRegionName(int regionID)
	{
		return allRegions_reversed.get(regionID);
	}

	public RegionPoint getRegionPoint(int regionID)
	{
		return regionPointMap.get(regionID);
	}

	public Flux<Integer> getOverlappingRegions(int location_radius, double location_latitude, double location_longitude)
	{
		return Flux.fromIterable(regionPoints).filter(r -> r.radius < 100 && r.overlaps(location_radius, location_latitude, location_longitude)).map(r -> r.getRegionID());
	}

	public void initRegions(Mono<String> sessionCache, MagentoService service)
	{
		logger.info("Reading Sunbonoo Regions...");

		sessionCache.subscribe(sessionID -> {
			CatalogCategoryTreeRequestParam rq = new CatalogCategoryTreeRequestParam();
			rq.setSessionId(sessionID);
			rq.setParentId("319");
			try
			{
				CatalogCategoryTreeResponseParam rsp = service.catalogCategoryTree(rq);
				CatalogCategoryTree tree = rsp.getResult();

				ArrayList<CatalogCategoryEntity> stack = new ArrayList<CatalogCategoryEntity>(Arrays.asList(tree.getChildren().getComplexObjectArray()));

				while (!stack.isEmpty())
				{
					CatalogCategoryEntity current = stack.remove(0);

					allRegions.put(current.getName(), current.getCategory_id());
					allRegions_reversed.put(current.getCategory_id(), current.getName());

					if (current.getChildren().isComplexObjectArraySpecified())
					{
						for (CatalogCategoryEntity child : current.getChildren().getComplexObjectArray())
						{
							stack.add(child);
						}
					}

				}

				// BlackList
				allRegions.remove(allRegions_reversed.get(1306));
				allRegions_reversed.remove(1306);

				allRegions_reversed.remove(599);

				logger.info(" - Mapping " + allRegions.size() + " regions");

				File folder = new File("sb_regions/");

				HashSet<Integer> toGet = new HashSet<Integer>();
				toGet.addAll(allRegions_reversed.keySet());

				if (folder.exists())
				{
					for (File f : folder.listFiles())
					{
						String name = f.getName();

						int id = Integer.parseInt(name.split("\\.")[0]);

						toGet.remove(id);

						try
						{
							String line = Files.readAllLines(f.toPath()).get(0);

							String[] split = line.split(",");

							double[] circle = MathUtil.coordRectToPointRadius(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));

							RegionPoint rp = new RegionPoint(id, circle[0], circle[1], circle[2]);

							regionPoints.add(rp);
						}
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				logger.info(" - Missing IDs for " + toGet.size() + " regions");

				HashMap<Integer, String> overrides = new HashMap<Integer, String>();
				overrides.put(943, "Soo, Kanarische Inseln, Spanien");
				overrides.put(1069, "Budino 15821");
				overrides.put(1349, "Sissi, Kreta");

				//
				boolean useGoogle = false;

				if (useGoogle) // Verbraucht ohne Datenbank extrem viele Google
								// API
								// Aufrufe, bitte nicht benutzen.
				{
					regionPoints.addAll(Flux.fromIterable(allRegions.entrySet()).filter(entry -> !Files.exists(FileSystems.getDefault().getPath("sb_regions", entry.getValue() + ".txt"))).flatMap(entry -> {
						return Mono.zip(Mono.just(entry.getValue()), geoHandler.geocode(overrides.containsKey(entry.getValue()) ? overrides.get(entry.getValue()) : entry.getKey()));
					}).flatMap(tuple -> {

						String readable = tuple.getT1() + " | " + allRegions_reversed.get(tuple.getT1());
						logger.info(" - Google Request Request for " + readable);

						int id = tuple.getT1();
						GeocodingResult[] results = tuple.getT2();

						if (results.length == 0)
						{
							logger.info(" - - No Google Result");
							return Mono.empty();
						}

						Geometry geometry = results[0].geometry;

						if (geometry.viewport == null)
						{
							logger.info(" - - Missing coordinates");

							return Mono.empty();
						}
						else
						{
							double[] circle = MathUtil.coordRectToPointRadius(geometry.viewport.southwest.lat, geometry.viewport.southwest.lng, geometry.viewport.northeast.lat, geometry.viewport.northeast.lng);

							Path regionPath = FileSystems.getDefault().getPath("sb_regions", id + ".txt");

							if (!Files.exists(regionPath))
							{
								try
								{
									Files.createFile(regionPath);
								}
								catch (IOException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								List<String> lines = new ArrayList<String>();

								lines.add(geometry.viewport.southwest.lat + "," + geometry.viewport.southwest.lng + "," + geometry.viewport.northeast.lat + "," + geometry.viewport.northeast.lng);

								try
								{
									Files.write(regionPath, lines);
								}
								catch (IOException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								logger.info(" - - Success");
							}

							return Mono.just(new RegionPoint(id, circle[0], circle[1], circle[2]));
						}
					}).collectList().block());
				}
			}
			catch (RemoteException e)
			{
				e.printStackTrace();
			}

			for (RegionPoint rp : regionPoints)
			{
				regionPointMap.put(rp.getRegionID(), rp);
			}

			logger.info("Done");
		});
	}
}
