package greyjoy.travelsmart.api;

public class AdapterNotFoundException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6835399841883305509L;

	public AdapterNotFoundException(int id)
	{
		super("Could not find adapter with id " + id);
	}

}
