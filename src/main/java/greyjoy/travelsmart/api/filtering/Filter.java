package greyjoy.travelsmart.api.filtering;

import java.util.Set;

public class Filter
{
	// Vendor
	boolean filterVendor;
	Set<String> vendor;

	// Location
	boolean filterLocation;
	int location_radius;
	double location_latitude;
	double location_longitude;

	// Date
	boolean filterDate;
	String date_min;
	String date_max;

	// Language
	boolean filterLanguage;
	String language;
	
	// Price
	boolean filterPrice;
	double price_min = -1;
	double price_max = -1;
	
	// Duration
	boolean filterDuration;
	double duration_min = -1;
	double duration_max = -1;
	
	// Rating
	boolean filterRating;
	double rating_min = -1;
	double rating_max = -1;
	
	// Category
	boolean filterCategory;
	Set<Integer> categories;

	public Set<Integer> categories()
	{
		return categories;
	}
	
	public boolean filterCategory()
	{
		return filterCategory;
	}
	
	public boolean filterRating()
	{
		return filterRating;
	}
	
	public double rating_min()
	{
		return rating_min;
	}
	
	public double rating_max()
	{
		return rating_max;
	}
	
	public boolean filterDuration()
	{
		return filterDuration;
	}
	
	public double duration_min()
	{
		return duration_min;
	}
	
	public double duration_max()
	{
		return duration_max;
	}
	
	public boolean filterPrice()
	{
		return filterPrice;
	}
	
	public double price_min()
	{
		return price_min;
	}
	
	public double price_max()
	{
		return price_max;
	}

	public boolean filterLanguage()
	{
		return filterLanguage;
	}

	public String language()
	{
		return language;
	}

	public boolean filterLocation()
	{
		return filterLocation;
	}

	public boolean filterVendor()
	{
		return filterVendor;
	}

	public Set<String> vendor()
	{
		return vendor;
	}

	public int location_radius()
	{
		return location_radius;
	}

	public double location_latitude()
	{
		return location_latitude;
	}

	public double location_longitude()
	{
		return location_longitude;
	}

	public boolean filterDate()
	{
		return filterDate;
	}

	public String date_min()
	{
		return date_min;
	}

	public String date_max()
	{
		return date_max;
	}
}
