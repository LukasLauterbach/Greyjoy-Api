package greyjoy.travelsmart.api.filtering;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import com.google.maps.model.GeocodingResult;
import com.google.maps.model.Geometry;
import com.google.maps.model.LatLng;

import greyjoy.travelsmart.api.handler.GeoHandler;
import reactor.core.publisher.Mono;

@Component
public class FilterHandler
{
	@Autowired
	GeoHandler geoHandler;

	public Mono<Filter> fromMap(Map<String, String> stringFilterMap)
	{
		Mono<Filter> mono = Mono.just(new Filter());

		if (stringFilterMap.containsKey("vendor"))
		{
			mono = mono.map(f -> {
				f.filterVendor = true;

				f.vendor = new HashSet<String>();
				f.vendor.addAll(Arrays.asList(stringFilterMap.get("vendor").split(",")));

				return f;
			});
		}

		if (stringFilterMap.containsKey("location") && stringFilterMap.containsKey("location_radius"))
		{
			mono = mono.zipWith(geoHandler.geocode(stringFilterMap.get("location"))).map(tuple -> {
				GeocodingResult[] results = tuple.getT2();
				Filter f = tuple.getT1();

				if (results.length == 0 || results[0].geometry == null || results[0].geometry.location == null)
				{
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find coordinates for _" + stringFilterMap.get("location") + "_");
				}

				LatLng location = results[0].geometry.location;
				
				f.filterLocation = true;

				f.location_radius = Integer.parseInt(stringFilterMap.get("location_radius"));
				f.location_latitude = location.lat;
				f.location_longitude = location.lng;

				return f;
			});
		}
		else if (stringFilterMap.containsKey("location_radius") && stringFilterMap.containsKey("location_latitude") && stringFilterMap.containsKey("location_longitude"))
		{
			mono = mono.map(f -> {
				f.filterLocation = true;

				f.location_radius = Integer.parseInt(stringFilterMap.get("location_radius"));
				f.location_latitude = Double.parseDouble(stringFilterMap.get("location_latitude"));
				f.location_longitude = Double.parseDouble(stringFilterMap.get("location_longitude"));
				return f;
			});
		}

		if (stringFilterMap.containsKey("date_min") && stringFilterMap.containsKey("date_max"))
		{
			mono = mono.map(f -> {
				f.filterDate = true;

				f.date_min = stringFilterMap.get("date_min");
				f.date_max = stringFilterMap.get("date_max");
				return f;
			});
		}
		
		if (stringFilterMap.containsKey("price_min") || stringFilterMap.containsKey("price_max"))
		{
			mono = mono.map(f -> {
				f.filterPrice = true;

				if (stringFilterMap.containsKey("price_min"))
				{
					f.price_min = Double.parseDouble(stringFilterMap.get("price_min"));
				}
				
				if (stringFilterMap.containsKey("price_max"))
				{
					f.price_max = Double.parseDouble(stringFilterMap.get("price_max"));
				}
				
				return f;
			});
		}
		
		if (stringFilterMap.containsKey("duration_min") || stringFilterMap.containsKey("duration_max"))
		{
			mono = mono.map(f -> {
				f.filterDuration = true;

				if (stringFilterMap.containsKey("duration_min"))
				{
					f.duration_min = Double.parseDouble(stringFilterMap.get("duration_min"));
				}
				
				if (stringFilterMap.containsKey("duration_max"))
				{
					f.duration_max = Double.parseDouble(stringFilterMap.get("duration_max"));
				}
				
				return f;
			});
		}
		
		if (stringFilterMap.containsKey("rating_min") || stringFilterMap.containsKey("rating_max"))
		{
			mono = mono.map(f -> {
				f.filterRating = true;

				if (stringFilterMap.containsKey("rating_min"))
				{
					f.rating_min = Double.parseDouble(stringFilterMap.get("rating_min"));
				}
				
				if (stringFilterMap.containsKey("rating_max"))
				{
					f.rating_max = Double.parseDouble(stringFilterMap.get("rating_max"));
				}
				
				return f;
			});
		}

		if (stringFilterMap.containsKey("language"))
		{
			mono = mono.map(f -> {
				f.filterLanguage = true;
				f.language = stringFilterMap.get("language");
				return f;
			});
		}
		
		if (stringFilterMap.containsKey("categories"))
		{
			mono = mono.map(f -> {
				f.filterCategory = true;
				f.categories = Arrays.asList(stringFilterMap.get("categories").split(",")).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toCollection(() -> new HashSet<Integer>()));
				return f;
			});
		}

		return mono;
	}
}
