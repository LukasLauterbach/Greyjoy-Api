package greyjoy.travelsmart.api.handler;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import greyjoy.travelsmart.adapter.ITravelAdapter;
import greyjoy.travelsmart.api.AdapterNotFoundException;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

@Component
public class AdapterHandler
{
	private @Autowired Log logger;

	List<ITravelAdapter> travelAdapters;

	Map<Integer, ITravelAdapter> adapterIds;
	Map<ITravelAdapter, Integer> adapterIds_reversed;

	public AdapterHandler(List<ITravelAdapter> travelAdapters)
	{
		this.travelAdapters = travelAdapters;
		this.adapterIds = new HashMap<Integer, ITravelAdapter>();

		adapterIds_reversed = new HashMap<ITravelAdapter, Integer>();
	}

	public int getID(ITravelAdapter adapter)
	{
		return adapterIds_reversed.get(adapter);
	}

	@PostConstruct
	public void init()
	{
		logger.info("Initializing Travel Adapters...");

		for (int id = 0; id < travelAdapters.size(); id++)
		{
			ITravelAdapter adapter = travelAdapters.get(id);

			logger.info(String.format(" - %d | %s | %s", id, adapter.getVendorID(), adapter.getVendorName()));

			adapterIds.put(id, adapter);
			adapterIds_reversed.put(adapter, id);
		}
	}

	public String encodeID(ITravelAdapter adapter, String adapterID)
	{
		return Base64.getEncoder().encodeToString((getID(adapter) + "_" + adapterID).getBytes(Charset.forName("UTF-8")));
	}

	public Tuple2<ITravelAdapter, String> decodeID(String id) throws AdapterNotFoundException
	{
		id = new String(Base64.getDecoder().decode(id.getBytes(Charset.forName("UTF-8"))));

		int delimiterIndex = id.indexOf('_');

		int adapterID = Integer.parseInt(id.substring(0, delimiterIndex));
		String actualID = id.substring(delimiterIndex + 1);

		if (!adapterIds.containsKey(adapterID))
		{
			throw new AdapterNotFoundException(adapterID);
		}

		ITravelAdapter adapter = adapterIds.get(adapterID);

		return Tuples.of(adapter, actualID);
	}

	public Flux<ITravelAdapter> getTravelAdapters()
	{
		return Flux.fromIterable(travelAdapters);
	}
}
