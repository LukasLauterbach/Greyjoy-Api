package greyjoy.travelsmart.api.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import greyjoy.travelsmart.api.model.Category;


public class CategoryHandler
{
	static CategoryHandler INSTANCE = new CategoryHandler();
	List<String> categories;

	public CategoryHandler()
	{
		categories = new ArrayList<String>();
		
		init();
	}
	
	public static CategoryHandler get()
	{
		return INSTANCE;
	}

	public void init()
	{
		categories.add("Allerlei");
		categories.add("Bootsausflüge");
		categories.add("Sport & Aktivurlaub");
		categories.add("Sightseeing & Kultur");
		categories.add("Abenteuerurlaub");
		categories.add("Essen & Trinken");
		categories.add("Nachtleben");
		categories.add("Freizeitparks");
		categories.add("Wellness & Beauty");
		categories.add("Shopping");
	}

	public Category getCategoryFromID(int id)
	{
		if (id < 0 || id > categories.size() - 1)
		{
			return null;
		}
		return new Category(id, categories.get(id));
	}

	public Category getCategoryFromName(String searchName)
	{
		for (int i = 0; i < categories.size(); i++)
		{
			String name = categories.get(i);
			if (searchName.equals(name))
			{
				return new Category(i, name);
			}
		}

		return null;
	}

	public List<Category> getCategories()
	{
		return categories.stream().map(s -> getCategoryFromName(s)).collect(Collectors.toList());
	}
}
