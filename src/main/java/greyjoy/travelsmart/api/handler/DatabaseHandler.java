package greyjoy.travelsmart.api.handler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component
public class DatabaseHandler
{
	Connection connection = null;
	Statement statement;
	
	public static final Object LOCK = new Object();
	
	public DatabaseHandler()
	{
		
	}
	
	@PreDestroy
	private void disconnect()
	{
		try
		{
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@PostConstruct
	private void connect()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
		}
		catch (ClassNotFoundException e1)
		{
			e1.printStackTrace();
		}
		
		try
		{
			// create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:cache.db");
			statement = connection.createStatement();
		}
		catch (SQLException e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	public PreparedStatement prepare(String query)
	{
		try
		{
			PreparedStatement ps = connection.prepareStatement(query);
			
			return ps;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void executeUpdate(String update)
	{
		try
		{
			statement.executeUpdate(update);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
