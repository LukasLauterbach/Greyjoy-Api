package greyjoy.travelsmart.api.handler;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.PendingResult.Callback;
import com.google.maps.model.GeocodingResult;

import reactor.core.publisher.Mono;

@Service
public class GeoHandler
{
	GeoApiContext geoContext;
	
	public GeoHandler()
	{
		
	}
	
	@PostConstruct
	public void init()
	{
		geoContext = new GeoApiContext.Builder().apiKey("AIzaSyCG2W2FIHH0lB2a2Ni7GQzu8Hd7i7RyLrE").build();
	}
	
	public Mono<GeocodingResult[]> geocode(String address)
	{
		return Mono.<GeocodingResult[]>create(sink -> {

			Callback<GeocodingResult[]> callback = new Callback<GeocodingResult[]>()
			{

				@Override
				public void onResult(GeocodingResult[] result)
				{
					sink.success(result);
				}

				@Override
				public void onFailure(Throwable e)
				{
					sink.error(e);
				}

			};

			GeocodingApiRequest request = GeocodingApi.geocode(geoContext, address);
			request.setCallback(callback);
		});
	}
}
