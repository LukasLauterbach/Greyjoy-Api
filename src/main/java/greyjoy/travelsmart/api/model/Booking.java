package greyjoy.travelsmart.api.model;

public class Booking
{
	Event[] recommendations;
	
	Event[] events;

	public Event[] getRecommendations()
	{
		return recommendations;
	}

	public void setRecommendations(Event[] recommendations)
	{
		this.recommendations = recommendations;
	}

	public Event[] getEvents()
	{
		return events;
	}

	public void setEvents(Event[] events)
	{
		this.events = events;
	}
	
	
	
}
