package greyjoy.travelsmart.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import greyjoy.travelsmart.api.model.price.Prices;

public class Event {
	String id;
	String title;
	String[] languages = new String[0];

	String location = "";
	double location_lat;
	double location_long;
	
	Category category = new Category(0, "Allerlei");

	String[] images = new String[0];

	Rating rating = new Rating(0, 0);

	String vendor;

	double displayPrice;
	
	Prices prices = new Prices();

	int duration;

	// Internal
	@JsonIgnore
	String adapterID;
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getLanguages() {
		return languages;
	}

	public void setLanguages(String[] languages) {
		this.languages = languages;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getLocation_lat() {
		return location_lat;
	}

	public void setLocation_lat(double location_lat) {
		this.location_lat = location_lat;
	}

	public double getLocation_long() {
		return location_long;
	}

	public void setLocation_long(double location_long) {
		this.location_long = location_long;
	}

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAdapterID() {
		return adapterID;
	}

	public void setAdapterID(String adapterID) {
		this.adapterID = adapterID;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;

	}

	public Category getCategory()
	{
		return category;
	}

	public void setCategory(Category category)
	{
		this.category = category;
	}

	public double getDisplayPrice()
	{
		return displayPrice;
	}

	public void setDisplayPrice(double displayPrice)
	{
		this.displayPrice = displayPrice;
	}

	public Prices getPrices()
	{
		return prices;
	}

	public void setPrices(Prices prices)
	{
		this.prices = prices;
	}
}
