package greyjoy.travelsmart.api.model;

import java.util.Collections;
import java.util.List;

public class EventDetails extends Event
{
	String description = "";

	List<Ticket> tickets = Collections.emptyList();
	
	String[] highlights = new String[0];

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<Ticket> getTickets()
	{
		return tickets;
	}

	public void setTickets(List<Ticket> tickets2)
	{
		this.tickets = tickets2;
	}
	
	public String[] getHighlights()
	{
		return highlights;
	}

	public void setHighlights(String[] highlights)
	{
		this.highlights = highlights;
	}
}
