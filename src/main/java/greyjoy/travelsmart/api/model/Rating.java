package greyjoy.travelsmart.api.model;

public class Rating
{
	double value;
	int count;

	public Rating(double value, int count)
	{
		super();
		this.value = value;
		this.count = count;
	}
	
	public Rating()
	{
		
	}

	public double getValue()
	{
		return value;
	}

	public int getCount()
	{
		return count;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rating other = (Rating) obj;
		if (count != other.count)
			return false;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}

}
