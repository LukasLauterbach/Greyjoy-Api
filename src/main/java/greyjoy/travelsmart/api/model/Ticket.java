package greyjoy.travelsmart.api.model;

public class Ticket
{
	String date;

	int available_slots;
	int total_slots;

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public int getAvailable_slots()
	{
		return available_slots;
	}

	public void setAvailable_slots(int available_slots)
	{
		this.available_slots = available_slots;
	}

	public int getTotal_slots()
	{
		return total_slots;
	}

	public void setTotal_slots(int total_slots)
	{
		this.total_slots = total_slots;
	}

}
