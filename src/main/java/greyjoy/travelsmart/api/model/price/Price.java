package greyjoy.travelsmart.api.model.price;

public class Price
{
	String name;
	double value;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getValue()
	{
		return value;
	}

	public void setValue(double value)
	{
		this.value = value;
	}


}
