package greyjoy.travelsmart.api.model.price;

import java.util.List;

public class PriceGroup
{
	String name;

	List<Price> list;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Price>  getList()
	{
		return list;
	}

	public void setList(List<Price>  list)
	{
		this.list = list;
	}



}
