package greyjoy.travelsmart.api.model.price;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Prices
{
	public enum TYPE
	{
		@JsonProperty("simple")SIMPLE, @JsonProperty("grouped")GROUPED;
		
		@Override
		public String toString()
		{
			return name().toLowerCase();
		}
	}

	TYPE type = TYPE.SIMPLE;
	List<PriceGroup> groups;

	List<Price> list;

	public TYPE getType()
	{
		return type;
	}

	public void setType(TYPE type)
	{
		this.type = type;
	}

	public List<PriceGroup> getGroups()
	{
		return groups;
	}

	public void setGroups(List<PriceGroup> groups)
	{
		this.groups = groups;
	}

	public List<Price> getList()
	{
		return list;
	}

	public void setList(List<Price> list)
	{
		this.list = list;
	}
}
