package greyjoy.travelsmart.api.routing;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.filtering.FilterHandler;
import greyjoy.travelsmart.api.handler.AdapterHandler;
import greyjoy.travelsmart.api.model.Booking;
import greyjoy.travelsmart.api.model.Event;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/booking")
public class BookingController
{
	EventController eventController;

	public BookingController(EventController eventController)
	{
		this.eventController = eventController;
	}

	@GetMapping(path = "")
	public Mono<Booking> getBooking(@RequestParam Map<String, String> stringFilterMap)
	{
		return eventController.getEvents(stringFilterMap).collectList().map(list -> {

			Booking booking = new Booking();
			booking.setEvents(list.toArray(new Event[0]));

			Collections.sort(list, new Comparator<Event>()
			{

				@Override
				public int compare(Event o1, Event o2)
				{
					return (int) (o2.getRating().getValue() - o1.getRating().getValue());
				}
			});

			Event[] recommendations = new Event[Math.min(list.size(), 6)];

			for (int i = 0; i < recommendations.length; i++)
			{
				recommendations[i] = list.get(i);
			}

			booking.setRecommendations(recommendations);

			return booking;
		});
	}
}
