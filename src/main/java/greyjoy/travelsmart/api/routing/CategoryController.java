package greyjoy.travelsmart.api.routing;

import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import greyjoy.travelsmart.api.handler.CategoryHandler;
import greyjoy.travelsmart.api.model.Category;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/categories")
public class CategoryController
{
	EventController eventController;

	public CategoryController(EventController eventController)
	{
		this.eventController = eventController;
	}

	@GetMapping(path = "")
	public Flux<Category> getBooking(@RequestParam Map<String, String> stringFilterMap)
	{
		return Flux.fromIterable(CategoryHandler.get().getCategories());
	}
}
