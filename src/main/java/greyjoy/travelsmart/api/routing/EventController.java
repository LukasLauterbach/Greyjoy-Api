package greyjoy.travelsmart.api.routing;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import greyjoy.travelsmart.adapter.ITravelAdapter;
import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.filtering.FilterHandler;
import greyjoy.travelsmart.api.handler.AdapterHandler;
import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.config.Config;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/events")
public class EventController
{
	AdapterHandler adapterHandler;
	FilterHandler filterHandler;

	public EventController(AdapterHandler adapterHandler, FilterHandler filterHandler)
	{
		this.adapterHandler = adapterHandler;
		this.filterHandler = filterHandler;
	}

	@GetMapping(path = "")
	public Flux<Event> getEvents(@RequestParam Map<String, String> stringFilterMap)
	{
		long startTime = System.currentTimeMillis();
		Mono<Filter> filterMono = filterHandler.fromMap(stringFilterMap);

		return filterMono.flatMapMany(filter -> {
			return adapterHandler.getTravelAdapters().filter(a -> filter.filterVendor() ? filter.vendor().contains(a.getVendorID()) : true).flatMap(a -> {
				try
				{
					return a.getEvents(filter).map(o -> {
						o.setVendor(a.getVendorID());
						o.setId(adapterHandler.encodeID(a, o.getAdapterID()));
						return o;
					}).takeUntilOther(Mono.just(1).delayElement(Duration.ofSeconds(Config.EVENTS_TIMEOUT)).doOnNext(i -> System.out.println(" - " + a.getVendorID() + ": Timeout after " + Config.EVENTS_TIMEOUT + " seconds"))).doOnComplete(() -> {
						long duration = (System.currentTimeMillis() - startTime);

						if (duration <= Config.EVENTS_TIMEOUT * 1000)
							System.out.println(" - " + a.getVendorID() + ": " + duration + "ms");
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				return Flux.empty();
			}).filter(a -> filter.filterDuration() ? a.getDuration() == -1 || (filter.duration_min() == -1 || a.getDuration() >= filter.duration_min()) && (filter.duration_max() == -1 || a.getDuration() <= filter.duration_max()) : true).filter(a -> filter.filterRating() ? a.getRating().getCount() != 0 && (filter.rating_min() == -1 || a.getRating().getValue() >= filter.rating_min()) && (filter.rating_max() == -1 || a.getRating().getValue() <= filter.rating_max()) : true).filter(e -> !filter.filterCategory() || filter.categories().contains(e.getCategory().getId())).doOnSubscribe((c) -> System.out.println("New Event Request"));
		});
	}

	@GetMapping(path = "/{eventId}")
	public Mono<EventDetails> getEventDetails(@PathVariable(value = "eventId") String eventId)
	{
		long startTime = System.currentTimeMillis();

		return Mono.just(eventId).map(eventID -> adapterHandler.decodeID(eventId)).flatMap(tuple -> {
			ITravelAdapter adapter = tuple.getT1();
			String id = tuple.getT2();

			return adapter.getEventDetails(id).map(de -> {
				de.setId(adapterHandler.encodeID(adapter, id));
				de.setVendor(adapter.getVendorID());
				return de;
			}).takeUntilOther(Mono.just(1).delayElement(Duration.ofSeconds(Config.DETAILS_TIMEOUT)).doOnNext(i -> System.out.println(" - " + adapter.getVendorID() + ": Timeout after " + Config.DETAILS_TIMEOUT + " seconds"))).doOnSuccess((e) -> {
				long duration = (System.currentTimeMillis() - startTime);

				if (duration <= Config.DETAILS_TIMEOUT * 1000)
					System.out.println(" - " + adapter.getVendorID() + ": " + duration + "ms");
			});
		}).doOnSubscribe((c) -> System.out.println("New Event Details Request (" + eventId + ")"));
	}
}