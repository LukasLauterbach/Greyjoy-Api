package greyjoy.travelsmart.api.routing;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import greyjoy.travelsmart.api.filtering.Filter;
import greyjoy.travelsmart.api.filtering.FilterHandler;
import greyjoy.travelsmart.api.handler.AdapterHandler;
import greyjoy.travelsmart.api.model.Event;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/recommendations")
public class RecommendationsController
{
	AdapterHandler adapterHandler;
	FilterHandler filterHandler;

	EventController eventController;

	public RecommendationsController(AdapterHandler adapterHandler, FilterHandler filterHandler, EventController eventController)
	{
		this.adapterHandler = adapterHandler;
		this.filterHandler = filterHandler;
		this.eventController = eventController;
	}

	@GetMapping(path = "")
	public Flux<Event> getEvents(@RequestParam Map<String, String> stringFilterMap)
	{
		return eventController.getEvents(stringFilterMap).collectList().map(list -> {
			Collections.shuffle(list);
			return list;
		}).flatMapMany(Flux::fromIterable).take(6);
	}
}
