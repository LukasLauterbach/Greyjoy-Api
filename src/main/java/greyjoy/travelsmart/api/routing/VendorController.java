package greyjoy.travelsmart.api.routing;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import greyjoy.travelsmart.api.handler.AdapterHandler;
import greyjoy.travelsmart.api.model.Vendor;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/vendors")
public class VendorController
{
	AdapterHandler adapterHandler;
	
	public VendorController(AdapterHandler adapterHandler)
	{
		this.adapterHandler = adapterHandler;
	}

	@GetMapping(path = "")
	public Mono<List<Vendor>> getVendors()
	{
		return adapterHandler.getTravelAdapters().map(t -> new Vendor(t.getVendorID(), t.getVendorName())).collectList();
	}
}
