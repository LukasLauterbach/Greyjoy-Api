package greyjoy.travelsmart.config;

public class Config
{
	public static boolean CACHE_SUNBONOO_INFO = true;
	public static boolean CACHE_SUNBONOO_WEBSITE = true;
	public static boolean CACHE_SUNBONOO_BOOKABLES = true;
	
	public static boolean CACHE_RENTAGUIDE_DETAILS = true;
	public static boolean CACHE_RENTAGUIDE_TAGS = true;
	
	
	// Timeouts
	public static int EVENTS_TIMEOUT = 100;
	public static int DETAILS_TIMEOUT = 10;
}
