package greyjoy.travelsmart.spring;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Scope;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

@SpringBootApplication
@ComponentScan(basePackages = { "greyjoy.travelsmart" }, excludeFilters = { @ComponentScan.Filter(type = FilterType.CUSTOM, value = Application.ComponentFilter.class) })
@Configuration
public class Application
{
	public static void main(String[] args) throws Exception
	{
		// BasicConfigurator.configure();

		System.setProperty("reactor.ipc.netty.workerCount", "10");

		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		new ConsoleThread(ctx).start();
	}

	@Bean
	@Scope("prototype")
	Log logger(InjectionPoint injectionPoint)
	{
		return LogFactory.getLog(injectionPoint.getField().getDeclaringClass());
	}
	
	public static class ComponentFilter implements TypeFilter
	{
		@Override
		public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException
		{
			return metadataReader.getClassMetadata().getClassName().startsWith("greyjoy.travelsmart.adapter.sunbonoo.magento");
		}
	}
}