package greyjoy.travelsmart.spring;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import greyjoy.travelsmart.api.handler.DatabaseHandler;
import greyjoy.travelsmart.config.Config;

public class ConsoleThread extends Thread
{
	ConfigurableApplicationContext appContext;
	DatabaseHandler dbHandler;

	WebClient requestClient;


	public ConsoleThread(ConfigurableApplicationContext appContext)
	{
		this.appContext = appContext;

		dbHandler = appContext.getBean(DatabaseHandler.class);

		requestClient = WebClient.builder().baseUrl("http://localhost:8080").build();

		this.setDaemon(true);
	}

	@Override
	public void run()
	{
		Scanner scanner = new Scanner(System.in);

		while (true)
		{
			String command = scanner.nextLine();

			if (command.equals("exit"))
			{
				appContext.close();
			}
			else if (command.startsWith("request"))
			{
				String[] split = command.split(" ");

				if (split.length == 2)
				{
					String request = split[1];

					String response = requestClient.get().uri(request).retrieve().bodyToMono(String.class).onErrorReturn("{}").block();

					Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

					JsonParser parser = new JsonParser();
					JsonElement json = parser.parse(response);

					response = gson.toJson(json);

					try
					{
						Files.write(Paths.get("./response.json"), response.getBytes(Charset.forName("UTF8")), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}

					System.out.println("Response written to response.json");
				}
			}
			else if (command.startsWith("timeout"))
			{
				String[] split = command.split(" ");

				if (split.length == 2)
				{
					if (split[1].equals("status"))
					{
						System.out.println("Events: " + Config.EVENTS_TIMEOUT + " seconds");
						System.out.println("Details: " + Config.DETAILS_TIMEOUT + " seconds");
					}
				}
				else if (split.length == 4)
				{
					if (split[1].equals("set"))
					{
						if (split[2].equals("events"))
						{
							try
							{
								Config.EVENTS_TIMEOUT = Integer.parseInt(split[3]);
							}
							catch (NumberFormatException e)
							{
								e.printStackTrace();
							}
							
							System.out.println("Set Events Timeout to "+Config.EVENTS_TIMEOUT + " seconds");
						}
						else if (split[2].equals("details"))
						{
							try
							{
								Config.DETAILS_TIMEOUT = Integer.parseInt(split[3]);
							}
							catch (NumberFormatException e)
							{
								e.printStackTrace();
							}
							
							System.out.println("Set Details Timeout to "+Config.DETAILS_TIMEOUT + " seconds");
						}
					}
				}
			}
			else if (command.startsWith("cache"))
			{
				String[] split = command.split(" ");

				if (split.length == 2)
				{
					if (split[1].equals("status"))
					{
						System.out.println("Sunbonoo");
						System.out.println(" Info = " + Config.CACHE_SUNBONOO_INFO);
						System.out.println(" Website = " + Config.CACHE_SUNBONOO_WEBSITE);
						System.out.println(" Bookables = " + Config.CACHE_SUNBONOO_BOOKABLES);

						System.out.println("Rentaguide");
						System.out.println(" Details = " + Config.CACHE_RENTAGUIDE_DETAILS);
						System.out.println(" Tags = " + Config.CACHE_RENTAGUIDE_TAGS);
					}
				}
				else if (split.length == 3)
				{
					if (split[1].equals("clear"))
					{
						boolean sunbonoo = false, rentaguide = false;

						switch (split[2])
						{
							case "all":
								sunbonoo = rentaguide = true;
								break;
							case "sunbonoo":
								sunbonoo = true;
								break;
							case "rentaguide":
								rentaguide = true;
								break;
						}

						if (sunbonoo)
						{
							System.out.println("Clearing Sunbonoo Cache...");

							System.out.print(" Website... ");
							dbHandler.executeUpdate("DELETE FROM SUNBONOO_WEBSITE");
							System.out.println("Done");

							System.out.print(" Info... ");
							dbHandler.executeUpdate("DELETE FROM SUNBONOO_INFO");
							System.out.println("Done");

							System.out.print(" Bookables... ");
							dbHandler.executeUpdate("DELETE FROM SUNBONOO_BOOKABLES");
							System.out.println("Done");
						}

						if (rentaguide)
						{
							System.out.println("Clearing Rentaguide Cache...");

							System.out.print(" Details... ");
							dbHandler.executeUpdate("DELETE FROM RENTAGUIDE_DETAILS");
							System.out.println("Done");

							System.out.print(" Tags... ");
							dbHandler.executeUpdate("DELETE FROM RENTAGUIDE_TAGS");
							System.out.println("Done");
						}
					}
				}
				else if (split.length == 4)
				{
					if (split[1].equals("toggle"))
					{
						String vendor = split[2];
						String cache = split[3];

						switch (vendor)
						{
							case "rentaguide":
								switch (cache)
								{
									case "details":
										Config.CACHE_RENTAGUIDE_DETAILS = !Config.CACHE_RENTAGUIDE_DETAILS;
										break;
									case "tags":
										Config.CACHE_RENTAGUIDE_TAGS = !Config.CACHE_RENTAGUIDE_TAGS;
										break;
								}
								break;
							case "sunbonoo":
								switch (cache)
								{
									case "info":
										Config.CACHE_SUNBONOO_INFO = !Config.CACHE_SUNBONOO_INFO;
										break;
									case "website":
										Config.CACHE_SUNBONOO_WEBSITE = !Config.CACHE_SUNBONOO_WEBSITE;
										break;
									case "bookables":
										Config.CACHE_SUNBONOO_BOOKABLES = !Config.CACHE_SUNBONOO_BOOKABLES;
										break;
								}
								break;
						}
					}
				}

			}
		}
	}
}
