package greyjoy.travelsmart.util;

import reactor.util.function.Tuple4;
import reactor.util.function.Tuples;

public class MathUtil
{
	public static Tuple4<Double, Double, Double, Double> pointRadiusToRectangle(int radius, double latitude, double longitude)
	{
		double RADIUS_EARTH = 6371; // in km

		// north-east corner of square
		double maxLat = latitude + Math.toDegrees(radius / RADIUS_EARTH);
		double maxLong = longitude + Math.toDegrees(radius / RADIUS_EARTH / Math.cos(Math.toRadians(latitude)));

		// south-west corner of square
		double minLat = latitude - Math.toDegrees(radius / RADIUS_EARTH);
		double minLong = longitude - Math.toDegrees(radius / RADIUS_EARTH / Math.cos(Math.toRadians(latitude)));

		return Tuples.of(minLat, minLong, maxLat, maxLong);
	}

	public static double[] latLongToVec3(double latitude, double longitude)
	{
		double r = 6371;
		double x = r * Math.cos(longitude) * Math.sin(latitude);
		double y = r * Math.sin(longitude) * Math.sin(latitude);
		double z = r * Math.cos(latitude);

		return new double[] { x, y, z };
	}

	public static double[] coordRectToPointRadius(double latitude_min, double longitude_min, double latitude_max, double longitude_max)
	{
		double latitude_center = (latitude_max + latitude_min) / 2;
		double longitude_center = (longitude_max + longitude_min) / 2;

		double d1 = distance(latitude_min, longitude_min, latitude_center, longitude_center);
		double d2 = distance(latitude_max, longitude_min, latitude_center, longitude_center);
		double d3 = distance(latitude_max, longitude_max, latitude_center, longitude_center);
		double d4 = distance(latitude_min, longitude_max, latitude_center, longitude_center);

		double max = Math.max(d1, Math.max(d2, Math.max(d3, d4)));

		return new double[] { max, latitude_center, longitude_center };
	}

	public static double distance(double lat1, double lon1, double lat2, double lon2)
	{
		double p = 0.017453292519943295; // Math.PI / 180
		double a = 0.5 - Math.cos((lat2 - lat1) * p) / 2 + Math.cos(lat1 * p) * Math.cos(lat2 * p) * (1 - Math.cos((lon2 - lon1) * p)) / 2;

		return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
	}
}
