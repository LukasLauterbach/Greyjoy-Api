package greyjoy;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.logging.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.spring.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "100s")
public class ConsistencyTest
{
	@Autowired
	private WebTestClient testClient;

	@LocalServerPort
	private int port;

	@Autowired
	private Log logger;

	@Autowired
	public ConsistencyTest()
	{

	}

	@Test
	public void checkEvents() throws Exception
	{
		Event[] events = testClient.get().uri("/events").exchange().expectStatus().isOk().expectBody(Event[].class).returnResult().getResponseBody();

		assertThat(events).as("returned events").isNotEmpty();
		assertThat(events).as("have ids").allSatisfy(e -> assertThat(e.getId()).isNotBlank());
		assertThat(events).as("have title").allSatisfy(e -> assertThat(e.getTitle()).isNotBlank());

		assertThat(events).as("has bookingkit events").anyMatch(e -> e.getVendor().equals("bookingkit"));
		assertThat(events).as("has sunbonoo events").anyMatch(e -> e.getVendor().equals("sunbonoo"));
		assertThat(events).as("has rent-a-guide events").anyMatch(e -> e.getVendor().equals("rentaguide"));

		for (Event event : events)
		{
			EventDetails details = testClient.get().uri("/events/{id}", event.getId()).exchange().expectStatus().isOk().expectBody(EventDetails.class).returnResult().getResponseBody();

			assertThat(event.getId()).as("equal id").isNotNull().isEqualTo(details.getId());
			assertThat(event.getTitle()).as("equal title").isEqualTo(details.getTitle());
			assertThat(event.getLanguages()).as("equal languages").isEqualTo(details.getLanguages());
			assertThat(event.getLocation()).as("equal location").isEqualTo(details.getLocation());
			assertThat(event.getLocation_lat()).as("equal location_latitude").isEqualTo(details.getLocation_lat());
			assertThat(event.getLocation_long()).as("equal location_longitude").isEqualTo(details.getLocation_long());
			assertThat(event.getImages()).as("equal images").isEqualTo(details.getImages());
			assertThat(event.getRating()).as("equal rating").isEqualTo(details.getRating());
			assertThat(event.getVendor()).as("equal vendor").isEqualTo(details.getVendor());
			assertThat(event.getDisplayPrice()).as("equal displayprice").isEqualByComparingTo(details.getDisplayPrice());
			assertThat(event.getPrices()).as("equal maxprice").isEqualToComparingFieldByFieldRecursively(details.getPrices());
			assertThat(event.getCategory()).as("equal category").isEqualTo(details.getCategory());
		}
	}
}
