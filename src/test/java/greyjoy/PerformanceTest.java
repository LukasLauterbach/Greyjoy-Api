package greyjoy;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.logging.Log;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;

import greyjoy.travelsmart.api.model.Event;
import greyjoy.travelsmart.api.model.EventDetails;
import greyjoy.travelsmart.spring.Application;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "30s")
public class PerformanceTest
{
	private WebClient webClient;

	@Autowired
	private Log logger;
	
	@LocalServerPort 
	int port;

	@Autowired
	public PerformanceTest()
	{
		
	}
	
	@Before
	public void init()
	{
		webClient = WebClient.builder().baseUrl("http://localhost:" + port).build();
	}

	@Test
	public void parallelEvents() throws Exception
	{
		int eventRequests = 4;
		
		// Build Cache
		
		logger.info("Building Cache...");
		webClient.get().uri("/events").retrieve().bodyToMono(Event[].class).block();

		logger.info("Making " + eventRequests + " concurrent /events requests");

		Mono<Event[]> results = Flux.range(1, eventRequests).flatMap(req -> {
			return webClient.get().uri("/events").retrieve().bodyToMono(Event[].class).doOnSuccess(e -> logger.info("Finished Request " + req + " with " + e.length + " events"));
		}).last().timeout(Duration.ofSeconds(10));

		Event[] events = results.block();

		List<Event> eventList = Arrays.asList(events);
		Collections.shuffle(eventList);

		int detailsRequests = 50;
		logger.info("Making " + detailsRequests + " concurrent /events/{id} requests");

		Flux.fromIterable(eventList).zipWith(Flux.range(1, detailsRequests)).take(detailsRequests).flatMap(tuple -> {
			Event event = tuple.getT1();
			int req = tuple.getT2();

			return webClient.get().uri("/events/{id}", event.getId()).retrieve().bodyToMono(EventDetails.class).doOnSuccess(e -> logger.info("Finished Request " + req));
		}).last().timeout(Duration.ofSeconds(10)).block();
	}
}
