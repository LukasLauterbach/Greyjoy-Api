package greyjoy;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import greyjoy.travelsmart.api.routing.EventController;
import greyjoy.travelsmart.spring.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SmokeTest
{
	@Autowired
	private EventController eventController;

	@Test
	public void contextLoads()
	{
		assertThat(eventController).isNotNull();
	}
}
